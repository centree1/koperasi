<div class="row">
		<div class="col-md-3">
			<div class="card text-white bg-primary mb-3" style="">
			  <div class="card-body row">
			  	<div class="col-md-4">
			  		<i class="fa fa-book fa-3x fa-fw"></i>
			  	</div>
			  	<div class="col-md-8 text-center">
			  		Jumlah Akun
            <?php
              $exe = mysqli_query($koneksi, "select count(*) as jumlah from tb_akun");
              $fetch = mysqli_fetch_object($exe);
            ?>
			  		<h3 class="card-title"><?= $fetch->jumlah; ?></h3>
			  	</div>
			    
			  </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-success mb-3" style="">
			  <div class="card-body row">
			  	<div class="col-md-4">
			  		<i class="fa fa-book fa-3x fa-fw"></i>
			  	</div>
			  	<div class="col-md-8 text-center">
			  		Jumlah Tahun Buku
            <?php
              $exe = mysqli_query($koneksi, "select count(*) as jumlah from tb_tahun_buku");
              $fetch = mysqli_fetch_object($exe);
            ?>
			  		<h3 class="card-title"><?= $fetch->jumlah; ?></h3>
			  	</div>
			    
			  </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-warning mb-3" style="">
			  <div class="card-body row">
			  	<div class="col-md-4">
			  		<i class="fa fa-user fa-3x fa-fw"></i>
			  	</div>
			  	<div class="col-md-8 text-center">
			  		Jumlah User
            <?php
              $exe = mysqli_query($koneksi, "select count(*) as jumlah from tb_user");
              $fetch = mysqli_fetch_object($exe);
            ?>
			  		<h3 class="card-title"><?= $fetch->jumlah; ?></h3>
			  	</div>
			    
			  </div>
			</div>
		</div> 
	</div>

     <?php get_notif('msgthnbku'); ?> 

     <div class="row">
      <div class="card shadow-sm border-0 col-md-3 " style="margin-left: 15px !important; margin-right: 15px !important;" >
                <div class=" card-body">
                   
                  <h3> <i class="fa fa-book"></i> List Tahun Buku</h3>
                  <i> 4 buku terbaru terakhir </i>
                  <br><br>

                  <table class="table table-hover" style="border-width: 0px;">
                    
          <!-- Item 1 -->
          <?php
              $query = "SELECT * FROM tb_tahun_buku order by awal_periode DESC LIMIT 4";
              $sql = mysqli_query($koneksi, $query);
              $a = 1;
              while ($row = mysqli_fetch_array($sql)) { 
                 
                ?>
                <tr  style="border-style: solid; border-right: 0px;border-bottom: 0px; border-top: 0px;">
                  <td><i class="fa fa-chevron-right fa-2x" style="color: #00b2e8;"></i></td>
                  <td style="font-weight: bold; text-align: right;">
                      <?=  tgl_indo2($row['awal_periode'])." - ".tgl_indo2($row['akhir_periode']); ?> <br style="margin-bottom: 20px !important;">
                      <button class="btn btn-primary btn-sm " style="margin-top: 10px;" onclick="buka_buku('<?= $row['id_tahun_buku']; ?>')">Buka Buku</button>
                  </td>
                </tr>
                   
            <?php $a++;} ?>
              <tr>
                  <td colspan="2">
                      <button class="btn btn-block btn-success" onclick="window.location = 'tahun_buku'" style="margin-top: 10px;"> See All </button>
                  </td>
                </tr>
          </table>


<!-- The Timeline -->



                </div>
             </div>  
     </div>

             
            

       


    <script>
function buka_buku(id){
    simple_ajax("id="+id,'config/buka_buku','','','Gagal, Buku tidak ditemukan');
}
</script>