<?php
include 'config/config.php';
$title = "Tipe Akun";
include 'template/header.php';
get_role_page('menu_tipe');
?>
 <?php get_notif('msgtpakn'); ?>
<!-- ============ Body content start ============= -->
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Tipe Akun
                        </label-tabel>
                        <div class="float-right">   
                            <button onclick="window.location = 'akun';" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali 
                            </button>
                            <button onclick="open_modal()" class="btn btn-success btn-icon-split mb-3">
                                <i class="fa fa-plus"></i> Tipe Akun 
                            </button>
                        </div>
                        <br>
                    </div>
                    <div class="container-form table-responsive ">
                      <table class="table table-striped table-hovered table-bordered dataTable" >
                       <thead><tr>
                            <th>id</th>
                            <th>Tipe Akun</th>
                            <th>Config</th>
                       </tr></thead>

                            <?php
                                $query = mysqli_query($koneksi,'select * from tb_tipe_akun');
                                while($row=mysqli_fetch_object($query)){
                            ?>
                            <tr>
                            <td><?= $row->id_tipe; ?></td>
                            <td><?= $row->nama_tipe; ?></td>
                            <td>
                                <button class='btn btn-primary btn-sm' onclick="open_modal('<?= $row->id_tipe; ?>','Edit Tipe Akun');">Edit</button>  
                                <button class='btn btn-danger btn-sm' onclick="deleteUser('<?= $row->id_tipe; ?>')">Hapus</button>
                            </td>
                                <?php } ?>
                            </tr>
                        
                    </table>
                    </div>
                    <div class="form-group" style="text-align: center;">
                          
                      </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<!-- Modal Tambah Akun-->
<div class="modal fade" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mdlhdr">Mohon Tunggu . . .</h5>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="mdlbdy"></div>    
    </div>
  </div>
</div>

<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    function open_modal(id="",action="tmbh"){
            $("#mdlhdr").empty();
        if(action=="tmbh"){
            $("#mdlhdr").append("Tambah Tipe Akun");
        }else{
            $("#mdlhdr").append(action);
        }
        get_with_ajax('id='+id, 'get_form_tambah_tipe_akun', "mdlbdy");
        $('#exampleModal').modal('show');
    }

    function deleteUser(id){
        pesan_confirm("Apakah anda yakin?", "Data yang telah dihapus tidak dapat dikembalikan", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("id="+id,'config/delete_tipe_akun','','Tipe Akun berhasil dihapus','Tipe Akun gagal dihapus');
            }
        });
    }
</script>

<?php include 'template/footer.php'; ?>
