<?php
include 'config/config.php';
$title = 'Tambah Akun';
include 'template/header.php';

?>
<!-- ============ Body content start ============= -->

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <form id="frmz">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Tambah Akun
                        </label-tabel>
                        <div class="float-right">
                            <a href="akun" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label >No. Akun</label>
                            <input required name="no_akun" type="text" class="form-control" placeholder="No. Akun">
                          </div>
                          <div class="form-group">
                            <label >Nama Akun</label>
                            <input required name="nama_akun" type="text" class="form-control" placeholder="Nama Akun">
                          </div>
                          <div class="form-group">
                            <label >Tipe Akun</label>
                            <select  required name="tipe_akun" class="form-control select2">
                                    <option value="">-- Pilih -- </option>
                                    <?php 
                                    $query=mysqli_query($koneksi, "SELECT * FROM tb_tipe_akun");  
                                    while($row=mysqli_fetch_array($query)){ ?>
                                        <option value="<?php echo $row['id_tipe']; ?>"><?php echo $row['nama_tipe']; ?></option>
                                    <?php } ?>
                                </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >Saldo Normal</label>
                                <input required name="saldo_normal" type="text" class="form-control" placeholder="Saldo Normal">
                              </div>
                              <div class="form-group">
                                <label >Penjelasan</label>
                                <input required name="penjelasan" type="text" class="form-control" placeholder="Penjelasan">
                              </div>
                              <div class="form-group">
                                <label >Kategori</label>
                                <select  required name="kategori" class="form-control select2">
                                    <option value="">-- Pilih -- </option>
                                    <?php 
                                    $query=mysqli_query($koneksi, "SELECT * FROM tb_kategori_akun");  
                                    while($row=mysqli_fetch_array($query)){ ?>
                                        <option value="<?php echo $row['id_kategori']; ?>"><?php echo $row['nama_kategori']; ?></option>
                                    <?php } ?>
                                </select>
                              </div>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center;">
                          
                          <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
                </div>
            </form>
            </div>
        </div>
        
    </div>
</div>

<script type="text/javascript">
        
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 

            var values = $(this).serialize();
            simple_ajax(values,'config/proses_tambah_coa','akun','Berhasil Tambah Akun','Gagal tambah Akun');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });
    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>