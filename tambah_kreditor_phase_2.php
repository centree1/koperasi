<?php 
include 'config/config.php';

$no_akun    = "&no_akun=".$_POST['no_akun'];
$nama_bank  = "&nama_bank=".$_POST['nama_bank'];
$cabang_bank = "&cabang_bank=".$_POST['cabang_bank'];
$no_telpon  = "&no_telpon=".$_POST['no_telpon'];
$no_hp      = "&no_hp=".$_POST['no_hp'];
$email      = "&email=".$_POST['email'];
$fax        = "&fax=".$_POST['fax'];
$kelurahan  = "&kelurahan=".$_POST['kelurahan'];
$kecamatan  = "&kecamatan=".$_POST['kecamatan'];
$kota       = "&kota=".$_POST['kota'];
$kode_pos   = "&kode_pos=".$_POST['kodepos'];

$post = $no_akun.$nama_bank.$cabang_bank.$no_telpon.$no_hp.$email.$fax.$kelurahan.$kecamatan.$kota.$kode_pos;
?>

<!-- Main Start Here -->

    <!-- Filter ------------------------------------------------------------>
   
                <form method="POST" id="frmx"> 
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <b style="color: red;">*</b> <label>Tanggal Peminjaman</label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Pinjaman" required="" name="tgl_pinjam" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Tanggal Jatuh Tempo Pelunasan</label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Jatuh Tempo" required="" name="tgl_jatuh_tempo" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Term</label>
                                <input type="text" class="form-control" name="term" placeholder="Term" required="" >
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Periode Cicilan</label>
                                <select name="periode_cicilan" class="form-control select2">
                                    <option value="TAHUNAN">Tahunan</option>
                                    <option value="BULANAN">Bulanan</option>
                                    <option value="HARIAN">Harian</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Banyak Cicilan</label>
                                <input type="number" class="form-control" placeholder="Banyak Cicilan" name="banyak_cicilan" >
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Tanggal Jatuh Tempo Cicilan </label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Jatuh Tempo Cicilan" name="jatuh_tempo_cicilan" autocomplete="off" >
                            </div>

                        <div class="form-group">
                        <b style="color: red;">*</b><label>Nominal Pinjaman</label>
                        <div class="input-group mb-2">
                        <!-- <div class="input-group-prepend">
                        <div class="input-group-text">Rp</div>
                        </div> -->
                        <input type="text" class="form-control CentreeRupiah" id="inlineFormInputGroup" placeholder="Nominal Pinjaman" name="nominal_pinjaman">
                        </div>
                        </div>

                            <div class="form-group">
                                <b style="color: red;">*</b><label>Bunga (%)</label>
                                <input type="number" class="form-control" placeholder="Bunga" required="" name="bunga" >
                            </div>

                            
                        <div class="form-group">
                        <b style="color: red;">*</b><label>Nominal Cicilan</label>
                        <div class="input-group mb-2">
                        <!-- <div class="input-group-prepend">
                        <div class="input-group-text">Rp</div>
                        </div> -->
                        <input type="text" class="form-control CentreeRupiah" id="inlineFormInputGroup" placeholder="Nominal Cicilan" name="nominal_cicilan">
                        </div>
                        </div>


                            <div class="form-group text-right">
                                <br><br>
                                <button type="button" class="btn btn-warning" id="prev">  <i class="fa fa-arrow-left"></i> Sebelumnya </button> 
                                <button type="submit" class="btn btn-success ">  <i class="fa fa-save"></i> Simpan </button>
                            </div>
                        </div>
                    </div>
                </form>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript">

     $('#frmx').submit(function(event) { 
        event.preventDefault(); //Prevent the default submit
        var values = $(this).serialize();
        simple_ajax(values+"<?= $post; ?>",'config/tambah_kreditor','kreditor','Berhasil menambah Kreditor baru');
        return false; //stop
    });

     $("#prev").click(function(event){
        $("#phase1").fadeToggle();
        $("#phase2").fadeToggle();
     });

     $('.datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false,
            sideBySide: true
        }).on('dp.change', function (e) { 
        //get attribut
            var name    = $(this).attr('name');
            var name_tmp  = name.split("___");
            var name_real = name_tmp[1];
            var value     = $(this).val();

            //change format date
            var arr = value.split("/");
            var datex = arr[2] + "-" + arr[1] + "-" + arr[0];

            //change date
            $("input[name='"+name_real+"']").val(datex); 

        });

</script>

<script type="text/javascript">
    $( ".CentreeTgl" ).CentreeTgl();  
    $( ".CentreeRupiah" ).CentreeRupiah();
    $(".CentreeRupiah").keyup(function(){
          //get attribut
        var name    = $(this).attr('name');
        var name_tmp  = name.split("___");
        var name_real = name_tmp[1];
        var angka     = $(this).val();
        var prefix    = "Rp. ";

        //change rupiah
        returnx = CentreeFormatRupiah(angka);

        var value  = $(this).val(returnx[0]);
        $("input[name='"+name_real+"']").val(returnx[1]); 

    });
</script>