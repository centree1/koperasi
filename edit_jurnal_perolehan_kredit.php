<?php
include 'config/config.php';
$title = 'Tambah Jurnal Kas Keluar';
include 'template/header.php';
cek_tahun_buku();
get_role_page('edit');

if(empty($_GET['id'])){
    set_notif('psnjpkb','Maaf, Jurnal tidak ditemukan','jurnal_perolehan_kredit_bank','danger','close');
}



@$idx       = $_GET['id'];





$query      = "select * from tb_jurnal_kreditor where tipe_jurnal='JPKB' AND no_jurnal='".$idx."' ";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);

$idd  = $fetch->id_kreditor ;

$query1      = "select * from tb_kreditor where id_kreditor ='".$idd."' ";
$execute    = mysqli_query($koneksi,$query1);
$ambil      = mysqli_fetch_object($execute);
$no_akunx = $ambil->no_akun;
$id_kreditorx = $ambil->id_kreditor;


//im_debugging($fetch);
if(empty($fetch)){
    set_notif('psnjpkb','Maaf, Jurnal tidak ditemukan','jurnal_perolehan_kredit_bank','danger','close');
}

?>
<!-- ============ Body content start ============= -->
<style type="text/css">
    .header-debit{
        background-color: #deffe7;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-kredit{
        background-color: #e3fdff;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-bank{
        background-color: #fff6b8;
        color: black;
        font-weight: bold;
        text-align: center;
    }
</style>

<form id="frmz">
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Edit Jurnal Perolehan Kredit Bank
                        </label-tabel>
                        <div class="float-right">
                            <a href="jurnal_perolehan_kredit_bank" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label >No. Jurnal Perolehen Kredit Bank</label>
                            <input required name="no_jurnal" type="text" class="form-control" placeholder="No. Jurnal Kas Keluar" value="<?= $fetch->no_jurnal; ?>"  readonly>
                          </div>
                          <div class="form-group">
                            <label >Tanggal</label>
                            <input class="form-control datetimepicker CentreeTgl" required name="tanggal" type="text" class="form-control" autocomplete="off" placeholder="Tanggal"  value="<?= $fetch->tanggal; ?>">
                          </div>
                          <div class="form-group">
                                <label >Memo</label>
                                <input name="memo" type="text" class="form-control" placeholder="Memo" value="<?= $fetch->memo; ?>">
                              </div>
                          
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >No. Identitas Bank</label>
                                <input required name="no_identitas_bank" type="text" class="form-control col-md-8" placeholder="No. Identitas Bank" value="<?= $ambil->no_akun; ?> - <?= $ambil->nama_bank; ?>" readonly>
                              </div>
                              <div class="form-group">
                                <label >Nama Bank</label>
                                <input name="nama_bank" type="text" class="form-control" placeholder="Nama Bank" value="<?= $ambil->nama_bank; ?>" readonly>
                              </div>
                              <div class="form-group">
                                <label >No. Bukti Kas Masuk</label>
                                <input required name="no_bukti" type="text" class="form-control" placeholder="No Bukti Kas Keluar" value="<?= $fetch->no_bukti; ?>">
                              </div>
                        </div>
                    </div>
                </div>

            <script type="text/javascript">
                function yakin(tipe,lokasi){
                    pesan_confirm("Ingin Pindah Halaman ?", "Data Akun yang belum tersimpan akan hilang", "Buka "+tipe).then((result) => {
                    //eksekusi
                    if(result===true){
                        window.location = lokasi;
                    }
                    });
                }
            </script>
            </div>
        </div>
        
    </div>
</div>


<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body table-responsive ">
                    <table class="table table-hover table-striped" border="1">
                        <thead>
                             <tr>
                                <td class="bg-debit"  style="width: 2%;"></td>
                                
                                <td colspan="4" class="bg-debit"  >DEBIT</td>
                              
                                <td colspan="4" class="bg-kredit"  >KREDIT</td>
                                
                            </tr>
                            <tr>
                                <td class="bg-debit"  rowspan="2" style="width: 2%;">
                                    <button type="button" class="btn btn-sm btn-success" onclick="addrow()"> <i  class="fa fa-plus "></i></button>
                                </td>
                               
                                <td class="bg-debit" colspan="2">BANK</td>
                                <td class="bg-debit" colspan="2" >Lainya</td>
                                 <td class="bg-kredit" colspan="2" >UTANG</td>
                                <td class="bg-kredit" colspan="2" >Lainya</td>
                               

                            </tr>

                            <tr>
                                <td class="bg-debit"  > Akun </td>
                                <td class="bg-debit" > Saldo </td>
                                <td class="bg-debit" > Akun </td>
                                <td class="bg-debit" > Saldo </td>
                                <td class="bg-kredit"> Akun </td>
                                <td class="bg-kredit"> Saldo </td>
                                <td class="bg-kredit" > Akun </td>
                                <td class="bg-kredit" > Saldo </td>
                            </tr>
                        </thead>
                        <tbody id="tbl">
<?php
    // debit bank
    $query  = "SELECT no_akun, nama_akun from tb_akun where  tipe_akun=20";
    $result = mysqli_query($koneksi,$query);
    $data_bank1   = mysqli_fetch_all($result,MYSQLI_ASSOC);


    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_lainya   = mysqli_fetch_all($result,MYSQLI_ASSOC);

   

    $query  = "SELECT no_akun, nama_akun from tb_akun where no_akun = $no_akunx ";
    $result = mysqli_query($koneksi,$query);
    $data_piutang   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $query  = "SELECT no_akun, nama_akun from tb_akun where tipe_akun=20";
    $result = mysqli_query($koneksi,$query);
    $data_bank    = mysqli_fetch_all($result,MYSQLI_ASSOC);
    $no_akun_bank = array();
    foreach ($data_bank as $key) {
        array_push($no_akun_bank, $key['no_akun']);
    }
    //im_debugging($no_akun_bank);

    $query  = "SELECT no_akun, nama_akun from tb_akun where saldo_normal = 'Kredit'";
    $result = mysqli_query($koneksi,$query);
    $data_kredit   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $query  = "SELECT * from tb_jurnal_kreditor_debit_kredit where no_jurnal = '".$idx   ."' order by row ASC";
    $result           = mysqli_query($koneksi,$query);
    $id               = 1;
    $no_jurnal_temp   = null;
    $row_temp         = null;
    $array_jurnal     = array();
    $temp_array       = array();
    while($data_jurnal   = mysqli_fetch_object($result)){ 
        
        //inisialisasi
        $saldo_debit  = null;  
        $saldo_lainya = null;
        $saldo_bank   = null;

        //get per row in jurnal debit kredit
        if($data_jurnal->row !== $row_temp){
            if(!empty($temp_array)){
                array_push($array_jurnal, $temp_array);
            }

            $temp_array     = array();
            $no_jurnal_temp = $data_jurnal->no_jurnal;
            $row_temp       = $data_jurnal->row;
            $temp_array['no_jurnal'] = $no_jurnal_temp;
            $temp_array['row'] = $row_temp;

            $temp_array['no_akun_debit_bank']  = null;
            $temp_array['saldo_debit_bank']    = null;

            $temp_array['no_akun_debit_lainya']  = null;
            $temp_array['saldo_debit_lainya']    = null;

            $temp_array['no_akun_kredit_bank']  = null;
            $temp_array['saldo_kredit_bank']    = null;

            $temp_array['no_akun_kredit_lainya']  = null;
            $temp_array['saldo_kredit_lainya']    = null;
        }

        //cek apakah masuk ke debit/kredit
        if($data_jurnal->keterangan == 'Debit Bank'){
            $temp_array['saldo_debit_bank'] = $data_jurnal->nominal;
            $temp_array['no_akun_debit_bank'] = $data_jurnal->no_akun;
        }elseif($data_jurnal->keterangan == 'Debit lainnya'){
            $temp_array['saldo_debit_lainya'] = $data_jurnal->nominal;
            $temp_array['no_akun_debit_lainya'] = $data_jurnal->no_akun;
        }elseif($data_jurnal->keterangan == 'Kredit Bank'){
            $temp_array['saldo_kredit_bank'] = $data_jurnal->nominal;
            $temp_array['no_akun_kredit_bank'] = $data_jurnal->no_akun;
        }else{
            $temp_array['saldo_kredit_lainya'] = $data_jurnal->nominal;
            $temp_array['no_akun_kredit_lainya'] = $data_jurnal->no_akun;
        }
    }
    array_push($array_jurnal, $temp_array);
    //im_debugging($array_jurnal);

    foreach ($array_jurnal as $row => $val) {
        //im_debugging($val);
      ?>
    
<tr>
    <td  class="bg-debit"  >
        <button type="button" class="btn btn-sm btn-danger" id="min-<?= $id; ?>" onclick="delete_row('min-<?= $id; ?>')"> 
            <i class="fa fa-minus "></i>
        </button>
    </td>

<!-- Bank -------------------------------------------->
<td class="bg-debit" > 
    <select class="form-control select2" name="debit_bank[<?= $id; ?>]" id="debit_bank_<?= $id; ?>">
        <option value="">--</option>
        <?php foreach ($data_bank1 as $key => $rowx) { ?>
            <option value="<?= $rowx['no_akun']; ?>" <?= selected_droplist($rowx['no_akun'],$val['no_akun_debit_bank']); ?>><?= $rowx['no_akun'].' - '.$rowx['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="bg-debit">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo"  name="saldo_debit_bank[<?= $id; ?>]" id="saldo_debit_bank_<?= $id; ?>" value="<?= $val['saldo_debit_bank']; ?>">
    </div>
</td>

<!-- End Bank -------------------------------------------->

<!-- Debit Lainya -------------------------------------------->
<td class="bg-debit" > 
    <select class="form-control select2" name="debit_lainya[<?= $id; ?>]">
        <option value="">--</option>
        <?php foreach ($data_lainya as $key => $rowx) { ?>
            <option value="<?= $rowx['no_akun']; ?>" <?= selected_droplist($rowx['no_akun'],$val['no_akun_debit_lainya']); ?>><?= $rowx['no_akun'].' - '.$rowx['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="bg-debit">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_debit_lainya[<?= $id; ?>]" value="<?= $val['saldo_debit_lainya']; ?>">
    </div>
</td>

<!-- End Debit -------------------------------------------->



<!-- Kredit Utang -------------------------------------------->
<td class="bg-kredit" > 
    <select class="form-control select2"   name="kredit_bank[<?= $id; ?>]" id="kredit_lainya_<?= $id; ?>"   name="saldo_kredit_lainya[]" id="saldo_kredit_lainya_<?= $id; ?>">
        <option value="">--</option>
        <?php foreach ($data_piutang as $key => $rowx) { ?>
            <option value="<?= $rowx['no_akun']; ?>" <?= selected_droplist($rowx['no_akun'],$val['no_akun_kredit_bank']); ?>><?= $rowx['no_akun'].' - '.$rowx['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="bg-kredit">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_bank[<?= $id; ?>]" value="<?= $val['saldo_kredit_bank']; ?>">
    </div>
</td>

<!-- End Kredit Utang -------------------------------------------->

<!-- Kredit Lainya-------------------------------------------->
<td class="bg-kredit" > 
    <select class="form-control select2" name="kredit_lainya[<?= $id; ?>]">
        <option value="">--</option>
        <?php foreach ($data_lainya as $key => $rowx) { ?>
            <option value="<?= $rowx['no_akun']; ?>" <?= selected_droplist($rowx['no_akun'],$val['no_akun_kredit_lainya']); ?>><?= $rowx['no_akun'].' - '.$rowx['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="bg-kredit">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_lainya[<?= $id; ?>]" value="<?= $val['saldo_kredit_lainya']; ?>">
    </div>
</td>

<!-- End Bank -------------------------------------------->
</tr>
<?php $id++; }   ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="form-group" style="text-align: center;">
                          <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-save fa-fw"></i> Simpan</button>
                      </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</form>

<script type="text/javascript">
        
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 
            var values = $(this).serialize();
            simple_ajax(values+'&no_jurnal=<?= $fetch->no_jurnal; ?>','config/proses_edit_jpkb','jurnal_perolehan_kredit_bank','Berhasil Ubah Data');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

        //table form
        var i = <?= $id; ?>;
        function addrow(){
            get_with_ajax("id="+i+"&no_akun=<?= $no_akunx ?>", "config/get_form_jpkb", "tbl","no");
            i++;
        }


        function delete_row(id){
            $("#"+id).closest('tr').remove()
        }

    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>