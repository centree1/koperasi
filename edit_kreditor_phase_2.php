<?php 
include 'config/config.php';

$no_akun    = "&no_akun=".$_POST['no_akun'];
$nama_bank  = "&nama_bank=".$_POST['nama_bank'];
$cabang_bank = "&cabang_bank=".$_POST['cabang_bank'];
$no_telpon  = "&no_telpon=".$_POST['no_telpon'];
$no_hp      = "&no_hp=".$_POST['no_hp'];
$email      = "&email=".$_POST['email'];
$fax        = "&fax=".$_POST['fax'];
$kelurahan  = "&kelurahan=".$_POST['kelurahan'];
$kecamatan  = "&kecamatan=".$_POST['kecamatan'];
$kota       = "&kota=".$_POST['kota'];
$kode_pos   = "&kode_pos=".$_POST['kodepos'];
$id         = "&id=".$_POST['id'];

$post = $no_akun.$nama_bank.$cabang_bank.$no_telpon.$no_hp.$email.$fax.$kelurahan.$kecamatan.$kota.$kode_pos.$id;

$id = $_POST['id'];
$query = "select * from tb_kreditor where id_kreditor='".$id."'";
$data = mysqli_fetch_object(mysqli_query($koneksi,$query));
?>

<!-- Main Start Here -->

    <!-- Filter ------------------------------------------------------------>
   
                <form method="POST" id="frmx"> 
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <b style="color: red;">*</b> <label>Tanggal Peminjaman</label>
                                <input type="text" class="form-control datetimepickerx CentreeTgl" placeholder="Tanggal Pinjaman" required="" name="tgl_pinjam" autocomplete="off" value="<?= $data->tanggal_peminjaman; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Tanggal Jatuh Tempo Pelunasan</label>
                                <input type="text" class="form-control datetimepickerx CentreeTgl" placeholder="Tanggal Jatuh Tempo" required="" name="tgl_jatuh_tempo" autocomplete="off" value="<?= $data->tanggal_jatuh_tempo; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Term</label>
                                <input type="text" class="form-control" name="term" placeholder="Term" required="" value="<?= $data->term; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Periode Cicilan</label>
                                <select name="periode_cicilan" class="form-control select2">
                                    <option value="TAHUNAN" <?= selected_droplist('TAHUNAN',$data->periode_cicilan); ?>>Tahunan</option>
                                    <option value="BULANAN" <?= selected_droplist('BULANAN',$data->periode_cicilan); ?>>Bulanan</option>
                                    <option value="HARIAN" <?= selected_droplist('HARIAN',$data->periode_cicilan); ?>>Harian</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Banyak Cicilan</label>
                                <input type="number" class="form-control" placeholder="Banyak Cicilan" name="banyak_cicilan" value="<?= $data->banyak_cicilan; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Tanggal Jatuh Tempo Cicilan </label>
                                <input autocomplete="off" type="text" class="form-control datetimepickerx CentreeTgl" placeholder="Jatuh Tempo Cicilan" name="jatuh_tempo_cicilan" value="<?= $data->jatuh_tempo_cicilan; ?>" >
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Nominal Pinjaman</label>
                                <input type="text" class="form-control CentreeRupiah" placeholder="Nominal Pinjaman" required="" name="nominal_pinjaman" value="<?= $data->nominal_pinjaman; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Bunga (%)</label>
                                <input type="number" class="form-control" placeholder="Bunga" required="" name="bunga" value="<?= $data->bunga; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Nominal Cicilan</label>
                                <input type="text" class="form-control CentreeRupiah" placeholder="Nominal Cicilan" required="" name="nominal_cicilan" value="<?= $data->nominal_cicilan; ?>">
                            </div>
                            <div class="form-group text-right">
                                <br><br>
                                <button type="button" class="btn btn-warning" id="prev">  <i class="fa fa-arrow-left"></i> Sebelumnya </button> 
                                <button type="submit" class="btn btn-success ">  <i class="fa fa-save"></i> Simpan </button>
                            </div>
                        </div>
                    </div>
                </form>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript">

     $('#frmx').submit(function(event) { 
        event.preventDefault(); //Prevent the default submit
        var values = $(this).serialize();
        simple_ajax(values+"<?= $post; ?>",'config/edit_kreditor','kreditor','Berhasil Mengubah Data Kreditor');
        return false; //stop
    });

     $("#prev").click(function(event){
        $("#phase1").fadeToggle();
        $("#phase2").fadeToggle();
     });

    $( ".CentreeTgl" ).CentreeTgl();  
     $('.datetimepickerx').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false,
            sideBySide: true
        }).on('dp.change', function (e) { 
        //get attribut
            var name    = $(this).attr('name');
            var name_tmp  = name.split("___");
            var name_real = name_tmp[1];
            var value     = $(this).val();

            //change format date
            var arr = value.split("/");
            var datex = arr[2] + "-" + arr[1] + "-" + arr[0];

            //change date
            $("input[name='"+name_real+"']").val(datex); 

        });

</script>


<script type="text/javascript">
    $( ".CentreeRupiah" ).CentreeRupiah();
    $(".CentreeRupiah").keyup(function(){
          //get attribut
        var name    = $(this).attr('name');
        var name_tmp  = name.split("___");
        var name_real = name_tmp[1];
        var angka     = $(this).val();
        var prefix    = "Rp. ";

        //change rupiah
        returnx = CentreeFormatRupiah(angka);

        var value  = $(this).val(returnx[0]);
        $("input[name='"+name_real+"']").val(returnx[1]); 

    });
</script>