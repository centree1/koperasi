<html>
<?php include 'config/config.php'; ?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/x-icon" href="<?= base_url('assets/img/logo-ico.ico'); ?>">


    <!-- FA CSS -->
    <link href="<?= base_url('assets/vendor/fa/css/font-awesome.min.css'); ?>" rel="stylesheet">


    <!-- Bootstrap CSS -->
    <link href="<?= base_url('assets/vendor/bootstrap-4.3.1/css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- custom scrollbar CSS -->
    <link href="<?= base_url('assets/vendor/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css'); ?>" rel="stylesheet">

    <!-- Customized CSS -->
    <link href="<?= base_url('assets/css/bluesidebar.css'); ?>" id="link" rel="stylesheet">

    <style type="text/css">
        .menu-text{
            font-size: 11pt !important;
        }

        .icon{
            font-size: 13pt !important;
        }
        .menu-separator{
            background-color: rgba(255, 255, 255, 0.2);  font-weight: bold; font-style: italic; color: #dbdbdb; text-align: left; padding-bottom: 10px; padding-left: 10px; padding-top: 10px; padding-left: 12px;
        }
    </style>

    <title>Koperasi Clean Template</title>
</head>

<body class="h-100">
    <div class="loader">
        <div>
            <div class="loader-animation">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <br>
            <h2 class="mt-4"><span class="font-weight-light">Koperasi Indonesia</span></h2>
            <p class="text-mute">Menyiapkan Halaman . . .</p>
        </div>
    </div>
    <div class="wrapper">
        <div class="sidebar h-100 compact scroll-y">
           
            <div class="sidebar-profile text-center">
                <figure class="avatar avatar-120 avatar-circle"><img src="<?= base_url('assets/img/logo.png'); ?>" alt=""></figure>
               
                <div class="dropdown mt-3">
                    <button class="btn btn-link dropdown-toggle no-caret w-100" type="button" id="dropdownMenuButton13" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="d-flex justify-content-center">
                            <span class="ml-3">
                                Periode 
                            <i class="fa fa-angle-down align-self-center "></i><br>
                                <span class="text-mute small">1 Jan 2019 - 31 Des 2019 </span>
                            </span>
                        </span>
                    </button>
                    <div class="dropdown-menu w-100 text-center " aria-labelledby="dropdownMenuButton13">
                        <a class="dropdown-item" href="">Saldo Awal</a>
                        <a class="dropdown-item text-primary" href="#">List Tahun Buku <i class="material-icons icon-right icon"></i></a>
                    </div>
                </div>
            </div>

            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-home fa-fw icon"></i><span class="menu-text">Beranda</span></a>
                </li>
                <li class="nav-item menu-separator">
                    Admin Panel
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-users fa-fw icon"></i><span class="menu-text">Users</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-list fa-fw icon"></i><span class="menu-text">Kategori Akun</span></a>
                </li>
                <li class="nav-item menu-separator">
                    Referensi
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-building fa-fw icon"></i><span class="menu-text">Profil Perusahaan</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-vcard fa-fw icon"></i><span class="menu-text">Daftar & Penjelasan Akun</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-bookmark fa-fw icon"></i><span class="menu-text">Kebijakan Akuntansi</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-credit-card fa-fw icon"></i><span class="menu-text">Daftar Kreditor</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-credit-card-alt fa-fw icon"></i><span class="menu-text">Daftar Debitor</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-refresh fa-fw icon"></i><span class="menu-text">Kredit Salur</span></a>
                </li>
                <li class="nav-item menu-separator">
                    Jurnal
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Umum</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Penyesuaian</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Penutup</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Perolehan Kredit Bank</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Pelunasan Kredit Bank</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Penyaluran Kredit</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Pelunasan Kredit Anggota</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Penyaluran Kredit Channeling</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Kas Masuk</span></a>
                </li>
            </ul>
            
        </div>
        <div class="content h-100 d-flex flex-column w-100">
            <header class="header">
                <!-- Fixed navbar -->
                <nav class="container-fluid">
                    <div class="row">
                        <div class="col-auto align-self-center">
                            <div class="row h-100">
                                <div class="col-auto">
                                    <button class="btn btn-sm btn-link menu-btn" type="button">
                                        <i class="fa fa-bars fa-2x"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col text-center align-self-center px-0 ">
                        </div>
                        <div class="col-auto text-right align-self-center">
                            <div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle no-caret" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell-o fa-2x"></i>
                                    </button>
                                    <div class="dropdown-menu align-center pb-0" aria-labelledby="dropdownMenuButton2">
                                        <div class="arrow pink-gradient"></div>
                                        <div class="bg-template text-white py-3 text-center">
                                            <p>Tidak ada Pesan</p>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-sm btn-link dropdown-toggle no-caret user-profile" type="button" id="dropdownMenuButton4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="avatar avatar-30 mr-2 ml-1"><img src="<?= base_url('assets/img/userx.png'); ?>" alt="">
                                        </span>
                                        <span class="template-primary d-none d-xl-inline-block text-left user-profile-text">
                                            Dimas S.
                                        </span>
                                        <i class="fa fa-angle-down icon-right"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right w-100" aria-labelledby="dropdownMenuButton4">
                                        <a class="dropdown-item" href="profile.html">Profil Saya</a>
                                        <a class="dropdown-item text-danger" href="#">Sign Out <i class="fa fa-sign-out icon-right icon"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>

            <!-- Begin page content -->
            <main class="flex-shrink-0">
                <div class="container-fluid" id="main-container">
                     <div class="row">
                        <div class="container-fluid">
                            <div class="card shadow-sm border-0 mb-4">
                                <div class="card-body">
                                    <div class="">
                                        <h2>Clean Template</h2>
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                </div>
            </main>

        </div>
        

    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/bootstrap-4.3.1/js/bootstrap.min.js'); ?>"></script>

    <!-- cookie css -->
    <script src="<?= base_url('assets/vendor/cookie/jquery.cookie.js'); ?>"></script>

    <!-- custom scrollbar -->
    <script src="<?= base_url('assets/vendor/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>

    <!-- Template main js -->
    <script src="<?= base_url('assets/js/main.js'); ?>"></script>

    <!-- color picker and Layout selector main js -->
    <script src="<?= base_url('assets/js/layout-colorpicker.js'); ?>"></script>

    <!-- page specific script -->
    <script>
        'user strict'
        $(document).ready(function() {

        });

    </script>

</body>

</html>
