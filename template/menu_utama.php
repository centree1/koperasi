

<button class="collapsiblex nav-item menu-separator"> <i class="fa fa-list-alt fa-fw"> </i>Referensi</button>
<div class="contentx">
   <li class="nav-item">
        <a class="nav-link" href="tahun_buku"><i class="fa fa-list fa-fw icon"></i><span class="menu-text">Tahun Buku</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="saldo_awal"><i class="fa fa-money fa-fw icon"></i><span class="menu-text">Saldo Awal</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="akun"><i class="fa fa-list fa-fw icon"></i><span class="menu-text">Daftar & Penjelasan Akun</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="kreditor"><i class="fa fa-credit-card fa-fw icon"></i><span class="menu-text">Daftar Kreditor</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="debitor"><i class="fa fa-credit-card-alt fa-fw icon"></i><span class="menu-text">Daftar Debitor</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="kredit_salur"><i class="fa fa-refresh fa-fw icon"></i><span class="menu-text">Kredit Salur</span></a>
    </li>   
    <li class="nav-item">
        <a class="nav-link" href="anggota"><i class="fa fa-users fa-fw icon"></i><span class="menu-text">Anggota</span></a>
    </li>
</div>

<button class="collapsiblex  nav-item menu-separator"> <i class="fa fa-book fa-fw"> </i> Jurnal</button>
<div class="contentx">
    <li class="nav-item">
        <a class="nav-link" href="jurnal_umum"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Umum</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jurnal_kas_masuk"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Kas Masuk</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jurnal_kas_keluar"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Kas Keluar</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jurnal_penyesuaian"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Penyesuaian</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jurnal_penutup"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Jurnal Penutup</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jurnal_perolehan_kredit_bank"><i class="fa fa-book fa-fw icon"></i><span style="display: none;">jurnal</span><span class="menu-text">Perolehan Kredit Bank</span></a>
    </li>
    <!-- <li class="nav-item">
        <a class="nav-link" href="#"><i class="fa fa-book fa-fw icon"></i><span style="display: none;">jurnal</span><span class="menu-text">Pelunasan Kredit Bank</span></a>
    </li> -->
    <li class="nav-item">
        <a class="nav-link" href="jurnal_penyaluran_kredit"><i class="fa fa-book fa-fw icon"></i><span style="display: none;">jurnal</span><span class="menu-text">Penyaluran Kredit</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jurnal_pelunasan_kredit_anggota"><i class="fa fa-book fa-fw icon"></i><span style="display: none;">jurnal</span><span class="menu-text">Pelunasan Kredit Anggota</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jurnal_pelunasan_kredit_channeling"><i class="fa fa-book fa-fw icon"></i><span style="display: none;">jurnal</span><span class="menu-text">Pelunasan Kredit Channeling</span></a>
    </li>
</div>

<button class="collapsiblex  nav-item menu-separator"> <i class="fa fa-book fa-fw"> </i> Buku Besar</button>
<div class="contentx">
   <li class="nav-item">
        <a class="nav-link" href="bb"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Buku Besar Akun</span></a>
    </li>
    <!-- <li class="nav-item">
        <a class="nav-link" href="jurnal_umum"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Buku Pembantu Utang Bank</span></a>
    </li> -->
    <li class="nav-item">
        <a class="nav-link" href="bb_pembantu_piutang_anggota"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Buku Pembantu Piutang Anggota</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="bb_pembantu_piutang_channeling"><i class="fa fa-book fa-fw icon"></i><span class="menu-text">Buku Pembantu Channeling</span></a>
    </li> 
</div>

<button class="collapsiblex  nav-item menu-separator"><i class="fa fa-bar-chart fa-fw"> </i> Laporan Keuangan</button>
<div class="contentx">
   <li class="nav-item">
        <a class="nav-link" href="neraca"><i class="fa fa-bar-chart  fa-fw icon"></i><span class="menu-text">Neraca</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="laporan_hasil_usaha"><i class="fa fa-bar-chart fa-fw icon"></i><span class="menu-text">Laporan Hasil Usaha</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="laporan_perubahan_ekuitas"><i class="fa fa-bar-chart fa-fw icon"></i><span class="menu-text">Laporan Perubahan Ekuitas</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="laporan_arus_kas"><i class="fa fa-bar-chart fa-fw icon"></i><span class="menu-text">Laporan Arus Kas</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="mutasi"><i class="fa fa-bar-chart fa-fw icon"></i><span class="menu-text">Mutasi Kas</span></a>
    </li> 
</div>


<?php if($_SESSION['role']=='ADMIN'){ ?> 
<button class="collapsiblex nav-item menu-separator"> <i class="fa fa-cog fa-fw"> </i>Admin Panel</button>
<div class="contentx">  
    <li class="nav-item">
        <a class="nav-link" href="user"><i class="fa fa-users fa-fw icon"></i><span class="menu-text">Users</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="log"><i class="fa fa-clock-o fa-fw icon"></i><span class="menu-text">Log</span></a>
    </li>
</div>

<?php }; ?>
<button onclick="window.location = 'panduan';" class="collapsiblex nav-item menu-separator" > <i class="fa fa-question-circle fa-fw"> </i> Panduan</button>

