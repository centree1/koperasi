</div>
</main>
        </div>
        

    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="<?= base_url('assets/js/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/bootstrap-4.3.1/js/bootstrap.min.js'); ?>"></script>

    <!-- cookie css -->
    <script src="<?= base_url('assets/vendor/cookie/jquery.cookie.js'); ?>"></script>

    <!-- custom scrollbar -->
    <script src="<?= base_url('assets/vendor/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>

    <!-- Template main js -->
    <script src="<?= base_url('assets/js/main.js'); ?>"></script>

    <!-- color picker and Layout selector main js -->
    <script src="<?= base_url('assets/js/layout-colorpicker.js'); ?>"></script>

    
    <!-- Datatables -->
    <script src="<?= base_url('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js'); ?>"></script>

    <script src="<?= base_url('assets/vendor/select2/dist/js/select2.min.js'); ?>"></script>
    
    
    <!-- page specific script -->
   

    <script type="text/javascript">
    $(document).ready( function () {
        $('.dataTable').DataTable();
        $('.select2').select2({
            theme: "bootstrap",
            dropdownAutoWidth : true,
            width: 'auto',
            placeholder:"--",
            allowClear: true,
            dropdownAutoWidth : true
        });

        $('.datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false,
            sideBySide: true
        }).on('dp.change', function (e) { 
        //get attribut
            var name    = $(this).attr('name');
            var name_tmp  = name.split("___");
            var name_real = name_tmp[1];
            var value     = $(this).val();

            //change format date
            var arr = value.split("/");
            var datex = arr[2] + "-" + arr[1] + "-" + arr[0];

            //change date
            $("input[name='"+name_real+"']").val(datex); 

        });

    } );

        $("#logout").click(function (event) {
        $.ajax({
            type: 'POST',
            url: 'config/logout',
            data: '',
            cache: false,
            async: true,
            success: function (data) {
                if (data) {
                    window.location = '<?= base_url(); ?>';
                } else {
                    pesan_error('Gagal!', 'Logout Gagal!');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                pesan_error("Gagal!", errorThrown);
            }
        });
        event.preventDefault(); //Prevent the default submit
        return false; //stop
        });
        </script>

        <script type="text/javascript">
            $( ".CentreeTgl" ).CentreeTgl();  
            $( ".CentreeRupiah" ).CentreeRupiah();
            $(".CentreeRupiah").keyup(function(){
                  //get attribut
                var name    = $(this).attr('name');
                var name_tmp  = name.split("___");
                var name_real = name_tmp[1];
                var angka     = $(this).val();
                var prefix    = "Rp. ";

                //change rupiah
                returnx = CentreeFormatRupiah(angka);

                var value  = $(this).val(returnx[0]);
                $("input[name='"+name_real+"']").val(returnx[1]); 

            });
        </script>
</body>

</html>
