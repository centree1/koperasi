<?php if($_SESSION['role'] !== 'ANGGOTA'){ ?> 
<button class="collapsiblex nav-item menu-separator"> <i class="fa fa-list-alt fa-fw"> </i> Menu Utama</button>
<div class="contentx">  
	<li class="nav-item">
	    <a class="nav-link" href="tahun_buku"><i class="fa fa-list fa-fw icon"></i><span class="menu-text">Tahun Buku</span></a>
	</li>
	<li class="nav-item">
	    <a class="nav-link" href="akun"><i class="fa fa-list fa-fw icon"></i><span class="menu-text">Daftar & Penjelasan Akun</span></a>
	</li>
	<li class="nav-item">
	    <a class="nav-link" href="profil_perusahaan"><i class="fa fa-building fa-fw icon"></i><span class="menu-text">Profil Perusahaan</span></a>
	</li>
	<li class="nav-item">
	    <a class="nav-link" href="kebijakan_akuntansi"><i class="fa fa-bookmark fa-fw icon"></i><span class="menu-text">Kebijakan Akuntansi</span></a>
	</li> 
    <li class="nav-item">
        <a class="nav-link" href="anggota"><i class="fa fa-users fa-fw icon"></i><span class="menu-text">Anggota</span></a>
    </li>
</div>
<?php } ?>
<?php if($_SESSION['role']=='ADMIN'){ ?> 
<button class="collapsiblex nav-item menu-separator"> <i class="fa fa-cog fa-fw"> </i>Admin Panel</button>
<div class="contentx">  
	<li class="nav-item">
	    <a class="nav-link" href="user"><i class="fa fa-users fa-fw icon"></i><span class="menu-text">Users</span></a>
	</li>
	<li class="nav-item">
	    <a class="nav-link" href="log"><i class="fa fa-clock-o fa-fw icon"></i><span class="menu-text">Log</span></a>
	</li>
</div>
<?php }; ?>
<button onclick="window.location = 'panduan';" class="collapsiblex nav-item menu-separator" > <i class="fa fa-question-circle fa-fw"> </i> Panduan</button>
