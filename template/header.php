<html>
<head>
    <?php
    #Get session
        if(ISSET($_SESSION['tahun_buku'])){
            $tahun_buku = $_SESSION['tahun_buku'];
            $awal_periode = $_SESSION['awal_periode'];
            $akhir_periode = $_SESSION['akhir_periode'];
        }
       //include 'config/config.php';
    ?>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/x-icon" href="<?= base_url('assets/img/logo-ico.ico'); ?>">


    <!-- FA CSS -->
    <link href="<?= base_url('assets/vendor/fa/css/font-awesome.min.css'); ?>" rel="stylesheet">


    <!-- Bootstrap CSS -->
    <link href="<?= base_url('assets/vendor/bootstrap-4.3.1/css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- custom scrollbar CSS -->
    <link href="<?= base_url('assets/vendor/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css'); ?>" rel="stylesheet">
    
    <!-- Customized CSS -->
    <?php if(empty($tahun_buku)){ ?>
        <style type="text/css">
        .loader{    
            background: linear-gradient(-45deg,  #08730c, #1bb379, #1b37b3) !important;
            background-size: 400% 400% !important;
            animation: gradientBG 3s ease infinite !important;
        }
        @keyframes gradientBG {
            0% {
                background-position: 50% 0%;
            }
            50% {
                background-position: 50% 100%;
            }
            100% {
                background-position: 50% 0%;
            }
        }
    </style>
        <link href="<?= base_url('assets/css/greensidebar.css'); ?>" id="link" rel="stylesheet">

    <?php }else{ ?>
        <style type="text/css">
        .loader{    
            background: linear-gradient(-45deg,  #1b37b3, #1bb379, #08730c) !important;
            background-size: 400% 400% !important;
            animation: gradientBG 3s ease infinite !important;
        }
        @keyframes gradientBG {
            0% {
                background-position: 0% 50%;
            }
            50% {
                background-position: 100% 50%;
            }
            100% {
                background-position: 0% 50%;
            }
        }
    </style>
        <link href="<?= base_url('assets/css/bluesidebar.css'); ?>" id="link" rel="stylesheet">
    <?php } ?>
    <!-- Customized CSS -->
    <link href="<?= base_url('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css'); ?>" id="link" rel="stylesheet">


    <script src="<?= base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>

     <!-- datetime -->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/datetimepicker/build/css/bootstrap-datetimepicker.min.css'); ?>">
    <script src="<?= base_url('assets/vendor/datetimepicker/build/js/moment.js'); ?>"></script>
    <script src="<?= base_url('assets/vendor/datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>

    <!-- swal2 -->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/sweetalert2/dist/sweetalert2.css'); ?>">
    <script src="<?= base_url('assets/vendor/sweetalert2/dist/sweetalert2.min.js'); ?>"></script>

    <!-- select2 -->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/select2/dist/css/select2.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/vendor/select2/dist/select2-bootstrap.min.css'); ?>">

    <!-- custom -->
    <script src="<?= base_url(); ?>config/config.js"></script>
    <!-- <style type="text/css">
        .sidebar {
            color: #fff;
            background: linear-gradient(-45deg,  #014f16,#014714,#017821) !important;
            background-size: 400% 400% !important;
            animation: gradientBG 5s ease infinite !important;
        }

@keyframes gradientBG {
    0% {
        background-position: 0% 50%;
    }
    50% {
        background-position: 100% 50%;
    }
    100% {
        background-position: 0% 50%;
    }
}
    </style> -->
    

    <style type="text/css">
        .menu-text{
            font-size: 11pt !important;
        }

        .icon{
            font-size: 13pt !important;
        }
        .menu-separator{
            background-color: rgba(255, 255, 255, 0.2);  font-weight: bold; font-style: italic; color: #dbdbdb; text-align: left; padding-bottom: 10px; padding-left: 10px; padding-top: 10px; padding-left: 12px;
        }

        label-tabel {
          font-size: 20pt;  
          font-weight: bold;
        }
        label-tabel-small {
          font-size: 12pt;  
          font-weight: bold;
          margin-bottom: 10px;
        }
        label {
            font-weight:  bold;
        }

        .title-desktop{
            display: inline-block !important;
        }
        .title-mobile{
        display: none !important;
        }
        .dim-hidden{
            display: none;
        }
        .dim-selected{
            background-color: #adf1ff !important;
            color: white !important;
        }

         @media (max-width: 768px) {
          .col-xs-12.dim-center {
                  text-align: center !important;
                  padding-top: 10px !important;
           }
           .dim-right{
            text-align: right;
           } 
           .title-desktop{
            display: none !important;
           }
           .title-mobile{
            display: block !important;
           }

        }

        .dim-memo{
            background-color: #fff5c9 !important;
            font-style: italic !important;
            text-align: center !important;
        }
        .dim-saldo{
            background-color: #c7ffff;
            text-align: center;
            font-weight: bold;
        }
        .dim-saldo-debit{
            background-color: #c7ffcd;
            font-weight: bold;
        }
        .dim-saldo-kredit{
            background-color: #ffc7c7;
            font-weight: bold;
        }
        
        .readonly{
            background-color: #f7f5f5 !important;
        }
        .disabled {
            cursor: not-allowed;
            pointer-events: none;
            background-color: #ededed !important;
        }
        .cari-menu{
            height: 40pt;
            padding-left: 20px;
            width: 100%;
            opacity: 0.8;
        }
    </style>
    <style>
    .collapsiblex { 
      color: white;
      cursor: pointer;
      padding: 18px;
      width: 100%;
      border: none;
      text-align: left;
      outline: none;
      font-size: 15px;
      margin-bottom: 5px;
    }

    .activex, .collapsiblex:hover {
      background-color: #555;
    }

    .collapsiblexy { 
      color: white;
      cursor: pointer;
      padding: 18px;
      width: 100%;
      border: none;
      text-align: left;
      outline: none;
      font-size: 15px;
      margin-bottom: 5px;
    }

    .activex, .collapsiblexy:hover {
      background-color: #555;
    }

    .contentx {
      max-height: 0;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
    }
    </style>
    <style type="text/css">
    
        .bg-debit{
            background-color: #ccfaff;
            color: black !important;
            font-weight: bold;
            text-align: center; 
        }

        .bg-kredit{
            background-color: #fdffbd;
            color: black !important;
            font-weight: bold;
            text-align: center; 
        }

        .bg-plus{
            background-color:   #e8e8e8;
            color: white !important;
            font-weight: bold;
            text-align: center;
            border-color: white !important;
        }
    </style>

    <?php
        if(!ISSET($title)){
            $title = 'Koperasi';
        }else{
            $title = 'Koperasi | '.$title;
        }
    ?>

    <!--chartjs -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendor/chartjs/Chart.css'); ?>">
    <script type="text/javascript" src="<?= base_url('assets/vendor/chartjs/Chart.js'); ?>"></script>

    <title><?= $title; ?></title>
</head>



<body class="h-100">
    <?php ob_start(); ?>
    <div class="loader">
        <div>
            <div class="loader-animation">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <br>
            <h2 class="mt-4"><span class="font-weight-light">Koperasi Indonesia</span></h2>
            <pre class="text-mute">Menyiapkan Halaman . . .</pre>
        </div>
    </div>
    <div class="wrapper">
        <div class="sidebar h-100 compact scroll-y">
           
            <div class="sidebar-profile text-center">
                <figure class="avatar avatar-120 avatar-circle"><img src="<?= base_url('assets/img/logo.png'); ?>" alt=""></figure>
               
                <?php if(ISSET($tahun_buku)){ ?>
                    <div class="dropdown mt-3">
                        <button class="btn btn-link dropdown-toggle no-caret w-100" type="button" id="dropdownMenuButton13" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="d-flex justify-content-center">
                                <span class="ml-3">
                                    Periode 
                                <i class="fa fa-angle-down align-self-center "></i><br>
                                    <span class="text-mute small"><?= tgl_indo2($awal_periode)." - ".tgl_indo2($akhir_periode); ?> </span>
                                </span>
                            </span>
                        </button>
                        <div class="dropdown-menu w-100 text-center " aria-labelledby="dropdownMenuButton13">
                            <a class="dropdown-item" href="saldo_awal">Saldo Awal</a>
                            <button class="dropdown-item text-danger"  onclick="tutup_buku()"> Tukar Tahun Buku <i class="material-icons icon-right icon"></i></button>
                            <script type="text/javascript">
                                function tutup_buku(){
                                    simple_ajax("",'config/tutup_buku','tahun_buku','','Gagal, Membuka List Buku');
                                }
                            </script>
                        </div>
                    </div>
                <?php } ?>
                </div>
                
<input type="text" id="mySearch" class="cari-menu" onkeyup="myFunction()" placeholder="Cari Menu">
            <ul class="nav flex-column" style="padding-top: 20px !important;" id="myMenu">
                <button class="collapsiblexy nav-item menu-separator" onclick="window.location = 'dashboard';"> 
                    <i class="fa fa-home fa-fw"> </i> Dashboard
                </button>
                <?php 
                    
                    if(!empty($tahun_buku)){
                        include 'template/menu_utama.php';
                    }else{
                        include 'template/menu_awal.php';
                    }
                ?>
            </ul>

            <script>
            var coll = document.getElementsByClassName("collapsiblex");
            var i;

            for (i = 0; i < coll.length; i++) {
              coll[i].addEventListener("click", function() {
                this.classList.toggle("activex");
                var content = this.nextElementSibling;
                if (content.style.maxHeight){
                  content.style.maxHeight = null;
                } else {
                  content.style.maxHeight = content.scrollHeight + "px";
                } 
              });
            }
            </script> 

            <script>
                var tmp = 0;
                function collapse_all(){
                    if (tmp==0){
                        $(".collapsiblex").click();
                        tmp = 1;
                    }
                }

                function myFunction() {
                  // Declare variables
                  var input, filter, ul, li, a, i;
                  input = document.getElementById("mySearch");

                    if(input.value==""){
                        console.log("asd");
                        tmp = 0;
                        $(".collapsiblex").click();
                    }else{
                        collapse_all();
                    }
                  filter = input.value.toUpperCase();
                  ul = document.getElementById("myMenu");
                  li = ul.getElementsByTagName("li");

                  // Loop through all list items, and hide those who don't match the search query
                  for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    //console.log(a);
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                      li[i].style.display = "";
                    } else {
                      li[i].style.display = "none";
                    }
                  }
                }
                </script>
            
        </div>
        <div class="content h-100 d-flex flex-column w-100 mb-5"  >
            <header class="header sticky-top " style="background-color: white; padding: 5px !important;" >
                <!-- Fixed navbar -->
                <nav class="container-fluid" >
                    <div class="row ">
                        <div class="col-auto align-self-center">
                            <div class="row h-100">
                                <div class="col-md-12 col-xs-12" >
                                    <button class="btn btn-sm btn-link menu-btn" type="button">
                                        <i class="fa fa-bars fa-2x"></i>
                                    </button>
                                    <!-- <div class="title-desktop"><b ><?= $title; ?></b></div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col text-center align-self-center px-0 ">
                        </div>
                        <div class="col-auto text-right align-self-center">
                            <div class="btn-group">
                                <!-- <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle no-caret" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell-o fa-2x"></i>
                                    </button>

                                    <div class="dropdown-menu align-center pb-0" aria-labelledby="dropdownMenuButton2">
                                        <div class="arrow pink-gradient"></div>
                                        <div class="bg-template text-white py-3 text-center">
                                            <p>Tidak ada Pesan</p>
                                        </div>
                                        
                                        
                                    </div>
                                </div> -->
                            </div>
                            
                            <div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-sm btn-link dropdown-toggle no-caret user-profile" type="button" id="dropdownMenuButton4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="avatar avatar-30 mr-2 ml-1"><img src="<?= base_url('assets/img/userx.png'); ?>" alt="">
                                        </span>
                                        <span class="template-primary d-none d-xl-inline-block text-left user-profile-text">
                                            <?= $_SESSION['nama']; ?>
                                        </span>
                                        <i class="fa fa-angle-down icon-right"></i>
                                    </button>
                                    <!-- <?php
                                        $query = 'select * from tb_user';
                                        $execute = mysqli_query($koneksi,$query);
                                        $row = mysqli_fetch_object($execute) ;
                                         //im_debugging ($row);
                                            

                                    
                                    ?> -->
                                    <div class="dropdown-menu dropdown-menu-right w-100" aria-labelledby="dropdownMenuButton4">
                                        <a class="dropdown-item" href='edit_user_profile'>Profil Saya</a>
                                        <button id="logout" class="dropdown-item text-danger">Sign Out <i class="fa fa-sign-out icon-right icon"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>

            </header>
            <!-- <div class="header title-mobile" style="background-color: #005a6c; padding: 5px !important; z-index: 0 !important; color: white; text-align: center;" >
                <b><?= $title; ?></b>
            </div> -->
            
            <main class="flex-shrink-0" style="padding-top: 2%;">
    <div class="container-fluid " id="main-container" >