<?php
include 'config/config.php';
$title = "Tambah Tipe Akun";
include 'template/header.php';
cek_tahun_buku();

?>
<style type="text/css">
    .container-form{
        padding-top: 40px;
        color: black;
    }
    label{
        font-weight: bold;
    }
</style>
<!-- ============ Body content start ============= -->

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   <div id="carouselExampleIndicators" data-interval="false" class="carousel slide" data-ride="carousel">
                      <form id="frmz">
                      <div class="carousel-inner">
                        <!-- Referensi Data Debitor -->
                        <div class="carousel-item active dim-border">
                          <label-tabel-small> Referensi Data Debitor</label-tabel-small>
                          <br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Kredit yang Disalurkan</label>
                                        <select  required id="id_kredit_salur" name="id_kredit_salur" class="form-control select2" onchange="cek_kredit_salur(this.value);">
                                            <?php 
                                            $query=mysqli_query($koneksi, "SELECT * FROM tb_kredit_salur,tb_akun WHERE tb_kredit_salur.no_akun = tb_akun.no_akun");
                                            while($row=mysqli_fetch_array($query)){ ?>
                                            <option value="<?php echo $row['id_kredit_salur']; ?>"><?php echo $row['nama_akun']." | ".$row['jenis_kredit']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                  
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>No Identitas Debitor</label>
                                    <input required readonly id="no_identitas_debitor" name="no_identitas_debitor" type="text" class="form-control" placeholder="No. Akun">
                                  </div>
                                </div>
                            </div>

                            <div class="mt-3 row">
                                <div class="col-md-6">
                                     <button class="btn btn-danger" onclick="history.back();" > <i class="fa fa-arrow-left"></i> Kembali </button>  
                                </div>
                                <div class="col-md-6 text-right">
                                    <a  class="btn btn-info" href="#carouselExampleIndicators" role="button" data-slide="next" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                                </div>
                            </div>
                        </div>

                        <!-- Data Pribadi -->
                        <div class="carousel-item  dim-border">
                            <label-tabel-small> Data Pribadi </label-tabel-small>
                            <br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama</label>
                                    <input required name="nama" type="text" class="form-control" placeholder="Nama" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <select required name="jenis_kelamin" class="form-control select2" onchange="check_requirement1()" onblur="check_requirement1()">
                                            <option  value="Pria">Pria</option>
                                            <option value="Wanita">Wanita</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input required name="tempat_lahir" type="text" class="form-control" placeholder="Tempat Lahir" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input required name="tanggal_lahir" type="date" class="form-control" placeholder="Tanggal Lahir" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Warga Negara</label>
                                    <input required name="warga_negara" type="text" class="form-control" placeholder="Warga Negara" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama Ibu Kandung (Sebelum Menikah)</label>
                                    <input required name="nama_ibu" type="text" class="form-control" placeholder="Nama Ibu Kandung" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>NPWP</label>
                                    <input required name="npwp" type="text" class="form-control" placeholder="NPWP" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>No KTP/Passpor</label>
                                    <input required name="no_ktp_passpor" type="text" class="form-control" placeholder="No KTP/Passpor" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Agama</label>
                                    <input required name="agama" type="text" class="form-control" placeholder="Agama" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>E-mail</label>
                                    <input required name="email" type="email" class="form-control" placeholder="E-mail" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Rumah(Sesuai KTP/Passpor)</label>
                                    <input required name="alamat_ktp" type="text" class="form-control" placeholder="Alamat Rumah" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Sesuai KTP/Passpor)</label>
                                    <input required name="kota_ktp" type="text" class="form-control" placeholder="Kota" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Sesuai KTP/Passpor)</label>
                                    <input required name="kode_pos_ktp" type="text" class="form-control" placeholder="Kode Pos" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Sesuai KTP/Passpor)</label>
                                    <input required name="telp" type="text" class="form-control" placeholder="Telp" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Sesuai KTP/Passpor)</label>
                                    <input required name="hp" type="text" class="form-control" placeholder="HP" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Rumah(Sekarang)</label>
                                    <input required name="alamat_sekarang" type="text" class="form-control" placeholder="Alamat Rumah" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Ditempati Sejak</label>
                                    <input required name="ditempati_sejak_sekarang" type="text" class="form-control" placeholder="Ditempati Sejak" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Sekarang)</label>
                                    <input required name="kota_sekarang" type="text" class="form-control" placeholder="Kota" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Sekarang)</label>
                                    <input required name="kode_pos_sekarang" type="text" class="form-control" placeholder="Kode Pos" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Sekarang)</label>
                                    <input required name="telp_sekarang" type="text" class="form-control" placeholder="Telp" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Sekarang)</label>
                                    <input required name="hp_sekarang" type="text" class="form-control" placeholder="HP" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status Kepemilikan</label>
                                        <select required name="status_kepemilikan" class="form-control select2" onchange="check_requirement1()" onblur="check_requirement1()">
                                            <option  value=""></option>
                                            <option  value="Pribadi">Pribadi</option>
                                            <option value="Keluarga">Keluarga</option>
                                            <option value="Dinas">Dinas</option>
                                            <option value="Sewa">Sewa</option>
                                            <option value="Kost">Kost</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Surat</label>
                                    <input required name="alamat_surat" type="text" class="form-control" placeholder="Alamat Surat" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota</label>
                                    <input required name="kota_surat" type="text" class="form-control" placeholder="Kota" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pendidikan Terakhir</label>
                                        <select required name="pendidikan_terakhir" class="form-control select2" onchange="check_requirement1()" onblur="check_requirement1()">
                                            <option  value=""></option>
                                            <option  value="SMA">SMA</option>
                                            <option value="D3">D3</option>
                                            <option value="S1">S1</option>
                                            <option value=">S1">>S1</option>
                                        </select>
                                      </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status Perkawinan</label>
                                        <select required name="status_perkawinan" class="form-control select2" id="statusx" onchange="cek_status_perkawinan(this.value); check_requirement1();"  onblur="check_requirement1()">
                                            <option value="">---- Pilih Nikah ----</option>
                                            <option value="Belum Nikah">Belum Nikah</option>
                                            <option value="Nikah">Nikah</option>
                                            <option value="Duda/Janda">Duda/Janda</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Jumlah Tanggungan</label>
                                    <input required name="jumlah_tanggungan" type="number" class="form-control" placeholder="Jumlah Tanggunan" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama(Untuk Keperluan Mendadak)</label>
                                    <input required name="nama_keperluan" type="text" class="form-control" placeholder="Nama" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat(Untuk Keperluan Mendadak)</label>
                                    <input required name="alamat_keperluan" type="text" class="form-control" placeholder="Alamat" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Untuk Keperluan Mendadak)</label>
                                    <input required name="kota_keperluan" type="text" class="form-control" placeholder="Kota" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Untuk Keperluan Mendadak)</label>
                                    <input required name="kode_pos_keperluan" type="text" class="form-control" placeholder="Kode Pos" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Untuk Keperluan Mendadak)</label>
                                    <input required name="telp_keperluan" type="text" class="form-control" placeholder="Telp" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Untuk Keperluan Mendadak)</label>
                                    <input required name="hp_keperluan" type="text" class="form-control" placeholder="HP" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Hubungan(Untuk Keperluan Mendadak)</label>
                                    <input required name="hubungan_keperluan" type="text" class="form-control" placeholder="Hubungan" onchange="check_requirement1()" onblur="check_requirement1()">
                                  </div>
                                </div>
                            </div>
                            <div class="mt-3 row">
                                <div class="col-md-6">
                                    <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                                </div>
                                <div class="col-md-6 text-right">
                                    <a class="btn btn-info disabled" href="#carouselExampleIndicators" id="next1" role="button" data-slide="next" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                          var list_requiredment1 = ['nama','tempat_lahir','warga_negara','npwp','agama','alamat_ktp','kode_pos_ktp','kode_pos_keperluan','hp','hp_sekarang','pendidikan_terakhir','jenis_kelamin','tanggal_lahir','nama_ibu','ditempati_sejak_sekarang','kode_pos_sekarang','alamat_surat','jumlah_tanggungan','alamat_keperluan','hp_keperluan','telp_sekarang','kota_surat','no_ktp_passpor','email','kota_ktp','telp','alamat_sekarang','kota_sekarang','status_kepemilikan','status_perkawinan','nama_keperluan','kota_keperluan','telp_keperluan','hubungan_keperluan'];
                          console.log(list_requiredment1.length);
                          check_requirement1();
                          function check_requirement1(){
                            var i = 0;
                            var valx = null;
                            var statx = 0;
                            while(i<list_requiredment1.length){
                              valx = $('[name="'+list_requiredment1[i]+'"]');
                              if(valx.val()==""){
                                valx.css("background-color","#fff9cf");
                              }else{
                                valx.css("background-color","white");
                                statx++;
                              }
                              //console.log(list_requiredment1[i]+" ==> "+valx.val());
                              i++;
                            }
                            if(statx >=list_requiredment1.length){
                              console.log("first "+statx);
                              console.log("First Yess");
                              $("#next1").removeClass('disabled');
                            }else{
                              console.log(statx);
                              $("#next1").addClass('disabled');
                            }
                          }
                        </script>

                        <!-- Data Pekerjaan -->
                        <div class="carousel-item  dim-border">
                          <label-tabel-small> Data Pekerjaan </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jenis Pekerjaan</label>
                                <select id="jenis_pekerjaan" required name="jenis_pekerjaan" class="form-control select2" onchange="cek_pekerjaan(this.value);"  onchange="check_requirement2()" onblur="check_requirement2()">
                                  <option  value=""></option>
                                  <option  value="Pegawai Negeri">Pegawai Negeri</option>
                                  <option value="CPNS/Honor">CPNS/Honor</option>
                                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                                  <option value="Profesional">Profesional</option>
                                  <option value="Wiraswasta">Wiraswasta</option>
                                  <option value="Lainnya">Lainnya</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan Lainnya</label>
                                <input id="jenis_pekerjaan_lainnya" disabled="disabled" name="jenis_pekerjaan_lainnya" type="text" class="form-control" placeholder="Jenis Pekerjaan" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input required name="nama_perusahaan" type="text" class="form-control" placeholder="Nama Perusahaan" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Bidang Usaha</label>
                                <input required name="bidang_usaha" type="text" class="form-control" placeholder="Bidang Usaha" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Alamat</label>
                                <input required name="alamat_perusahaan" type="text" class="form-control" placeholder="Alamat" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kota</label>
                                <input required name="kota_perusahaan" type="text" class="form-control" placeholder="Kota" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kode Pos</label>
                                <input required name="kode_pos_perusahaan" type="text" class="form-control" placeholder="Kode Pos" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Telp</label>
                                <input required name="telp_perusahaan" type="text" class="form-control" placeholder="Telp" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>HP</label>
                                <input required name="hp_perusahaan" type="text" class="form-control" placeholder="HP" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Mulai Bekerja Sejak(Tahun)</label>
                                <input required name="mulai_bekerja" type="text" class="form-control" placeholder="Mulai Bekerja Sejak" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jabatan</label>
                                <input required name="jabatan_perusahaan" type="text" class="form-control" placeholder="Jabatan" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Atasan</label>
                                <input required name="nama_atasan" type="text" class="form-control" placeholder="Nama Atasan" onchange="check_requirement2()" onblur="check_requirement2()">
                              </div>
                            </div>
                          </div>
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev"> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                            </div>
                            <div class="col-md-6 text-right">
                              <button type="button" class="btn btn-info"  role="button" data-slide="next" id="nextx" onclick='
                $("html, body").animate({scrollTop: 0}, 1000); check_requirement2()' onblur="check_requirement2()"> Selanjutnya <i class="fa fa-arrow-right"></i> </button> 
                            </div>
                          </div>
                        </div>

                        <script type="text/javascript">
                          var list_requiredment2 = ['jenis_pekerjaan','nama_perusahaan','bidang_usaha','alamat_perusahaan','kota_perusahaan','kode_pos_perusahaan','telp_perusahaan','hp_perusahaan','mulai_bekerja','jabatan_perusahaan','nama_atasan'];
                          console.log(list_requiredment2.length);
                          check_requirement2();
                          function check_requirement2(){
                            var i = 0;
                            var valx = null;
                            var stat = 0;
                            while(i<list_requiredment2.length){
                              valx = $('[name="'+list_requiredment2[i]+'"]');
                              if(valx.val()==""){
                                valx.css("background-color","#fff9cf");
                              }else{
                                valx.css("background-color","white");
                                stat++;
                              }
                              i++;
                            }
                            if(stat >= list_requiredment2.length){
                              console.log(stat);
                              console.log("Second Yess");
                              $("#nextx").removeClass('disabled');
                            }else{
                              console.log(stat);
                              $("#nextx").addClass('disabled');
                            }
                          }
                        </script>

                        <!-- Data Suami Istri -->
                        <div class="carousel-item  dim-border" id="carousel_suami_istri">
                          <label-tabel-small> Data Suami Istri </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama</label>
                                <input name="nama_pasangan" type="text" class="form-control" placeholder="Nama" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input name="tempat_lahir_pasangan" type="text" class="form-control" placeholder="Tempat Lahir" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input name="tanggal_lahir_pasangan" type="date" class="form-control" placeholder="Tanggal Lahir" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan</label>
                                <select id="jenis_pekerjaan_pasangan" name="jenis_pekerjaan_pasangan" class="form-control select2" onchange="cek_pekerjaan_pasangan(this.value);check_requirement3();"  onchange="check_requirement3()" onblur="check_requirement3()">
                                  <option  value="Pegawai Negeri">Pegawai Negeri</option>
                                  <option value="CPNS/Honor">CPNS/Honor</option>
                                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                                  <option value="Profesional">Profesional</option>
                                  <option value="Wiraswasta">Wiraswasta</option>
                                  <option value="Lainnya">Lainnya</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input name="nama_perusahaan_pasangan" type="text" class="form-control" placeholder="Nama Perusahaan" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan Lainnya</label>
                                <input id="jenis_pekerjaan_pasangan_lainnya" disabled="disabled" name="jenis_pekerjaan_pasangan_lainnya" type="text" class="form-control" placeholder="Jenis Pekerjaan" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Bidang Usaha</label>
                                <input name="bidang_usaha_pasangan" type="text" class="form-control" placeholder="Bidang Usaha" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Mulai Bekerja Sejak(Tahun)</label>
                                <input name="mulai_bekerja_pasangan" type="text" class="form-control" placeholder="Mulai Bekerja Sejak" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Alamat</label>
                                <input name="alamat_perusahaan_pasangan" type="text" class="form-control" placeholder="Alamat" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kota</label>
                                <input name="kota_perusahaan_pasangan" type="text" class="form-control" placeholder="Kota" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kode Pos</label>
                                <input name="kode_pos_perusahaan_pasangan" type="text" class="form-control" placeholder="Kode Pos" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Telp</label>
                                <input name="telp_perusahaan_pasangan" type="text" class="form-control" placeholder="Telp" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>HP</label>
                                <input name="hp_perusahaan_pasangan" type="text" class="form-control" placeholder="HP" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jabatan</label>
                                <input name="jabatan_perusahaan_pasangan" type="text" class="form-control" placeholder="Jabatan" onchange="check_requirement3()" onblur="check_requirement3()">
                              </div>
                            </div>
                          </div>                            
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                            </div>
                            <div class="col-md-6 text-right">
                              <a class="btn btn-info" href="#carouselExampleIndicators" role="button" id="nexty" data-slide="next" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                            </div>
                          </div>
                        </div>

                        <script type="text/javascript">
                          /*var list_requiredment4 = ['jumlah_kredit','jangka_waktu','maksimum_angsuran','tanggal_penyaluran','tanggal_jatuh_tempo','periode_cicilan','jatuh_tempo_cicilan','nominal_cicilan','keterangan','tujuan_penggunaan','term','banyak_cicilan','bunga'];*/

                          var list_requiredment3 = ['nama_pasangan','tempat_lahir_pasangan','tanggal_lahir_pasangan','jenis_pekerjaan_pasangan','nama_perusahaan_pasangan','bidang_usaha_pasangan','mulai_bekerja_pasangan','alamat_perusahaan_pasangan','kota_perusahaan_pasangan','kode_pos_perusahaan_pasangan','telp_perusahaan_pasangan','hp_perusahaan_pasangan','jabatan_perusahaan_pasangan'];
                            console.log('3 => '+list_requiredment3.length);

                            check_requirement3()
                            function check_requirement3(){
                            var i = 0;
                            var valx = null;
                            var stat = 0;
                            while(i<list_requiredment3.length){
                              valx = $('[name="'+list_requiredment3[i]+'"]');
                              if(valx.val()==""){
                                valx.css("background-color","#fff9cf");
                              }else{
                                valx.css("background-color","white");
                                stat++;
                              }
                              i++;
                            }
                            if(stat >= list_requiredment3.length){
                              console.log(stat);
                              console.log("Third Yess");
                              $("#nexty").removeClass('disabled');
                            }else{
                              console.log(stat);
                              $("#nexty").addClass('disabled');
                            }
                          }
                        </script>

                        <!-- Data Pemberian Kredit -->
                        <div class="carousel-item dim-border">
                          <label-tabel-small> Data Pemberian Kredit </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jumlah Kredit yang Diajukan</label>
                                <input required name="jumlah_kredit" type="text" class="form-control" placeholder="Jumlah Kredit"  onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Keterangan</label>
                                <select id="keterangan" required name="keterangan" class="form-control select2" onchange="cek_keterangan(this.value); check_requirement4();" onblur="check_requirement4()">
                                  <option  value="Baru">Baru</option>
                                  <option value="Take Over">Take Over</option>
                                </select>
                              </div>
                            </div>
                          </div>       
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Jangka Waktu</label>
                                <input required name="jangka_waktu" type="text" class="form-control" placeholder="Jangka Waktu" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Take Over dari</label>
                                <input id="keterangan_take_over" disabled="disabled" required name="keterangan_take_over" type="text" class="form-control" placeholder="Take Over" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Maksimum Angsuran yang Diinginkan</label>
                                <input required name="maksimum_angsuran" type="text" class="form-control" placeholder="Maksimum Angsuran" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tujuan Penggunaan Kredit</label>
                                <select required id="tujuan_penggunaan" name="tujuan_penggunaan" class="form-control select2" onchange="cek_tujuan_kredit(this.value); check_requirement4();" onblur="check_requirement4()">
                                  <option value="Rehabilitasi/Renovasi Rumah">Rehabilitasi/Renovasi Rumah</option>
                                  <option value="Keperluan Rumah Tangga">Keperluan Rumah Tangga</option>
                                  <option value="Biaya Pendidikan">Biaya Pendidikan</option>
                                  <option value="Biaya Pengobatan">Biaya Pengobatan</option>
                                  <option value="Keperluan Lainnya yang bersifat konsumtif">Keperluan Lainnya yang bersifat konsumtif</option>
                                  <option value="Biaya Pernikahan/Perkawinan">Biaya Pernikahan/Perkawinan</option>
                                  <option value="KAG Mix">KAG Mix</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Penyaluran Kredit</label>
                                <input required name="tanggal_penyaluran" type="date" class="form-control" placeholder="Tanggal Penyaluran Kredit"  onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tujuan Penggunaan Lainnya</label>
                                <input id="tujuan_penggunaan_lainnya" disabled="disabled" required name="tujuan_penggunaan_lainnya" type="text" class="form-control" placeholder="Tujuan Penggunaan Lainnya"  onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Jatuh Tempo</label>
                                <input required name="tanggal_jatuh_tempo" type="date" class="form-control" placeholder="Tanggal Jatuh Tempo" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Term</label>
                                <input required name="term" type="text" class="form-control" placeholder="Term" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Periode Cicilan</label>
                                <input required name="periode_cicilan" type="text" class="form-control" placeholder="Periode Cicilan" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Banyak Cicilan</label>
                                <input required name="banyak_cicilan" type="text" class="form-control" placeholder="Banyak Cicilan" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jatuh Tempo Cicilan</label>
                                <input required name="jatuh_tempo_cicilan" type="text" class="form-control" placeholder="Jatuh Tempo Cicilan" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Bunga</label>
                                <input required name="bunga" type="text" class="form-control" placeholder="Bunga" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nominal Cicilan</label>
                                <input required name="nominal_cicilan" type="text" class="form-control" placeholder="Nominal Cicilan" onchange="check_requirement4()" onblur="check_requirement4()">
                              </div>
                            </div>
                          </div>
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <button class="btn btn-secondary" type="button" id="previousx" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> <i class="fa fa-arrow-left"></i> Sebelum </button> 
                            </div>
                            <div class="col-md-6 text-right">
                              <button class="btn btn-info" type="submit" id="save"> <i class="fa fa-save"></i> Simpan  </button> 
                            </div>
                          </div>
                        </div>
                        <script type="text/javascript">
                          var list_requiredment4 = ['jumlah_kredit','jangka_waktu','maksimum_angsuran','tanggal_penyaluran','tanggal_jatuh_tempo','periode_cicilan','jatuh_tempo_cicilan','nominal_cicilan','keterangan','tujuan_penggunaan','term','banyak_cicilan','bunga'];

                          /*var list_requiredment3 = ['nama_pasangan','tempat_lahir_pasangan','tanggal_lahir_pasangan','jenis_pekerjaan_pasangan','nama_perusahaan_pasangan','bidang_usaha_pasangan','mulai_bekerja_pasangan','alamat_perusahaan_pasangan','kota_perusahaan_pasangan','kode_pos_perusahaan_pasangan','telp_perusahaan_pasangan','hp_perusahaan_pasangan','jabatan_perusahaan_pasangan'];*/
                            console.log('4 => '+list_requiredment4.length);

                            check_requirement4()
                            function check_requirement4(){
                            var i = 0;
                            var valx = null;
                            var stat = 0;
                            while(i<list_requiredment4.length){
                              valx = $('[name="'+list_requiredment4[i]+'"]');
                              if(valx.val()==""){
                                valx.css("background-color","#fff9cf");
                              }else{
                                valx.css("background-color","white");
                                stat++;
                              }
                              i++;
                            }

                            if(stat >= list_requiredment4.length){
                              console.log(stat);
                              console.log("Fourth Yess");
                              $("#save").removeClass('disabled');
                            }else{
                              console.log(stat);
                              $("#save").addClass('disabled');
                            }
                          }
                        </script>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<script type="text/javascript">
        $('document').ready(function(){
          let value = $("#id_kredit_salur").val();
          ajax_debitor(value);
        });
        function cek_kredit_salur(value){
          ajax_debitor(value);
        }
        //cek status kawin
        var status = "false"; //if true = sudah nikah
        function cek_status_perkawinan(value){
            if(value=="Nikah"){
               status = "true";
               console.log("nikah ="+status);
            }else{
               status = "false";
               console.log("nikah ="+status);
            }
        }

        function cek_pekerjaan(value){
          if(value=="Lainnya"){
            document.getElementById("jenis_pekerjaan_lainnya").disabled = "";
          }else{
            document.getElementById("jenis_pekerjaan_lainnya").disabled = "disabled";
          }
        }

        function cek_pekerjaan_pasangan(value){
          if(value=="Lainnya"){
            document.getElementById("jenis_pekerjaan_pasangan_lainnya").disabled = "";
          }else{
            document.getElementById("jenis_pekerjaan_pasangan_lainnya").disabled = "disabled";
          }
        }

        function cek_keterangan(value){
          if(value=="Take Over"){
            document.getElementById("keterangan_take_over").disabled = "";
          }else{
            document.getElementById("keterangan_take_over").disabled = "disabled";
          }
        }
        function cek_tujuan_kredit(value){
          if(value=="Keperluan Lainnya yang bersifat konsumtif"){
            document.getElementById("tujuan_penggunaan_lainnya").disabled = "";
          }else{
            document.getElementById("tujuan_penggunaan_lainnya").disabled = "disabled";
          }
        }

        $( document ).ready(function() {
            $("#nextx").click(function(){
                if(status=="false"){
                    console.log('false');
                     $("#carouselExampleIndicators").carousel(4); //Skip slide nikah
                }else{
                    console.log('true');
                    $("#carouselExampleIndicators").carousel("next");
                }
                $("html, body").animate({scrollTop: 0}, 1000);
          });

            $("#previousx").click(function(){
                if(status=="false"){
                    console.log('false');
                     $("#carouselExampleIndicators").carousel(2); //Skip slide nikah
                }else{
                    console.log('true');
                    $("#carouselExampleIndicators").carousel("prev");
                }
                $("html, body").animate({scrollTop: 0}, 1000);
          });
        });
         
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 

            var values = $(this).serialize();
            simple_ajax(values,'config/tambah_debitor','debitor','Data Debitor Sukses Ditambahkan','Gagal tambah Akun');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

          $("input").focus(function(){
            $(this).css("background-color", "white");
          });
    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>