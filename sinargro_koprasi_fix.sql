/*
 Navicat Premium Data Transfer

 Source Server         : Local Dimpul
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : sinargro_koprasi_fix

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 26/11/2019 19:33:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_akun
-- ----------------------------
DROP TABLE IF EXISTS `tb_akun`;
CREATE TABLE `tb_akun`  (
  `no_akun` int(11) NOT NULL,
  `nama_akun` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipe_akun` int(11) NOT NULL,
  `saldo_normal` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `penjelasan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kategori` int(11) NOT NULL,
  PRIMARY KEY (`no_akun`) USING BTREE,
  INDEX `id_kategori`(`id_kategori`) USING BTREE,
  INDEX `tipe_akun`(`tipe_akun`) USING BTREE,
  CONSTRAINT `tb_akun_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori_akun` (`id_kategori`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_akun_ibfk_2` FOREIGN KEY (`tipe_akun`) REFERENCES `tb_tipe_akun` (`id_tipe`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_akun
-- ----------------------------
INSERT INTO `tb_akun` VALUES (100, 'Kas Kecil Kantor', 1, 'Debit', 'Jumlah uang yang ada pada kas kecil kantor', 10);
INSERT INTO `tb_akun` VALUES (101, 'BSM Escrow', 1, 'Debit', 'Suatu perjanjian legal dimana sebuah barang (uang) disimpan ke Bank Syariah Mandiri (agen escrow)  yang bersifat sementara sampai isi kontrak terpenuhi', 10);
INSERT INTO `tb_akun` VALUES (102, 'Bank Riau Escrow', 1, 'Debit', 'Suatu perjanjian legal dimana sebuah barang (uang) disimpan ke Bank Riau (agen escrow)  yang bersifat sementara sampai isi kontrak terpenuhi', 10);
INSERT INTO `tb_akun` VALUES (103, 'Cadangan Kas', 1, 'Debit', 'Jumlah uang kas yang tidak boleh digunakan. Digunakan sebagai jaminan likuiditas yang dihitung berdasarkan persentase dari hutang lancar. Besarnya 10% dari hutang lancar', 10);
INSERT INTO `tb_akun` VALUES (104, 'Kas Bon Karyawan', 21, 'Debit', 'Uang kas yang dipinjam pegawai untuk keperluan pribadinya yang besarnya tidak melebihi gaji karyawan tersebut.', 10);
INSERT INTO `tb_akun` VALUES (105, 'Bank Mandiri', 20, 'Debit', 'Uang kas yang ada didalam rekening koperasi di Bank Mandiri', 10);
INSERT INTO `tb_akun` VALUES (106, 'Bank Mandiri Syariah', 20, 'Debit', 'Uang kas yang ada didalam rekening koperasi di Bank Syariah Mandiri', 10);
INSERT INTO `tb_akun` VALUES (107, 'Bank Riau', 20, 'Debit', 'Uang kas yang ada didalam rekening koperasi di Bank Riau', 10);
INSERT INTO `tb_akun` VALUES (108, 'Bank BPR Dana Nusantara', 20, 'Debit', 'Uang kas yang ada didalam rekening koperasi di Bank BPR Dana Nusantara', 10);
INSERT INTO `tb_akun` VALUES (109, 'Barang Pakai Habis', 22, 'Debit', 'Barang-barang yang dimiliki perusahaan yang bersifat habis dipakai dan sifatnya penggunaannya berulang yang bertujuan untuk melengkapi kebutuhan bisnis perusahaan dengan masa manfaat kurang dari satu ', 10);
INSERT INTO `tb_akun` VALUES (110, 'Sewa Dibayar Dimuka', 23, 'Debit', 'Beban sewa yang dibayar dimuka', 10);
INSERT INTO `tb_akun` VALUES (111, 'Uang Muka Supplier', 23, 'Debit', 'Uang muka yang dibayarkan pada supplier untuk pembelian Aktiva Tetap', 10);
INSERT INTO `tb_akun` VALUES (112, 'Uang Muka Rekanan', 23, 'Debit', 'Uang muka yang dibayarkan koperasi kepada pihak ketiga sehubungan dengan adanya pekerjaan yang sedang dikerjakan oleh pihak ketiga tersebut', 10);
INSERT INTO `tb_akun` VALUES (113, 'Panjar PPH 25', 23, 'Debit', 'Uang muka pajak/ angsuran pajak PPh Pasal 25 yang disetorkan setiap bulan', 10);
INSERT INTO `tb_akun` VALUES (114, 'Cadangan Kerugian Piutang KAK', 21, 'Kredit', 'Taksiran terhadap kerugian piutang yang akan menjadi beban perusahaan untuk tahun yang bersangkutan atas kredit KAK', 10);
INSERT INTO `tb_akun` VALUES (115, 'Cadangan Kerugian Piutang KAG', 21, 'Kredit', 'Taksiran terhadap kerugian piutang yang akan menjadi beban perusahaan untuk tahun yang bersangkutan atas kredit KAG', 10);
INSERT INTO `tb_akun` VALUES (116, 'Cadangan Kerugian Piutang KAE', 21, 'Kredit', 'Taksiran terhadap kerugian piutang yang akan menjadi beban perusahaan untuk tahun yang bersangkutan atas kredit KAE', 10);
INSERT INTO `tb_akun` VALUES (117, 'Cadangan Kerugian Piutang KAM', 21, 'Kredit', 'Taksiran terhadap kerugian piutang yang akan menjadi beban perusahaan untuk tahun yang bersangkutan atas kredit KAM', 10);
INSERT INTO `tb_akun` VALUES (160, 'KAE Bank Syariah Mandiri', 21, 'Debit', 'Kredit multiguna yang disalurkan koperasi kepada anggota menggunakan dana dari Bank Syariah Mandiri', 14);
INSERT INTO `tb_akun` VALUES (161, 'KAM Bank Mandiri', 21, 'Debit', 'Kredit multiguna yang disalurkan koperasi kepada anggota menggunakan dana dari Bank Mandiri', 14);
INSERT INTO `tb_akun` VALUES (162, 'KAK Koperasi', 21, 'Debit', 'Kredit terbatas multiguna yang disalurkan koperasi kepada anggota menggunakan dana dari sendiri', 14);
INSERT INTO `tb_akun` VALUES (163, 'KAB & P Koperasi', 21, 'Debit', 'Kredit terbatas multiguna yang disalurkan koperasi kepada anggota menggunakan dana dari sendiri yang peruntukannya adalah untuk biaya berobat dan pendidikan ', 14);
INSERT INTO `tb_akun` VALUES (164, 'KKLK BNI', 21, 'Debit', 'Kredit yang diberikan kepada anggota koperasi menggunakan dana pihak ketiga yaitu Bank BNI. Koperasi menggunakan dana pihak ketiga atas dan untuk nama koperasi. Koperasi melakukan analisa kredit sendi', 14);
INSERT INTO `tb_akun` VALUES (170, 'Peralatan', 25, 'Debit', 'Aktiva tetap berupa peralatan yang dimiliki oleh koperasi. Yang termasuk kedalam peralatan ini adalah pembelian peralatan dengan nominal diatas Rp. 1.000.000,-. Pada akhir periode peralatan ini akan d', 11);
INSERT INTO `tb_akun` VALUES (171, 'Bangunan', 25, 'Debit', 'Aktiva tetap berupa bangunan yang dimiliki oleh koperasi.  Pada akhir periode bangunan ini akan disusutkan menggunakan metode saldo menurun, besarnya penyusutan pada periode tersebut akan dibebankan s', 11);
INSERT INTO `tb_akun` VALUES (172, 'Tanah', 25, 'Debit', 'Aktiva tetap berupa tanah yang dimiliki oleh koperasi', 11);
INSERT INTO `tb_akun` VALUES (173, 'Kendaraan Operasional', 25, 'Debit', 'Aktiva tetap berupa kendaraan yang dimiliki oleh koperasi. Pada akhir periode kendaraan ini akan disusutkan menggunakan metode saldo menurun, besarnya penyusutan pada periode tersebut akan dibebankan ', 11);
INSERT INTO `tb_akun` VALUES (174, 'Akumulasi Penyusutan Peralatan', 26, 'Kredit', 'Jumlah beban penyusutan peralatan yang telah dibebankan menjadi biaya. Di Neraca Akumulasi Penyusutan ini ditampilkan sebagai pengurang aktiva tetap', 11);
INSERT INTO `tb_akun` VALUES (175, 'Akumulasi Penyusutan Bangunan', 26, 'Kredit', 'Jumlah beban penyusutan bangunan yang telah dibebankan menjadi biaya. Di Neraca Akumulasi Penyusutan ini ditampilkan sebagai pengurang aktiva tetap', 11);
INSERT INTO `tb_akun` VALUES (176, 'Akumulasi Penyusutan Kendaraan Operasional', 26, 'Kredit', 'Jumlah beban penyusutan kendaraan yang telah dibebankan menjadi biaya. Di Neraca Akumulasi Penyusutan ini ditampilkan sebagai pengurang aktiva tetap', 11);
INSERT INTO `tb_akun` VALUES (190, 'Piutang Pendapatan Bunga', 24, 'Debit', 'Pendapatan bunga koperasi atas transaksi penyaluran kredit dari dana pihak ketiga (Chanelling dan Executing)', 15);
INSERT INTO `tb_akun` VALUES (200, 'Selisih Lebih Angsuran', 32, 'Kredit', 'Kewajiban lancar pada anggota koperasi berupa pembulatan dari tagihan cicilan kredit anggota', 16);
INSERT INTO `tb_akun` VALUES (201, 'Hutang Cicilan BPRDN', 32, 'Kredit', 'Kewajiban lancar kepada BPR Dana Nusantara atas cicilan kreditnya. Cicilan kredit tersebut telah ditagih koperasi kepada anggota pada akhir bulan berjalan dan akan disetorkan ke BPR Dana Nusantara pad', 16);
INSERT INTO `tb_akun` VALUES (202, 'Hutang Cicilan Bank Riau', 32, 'Kredit', 'Kewajiban lancar kepada Bank Riau atas cicilan kreditnya. Cicilan kredit tersebut telah ditagih koperasi kepada anggota pada akhir bulan berjalan dan akan disetorkan ke Bank Riau pada awal bulan berik', 16);
INSERT INTO `tb_akun` VALUES (203, 'Hutang Cicilan BSM', 32, 'Kredit', 'Kewajiban lancar kepada Bank Syariah Mandiri atas cicilan kreditnya. Cicilan kredit tersebut telah ditagih koperasi kepada anggota pada akhir bulan berjalan dan akan disetorkan ke Bank Syariah Mandiri', 16);
INSERT INTO `tb_akun` VALUES (204, 'Hutang Cicilan Bank Mandiri', 32, 'Kredit', 'Kewajiban lancar kepada Bank Mandiri atas cicilan kreditnya. Cicilan kredit tersebut telah ditagih koperasi kepada anggota pada akhir bulan berjalan dan akan disetorkan ke Bank Mandiri pada awal bulan', 16);
INSERT INTO `tb_akun` VALUES (205, 'Hutang PPH 4 ayat 2', 32, 'Kredut', '', 16);
INSERT INTO `tb_akun` VALUES (206, 'Hutang PPH 21', 32, 'Kredit', 'Kewajiban lancar kepada kantor pajak atas PPH Pasal 21 yang masih terhutang. Jumlah hutang ini diketahui pada akhir periode setelah melakukan perhitungan penggajian', 16);
INSERT INTO `tb_akun` VALUES (207, 'Hutang PPH 25', 32, 'Kredit', 'Kewajiban lancar kepada kantor pajak atas PPH Pasal 25 yang masih terhutang. Jumlah hutang ini diketahui pada akhir periode setelah mengurangkan jumlah pajak PPH 25 yang seharusnya dibayar dengan panj', 16);
INSERT INTO `tb_akun` VALUES (208, 'Hutang PPH 29', 32, 'Kredit', 'Kewajiba lancar kepada kantor pajak atas PPH tahunan yang kurang dibayar', 16);
INSERT INTO `tb_akun` VALUES (209, 'Hutang Gaji', 32, 'Kredit', 'Kewajiban lancar kepada karyawan dan pengurus berupa gaji dan honor yang belum dibayar pada periode tersebut', 16);
INSERT INTO `tb_akun` VALUES (210, 'Hutang Lancar Lainnya', 32, 'Kredit', 'Kewajiban lancar kepada pihak ketiga yang tidak tergolong kedalam jenis - jenis hutang lancar yang telah dijelaskan sebelumnya yang belum dibayar pada periode tersebut', 16);
INSERT INTO `tb_akun` VALUES (211, 'Giro', 32, 'Kredit', 'Sebuah produk bank dalam rangka menghimpun dana dari pihak ketiga dan pencairannya dapat diambil sewaktu-waktu atau ditarik sampai ke batas limit yang telah ditentukan oleh pihak bank.', 17);
INSERT INTO `tb_akun` VALUES (212, 'Tabungan Berjangka / Deposito', 32, 'Kredit', 'Tabungan pihak ketiga pada koperasi dalam bentuk berjangka yang sekaligus merupakan kewajiban koperasi terhadap pihak ketiga tersebut', 17);
INSERT INTO `tb_akun` VALUES (213, 'Tabungan Harian', 32, 'Kredit', 'Tabungan pihak ketiga pada koperasi dalam bentuk harian yang sekaligus merupakan kewajiban koperasi terhadap pihak ketiga tersebut', 17);
INSERT INTO `tb_akun` VALUES (220, 'Simpanan Sukarela', 31, 'Kredit', 'Kewajiban jangka panjang kepada pengurus', 18);
INSERT INTO `tb_akun` VALUES (221, 'KI BPRDN', 31, 'Kredit', 'Kewajiban jangka panjang kepada BPR Dana Nusantara atas kredit investasi yang diberikan kepada koperasi untuk memperoleh peralatan air minum isi ulang (qimqua)', 18);
INSERT INTO `tb_akun` VALUES (222, 'Hutang Blokir Jaminan KAE', 31, 'Kredit', 'Kewajiban jangka panjang kepada anggota koperasi atas blokir dana kredit KAE yang diberikan oleh Bank Syariah Mandiri. Besarnya jumlah hutang blokir cicilan KAE ini adalah sebesar 1 (satu) bulan tagih', 18);
INSERT INTO `tb_akun` VALUES (223, 'Hutang Blokir Jaminan KAM', 31, 'Kredit', 'Kewajiban jangka panjang kepada anggota koperasi atas blokir dana kredit KAM yang diberikan oleh Bank Mandiri. Besarnya jumlah hutang blokir cicilan KAM ini adalah sebesar 1 (satu) bulan tagihan cicil', 18);
INSERT INTO `tb_akun` VALUES (224, 'KI Bank Riau', 31, 'Kredit', 'Kewajiban jangka panjang kepada Bank Riau atas kredit kepemilikan bangunan yang diberikan kepada koperasi untuk memperoleh bangunan ruko yang digunakan sebagai kantor oleh koperasi', 18);
INSERT INTO `tb_akun` VALUES (225, 'Hutang Pembiayaan BSM', 31, 'Kredit', 'Kewajiban jangka panjang kepada Bank Syariah Mandiri dalam bentuk pembiayaan kredit KAE yang disalurkan koperasi', 18);
INSERT INTO `tb_akun` VALUES (226, 'Hutang Pembiayaan Bank Mandiri', 31, 'Kredit', 'Kewajiban jangka panjang kepada Bank Mandiri dalam bentuk pembiayaan kredit KAM yang disalurkan koperasi', 18);
INSERT INTO `tb_akun` VALUES (227, 'Surat Berharga', 31, 'Kredit', 'Kewajiban jangka panjang dalam bentuk surat berharga', 18);
INSERT INTO `tb_akun` VALUES (300, 'Simpanan Pokok', 30, 'Kredit', 'Modal koperasi yang dihimpun dari anggota. Simpanan pokok hanya dibayar sekali oleh anggota pada saat mendaftar sebagai anggota. Besarnya simpanan pokok koperasi adalah sebesar Rp. 200.000,- per anggo', 19);
INSERT INTO `tb_akun` VALUES (301, 'Simpanan Wajib', 30, 'Kredit', 'Modal koperasi yang dihimpun dari anggota. Simpanan wajib dibayar setiap bulannya oleh anggota. Besarnya simpanan wajib koperasi adalah sebesar Rp. 10.000,- per bulan per anggota. Jika anggota berhent', 19);
INSERT INTO `tb_akun` VALUES (302, 'Cadangan Modal', 30, 'Kredit', 'Cadangan modal koperasi yang tidak boleh digunakan pada periode tersebut. Besarnya cadangan modal ini 20% dari keuntungan bersih periode sebelumnya', 19);
INSERT INTO `tb_akun` VALUES (303, 'SHU Laba (Rugi) Periode Sebelumnya', 30, 'Kredit', 'Komponen modal koperasi yang berupa sisa hasil usaha bersih dari periode sebelumnya. SHU Laba (Rugi) Periode Sebelumnya ini didapat dengan cara mengurangkan SHU Laba (Rugi) Periode Sebelumnya dengan p', 19);
INSERT INTO `tb_akun` VALUES (304, 'SHU Laba (Rugi) Periode Berjalan', 30, 'Kredit', ' Komponen modal koperasi yang berupa sisa hasil usaha bersih dari periode berjalan. SHU Laba (Rugi) Periode berjalan ini didapat dengan cara mengurangkan SHU Laba (Rugi) Periode berjalan dengan pajak ', 19);
INSERT INTO `tb_akun` VALUES (305, 'Pembagian SHU', 30, 'Kredit', 'Akun penampung untuk mencatat pembagian SHU pada akhir periode', 19);
INSERT INTO `tb_akun` VALUES (401, 'Pendapatan Jasa KAE-Executing', 33, 'Kredit', 'Salah satu jenis pendapatan koperasi yang berupa pendapatan jasa atas pengurusan kredit anggota dari Bank Syariah Mandiri. Termasuk juga didalamnya bagi hasil yang diberikan oleh Bank Syariah Mandiri ', 20);
INSERT INTO `tb_akun` VALUES (402, 'Pendapatan Jasa KKLK BNI-Executing', 33, 'Kredit', 'Salah satu jenis pendapatan koperasi yang berupa pendapatan jasa atas pengurusan kredit anggota dari Bank BNI. ', 20);
INSERT INTO `tb_akun` VALUES (403, 'Pendapatan Jasa KAG-Chanelling', 33, 'Kredit', 'Salah satu jenis pendapatan koperasi yang berupa pendapatan jasa atas pengurusan kredit anggota dari Bank Riau. Termasuk juga didalamnya bagi hasil yang diberikan oleh Bank Riau kepada koperasi.', 20);
INSERT INTO `tb_akun` VALUES (404, 'Pendapatan Jasa KAK-Chanelling', 33, 'Kredit', 'Salah satu jenis pendapatan koperasi yang berupa pendapatan jasa atas pengurusan kredit anggota koperasi menggunakan dana koperasi sendiri. Termasuk juga didalamnya bunga yang dibebankan kepada anggot', 20);
INSERT INTO `tb_akun` VALUES (405, 'Pendapatan Fee Payroll', 33, 'Kredit', 'Salah satu jenis pendapatan koperasi dari jasa pemotongan rekening anggota untuk membayar tagihan cicilan kredit mereka di bank. Pendapatan ini langsung menjadi beban administrasi bank karena pada pri', 20);
INSERT INTO `tb_akun` VALUES (501, 'Beban Administrasi Pembiayaan BSM', 27, 'Debit', 'Harga pokok untuk mendapatkan pembiayaan Bank Syariah Mandiri. Yang termasuk kedalam beban administrasi pembiayaan bsm ini adalah, beban asuransi dan beban administrasi yang berhubungan dengan pembiay', 21);
INSERT INTO `tb_akun` VALUES (502, 'Beban Administrasi Pembiayaan Bank Mandiri', 27, 'Debit', 'Harga pokok untuk mendapatkan pembiayaan Bank Mandiri. Yang termasuk kedalam beban administrasi pembiayaan ini adalah, beban asuransi dan beban administrasi yang berhubungan dengan pembiayaan Bank Man', 21);
INSERT INTO `tb_akun` VALUES (503, 'Beban Bunga Pembiayaan BSM', 27, 'Debit', 'Harga pokok yang dibayarkan koperasi atas bunga pembiayaan Bank Syariah Mandiri yang dibebankan kepada koperasi', 21);
INSERT INTO `tb_akun` VALUES (504, 'Beban Bunga Pembiayaan Bank Mandiri', 27, 'Debit', 'Harga pokok yang dibayarkan koperasi atas bunga pembiayaan Bank Mandiri yang dibebankan kepada koperasi', 21);
INSERT INTO `tb_akun` VALUES (600, 'Beban  Telepon/Fax/Pos', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran telepon, fax dan pos', 22);
INSERT INTO `tb_akun` VALUES (601, 'Beban Listrik dan Air', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran listrik dan air', 22);
INSERT INTO `tb_akun` VALUES (602, 'Beban Alat Tulis Kantor', 28, 'Debit', 'Beban operasi periode berjalan berupa pemakaian perlengkapan ATK', 22);
INSERT INTO `tb_akun` VALUES (603, 'Beban Pemeliharaan Peralatan & Kantor', 28, 'Debit', 'Beban operasi periode berjalan yang digunakan untuk pembayaran biaya pemeliharaan peralatan dan bangunan tetapi yang tidak memperpanjang umur aktiva sehingga tidak perlu dikapitalisir dan dapat dibeba', 22);
INSERT INTO `tb_akun` VALUES (604, 'Beban Pemeliharaan Kendaraan', 28, 'Debit', 'Beban operasi periode berjalan yang digunakan untuk pembayaran biaya pemeliharaan kendaraan operasional, tetapi yang tidak memperpanjang umur aktiva sehingga tidak perlu dikapitalisir dan dapat dibeba', 22);
INSERT INTO `tb_akun` VALUES (605, 'Beban Pembinaan Anggota', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran keperluan administrasi kantor, termasuk juga kedalamnya adalah beban pembinaan anggota dan sosial', 22);
INSERT INTO `tb_akun` VALUES (606, 'Beban Pajak & Retribusi', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran keperluan pajak dan retribusi. Yang termasuk kedalam beban pajak dan retribusi ini adalah PBB, parkir dll', 22);
INSERT INTO `tb_akun` VALUES (607, 'Beban Sewa Kantor', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran sewa kantor', 22);
INSERT INTO `tb_akun` VALUES (608, 'Beban Perizinan ', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran perizinan', 22);
INSERT INTO `tb_akun` VALUES (609, 'Beban Iklan', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran iklan', 22);
INSERT INTO `tb_akun` VALUES (610, 'Beban Gaji Dan Honor', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran gaji dan honor', 22);
INSERT INTO `tb_akun` VALUES (611, 'Bonus Karyawan', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran bonus karyawan', 22);
INSERT INTO `tb_akun` VALUES (612, 'Beban Konsumsi', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran konsumsi', 22);
INSERT INTO `tb_akun` VALUES (613, 'Beban Perjalanan Dinas', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran perjalanan dinas', 22);
INSERT INTO `tb_akun` VALUES (614, 'Beban pengobatan ', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran beban pengobatan', 22);
INSERT INTO `tb_akun` VALUES (615, 'Beban Penyusutan ', 28, 'Debit', 'Beban operasi periode berjalan berupa penyusutan aktiva tetap yang tidak tergolong kedalam harga pokok jasa', 22);
INSERT INTO `tb_akun` VALUES (616, 'Beban Amortisasi', 28, 'Debit', 'Beban operasi periode berjalan berupa amortisasi aktiva lain - lain yang tidak tergolong kedalam harga pokok jasa', 22);
INSERT INTO `tb_akun` VALUES (617, 'Beban Fee Konsultan', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran beban fee konsultan', 22);
INSERT INTO `tb_akun` VALUES (618, 'Beban Asuransi', 28, 'Debit', 'Beban operasi periode berjalan berupa pembayaran beban asuransi', 22);
INSERT INTO `tb_akun` VALUES (619, 'Beban Piutang Tidak Tertagih KAK', 28, 'Debit', 'Beban operasi periode berjalan berupa persentase dari piutang KAK yang dapat dibebankan sebagai piutang tidak tertagih', 22);
INSERT INTO `tb_akun` VALUES (620, 'Beban Piutang Tidak Tertagih KAG', 28, 'Debit', 'Beban operasi periode berjalan berupa persentase dari piutang KAG yang dapat dibebankan sebagai piutang tidak tertagih', 22);
INSERT INTO `tb_akun` VALUES (621, 'Beban Piutang Tidak Tertagih KAE', 28, 'Debit', 'Beban operasi periode berjalan berupa persentase dari piutang KAE yang dapat dibebankan sebagai piutang tidak tertagih', 22);
INSERT INTO `tb_akun` VALUES (622, 'Beban Piutang Tidak Tertagih KAM', 28, 'Debit', 'Beban operasi periode berjalan berupa persentase dari piutang KAM yang dapat dibebankan sebagai piutang tidak tertagih', 22);
INSERT INTO `tb_akun` VALUES (623, 'Beban PPh  Badan', 28, 'Debit', 'Beban pajak penghasilan koperasi setiap tahun', 22);
INSERT INTO `tb_akun` VALUES (700, 'Beban Administrasi Bank', 29, 'Debit', 'Beban lain - lain periode berjalan berupa beban administrasi giro, termasuk juga didalamnya beban fee payroll', 23);
INSERT INTO `tb_akun` VALUES (701, 'Rugi Penjualan Aktiva', 29, 'Debit', 'Beban lain - lain periode berjalan jika terjadi kerugian atas penjualan aktiva tetap koperasi', 23);
INSERT INTO `tb_akun` VALUES (800, 'Laba Penjualan Aktiva', 34, 'Kredit', 'Pendapatan lain - lain periode berjalan jika terjadi laba atas penjualan aktiva tetap koperasi', 23);
INSERT INTO `tb_akun` VALUES (801, 'Pendapatan Bunga Bank', 34, 'Kredit', 'Pendapatan lain-lain pada periode berjalan atas bunga rekening giro', 23);
INSERT INTO `tb_akun` VALUES (802, 'Pendapatan Denda Chanelling', 34, 'Kredit', 'Pendapatan denda yang diperoleh atas keterlambatan angsuran kredit yang anggota', 23);
INSERT INTO `tb_akun` VALUES (803, 'Pendapatan Denda Executing', 34, 'Kredit', 'Pendapatan denda yang diperoleh atas keterlambatan angsuran kredit yang anggota', 23);
INSERT INTO `tb_akun` VALUES (9999, 'test', 24, 'Debit', 'Debit', 16);

-- ----------------------------
-- Table structure for tb_debitor
-- ----------------------------
DROP TABLE IF EXISTS `tb_debitor`;
CREATE TABLE `tb_debitor`  (
  `id_debitor` int(11) NOT NULL AUTO_INCREMENT,
  `id_kredit_salur` int(11) NOT NULL,
  `no_id_debitor` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah_kredit` bigint(20) NOT NULL,
  `keterangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jangka_waktu` int(11) NOT NULL,
  `maksimum_angsuran` bigint(20) NOT NULL,
  `tujuan_penggunaan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `penjelasan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_penyaluran` date NOT NULL,
  `tanggal_jatuh_tempo` date NOT NULL,
  `term` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `periode_cicilan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `banyak_cicilan` int(11) NOT NULL,
  `jatuh_tempo_cicilan` int(11) NOT NULL,
  `bunga` int(11) NOT NULL,
  `nominal_cicilan` int(11) NOT NULL,
  `total_lunas` int(11) NOT NULL,
  `top_up` bigint(20) NOT NULL,
  `saldo_akhir` bigint(20) NOT NULL,
  `sisa_masa_cicilan` bigint(20) NOT NULL,
  `id_tahun_buku` int(11) NOT NULL,
  PRIMARY KEY (`id_debitor`) USING BTREE,
  INDEX `id_kredit_salur`(`id_kredit_salur`) USING BTREE,
  INDEX `no_id_debitor`(`no_id_debitor`) USING BTREE,
  INDEX `id_tahun_buku`(`id_tahun_buku`) USING BTREE,
  CONSTRAINT `tb_debitor_ibfk_1` FOREIGN KEY (`id_kredit_salur`) REFERENCES `tb_kredit_salur` (`id_kredit_salur`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_debitor_ibfk_2` FOREIGN KEY (`no_id_debitor`) REFERENCES `tb_debitor_pribadi` (`no_id_debitor`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_debitor_ibfk_3` FOREIGN KEY (`id_tahun_buku`) REFERENCES `tb_tahun_buku` (`id_tahun_buku`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_debitor_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `tb_debitor_pekerjaan`;
CREATE TABLE `tb_debitor_pekerjaan`  (
  `no_id_debitor` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jenis_pekerjaan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_perusahaan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bidang_usaha` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kota` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `telp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mulai_bekerja` int(11) NOT NULL,
  `jabatan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_atasan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`no_id_debitor`) USING BTREE,
  CONSTRAINT `tb_debitor_pekerjaan_ibfk_1` FOREIGN KEY (`no_id_debitor`) REFERENCES `tb_debitor_pribadi` (`no_id_debitor`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_debitor_pekerjaan_pasangan
-- ----------------------------
DROP TABLE IF EXISTS `tb_debitor_pekerjaan_pasangan`;
CREATE TABLE `tb_debitor_pekerjaan_pasangan`  (
  `no_id_debitor` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_pasangan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tempat_lahir_pasangan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_lahir_pasangan` date NOT NULL,
  `pekerjaan_pasangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_perusahaan_pasangan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bidang_usaha_pasangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mulai_bekerja_pasangan` int(11) NOT NULL,
  `alamat_pasangan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jabatan_pasangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kota_pasangan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos_pasangan` int(11) NOT NULL,
  `telp_pasangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp_pasangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`no_id_debitor`) USING BTREE,
  CONSTRAINT `tb_debitor_pekerjaan_pasangan_ibfk_1` FOREIGN KEY (`no_id_debitor`) REFERENCES `tb_debitor_pribadi` (`no_id_debitor`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_debitor_pribadi
-- ----------------------------
DROP TABLE IF EXISTS `tb_debitor_pribadi`;
CREATE TABLE `tb_debitor_pribadi`  (
  `no_id_debitor` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jenis_kelamin` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tempat_lahir` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `warga_negara` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_ibu` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `npwp` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_ktp_passport` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `agama` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_ktp` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kota_ktp` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos_ktp` int(11) NOT NULL,
  `telp_ktp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp_ktp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_sekarang` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ditempati_sejak_sekarang` int(11) NOT NULL,
  `kota_sekarang` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos_sekarang` int(11) NOT NULL,
  `telp_sekarang` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp_sekarang` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_kepemilikan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_surat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kota_surat` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pendidikan_terakhir` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_perkawinan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah_tanggungan` int(11) NOT NULL,
  `nama_keperluan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_keperluan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kota_keperluan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos_keperluan` int(11) NOT NULL,
  `telp_keperluan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp_keperluan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hubungan_keperluan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`no_id_debitor`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_jurnal
-- ----------------------------
DROP TABLE IF EXISTS `tb_jurnal`;
CREATE TABLE `tb_jurnal`  (
  `no_jurnal` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_bukti` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` date NOT NULL,
  `memo` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipe_jurnal` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_tahun_buku` int(11) NOT NULL,
  PRIMARY KEY (`no_jurnal`) USING BTREE,
  INDEX `id_tahun_buku`(`id_tahun_buku`) USING BTREE,
  CONSTRAINT `tb_jurnal_ibfk_1` FOREIGN KEY (`id_tahun_buku`) REFERENCES `tb_tahun_buku` (`id_tahun_buku`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_jurnal
-- ----------------------------
INSERT INTO `tb_jurnal` VALUES ('JKK1', 'asd', '2019-11-14', '', 'JKK', 11);
INSERT INTO `tb_jurnal` VALUES ('JKK2', 'asdx', '2019-11-27', '', 'JKK', 11);

-- ----------------------------
-- Table structure for tb_jurnal_debit_kredit
-- ----------------------------
DROP TABLE IF EXISTS `tb_jurnal_debit_kredit`;
CREATE TABLE `tb_jurnal_debit_kredit`  (
  `no_jurnal` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_akun` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `nominal` bigint(20) NOT NULL,
  `keterangan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  INDEX `no_akun`(`no_akun`) USING BTREE,
  INDEX `no_jurnal`(`no_jurnal`) USING BTREE,
  CONSTRAINT `tb_jurnal_debit_kredit_ibfk_1` FOREIGN KEY (`no_akun`) REFERENCES `tb_akun` (`no_akun`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_jurnal_debit_kredit_ibfk_2` FOREIGN KEY (`no_jurnal`) REFERENCES `tb_jurnal` (`no_jurnal`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_kategori_akun
-- ----------------------------
DROP TABLE IF EXISTS `tb_kategori_akun`;
CREATE TABLE `tb_kategori_akun`  (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_kategori_akun
-- ----------------------------
INSERT INTO `tb_kategori_akun` VALUES (10, 'AKTIVA LANCAR');
INSERT INTO `tb_kategori_akun` VALUES (11, 'AKTIVA TETAP');
INSERT INTO `tb_kategori_akun` VALUES (14, 'KREDIT YANG DISALURKAN');
INSERT INTO `tb_kategori_akun` VALUES (15, 'RUPA-RUPA');
INSERT INTO `tb_kategori_akun` VALUES (16, 'KEWAJIBAN-LANCAR');
INSERT INTO `tb_kategori_akun` VALUES (17, 'TABUNGAN');
INSERT INTO `tb_kategori_akun` VALUES (18, 'KEWAJIBAN JANGKA PANJANG');
INSERT INTO `tb_kategori_akun` VALUES (19, 'EKUITAS');
INSERT INTO `tb_kategori_akun` VALUES (20, 'PENDAPATAN');
INSERT INTO `tb_kategori_akun` VALUES (21, 'BEBAN OPERASIONAL');
INSERT INTO `tb_kategori_akun` VALUES (22, 'BEBAN UMUM DAN ADMINISTRASI');
INSERT INTO `tb_kategori_akun` VALUES (23, 'PENDAPATAN DAN BEBAN LAIN-LAIN');

-- ----------------------------
-- Table structure for tb_kredit_salur
-- ----------------------------
DROP TABLE IF EXISTS `tb_kredit_salur`;
CREATE TABLE `tb_kredit_salur`  (
  `no_akun` int(11) NOT NULL,
  `jenis_kredit` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `saldo` bigint(20) NOT NULL,
  `id_kredit_salur` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_kredit_salur`) USING BTREE,
  INDEX `no_akun`(`no_akun`) USING BTREE,
  CONSTRAINT `tb_kredit_salur_ibfk_1` FOREIGN KEY (`no_akun`) REFERENCES `tb_akun` (`no_akun`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_kredit_salur_ibfk_2` FOREIGN KEY (`no_akun`) REFERENCES `tb_akun` (`no_akun`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_kreditor
-- ----------------------------
DROP TABLE IF EXISTS `tb_kreditor`;
CREATE TABLE `tb_kreditor`  (
  `no_akun` int(11) NOT NULL,
  `nama_bank` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `cabang_bank` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telepon` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_hp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fax` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kelurahan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kecamatan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kota` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `tanggal_peminjaman` date NOT NULL,
  `tanggal_jatuh_tempo` date NOT NULL,
  `term` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `periode_cicilan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `banyak_cicilan` int(11) NOT NULL,
  `jatuh_tempo_cicilan` int(11) NOT NULL,
  `nominal_pinjaman` bigint(20) NOT NULL,
  `bunga` int(11) NOT NULL,
  `nominal_cicilan` bigint(20) NOT NULL,
  `total_lunas` bigint(20) NOT NULL,
  `top_up` bigint(20) NOT NULL,
  `saldo_akhir` bigint(20) NOT NULL,
  `sisa_masa_cicilan` int(11) NOT NULL,
  `id_kreditor` int(11) NOT NULL AUTO_INCREMENT,
  `id_tahun_buku` int(11) NOT NULL,
  PRIMARY KEY (`id_kreditor`) USING BTREE,
  INDEX `no_akun`(`no_akun`) USING BTREE,
  INDEX `id_tahun_buku`(`id_tahun_buku`) USING BTREE,
  CONSTRAINT `tb_kreditor_ibfk_1` FOREIGN KEY (`no_akun`) REFERENCES `tb_akun` (`no_akun`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_kreditor_ibfk_2` FOREIGN KEY (`id_tahun_buku`) REFERENCES `tb_tahun_buku` (`id_tahun_buku`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_saldo_awal
-- ----------------------------
DROP TABLE IF EXISTS `tb_saldo_awal`;
CREATE TABLE `tb_saldo_awal`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tahun_buku` int(11) NOT NULL,
  `no_akun` int(11) NOT NULL,
  `saldo` bigint(20) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_tahun_buku`(`id_tahun_buku`) USING BTREE,
  INDEX `no_akun`(`no_akun`) USING BTREE,
  CONSTRAINT `tb_saldo_awal_ibfk_1` FOREIGN KEY (`id_tahun_buku`) REFERENCES `tb_tahun_buku` (`id_tahun_buku`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_saldo_awal_ibfk_2` FOREIGN KEY (`no_akun`) REFERENCES `tb_akun` (`no_akun`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_saldo_awal
-- ----------------------------
INSERT INTO `tb_saldo_awal` VALUES (1, 11, 100, 70000);
INSERT INTO `tb_saldo_awal` VALUES (6, 11, 107, 90000);
INSERT INTO `tb_saldo_awal` VALUES (7, 11, 101, 2000000);

-- ----------------------------
-- Table structure for tb_tahun_buku
-- ----------------------------
DROP TABLE IF EXISTS `tb_tahun_buku`;
CREATE TABLE `tb_tahun_buku`  (
  `id_tahun_buku` int(11) NOT NULL AUTO_INCREMENT,
  `awal_periode` date NOT NULL,
  `akhir_periode` date NOT NULL,
  PRIMARY KEY (`id_tahun_buku`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_tahun_buku
-- ----------------------------
INSERT INTO `tb_tahun_buku` VALUES (11, '2019-10-26', '2019-11-01');

-- ----------------------------
-- Table structure for tb_tipe_akun
-- ----------------------------
DROP TABLE IF EXISTS `tb_tipe_akun`;
CREATE TABLE `tb_tipe_akun`  (
  `id_tipe` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tipe` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_tipe`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_tipe_akun
-- ----------------------------
INSERT INTO `tb_tipe_akun` VALUES (1, 'Kas');
INSERT INTO `tb_tipe_akun` VALUES (20, 'Bank');
INSERT INTO `tb_tipe_akun` VALUES (21, 'Piutang');
INSERT INTO `tb_tipe_akun` VALUES (22, 'Perlengkapan');
INSERT INTO `tb_tipe_akun` VALUES (23, 'Aktiva');
INSERT INTO `tb_tipe_akun` VALUES (24, 'Aktiva Lainya');
INSERT INTO `tb_tipe_akun` VALUES (25, 'Aktiva Tetap');
INSERT INTO `tb_tipe_akun` VALUES (26, 'Akumulasi Depresiasi Aktiva Tetap');
INSERT INTO `tb_tipe_akun` VALUES (27, 'Beban Operasional');
INSERT INTO `tb_tipe_akun` VALUES (28, 'Beban Umum dan Administrasi');
INSERT INTO `tb_tipe_akun` VALUES (29, 'Biaya Lain-Lain');
INSERT INTO `tb_tipe_akun` VALUES (30, 'Ekuitas');
INSERT INTO `tb_tipe_akun` VALUES (31, 'Kewajiban Jangka Panjang');
INSERT INTO `tb_tipe_akun` VALUES (32, 'Kewajiban Lancar');
INSERT INTO `tb_tipe_akun` VALUES (33, 'Pendapatan');
INSERT INTO `tb_tipe_akun` VALUES (34, 'Pendapatan Lain-Lain');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `no_id` int(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`no_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 125 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'Dimas Syaiful', 'admin', 'ADMIN', '$2y$10$/UnxQqM3mCkKW0m3Rcyq.eA8Pmzsk5himj9tAtlj.tBl7mFyUIAI6');
INSERT INTO `tb_user` VALUES (124, 'Pierre Davidson', '123', 'ADMIN', '$2y$10$.Yo0NSlRNtaEpZF3Sb4Uee7lzPdQhP1uGdRkd1LzkVuNjinQaUlB6');

-- ----------------------------
-- Table structure for temp_id_jurnal
-- ----------------------------
DROP TABLE IF EXISTS `temp_id_jurnal`;
CREATE TABLE `temp_id_jurnal`  (
  `ID` int(11) NULL DEFAULT NULL,
  `tipe` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of temp_id_jurnal
-- ----------------------------
INSERT INTO `temp_id_jurnal` VALUES (3, 'JKK');
INSERT INTO `temp_id_jurnal` VALUES (4, 'JKK');
INSERT INTO `temp_id_jurnal` VALUES (5, 'JKK');
INSERT INTO `temp_id_jurnal` VALUES (6, 'JKK');
INSERT INTO `temp_id_jurnal` VALUES (7, 'JKK');
INSERT INTO `temp_id_jurnal` VALUES (8, 'JKK');

-- ----------------------------
-- View structure for v_jurnal
-- ----------------------------
DROP VIEW IF EXISTS `v_jurnal`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_jurnal` AS select `j`.`no_jurnal` AS `no_jurnal`,`j`.`no_bukti` AS `no_bukti`,`j`.`tanggal` AS `tanggal`,`j`.`memo` AS `memo`,`j`.`tipe_jurnal` AS `tipe_jurnal`,`j`.`id_tahun_buku` AS `id_tahun_buku`,`d`.`row` AS `row`,`d`.`keterangan` AS `keterangan`,`d`.`no_akun` AS `no_akun`,`a`.`nama_akun` AS `nama_akun`,`d`.`nominal` AS `nominal` from ((`tb_jurnal` `j` join `tb_jurnal_debit_kredit` `d`) join `tb_akun` `a`) where `j`.`no_jurnal` = `d`.`no_jurnal` and `d`.`no_akun` = `a`.`no_akun` order by `j`.`tanggal` ;

-- ----------------------------
-- View structure for v_kreditor
-- ----------------------------
DROP VIEW IF EXISTS `v_kreditor`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_kreditor` AS select `k`.`no_akun` AS `no_akun`,`k`.`nama_bank` AS `nama_bank`,`k`.`cabang_bank` AS `cabang_bank`,`k`.`no_telepon` AS `no_telepon`,`k`.`no_hp` AS `no_hp`,`k`.`email` AS `email`,`k`.`fax` AS `fax`,`k`.`kelurahan` AS `kelurahan`,`k`.`kecamatan` AS `kecamatan`,`k`.`kota` AS `kota`,`k`.`kode_pos` AS `kode_pos`,`k`.`tanggal_peminjaman` AS `tanggal_peminjaman`,`k`.`tanggal_jatuh_tempo` AS `tanggal_jatuh_tempo`,`k`.`term` AS `term`,`k`.`periode_cicilan` AS `periode_cicilan`,`k`.`banyak_cicilan` AS `banyak_cicilan`,`k`.`jatuh_tempo_cicilan` AS `jatuh_tempo_cicilan`,`k`.`nominal_pinjaman` AS `nominal_pinjaman`,`k`.`bunga` AS `bunga`,`k`.`nominal_cicilan` AS `nominal_cicilan`,`k`.`total_lunas` AS `total_lunas`,`k`.`top_up` AS `top_up`,`k`.`saldo_akhir` AS `saldo_akhir`,`k`.`sisa_masa_cicilan` AS `sisa_masa_cicilan`,`k`.`id_kreditor` AS `id_kreditor`,`k`.`id_tahun_buku` AS `id_tahun_buku`,`a`.`nama_akun` AS `nama_akun` from (`tb_akun` `a` join `tb_kreditor` `k`) where `a`.`no_akun` = `k`.`no_akun` ;

-- ----------------------------
-- Function structure for get_id_jurnal
-- ----------------------------
DROP FUNCTION IF EXISTS `get_id_jurnal`;
delimiter ;;
CREATE FUNCTION `get_id_jurnal`(`tipe_jurnal_x` VARCHAR(10))
 RETURNS longtext CHARSET latin1
BEGIN
	DECLARE x VARCHAR(10);
	DECLARE y INT;
	DECLARE z INT;

	#ambil kode terakhir dari table jurnal
	SELECT max(no_jurnal) INTO x from tb_jurnal where tipe_jurnal = tipe_jurnal_x;
	IF x is null then
		SELECT sum(0) into y;
	else
		SELECT SUBSTRING(x, 4, 10000) into y;
	end if;
	
		 
	#masukan kode terakhir ke table temp_id dan kemudian select id terakhir dari temp
	#menghindari ketika lebih dari 1 user memasukkan jurnal kedalam table secara bersamaan	
	SELECT count(ID) INTO z from temp_id_jurnal where tipe = tipe_jurnal_x AND ID  = y+1;
	
	if z<1 then
		 if z is null then
				INSERT into temp_id_jurnal values(1,tipe_jurnal_x);
				return 1;
		 else
				INSERT into temp_id_jurnal values(y+1,tipe_jurnal_x);
				return y+1;
		 end if;
	   
	else
		 SELECT max(ID) INTO y from temp_id_jurnal where tipe = tipe_jurnal_x;
		 INSERT into temp_id_jurnal values(y+1,tipe_jurnal_x);
		 SELECT max(ID) INTO z from temp_id_jurnal where tipe = tipe_jurnal_x;
		 return z+1;
	end if;
	
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
