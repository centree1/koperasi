<?php 
include 'config/config.php';
$title = "Laporan Arus Kas";
include 'template/header.php';
cek_tahun_buku();
?>

<!-- Main Start Here -->
 
     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-4" style="padding-left: 100px; text-align: center;">
                          <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
                          <h4> KOPKARKIM BIDA</h4>
                        </div>
                        <div class="col-md-8" style="padding-top: 40px;">
                          <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
                      <h4 style="text-align: center;">LAPORAN ARUS KAS</h4>
                      <h4 style="text-align: center;"> PERIODE YANG BERAKHIR PADA <?= tgl_indo($_SESSION['akhir_periode']); ?></h4>
                      <!-- <h5 style="text-align: center;">Periode <?= $awal_periode; ?>  sampai <?= $akhir_periode; ?> </h5>
                       -->  </div>
                      </div>

                    <div style="margin-top: 10px; margin-bottom: 10px; border-style: solid; border-width: 1px; "></div>
                     
                    <div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
                        <table class="table table-hover  "  >
                            <tr>
                                <td width="60%"></td>
                                <td width="20%" style="font-weight: bold;">Tahun </td>
                                <td width="20%" style="font-weight: bold;">Tahun</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">ARUS KAS DARI AKTIVITAS OPERASI</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Sisa Hasil Usaha</td>
                                <td></td>
                                <td>Rp.</td>
                            </tr>
                            <tr>
                                <td>Penyesuaian sisa hasil usaha ke hasil</td>
                                <td>Rp.</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Kas yang disediakan dari akivitas operasi</td>
                                <td>Rp.</td>
                                <td></td>
                            </tr>
                                    
                            <?php
                                $id_tahun_buku = $_SESSION['tahun_buku'];
                                $query = "SELECT a.nama_akun, sum(b.saldo) as saldo from bb_akun b, tb_akun a, tb_kategori_akun k where a.no_akun = b.no_akun AND a.id_kategori = k.id_kategori AND k.id_kategori in('10') AND b.id_tahun_buku = '".$id_tahun_buku."'  group by a.no_akun";
                                $exe   = mysqli_query($koneksi,$query);
                                while($row = mysqli_fetch_object($exe)){ ?>
                                    <tr>
                                        <td><?= $row->nama_akun; ?></td>
                                        <td><?= rupiah($row->saldo,"Rp. "); ?></td>
                                        <td></td>

                                    </tr>
                            <?php } ?>
                            <tr>
                                <td>Hutang Lancar Lainnya</td>
                                <td>Rp.</td>
                                <td>Rp. (10.000,000)</td>
                            </tr>
                            <tr>
                                <td>Kas Bersih dari Aktivitas Operasi</td>
                                <td></td>
                                <td style="font-weight: bold;">Rp. 10.000,000</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>