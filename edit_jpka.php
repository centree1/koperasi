<?php
include 'config/config.php';
$title = 'Edit Jurnal Pelunasan Kredit Anggota';
include 'template/header.php';
cek_tahun_buku();
get_role_page('edit');

if(empty($_GET['id'])){
    set_notif('psnjrnal','Maaf, Jurnal tidak ditemukan','jurnal_pelunasan_kredit_anggota','danger','close');
}


@$idx       = $_GET['id'];

$query      = "select id_debitor, no_id_debitor,nama,no_jurnal,tanggal,no_bukti,jenis_kredit, urutan_debit,memo, tipe_jurnal from v_jurnal_debitor where  no_jurnal='".$idx."' GROUP BY no_jurnal";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);

if(empty($fetch)){
    set_notif('psnjrnal',$query,'jurnal_pelunasan_kredit_anggota','danger','close');
}

//get id jurnal
$id         = $idx;
?>
<!-- ============ Body content start ============= -->
<?php
    if($fetch->tipe_jurnal == "JPKA"){
        include 'edit_jpka_executing.php';
    }else{
        include 'edit_jpka_channeling.php';
    }
?>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>