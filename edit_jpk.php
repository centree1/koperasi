<?php
include 'config/config.php';
$title = 'Edit Jurnal Penyaluran Kredit';
include 'template/header.php';
cek_tahun_buku();
get_role_page('edit');

if(empty($_GET['id'])){
    set_notif('psnjrnal','Maaf, Jurnal tidak ditemukan','jurnal_penyaluran_kredit','danger','close');
}


@$idx       = $_GET['id'];

$query      = "select id_debitor, no_id_debitor,nama,no_jurnal,tanggal,no_bukti,jenis_kredit, urutan_debit,memo from v_jurnal_debitor where tipe_jurnal='JPK' AND no_jurnal='".$idx."' GROUP BY no_jurnal";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);

if(empty($fetch)){
    set_notif('psnjrnal','Maaf, Jurnal tidak ditemukan','jurnal_penyaluran_kredit','danger','close');
}

//get id jurnal
$id         = $idx;
?>
<!-- ============ Body content start ============= -->
<style type="text/css">
    .header-debit{
        background-color: #deffe7;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-kredit{
        background-color: #e3fdff;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-bank{
        background-color: #fff6b8;
        color: black;
        font-weight: bold;
        text-align: center;
    }
</style>

<form id="frmz">
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Edit Jurnal Penyaluran Kredit
                        </label-tabel>
                        <div class="float-right">
                            <a href="jurnal_penyaluran_kredit" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label >No. Jurnal</label>
                            <input required type="text" class="form-control readonly" placeholder="No. Jurnal Kas Keluar" value="<?= $id; ?>"  readonly>
                          </div>
                          <div class="form-group">
                            <label >Jenis Kredit yang disalurkan</label>

                            <select class="form-control select2" disabled="" onchange="disable_change()">
                            <option value="Executing" <?= selected_droplist('Executing', $fetch->jenis_kredit); ?>>Executing</option> 
                            <option value="Dana Sendiri" <?= selected_droplist('Dana Sendiri', $fetch->jenis_kredit); ?>>Dana Sendiri</option> 
                            </select>
                          </div>
                          <div class="form-group">
                            <label >No. Identitas Debitor</label>
                            <select class="form-control select2" disabled="" required="" onchange="disable_change()" id="no_identitas_debitor">
                            <option value="<?= $id_debitor; ?>" selected><?= $fetch->no_id_debitor; ?></option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label >Nama Debitor</label>
                            <input name="nama_debitor" type="text" class="form-control readonly" placeholder="Nama Debitor" onchange="disable_change();" readonly="" value="<?= $fetch->nama; ?>" required>
                          </div>                           
                        </div>
                        <div class="col-md-6">
                              <div class="form-group">
                                <label >Pencairan Ke</label>
                                <input name="pencairan_ke" type="text" class="form-control " placeholder="Pencairan Ke" value="<?= $fetch->urutan_debit; ?> " required>
                              </div>
                            <div class="form-group">
                                <label >Tanggal</label>
                                 <input class="form-control datetimepicker CentreeTgl" required name="tanggal" type="text" class="form-control" autocomplete="off" placeholder="Tanggal" value="<?= $fetch->tanggal; ?> " required>
                              </div>
                              <div class="form-group">
                                <label >No. Bukti Kas Keluar</label>
                                <input name="no_bukti" type="text" class="form-control" placeholder="No Bukti" value="<?= $fetch->no_bukti; ?>"  required>
                              </div>
                              <div class="form-group">
                                <label >Memo</label>
                                <input name="memo" type="text" class="form-control" placeholder="Memo" value="<?= $fetch->memo; ?> ">
                              </div>
                        </div>
                    </div>
                </div>

            <script type="text/javascript">
                function yakin(tipe,lokasi){
                    pesan_confirm("Ingin Pindah Halaman ?", "Data Akun yang belum tersimpan akan hilang", "Buka "+tipe).then((result) => {
                    //eksekusi
                    if(result===true){
                        window.location = lokasi;
                    }
                    });
                }
            </script>
            </div>
        </div>
        
    </div>
</div>


<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body table-responsive ">
                    <table class="table table-hover table-striped" border="1">
                        <thead>
                            <tr>
                                <td class="header-debit"  style="width: 2%;"></td>
                                <td colspan="2" class="header-debit" style="width: 30%;">DEBIT</td>
                                <td colspan="6" class="header-bank"  style="width: 60%;">KREDIT</td>
                            </tr>
                            <tr>
                                <td class="header-debit"  rowspan="2" style="width: 2%;">
                                    <button type="button" class="btn btn-sm btn-success" onclick="addrow()"> <i  class="fa fa-plus "></i></button>
                                </td>
                                <td class="header-debit"  rowspan="2" style="width: 15%;">Akun</td>
                                <td class="header-debit" rowspan="2" style="width: 15%;">Saldo</td>
                                <td class="header-bank" colspan="2" style="width: 30%;">BANK</td>
                                <td class="header-kredit" colspan="3" style="width: 30%;">Lainya</td>
                            </tr>
                            <tr>
                                <td class="header-bank"  style="width: 15%;"> Akun </td>
                                <td class="header-bank" style="width: 15%;"> Saldo </td>
                                <td class="header-kredit" style="width: 15%;"> Akun </td>
                                <td class="header-kredit" style="width: 15%;"> Saldo </td>
                            </tr>
                        </thead>
                        <tbody id="tbl">
<!---------------------------------------------------------- -->
<?php
    $id_identitas_debitor = explode(",",$fetch->no_id_debitor); 
    $akun = $id_identitas_debitor[0];
    //im_debugging($akun);

    //get selected debit
    $query  = "SELECT no_akun, nama_akun from tb_akun where no_akun='".$akun."'";
    $result = mysqli_query($koneksi,$query);
    $data_selected   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    //get akun tipe bank
    $query  = "SELECT no_akun, nama_akun from tb_akun where tipe_akun=20";
    $result = mysqli_query($koneksi,$query);
    $data_bank   = mysqli_fetch_all($result,MYSQLI_ASSOC);
    $no_akun_bank = array();
    foreach ($data_bank as $key) {
        array_push($no_akun_bank, $key['no_akun']);
    }

    //get akun tipe bank
    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_lainya   = mysqli_fetch_all($result,MYSQLI_ASSOC);
    //im_debugging($data_lainya);

    $query  = "SELECT * from tb_jurnal_debitor_debit_kredit where no_jurnal = '".$idx   ."' order by row ASC";
    $result           = mysqli_query($koneksi,$query);
    $id               = 1;
    $no_jurnal_temp   = null;
    $row_temp         = null;
    $array_jurnal     = array();
    $temp_array       = array();
    while($data_jurnal   = mysqli_fetch_object($result)){ 
        
        //inisialisasi
        $saldo_debit  = null;  
        $saldo_lainya = null;
        $saldo_bank   = null;

        //get per row in jurnal debit kredit
        if($data_jurnal->row !== $row_temp){
            if(!empty($temp_array)){
                array_push($array_jurnal, $temp_array);
            }

            $temp_array     = array();
            $no_jurnal_temp = $data_jurnal->no_jurnal;
            $row_temp       = $data_jurnal->row;
            $temp_array['no_jurnal'] = $no_jurnal_temp;
            $temp_array['row'] = $row_temp;
            $temp_array['saldo_debit']    = null;
            $temp_array['saldo_bank']     = null;
            $temp_array['saldo_lainya']   = null; 
            $temp_array['no_akun_lainya'] = null;
            $temp_array['no_akun_debit']  = null;
            $temp_array['no_akun_bank']   = null;
        }


        //cek apakah masuk ke debit/kredit
        if($data_jurnal->keterangan == 'Debit'){
            $temp_array['saldo_debit'] = $data_jurnal->nominal;
            $temp_array['no_akun_debit'] = $data_jurnal->no_akun;
        }elseif($data_jurnal->keterangan == 'Kredit Bank'){
            $temp_array['saldo_bank'] = $data_jurnal->nominal;
            $temp_array['no_akun_bank'] = $data_jurnal->no_akun;
        }else{
            $temp_array['saldo_lainya'] = $data_jurnal->nominal;
            $temp_array['no_akun_lainya'] = $data_jurnal->no_akun;
        }
        $id++;
    }
    array_push($array_jurnal, $temp_array);
    //im_debugging($array_jurnal);
     foreach ($array_jurnal as $rowx => $val) {
?>
<tr>
    <td  class="header-debit"  >
        <button type="button" class="btn btn-sm btn-danger" id="min-<?= $rowx; ?>" onclick="delete_row('min-<?= $rowx; ?>')"> 
            <i class="fa fa-minus "></i>
        </button>
    </td>

<!-- Debit -------------------------------------------->
<td class="header-debit" > 
    <select class="form-control select22" name="debit[<?= $rowx; ?>]">
        <option value="">--</option>
        <?php foreach ($data_selected as $key => $row) { ?>
            <option value="<?= $row['no_akun']; ?>" selected><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="header-debit">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_debit[<?= $rowx; ?>]" value="<?= $val['saldo_debit']; ?>">
    </div>
</td>
<!-- End Debit -->

<!-- Kredit Bank -------------------------------------------->
<td class="header-bank" > 
    <select class="form-control select22" name="kredit_bank[<?= $rowx; ?>]">
        <option value="">--</option>
        <?php foreach ($data_bank as $key => $row) { ?>
            <option value="<?= $row['no_akun']; ?>" <?= selected_droplist($val['no_akun_bank'],$row['no_akun']); ?>><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="header-bank">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_bank[<?= $rowx; ?>]" value="<?= $val['saldo_bank']; ?>">
    </div>
</td>
<!-- End Kredit Bank  -->

<!-- Kredit Lainya -------------------------------------------->
<td class="header-kredit" > 
    <select class="form-control select22" name="kredit_lainya[<?= $rowx; ?>]">
        <option value="">--</option>
        <?php foreach ($data_lainya as $key => $row) { ?>
            <option value="<?= $row['no_akun']; ?>" <?= selected_droplist($val['no_akun_lainya'],$row['no_akun']); ?>><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="header-kredit">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_lainya[<?= $rowx; ?>]" value="<?= $val['saldo_lainya']; ?>">
    </div>
</td>
<!-- End Kredit Lainya  -->

</tr>

<?php } ?>

<!--------------------------------------------------------- -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="form-group" style="text-align: center;">
                          <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-save fa-fw"></i> Simpan</button>
                      </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</form>
<div id="tmp"></div>
<script type="text/javascript">
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 
            var values = $(this).serialize();
            simple_ajax(values+"&no_jurnal=<?= $idx; ?>",'config/proses_edit_jpk','jurnal_penyaluran_kredit','Berhasil Edit Jurnal ');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

        //table form
        var arr = [];
        var i = "<?= $id; ?>";
        function addrow(){
            var no_debit = $("#no_identitas_debitor").val();
            var kredit_salur = $("#kredit_salur").val();
            if(no_debit==""){
                pesan_error("Oops...", "Mohon pilih identitas debitor terlebih dahulu", url="");
                return 0;
            }
            var no_debit = $("#no_identitas_debitor :selected").text();
            get_with_ajax("id="+no_debit+"&row="+i, "config/get_form_jpk", "tbl","no");
            i++;
        }

        /*function get_debitor_list(value){
            get_with_ajax("id="+value, "config/get_debitor_list", "no_identitas_debitor");
        }*/

        /*function get_debitor_data(value){
            var no_debit = $("#no_identitas_debitor :selected").text();

            $.ajax({
                type: 'POST',
                url: 'config/get_data_debitor',
                data: "id="+value+"&no_identitas_debitor="+no_debit,
                beforeSend: function(){
                  pesan_tunggu(); //ini biar nampak loading nya
                },
                success: function (param) {
                    swal.close();
                    arr = JSON.parse(param);
                    console.log(arr);
                    var cicilan_ke = parseInt(arr['jumlah_pencairan'])+1;
                    if(isNaN(cicilan_ke)){
                        cicilan_ke = "";
                    }
                    //set
                    $("#nama_debitor").val(arr['nama']);
                    $("#pencairan_ke").val(cicilan_ke);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                  pesan_error("Gagal!", errorThrown);
                }
              });
        }*/

        function delete_row(id){
            console.log("remove "+id);
            $("#"+id).closest('tr').remove()
        }

    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>