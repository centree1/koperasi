<?php
include 'config/config.php';
$title = "Tambah Tipe Akun";
include 'template/header.php';
cek_tahun_buku();

?>
<style type="text/css">
    .container-form{
        padding-top: 40px;
        color: black;
    }
    label{
        font-weight: bold;
    }
</style>
<!-- ============ Body content start ============= -->

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   <div id="carouselExampleIndicators" data-interval="false" class="carousel slide" data-ride="carousel">
                      <form id="frmz">
                      <div class="carousel-inner">
                        <!-- Referensi Data Debitor -->
                        <div class="carousel-item active dim-border">
                          <label-tabel-small> Referensi Data Debitor</label-tabel-small>
                          <br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Kredit yang Disalurkan</label>
                                        <select  id="id_kredit_salur" name="id_kredit_salur" class="form-control select2" onchange="cek_kredit_salur(this.value);">
                                            <?php 
                                            $query=mysqli_query($koneksi, "SELECT * FROM tb_kredit_salur,tb_akun WHERE tb_kredit_salur.no_akun = tb_akun.no_akun");
                                            while($row=mysqli_fetch_array($query)){ ?>
                                            <option value="<?php echo $row['id_kredit_salur']; ?>"><?php echo $row['nama_akun']." | ".$row['jenis_kredit']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                  
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>No Identitas Debitor</label>
                                    <input  readonly id="no_identitas_debitor" name="no_identitas_debitor" type="text" class="form-control" placeholder="No. Akun">
                                  </div>
                                </div>
                            </div>

                            <div class="mt-3 row">
                                <div class="col-md-6">
                                     <button class="btn btn-danger" type="button" onclick="history.back();" > <i class="fa fa-arrow-left"></i> Kembali </button>  
                                </div>
                                <div class="col-md-6 text-right">
                                    <a  class="btn btn-info" href="#carouselExampleIndicators" role="button" data-slide="next" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                                </div>
                            </div>
                        </div>

                        <!-- Data Pribadi -->
                        <div class="carousel-item  dim-border">
                            <label-tabel-small> Data Pribadi </label-tabel-small>
                            <br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama</label>
                                    <input  name="nama" type="text" class="form-control" placeholder="Nama"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <select name="jenis_kelamin" class="form-control select2"  >
                                            <option  value="Pria">Pria</option>
                                            <option value="Wanita">Wanita</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input  name="tempat_lahir" type="text" class="form-control" placeholder="Tempat Lahir"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input  name="tanggal_lahir" type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Lahir"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Warga Negara</label>
                                    <input  name="warga_negara" type="text" class="form-control" placeholder="Warga Negara"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama Ibu Kandung (Sebelum Menikah)</label>
                                    <input  name="nama_ibu" type="text" class="form-control" placeholder="Nama Ibu Kandung"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>NPWP</label>
                                    <input  name="npwp" type="text" class="form-control" placeholder="NPWP"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>No KTP/Passpor</label>
                                    <input  name="no_ktp_passpor" type="text" class="form-control" placeholder="No KTP/Passpor"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Agama</label>
                                    <input  name="agama" type="text" class="form-control" placeholder="Agama"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>E-mail</label>
                                    <input  name="email" type="text" class="form-control" placeholder="E-mail"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Rumah(Sesuai KTP/Passpor)</label>
                                    <input  name="alamat_ktp" type="text" class="form-control" placeholder="Alamat Rumah"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Sesuai KTP/Passpor)</label>
                                    <input  name="kota_ktp" type="text" class="form-control" placeholder="Kota"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Sesuai KTP/Passpor)</label>
                                    <input  name="kode_pos_ktp" type="text" class="form-control" placeholder="Kode Pos"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Sesuai KTP/Passpor)</label>
                                    <input  name="telp" type="text" class="form-control" placeholder="Telp"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Sesuai KTP/Passpor)</label>
                                    <input  name="hp" type="text" class="form-control" placeholder="HP"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Rumah(Sekarang)</label>
                                    <input  name="alamat_sekarang" type="text" class="form-control" placeholder="Alamat Rumah"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Ditempati Sejak</label>
                                    <input  name="ditempati_sejak_sekarang" type="text" class="form-control" placeholder="Ditempati Sejak"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Sekarang)</label>
                                    <input  name="kota_sekarang" type="text" class="form-control" placeholder="Kota"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Sekarang)</label>
                                    <input  name="kode_pos_sekarang" type="text" class="form-control" placeholder="Kode Pos"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Sekarang)</label>
                                    <input  name="telp_sekarang" type="text" class="form-control" placeholder="Telp"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Sekarang)</label>
                                    <input  name="hp_sekarang" type="text" class="form-control" placeholder="HP"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status Kepemilikan</label>
                                        <select  name="status_kepemilikan" class="form-control select2"  >
                                            <option  value=""></option>
                                            <option  value="Pribadi">Pribadi</option>
                                            <option value="Keluarga">Keluarga</option>
                                            <option value="Dinas">Dinas</option>
                                            <option value="Sewa">Sewa</option>
                                            <option value="Kost">Kost</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Surat</label>
                                    <input  name="alamat_surat" type="text" class="form-control" placeholder="Alamat Surat"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota</label>
                                    <input  name="kota_surat" type="text" class="form-control" placeholder="Kota"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pendidikan Terakhir</label>
                                        <select  name="pendidikan_terakhir" class="form-control select2"  >
                                            <option  value=""></option>
                                            <option  value="SMA">SMA</option>
                                            <option value="D3">D3</option>
                                            <option value="S1">S1</option>
                                            <option value=">S1">>S1</option>
                                        </select>
                                      </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status Perkawinan</label>
                                        <select  name="status_perkawinan" class="form-control select2" id="statusx" onchange="cek_status_perkawinan(this.value); "  >
                                            <option value="">---- Pilih Nikah ----</option>
                                            <option value="Belum Nikah">Belum Nikah</option>
                                            <option value="Nikah">Nikah</option>
                                            <option value="Duda/Janda">Duda/Janda</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Jumlah Tanggungan</label>
                                    <input  name="jumlah_tanggungan" type="number" class="form-control" placeholder="Jumlah Tanggunan"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama(Untuk Keperluan Mendadak)</label>
                                    <input  name="nama_keperluan" type="text" class="form-control" placeholder="Nama"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat(Untuk Keperluan Mendadak)</label>
                                    <input  name="alamat_keperluan" type="text" class="form-control" placeholder="Alamat"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Untuk Keperluan Mendadak)</label>
                                    <input  name="kota_keperluan" type="text" class="form-control" placeholder="Kota"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Untuk Keperluan Mendadak)</label>
                                    <input  name="kode_pos_keperluan" type="text" class="form-control" placeholder="Kode Pos"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Untuk Keperluan Mendadak)</label>
                                    <input  name="telp_keperluan" type="text" class="form-control" placeholder="Telp"  >
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Untuk Keperluan Mendadak)</label>
                                    <input  name="hp_keperluan" type="text" class="form-control" placeholder="HP"  >
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Hubungan(Untuk Keperluan Mendadak)</label>
                                    <input  name="hubungan_keperluan" type="text" class="form-control" placeholder="Hubungan"  >
                                  </div>
                                </div>
                            </div>
                            <div class="mt-3 row">
                                <div class="col-md-6">
                                    <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                                </div>
                                <div class="col-md-6 text-right">
                                    <a class="btn btn-info " href="#carouselExampleIndicators" id="next1" role="button" data-slide="next" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                                </div>
                            </div>
                        </div>

                        

                        <!-- Data Pekerjaan -->
                        <div class="carousel-item  dim-border">
                          <label-tabel-small> Data Pekerjaan </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jenis Pekerjaan</label>
                                <select id="jenis_pekerjaan"  name="jenis_pekerjaan" class="form-control select2" onchange="cek_pekerjaan(this.value);"  onchange="" onblur="">
                                  <option  value=""></option>
                                  <option  value="Pegawai Negeri">Pegawai Negeri</option>
                                  <option value="CPNS/Honor">CPNS/Honor</option>
                                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                                  <option value="Profesional">Profesional</option>
                                  <option value="Wiraswasta">Wiraswasta</option>
                                  <option value="Lainnya">Lainnya</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan Lainnya</label>
                                <input id="jenis_pekerjaan_lainnya" disabled="disabled" name="jenis_pekerjaan_lainnya" type="text" class="form-control" placeholder="Jenis Pekerjaan" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input  name="nama_perusahaan" type="text" class="form-control" placeholder="Nama Perusahaan" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Bidang Usaha</label>
                                <input  name="bidang_usaha" type="text" class="form-control" placeholder="Bidang Usaha" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Alamat</label>
                                <input  name="alamat_perusahaan" type="text" class="form-control" placeholder="Alamat" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kota</label>
                                <input  name="kota_perusahaan" type="text" class="form-control" placeholder="Kota" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kode Pos</label>
                                <input  name="kode_pos_perusahaan" type="text" class="form-control" placeholder="Kode Pos" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Telp</label>
                                <input  name="telp_perusahaan" type="text" class="form-control" placeholder="Telp" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>HP</label>
                                <input  name="hp_perusahaan" type="text" class="form-control" placeholder="HP" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Mulai Bekerja Sejak(Tahun)</label>
                                <input  name="mulai_bekerja" type="text" class="form-control" placeholder="Mulai Bekerja Sejak" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jabatan</label>
                                <input  name="jabatan_perusahaan" type="text" class="form-control" placeholder="Jabatan" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Atasan</label>
                                <input  name="nama_atasan" type="text" class="form-control" placeholder="Nama Atasan" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev"> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                            </div>
                            <div class="col-md-6 text-right">
                              <button type="button" class="btn btn-info"  role="button" data-slide="next" id="nextx" onclick='
                $("html, body").animate({scrollTop: 0}, 1000); ' onblur=""> Selanjutnya <i class="fa fa-arrow-right"></i> </button> 
                            </div>
                          </div>
                        </div>


                        <!-- Data Suami Istri -->
                        <div class="carousel-item  dim-border" id="carousel_suami_istri">
                          <label-tabel-small> Data Suami Istri </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama</label>
                                <input name="nama_pasangan" type="text" class="form-control" placeholder="Nama" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input name="tempat_lahir_pasangan" type="text" class="form-control" placeholder="Tempat Lahir" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input name="tanggal_lahir_pasangan" type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Lahir" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan</label>
                                <select id="jenis_pekerjaan_pasangan" name="jenis_pekerjaan_pasangan" class="form-control select2" onchange="cek_pekerjaan_pasangan(this.value);;"  onchange="" onblur="">
                                  <option  value="Pegawai Negeri">Pegawai Negeri</option>
                                  <option value="CPNS/Honor">CPNS/Honor</option>
                                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                                  <option value="Profesional">Profesional</option>
                                  <option value="Wiraswasta">Wiraswasta</option>
                                  <option value="Lainnya">Lainnya</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input name="nama_perusahaan_pasangan" type="text" class="form-control" placeholder="Nama Perusahaan" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan Lainnya</label>
                                <input id="jenis_pekerjaan_pasangan_lainnya" disabled="disabled" name="jenis_pekerjaan_pasangan_lainnya" type="text" class="form-control" placeholder="Jenis Pekerjaan" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Bidang Usaha</label>
                                <input name="bidang_usaha_pasangan" type="text" class="form-control" placeholder="Bidang Usaha" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Mulai Bekerja Sejak(Tahun)</label>
                                <input name="mulai_bekerja_pasangan" type="text" class="form-control" placeholder="Mulai Bekerja Sejak" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Alamat</label>
                                <input name="alamat_perusahaan_pasangan" type="text" class="form-control" placeholder="Alamat" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kota</label>
                                <input name="kota_perusahaan_pasangan" type="text" class="form-control" placeholder="Kota" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kode Pos</label>
                                <input name="kode_pos_perusahaan_pasangan" type="text" class="form-control" placeholder="Kode Pos" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Telp</label>
                                <input name="telp_perusahaan_pasangan" type="text" class="form-control" placeholder="Telp" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>HP</label>
                                <input name="hp_perusahaan_pasangan" type="text" class="form-control" placeholder="HP" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jabatan</label>
                                <input name="jabatan_perusahaan_pasangan" type="text" class="form-control" placeholder="Jabatan" onchange="" onblur="">
                              </div>
                            </div>
                          </div>                            
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                            </div>
                            <div class="col-md-6 text-right">
                              <a class="btn btn-info" href="#carouselExampleIndicators" role="button" id="nexty" data-slide="next" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                            </div>
                          </div>
                        </div>

                       

                        <!-- Data Pemberian Kredit -->
                        <div class="carousel-item dim-border">
                          <label-tabel-small> Data Pemberian Kredit </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jumlah Kredit yang Diajukan</label>
                                <input  name="jumlah_kredit" type="text" class="form-control CentreeRupiah" placeholder="Jumlah Kredit"  onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Keterangan</label>
                                <select id="keterangan"  name="keterangan" class="form-control select2" onchange="cek_keterangan(this.value); ;" onblur="">
                                  <option  value="Baru">Baru</option>
                                  <option value="Take Over">Take Over</option>
                                </select>
                              </div>
                            </div>
                          </div>       
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Jangka Waktu</label>
                                <input  name="jangka_waktu" type="text" class="form-control" placeholder="Jangka Waktu" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Take Over dari</label>
                                <input id="keterangan_take_over" disabled="disabled"  name="keterangan_take_over" type="text" class="form-control" placeholder="Take Over" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Maksimum Angsuran yang Diinginkan</label>
                                <input  name="maksimum_angsuran" type="text" class="form-control CentreeRupiah" placeholder="Maksimum Angsuran" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tujuan Penggunaan Kredit</label>
                                <select  id="tujuan_penggunaan" name="tujuan_penggunaan" class="form-control select2" onchange="cek_tujuan_kredit(this.value); ;" onblur="">
                                  <option value="Rehabilitasi/Renovasi Rumah">Rehabilitasi/Renovasi Rumah</option>
                                  <option value="Keperluan Rumah Tangga">Keperluan Rumah Tangga</option>
                                  <option value="Biaya Pendidikan">Biaya Pendidikan</option>
                                  <option value="Biaya Pengobatan">Biaya Pengobatan</option>
                                  <option value="Keperluan Lainnya yang bersifat konsumtif">Keperluan Lainnya yang bersifat konsumtif</option>
                                  <option value="Biaya Pernikahan/Perkawinan">Biaya Pernikahan/Perkawinan</option>
                                  <option value="KAG Mix">KAG Mix</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Penyaluran Kredit</label>
                                <input required="" autocomplete="off" name="tanggal_penyaluran" type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Penyaluran Kredit"  onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tujuan Penggunaan Lainnya</label>
                                <input id="tujuan_penggunaan_lainnya" disabled="disabled"  name="tujuan_penggunaan_lainnya" type="text" class="form-control" placeholder="Tujuan Penggunaan Lainnya"  onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Jatuh Tempo</label>
                                <input required="" autocomplete="off" name="tanggal_jatuh_tempo" type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Jatuh Tempo" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Term</label>
                                <input  name="term" type="text" class="form-control" placeholder="Term" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Periode Cicilan</label>
                                <select name="periode_cicilan" class="form-control select2">
                                    <option value="bulanan">Bulanan</option>
                                    <option value="tahunan">Tahunan</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Banyak Cicilan</label>
                                <input  name="banyak_cicilan" type="number" class="form-control" placeholder="Banyak Cicilan" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Jatuh Tempo Cicilan</label>
                                <input name="jatuh_tempo_cicilan" type="number" class="form-control" placeholder="Jatuh Tempo Cicilan" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Bunga</label>
                                <input  name="bunga" type="number" class="form-control" placeholder="Bunga" onchange="" onblur="">
                                <small>Angka diatas sudah dalam bentuk persen</small>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nominal Cicilan</label>
                                <input  name="nominal_cicilan" type="text" class="form-control CentreeRupiah" placeholder="Nominal Cicilan" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Username Anggota</label><br>
                                <input id="nama_anggota" readonly="" type="text" class="form-control" placeholder="Pilih Anggota" style="width: 60%; display: inline-block;" onchange="" onblur="" required="">
                                <button type="button" class="btn btn-primary" style="display: inline-block;" onclick="open_modal()"> <i class="fa fa-search"></i> Cari </button>
                                <input id="id_anggota" readonly="" name="anggota" type="text" class="form-control" placeholder="Pilih Aggota" style="width: 60%; display: none;" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <button class="btn btn-secondary" type="button" id="previousx" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> <i class="fa fa-arrow-left"></i> Sebelum </button> 
                            </div>
                            <div class="col-md-6 text-right">
                              <button class="btn btn-info" type="submit" id="save"> <i class="fa fa-save"></i> Simpan  </button> 
                            </div>
                          </div>
                        </div>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


<!-- Modal Tambah Akun-->
<div class="modal fade" id="exampleModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mdlhdr">Mohon Tunggu . . .</h5>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="mdlbdy"></div>    
    </div>
  </div>
</div>

<script type="text/javascript">
    function open_modal(id="",action="tmbh"){
        $("#mdlhdr").empty(); 
        $("#mdlhdr").append("Browse Karyawan"); 
        get_with_ajax('id='+id, 'get_user_anggota', "mdlbdy");
        $('#exampleModal').modal('show');
    }

        $('document').ready(function(){
          let value = $("#id_kredit_salur").val();
          ajax_debitor(value);
        });
        function cek_kredit_salur(value){
          ajax_debitor(value);
        }
        //cek status kawin
        var status = "false"; //if true = sudah nikah
        function cek_status_perkawinan(value){
            if(value=="Nikah"){
               status = "true";
               console.log("nikah ="+status);
            }else{
               status = "false";
               console.log("nikah ="+status);
            }
        }

        function cek_pekerjaan(value){
          if(value=="Lainnya"){
            document.getElementById("jenis_pekerjaan_lainnya").disabled = "";
          }else{
            document.getElementById("jenis_pekerjaan_lainnya").disabled = "disabled";
          }
        }

        function cek_pekerjaan_pasangan(value){
          if(value=="Lainnya"){
            document.getElementById("jenis_pekerjaan_pasangan_lainnya").disabled = "";
          }else{
            document.getElementById("jenis_pekerjaan_pasangan_lainnya").disabled = "disabled";
          }
        }

        function cek_keterangan(value){
          if(value=="Take Over"){
            document.getElementById("keterangan_take_over").disabled = "";
          }else{
            document.getElementById("keterangan_take_over").disabled = "disabled";
          }
        }
        function cek_tujuan_kredit(value){
          if(value=="Keperluan Lainnya yang bersifat konsumtif"){
            document.getElementById("tujuan_penggunaan_lainnya").disabled = "";
          }else{
            document.getElementById("tujuan_penggunaan_lainnya").disabled = "disabled";
          }
        }

        $( document ).ready(function() {
            $("#nextx").click(function(){
                if(status=="false"){
                    console.log('false');
                     $("#carouselExampleIndicators").carousel(4); //Skip slide nikah
                }else{
                    console.log('true');
                    $("#carouselExampleIndicators").carousel("next");
                }
                $("html, body").animate({scrollTop: 0}, 1000);
          });

            $("#previousx").click(function(){
                if(status=="false"){
                    console.log('false');
                     $("#carouselExampleIndicators").carousel(2); //Skip slide nikah
                }else{
                    console.log('true');
                    $("#carouselExampleIndicators").carousel("prev");
                }
                $("html, body").animate({scrollTop: 0}, 1000);
          });
        });
         
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 
            var values = $(this).serialize();
            simple_ajax(values,'config/tambah_debitor','debitor','Data Debitor Sukses Ditambahkan','Gagal tambah Akun');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

          $("input").focus(function(){
            $(this).css("background-color", "white");
          });
    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>