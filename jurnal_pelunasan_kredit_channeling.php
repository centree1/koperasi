<?php 
include 'config/config.php';
$title = "Jurnal Pelunasan Kredit Channeling";
include 'template/header.php';
cek_tahun_buku();
?>

<!-- Main Start Here -->
    <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class=" row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center">
                             <h5 style="display: inline-block;"> <i class="fa fa-credit-card-alt fa-fw"></i>  Jurnal Pelunasan Kredit Channeling   </h5>
                        </div> 
                        <div class="col-md-6 col-xs-12 dim-center text-right">
                            <button type="button" onclick="toggle_filter()" class="btn btn-primary" ><i class="fa fa-filter fa-fw"></i> Filter </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        get_notif('psnjrnal');
    ?>

    <!-- Filter ------------------------------------------------------------>
    <div class="row" id="filter" style="display: none;">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <form method="POST" id="frmz"> 
                <div class="card-body">
                    <div class="row mb-4" >
                        <div class="col-md-6 col-6 ">
                             <h4 style="display: inline-block;"> <i class="fa fa-filter fa-fw"></i> Filter </h4>
                        </div> 
                        <div  class="col-md-6 col-6 text-right">
                            <button type="button" onclick="toggle_filter()" class="btn btn-secondary"><i class="fa fa-close fa-fw"></i> </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Filter Berdasarkan</label>
                                <select id="tipe" class="form-control select2" required="" name="tipe" >
                                    <option value=""> -- Pilih --</option>
                                    <option value="Periode"> Periode </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Dari Tanggal</label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Dari Tanggal" required="" id="tglAwal" name="dari_tgl" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Sampai Tanggal</label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Sampai Tanggal" required="" id="tglAkhir" name="sampe" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label></label>
                                <button type="submit" class="btn btn-primary btn-block" > <i class="fa fa-filter fa-fw"></i> Filter </button>
                                <button type="button" onclick="load_table()" class="btn btn-warning btn-block" ><i class="fa fa-refresh fa-spin fa-fw"></i> Reset </button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Filter -------------------------------------------------------->

     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    
                </div>
                    
                </div>
            </div>
        </div>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript">
     $('#frmz').submit(function(event) { 
        event.preventDefault(); //Prevent the default submit
        var values = $(this).serialize();
        console.log(values);
        get_with_ajax(values+"&channeling=true", "config/get_table_jpka", "tbl");
        return false; //stop
    });

    var selected_row = null;

    function toggle_filter(){
        $("#filter").fadeToggle();
    }
    function filterMe(){
        var tgl1 = $("#tglAwal").val();
        var tgl2 = $("#tglAkhir").val();
        var tipe = $("#tipe").val();
        load_table(tglAwal,tglAkhir);
    }
    load_table();
    function load_table(tipe="",tglAwal="",tglAkhir=""){
            get_with_ajax("dari_tgl="+tglAwal+"&sampe="+tglAkhir+"&channeling=true", "config/get_table_jpka", "tbl");
    }


</script>

<?php include 'template/footer.php'; ?>