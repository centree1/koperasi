<?php
  include 'config/config.php';
  $nama = NULL;
  $id = NULL;
  $action = "tambah";
  if(!empty($_POST['id'])){
      $query = mysqli_query($koneksi,'select * from tb_tipe_akun where id_tipe="'.$_POST['id'].'"');
      $row = mysqli_fetch_object($query);
      $id = $row->id_tipe;
      $nama = $row->nama_tipe;
      $action = "edit";
?>
    <div class="form-group">
        <label >ID Tipe</label>
        <input name="id" type="text" class="form-control" value="<?= $id; ?>" placeholder="ID Tipe Akun" readonly>
    </div>
<?php } ?>
    
<form id="frmzx">
    <div class="form-group">
      <label >Nama Tipe</label>
      <input name="tipe_akun" type="text" class="form-control" placeholder="Nama Tipe Akun" value="<?= $nama; ?>">
    </div>

  <div class="form-group" style="text-align: center;">
        <button type="submit" class="btn btn-success">Simpan</button>
  </div>
</form>

<script type="text/javascript">
//Tambah Karyawan
  $('#frmzx').submit(function(event) { 
    event.preventDefault(); //disable refresh when submit
    var action = '<?= $action; ?>';
    if(action=='edit'){
      var url = 'config/edit_tipe_akun';
    }else{
      var url = 'config/tambah_tipe_akun';
    }
    var values = $(this).serialize()+"&id=<?= $id; ?>";
    simple_ajax(values,url,'','','Gagal '+action+' Tipe Akun');
    return false; 
});
</script>