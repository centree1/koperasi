<?php
include 'config/config.php';
$title = "Tambah Anggota";
include 'template/header.php';

?>

<!-- ============ Body content start ============= -->

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <h4 style="display: inline-block;"> <i class="fa fa-plus fa-fw"></i> Tambah Anggota </h4>
                        <div class="float-right">
                            <button onclick="window.location = 'user'" class="btn btn-secondary btn">
                                    <i class="fa fa-arrow-left"></i> Kembali 
                            </button>
                        </div>
                        <br>
                    </div> 
                    <form id="frm_tambah">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama" required="" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" required="" autocomplete="off">
                            </div> 
                             <div class="form-group">
                            <label>Password</label>
                            <input type="password" id="pwd1" class="form-control" name="password" placeholder="Password" required="" onchange="cek_pwd();">
                            </div>

                            <div class="form-group">
                            <label>Retype Password</label>
                            <input type="password" id="pwd2" class="form-control" name="password2" placeholder="Retype Password" required="" onchange="cek_pwd();">
                            </div>

                            <div id="notif"></div>
                        <center>
                        <button type="submit" class="btn btn-success btn-lg" id="btn"> <i class="fa fa-save"></i> Simpan</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    //Tambah Data User
    $('#frm_tambah').submit(function(event) { 
        event.preventDefault();
        var values = $(this).serialize();
	    simple_ajax(values+'&role=ANGGOTA','config/tambah_user','anggota','Berhasil menambah Anggota baru');
        return false; //stop
    });
</script>
<script type="text/javascript">                
//jika pwd n re type tidak sama 
      function cek_pwd(){
        $("#notif").empty();
        var pwd1 = $("#pwd1").val();
        var pwd2 = $("#pwd2").val();

        if(pwd1 !== pwd2){
          var notif = '<div class="alert alert-danger"> <i class="fa fa-close"> </i> Password tidak sama </i>';
          $("#notif").append(notif);
          $("#btn").prop("disabled", true);
        }else{
          $("#notif").empty();
          $("#btn").prop("disabled", false);
        }
      }
</script>

<?php include 'template/footer.php'; ?>