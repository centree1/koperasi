<?php
include 'config/config.php';
$title = 'Tambah Jurnal Kas Keluar';
include 'template/header.php';
cek_tahun_buku();

/*$query      =  "select no_jurnal from tb_jurnal where tipe_jurnal='JKK' ORDER BY no_jurnal DESC LIMIT 1";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);
if(!empty($fetch)){
    $last_num   =  str_replace("JKK","",$fetch->no_jurnal); 
}else{
    $last_num = 0;
}

$new_num    = $last_num+1;*/

//get id jurnal
$query      =  "select get_id_jurnal('JKK') as id";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);
$id         = 'JKK'.$fetch->id;
?>
<!-- ============ Body content start ============= -->
<style type="text/css">
    .header-debit{
        background-color: #deffe7;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-kredit{
        background-color: #e3fdff;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-bank{
        background-color: #fff6b8;
        color: black;
        font-weight: bold;
        text-align: center;
    }
</style>

<form id="frmz">
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Tambah Jurnal Kas Keluar
                        </label-tabel>
                        <div class="float-right">
                            <a href="jurnal_kas_keluar" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label >No. Jurnal Kas Keluar</label>
                            <input required name="no_jurnal" type="text" class="form-control" placeholder="No. Jurnal Kas Keluar" value="<?= $id; ?>"  readonly>
                          </div>
                          <div class="form-group">
                            <label >Tanggal</label>
                            <input class="form-control datetimepicker CentreeTgl" required name="tanggal" type="text" class="form-control" autocomplete="off" placeholder="Tanggal" >
                          </div>
                          
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >No. Bukti Kas Keluar</label>
                                <input required name="no_bukti" type="text" class="form-control" placeholder="No Bukti Kas Keluar">
                              </div>
                              <div class="form-group">
                                <label >Memo</label>
                                <input name="memo" type="text" class="form-control" placeholder="Memo">
                              </div>
                        </div>
                    </div>
                </div>

            <script type="text/javascript">
                function yakin(tipe,lokasi){
                    pesan_confirm("Ingin Pindah Halaman ?", "Data Akun yang belum tersimpan akan hilang", "Buka "+tipe).then((result) => {
                    //eksekusi
                    if(result===true){
                        window.location = lokasi;
                    }
                    });
                }
            </script>
            </div>
        </div>
        
    </div>
</div>


<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body table-responsive ">
                    <table class="table table-hover table-striped" border="1">
                        <thead>
                            <tr>
                                <td class="bg-debit"  style="width: 2%;"></td>
                                <td colspan="2" class="bg-debit" style="width: 30%;">DEBIT</td>
                                <td colspan="6" class="bg-kredit"  style="width: 60%;">KREDIT</td>
                            </tr>
                            <tr>
                                <td class="bg-debit"  rowspan="2" style="width: 2%;">
                                    <button type="button" class="btn btn-sm btn-success" onclick="addrow()"> <i  class="fa fa-plus "></i></button>
                                </td>
                                <td class="bg-debit"  rowspan="2" style="width: 15%;">Akun</td>
                                <td class="bg-debit" rowspan="2" style="width: 15%;">Saldo</td>
                                <td class="bg-kredit" colspan="2" style="width: 30%;">BANK</td>
                                <td class="bg-kredit" colspan="3" style="width: 30%;">Lainya</td>
                            </tr>
                            <tr>
                                <td class="bg-kredit"  style="width: 15%;"> Akun </td>
                                <td class="bg-kredit" style="width: 15%;"> Saldo </td>
                                <td class="bg-kredit" style="width: 15%;"> Akun </td>
                                <td class="bg-kredit" style="width: 15%;"> Saldo </td>
                            </tr>
                        </thead>
                        <tbody id="tbl">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="form-group" style="text-align: center;">
                          <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-save fa-fw"></i> Simpan</button>
                      </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</form>

<script type="text/javascript">
        
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 
            var values = $(this).serialize();
            simple_ajax(values,'config/proses_tambah_jkk','jurnal_kas_keluar','Berhasil Tambah ');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

        //table form
        var i = 1;
        function addrow(){
            get_with_ajax("id="+i, "config/get_form_jkk", "tbl","no");
            i++;
        }


        function delete_row(id){
            $("#"+id).closest('tr').remove()
        }

    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>