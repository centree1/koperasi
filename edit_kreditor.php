<?php 
include 'config/config.php';
$title = "Daftar Nama Debitor";
include 'template/header.php';
cek_tahun_buku();
get_role_page('edit');

if(empty($_GET['id'])){
    set_notif('psnkreditor','Maaf, Kreditor tidak ditemukan','kreditor','danger','close');
}

$id = $_GET['id'];
$query = "select * from tb_kreditor where id_kreditor='".$id."'";
$data = mysqli_fetch_object(mysqli_query($koneksi,$query));
if(empty($data)){
    set_notif('psnkreditor','Maaf, Kreditor tidak ditemukan','kreditor','danger','close');
}
?>

<!-- Main Start Here -->
    <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class=" row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center">
                             <h5 style="display: inline-block;"> <i class="fa fa-pencil fa-fw"></i>  Edit Bank Kreditor   </h5>
                        </div> 
                        <div class="col-md-6 col-xs-12 dim-center text-right">
                             <a href="kreditor" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Filter ------------------------------------------------------------>
    <div class="row" id="phase1" >
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <form method="POST" id="frmz"> 
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <b style="color: red;">*</b><label> Akun</label>
                                <select id="no_akun" class="form-control select2" readonly disabled="">
                                    <option value="">-- Pilih Akun --</option>
                                    <?php 
                                        $sql = mysqli_query($koneksi,"Select * from tb_akun where saldo_normal='Kredit'");
                                        while($row = mysqli_fetch_object($sql)){ ?>
                                            <option value="<?= $row->no_akun ?>" <?= selected_droplist($row->no_akun,$data->no_akun); ?>> <?= $row->no_akun." ".$row->nama_akun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label>Nama Akun</label>
                                <input type="text" class="form-control" placeholder="Nama Akun" required=""  readonly="" id="nama_akun">
                            </div> -->
                            <!-- <div class="form-group">
                                <label>No. ID Kreditor</label>
                                <input type="text"  class="form-control" placeholder="ID" required="" id="id_kreditor">
                            </div> -->
                            <div class="form-group">
                                <b style="color: red;">*</b> <label>Nama Bank</label>
                                <input type="text" class="form-control" name="nama_bank" placeholder="Nama Bank" required="" value="<?= $data->nama_bank; ?>" >
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Cabang Bank</label>
                                <input type="text" class="form-control" name="cabang_bank" placeholder="Cabang Bank" required="" value="<?= $data->cabang_bank; ?>" >
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>No. Telepon</label>
                                <input type="text" class="form-control" name="no_telpon" placeholder="No. Telepon" required=""   value="<?= $data->no_telepon; ?>">
                            </div>
                            <div class="form-group">
                                <label>No. Handphone</label>
                                <input type="text" class="form-control" name="no_hp" placeholder="No. Handphone"  value="<?= $data->no_hp; ?>">
                            </div>
                            <div class="form-group">
                                <label>E-Mail</label>
                                <input type="text" class="form-control" name="email" placeholder="E-Mail" value="<?= $data->email; ?>" >
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label>Faximile</label>
                                <input type="text" class="form-control" name="fax" placeholder="Fax"   value="<?= $data->fax; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Kelurahan</label>
                                <input type="text" class="form-control" name="kelurahan" placeholder="Kelurahan" required=""  value="<?= $data->kelurahan; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Kecamatan</label>
                                <input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan" required="" value="<?= $data->kecamatan; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Kota</label>
                                <input type="text" class="form-control" name="kota" placeholder="Kota" required=""  value="<?= $data->kota; ?>">
                            </div>
                            <div class="form-group">
                                <b style="color: red;">*</b><label>Kode Pos</label>
                                <input type="number" class="form-control" name="kodepos" placeholder="Kode Pos" required="" value="<?= $data->kode_pos; ?>">
                            </div>
                            <div class="form-group text-right">
                                <br><br>
                                <button type="submit" class="btn btn-success ">  <i class="fa fa-arrow-right"></i> Selanjutnya </button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Filter -------------------------------------------------------->

     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="phase2" style="display: none;">
                    
                </div>
                    
                </div>
            </div>
        </div>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript">

     $('#frmz').submit(function(event) { 
        event.preventDefault(); //Prevent the default submit
        var values = $(this).serialize();
        get_with_ajax(values+"&no_akun=<?= $data->no_akun; ?>&id=<?= $id; ?>", "edit_kreditor_phase_2", "phase2");
        $("#phase1").fadeToggle();
        $("#phase2").fadeToggle();
        return false; //stop
    });
</script>

<?php include 'template/footer.php'; ?>