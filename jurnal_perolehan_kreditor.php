<?php 
include 'config/config.php';
$title = "Daftar Nama Debitor";
include 'template/header.php';
cek_tahun_buku();

?>

<!-- Main Start Here -->
    <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class=" row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center">
                             <h5 style="display: inline-block;"> <i class="fa fa-credit-card-alt fa-fw"></i>  Daftar Nama Bank Kreditor   </h5>
                        </div> 
                        <div class="col-md-6 col-xs-12 dim-center text-right">

                            <a href="jurnal_perolehan_kredit_bank" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    

     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    
                </div>
                    
                </div>
            </div>
        </div>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript">
     $('#frmz').submit(function(event) { 
        event.preventDefault(); //Prevent the default submit
        var values = $(this).serialize();
        console.log(values);
        get_with_ajax(values, "config/get_table_perolehan_kredit_bank", "tbl");
        return false; //stop
    });

    var selected_row = null;

    function toggle_filter(){
        $("#filter").fadeToggle();
    }
    function filterMe(){
        var tgl1 = $("#tglAwal").val();
        var tgl2 = $("#tglAkhir").val();
        var tipe = $("#tipe").val();
        load_table(tglAwal,tglAkhir);
    }
    load_table();
    function load_table(tipe="",tglAwal="",tglAkhir=""){
            get_with_ajax("tipe="+tipe+"&dari_tgl="+tglAwal+"&sampe="+tglAkhir, "config/get_table_perolehan_kreditor", "tbl");
    }


</script>

<?php include 'template/footer.php'; ?>