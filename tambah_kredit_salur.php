<?php
include 'config/config.php';
$title = "Tambah kredit Salur";
include 'template/header.php';

?>

<!-- ============ Body content start ============= -->

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <h4 style="display: inline-block;"> <i class="fa fa-plus fa-fw"></i> Tambah Kredit Yang Disalurkan </h4>
                        <div class="float-right">
                            <button onclick="window.location = 'kredit_salur'" class="btn btn-secondary btn">
                                    <i class="fa fa-arrow-left"></i> Kembali 
                            </button>
                        </div>
                        <br>
                    </div> 
                    <form id="frm_tambah">
                        <div class="form-group">
                               <b style="color: red;">*</b><label> Akun</label>
                                <select name="no_akun" id="no_akun" class="form-control select2">
                                    <option value="">-- Pilih Akun --</option>
                                    <?php 
                                        /*$sql = mysqli_query($koneksi,"Select * from tb_akun where saldo_normal='Kredit' or no_akun in(160, 161, 162, 163, 164)");*/
                                        $sql = mysqli_query($koneksi,"Select * from tb_akun");
                                        while($row = mysqli_fetch_object($sql)){ ?>
                                            <option value="<?= $row->no_akun ?>"> <?= $row->no_akun." ".$row->nama_akun ?></option>
                                    <?php } ?> 
                                </select>
                            </div>
                         
                    
                         
                            <div class="form-group">
                                <label>Jenis Kredit Salur</label>
                                <select class="select2" required="" name="kredit_salur"> 
                                    <option value=""> ---- Pilih Kredit Salur ---- </option>
                                    <option value="Executing"> Executing </option>
                                    <option value="Dana Sendiri"> Dana Sendiri  </option>
                                    <option value="Chaneling"> Chaneling  </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Saldo</label>
                                <input type="text" class="form-control CentreeRupiah" name="saldo" required="" autocomplete="off">
                            </div>
                         
                            <!--  <div class="form-group">
                            <label>Password</label>
                            <input type="password" id="pwd1" class="form-control" name="password" placeholder="Password" required="" onchange="cek_pwd();">
                            </div>

                            <div class="form-group">
                            <label>Retype Password</label>
                            <input type="password" id="pwd2" class="form-control" name="password2" placeholder="Retype Password" required="" onchange="cek_pwd();">
                            </div> -->

                            <div id="notif"></div>
                        <center>
                        <button type="submit" class="btn btn-success btn-lg" id="btn"> <i class="fa fa-save"></i> Simpan</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"> 
    //Tambah Data User
    $('#frm_tambah').submit(function(event) { 
        event.preventDefault();
        var values = $(this).serialize();
        simple_ajax(values,'config/kredit_salur_config','kredit_salur','Berhasil menambah Kredit Salur');
        return false; //stop
    });
</script>

<?php include 'template/footer.php'; ?>