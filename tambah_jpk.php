<?php
include 'config/config.php';
$title = 'Tambah Jurnal Penyaluran Kredit';
include 'template/header.php';
cek_tahun_buku();

/*$query      =  "select no_jurnal from tb_jurnal where tipe_jurnal='JKK' ORDER BY no_jurnal DESC LIMIT 1";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);
if(!empty($fetch)){
    $last_num   =  str_replace("JKK","",$fetch->no_jurnal); 
}else{
    $last_num = 0;
}

$new_num    = $last_num+1;*/

//get id jurnal
$query      =  "select get_id_jurnal('JPK') as id";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);
$id         = 'JPK'.$fetch->id;
?>
<!-- ============ Body content start ============= -->
<style type="text/css">
    .header-debit{
        background-color: #deffe7;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-kredit{
        background-color: #e3fdff;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-bank{
        background-color: #fff6b8;
        color: black;
        font-weight: bold;
        text-align: center;
    }
</style>

<form id="frmz">
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Tambah Jurnal Penyaluran Kredit
                        </label-tabel>
                        <div class="float-right">
                            <a href="jurnal_penyaluran_kredit" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label >No. Jurnal</label>
                            <input required name="no_jurnal" type="text" class="form-control readonly" placeholder="No. Jurnal Kas Keluar" value="<?= $id; ?>"  readonly>
                          </div>
                          <div class="form-group">
                            <label >Jenis Kredit yang disalurkan</label>

                            <select class="form-control select2" name="kredit_salur" id="kredit_salur" required="" onchange="get_debitor_list(this.value)">
                            <option value="">-- Pilih --</option>
                            <option value="Executing">Executing</option> 
                            <option value="Dana Sendiri">Dana Sendiri</option> 
                            </select>
                          </div>
                          <div class="form-group">
                            <label >No. Identitas Debitor</label>
                            <select class="form-control select2" name="no_identitas_debitor" required="" id="no_identitas_debitor" onchange="get_debitor_data(this.value)" required>
                            <option value="">-- Pilih --</option> 
                            </select>
                          </div>
                          <div class="form-group">
                            <label >Nama Debitor</label>
                            <input name="nama_debitor" type="text" class="form-control readonly" placeholder="Nama Debitor" readonly="" id="nama_debitor" required>
                          </div>                           
                        </div>
                        <div class="col-md-6">
                              <div class="form-group">
                                <label >Pencairan Ke</label>
                                <input name="pencairan_ke" type="text" class="form-control" placeholder="Pencairan Ke"  id="pencairan_ke" required>
                              </div>
                            <div class="form-group">
                                <label >Tanggal</label>
                                 <input class="form-control datetimepicker CentreeTgl" required name="tanggal" type="text" class="form-control" autocomplete="off" placeholder="Tanggal" required>
                              </div>
                              <div class="form-group">
                                <label >No. Bukti Kas Keluar</label>
                                <input name="no_bukti" type="text" class="form-control" placeholder="No Bukti" required>
                              </div>
                              <div class="form-group">
                                <label >Memo</label>
                                <input name="memo" type="text" class="form-control" placeholder="Memo">
                              </div>
                        </div>
                    </div>
                </div>

            <script type="text/javascript">
                function yakin(tipe,lokasi){
                    pesan_confirm("Ingin Pindah Halaman ?", "Data Akun yang belum tersimpan akan hilang", "Buka "+tipe).then((result) => {
                    //eksekusi
                    if(result===true){
                        window.location = lokasi;
                    }
                    });
                }
            </script>
            </div>
        </div>
        
    </div>
</div>


<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body table-responsive ">
                    <table class="table table-hover table-striped" border="1">
                        <thead>
                            <tr>
                                <td class="bg-debit"  style="width: 2%;"></td>
                                <td colspan="2" class="bg-debit" style="width: 30%;">DEBIT</td>
                                <td colspan="6" class="bg-kredit"  style="width: 60%;">KREDIT</td>
                            </tr>
                            <tr>
                                <td class="bg-debit"  rowspan="2" style="width: 2%;">
                                    <button type="button" class="btn btn-sm btn-success" onclick="addrow()"> <i  class="fa fa-plus "></i></button>
                                </td>
                                <td class="bg-debit"  rowspan="2" style="width: 15%;">Akun</td>
                                <td class="bg-debit" rowspan="2" style="width: 15%;">Saldo</td>
                                <td class="bg-kredit" colspan="2" style="width: 30%;">BANK</td>
                                <td class="bg-kredit" colspan="3" style="width: 30%;">Lainya</td>
                            </tr>
                            <tr>
                                <td class="bg-kredit"  style="width: 15%;"> Akun </td>
                                <td class="bg-kredit" style="width: 15%;"> Saldo </td>
                                <td class="bg-kredit" style="width: 15%;"> Akun </td>
                                <td class="bg-kredit" style="width: 15%;"> Saldo </td>
                            </tr>
                        </thead>
                        <tbody id="tbl">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="form-group" style="text-align: center;">
                          <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-save fa-fw"></i> Simpan</button>
                      </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</form>
<div id="tmp"></div>
<script type="text/javascript">
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 
            var values = $(this).serialize();
            simple_ajax(values,'config/proses_tambah_jpk','jurnal_penyaluran_kredit','Berhasil Tambah ');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

        //table form
        var arr = [];
        var i = 1;
        function addrow(){
            var no_debit = $("#no_identitas_debitor").val();
            var kredit_salur = $("#kredit_salur").val();
            if(no_debit==""){
                pesan_error("Oops...", "Mohon pilih identitas debitor terlebih dahulu", url="");
                return 0;
            }
            var no_debit = $("#no_identitas_debitor :selected").text();
            get_with_ajax("id="+no_debit+"&row="+i, "config/get_form_jpk", "tbl","no");
            i++;
        }

        function get_debitor_list(value){
            get_with_ajax("id="+value, "config/get_debitor_list", "no_identitas_debitor");
        }

        function get_debitor_data(value){
            var no_debit = $("#no_identitas_debitor :selected").text();

            $.ajax({
                type: 'POST',
                url: 'config/get_data_debitor',
                data: "id="+value+"&no_identitas_debitor="+no_debit,
                beforeSend: function(){
                  pesan_tunggu(); //ini biar nampak loading nya
                },
                success: function (param) {
                    swal.close();
                    arr = JSON.parse(param);
                    console.log(arr);
                    var cicilan_ke = parseInt(arr['jumlah_pencairan'])+1;
                    if(isNaN(cicilan_ke)){
                        cicilan_ke = "";
                    }
                    //set
                    $("#nama_debitor").val(arr['nama']);
                    $("#pencairan_ke").val(cicilan_ke);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                  pesan_error("Gagal!", errorThrown);
                }
              });
        }

        function delete_row(id){
            console.log("remove "+id);
            $("#"+id).closest('tr').remove()
        }

    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>