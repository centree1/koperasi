<?php
include 'config/config.php';
$title = "Edit Tahun Buku";
include 'template/header.php';
get_role_page('edit');
?>

<!-- ============ Body content start ============= -->

<?php
    if(empty($_GET['id'])){
        set_notif('psnbku','Maaf, Tahun buku tidak ditemukan','tahun_buku','danger','close');
    }

    $id = $_GET['id'];
    $query = "select * from tb_tahun_buku where id_tahun_buku='".$id."'";
    $data = mysqli_fetch_object(mysqli_query($koneksi,$query));
    if(empty($data)){
        set_notif('psnbku','Maaf, Tahun buku tidak ditemukan','tahun_buku','danger','close');
    }

    $newDate1 = date("d-m-Y", strtotime($data->awal_periode));
    $newDate2 = date("d-m-Y", strtotime($data->akhir_periode));
?>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Edit Tahun Buku
                        </label-tabel>
                        <div class="float-right">
                        </div>
                        <br>
                    </div>
                    <form id="frm_edit">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Awal Periode</label>
                                <input type="text" class="form-control" id="dtp1" name="dtp1" required="" autocomplete="off" value="<?= $newDate1; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Akhir Periode</label>
                            <input type="text" class="form-control" id="dtp2" name="dtp2" required="" autocomplete="off"  value="<?= $newDate2; ?>"   >
                        </div>
                        </div>
                        
                        

                        <center>
                        <a href="tahun_buku" class="btn btn-warning btn-lg" role="button" aria-pressed="true">Kembali</a>
                        <button type="submit" class="btn btn-success btn-lg">Simpan</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    //Tambah Data User
    $('#frm_edit').submit(function(event) { 
        event.preventDefault();
        var values = $(this).serialize();
	    simple_ajax(values+'&id=<?= $id; ?>','config/edit_buku','tahun_buku','Berhasil Mengubah Tahun Buku baru','Data Buku gagal Diubah');
        return false; //stop
    });
</script>

 <script type="text/javascript">
    $(function () {
        $('#dtp1').datetimepicker({
            format: 'DD-MM-YYYY',
            viewMode: 'years'
        });
        $('#dtp2').datetimepicker({
            useCurrent: false ,
            format: 'DD-MM-YYYY',
            viewMode: 'years'
        });
        $("#dtp1").on("dp.change", function (e) {
            $('#dtp2').data("DateTimePicker").minDate(e.date);
        });
        /*$("#dtp2").on("dp.change", function (e) {
            $('#dtp1').data("DateTimePicker").maxDate(e.date);
        });*/
    });
</script>
<?php include 'template/footer.php'; ?>