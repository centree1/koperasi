<?php
include 'config/config.php';
$title = "Panduan";
include 'template/header.php';
?>

<!-- ============ Body content start ============= -->
<?php
    //get_notif('psnakn');
?>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center " style="display: inline-block;">
                            <h4 style="display: inline-block;"> <i class="fa fa-building fa-fw icon"></i> Panduan Aplikasi </h4>
                        </div>
                    <?php
                        $role = $_SESSION['role'];
                        if($role == 'SYSADMIN'){ ?>
                        <div class="col-md-6 col-xs-12 dim-center text-right" style="display: inline-block;">
                            <button type="button"  class="btn btn-success" onclick="upload()"><i class="fa fa-upload fa-fw"></i> File Pdf </button>
                        </div>
                    </div> 
                    <?php }  ?>        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card r-0 b-0 shadow">
        <div class="card-body">
             <iframe src="<?= base_url(); ?>file_koperasi/panduan_aplikasi.pdf" width="100%" height="500px"> </iframe>
        </div>
    </div>
</div>
<?php if($role == 'SYSADMIN'){ ?>
<!-- ModalUpload-->
<div class="modal fade" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mdlhdr"> <i class="fa fa-upload"></i> Upload File PDF</h5>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
          <form id="frmz">
      <div class="modal-body" id="mdlbdy">
          <div class="form-group">
            <label >File Pdf</label>
            <input required="" name="pdf" type="file" class="form-control-file" >
            <small style="margin-top: 10px;"> <i style="color: red;">*</i> Mengupload file pdf akan mereplace file sebelumnya</small>
          </div>
        
      </div>  
      <div class="modal-footer text-center">
         <button class="btn btn-success">
                  <i class="fa fa-save"></i> Simpan
              </button>
      </div> 
      <input type="text" readonly="" hidden="" name="tipe" value="panduan_aplikasi">

      </form> 
    </div>
  </div>
</div>
<?php } ?>
<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    function upload(){
        $('#exampleModal').modal('show');
    }

    $('#frmz').submit(function(event) { 
        event.preventDefault(); 
        var values = new FormData(this);
        ajax_with_upload(values,'config/upload_file','','panduan','Berhasil mengupload file pdf');
        return false; //stop
    });
</script>

<?php include 'template/footer.php'; ?>