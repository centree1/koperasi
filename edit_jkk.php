<?php
include 'config/config.php';
$title = 'Tambah Jurnal Kas Keluar';
include 'template/header.php';
cek_tahun_buku();

get_role_page('edit');
if(empty($_GET['id'])){
    set_notif('psnjrnal','Maaf, Jurnal tidak ditemukan','jurnal_kas_keluar','danger','close');
}


@$idx       = $_GET['id'];

$query      = "select * from tb_jurnal where tipe_jurnal='JKK' AND no_jurnal='".$idx."' ORDER BY no_jurnal DESC LIMIT 1";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);
if(empty($fetch)){
    set_notif('psnjrnal','Maaf, Jurnal tidak ditemukan','jurnal_kas_keluar','danger','close');
}

?>
<!-- ============ Body content start ============= -->
<style type="text/css">
    .header-debit{
        background-color: #deffe7;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-kredit{
        background-color: #e3fdff;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-bank{
        background-color: #fff6b8;
        color: black;
        font-weight: bold;
        text-align: center;
    }
</style>

<form id="frmz">
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Ubah Jurnal Kas Keluar
                        </label-tabel>
                        <div class="float-right">
                            <a href="jurnal_kas_keluar" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label >No. Jurnal Kas Keluar</label>
                            <input required type="text" class="form-control" placeholder="No. Jurnal Kas Keluar"  readonly>
                          </div>
                          <div class="form-group">
                            <label >Tanggal</label>
                            <input class="form-control datetimepicker CentreeTgl" required name="tanggal" type="text" class="form-control" placeholder="Tanggal"value="<?= $fetch->tanggal; ?>" autocomplete="off">
                          </div>
                          
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >No. Bukti Kas Keluar</label>
                                <input required name="no_bukti" type="text" class="form-control" value="<?= $fetch->no_bukti; ?>" placeholder="No Bukti Kas Keluar">
                              </div>
                              <div class="form-group">
                                <label >Memo</label>
                                <input name="memo" type="text" class="form-control" value="<?= $fetch->memo; ?>" placeholder="Memo">
                              </div>
                        </div>
                    </div>
                </div>

            <script type="text/javascript">
                function yakin(tipe,lokasi){
                    pesan_confirm("Ingin Pindah Halaman ?", "Data Akun yang belum tersimpan akan hilang", "Buka "+tipe).then((result) => {
                    //eksekusi
                    if(result===true){
                        window.location = lokasi;
                    }
                    });
                }
            </script>
            </div>
        </div>
        
    </div>
</div>


<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body table-responsive ">
                    <table class="table table-hover table-striped" border="1">
                        <thead>
                            <tr>
                                <td class="header-debit"  style="width: 2%;"></td>
                                <td colspan="2" class="header-debit" style="width: 30%;">DEBIT</td>
                                <td colspan="6" class="header-bank"  style="width: 60%;">KREDIT</td>
                            </tr>
                            <tr>
                                <td class="header-debit"  rowspan="2" style="width: 2%;">
                                    <button type="button" class="btn btn-sm btn-success" onclick="addrow()"> <i  class="fa fa-plus "></i></button>
                                </td>
                                <td class="header-debit"  rowspan="2" style="width: 15%;">Akun</td>
                                <td class="header-debit" rowspan="2" style="width: 15%;">Saldo</td>
                                <td class="header-bank" colspan="2" style="width: 30%;">BANK</td>
                                <td class="header-kredit" colspan="3" style="width: 30%;">Lainya</td>
                            </tr>
                            <tr>
                                <td class="header-bank"  style="width: 15%;"> Akun </td>
                                <td class="header-bank" style="width: 15%;"> Saldo </td>
                                <td class="header-kredit" style="width: 15%;"> Akun </td>
                                <td class="header-kredit" style="width: 15%;"> Saldo </td>
                            </tr>
                        </thead>
                        <tbody id="tbl">
<?php
    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_debit   = mysqli_fetch_all($result,MYSQLI_ASSOC);


    $query  = "SELECT no_akun, nama_akun from tb_akun where tipe_akun=20";
    $result = mysqli_query($koneksi,$query);
    $data_bank    = mysqli_fetch_all($result,MYSQLI_ASSOC);
    $no_akun_bank = array();
    foreach ($data_bank as $key) {
        array_push($no_akun_bank, $key['no_akun']);
    }
    //im_debugging($no_akun_bank);

    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_kredit   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $query  = "SELECT * from tb_jurnal_debit_kredit where no_jurnal = '".$idx."' order by row ASC";
    $result           = mysqli_query($koneksi,$query);
    $id               = 1;
    $no_jurnal_temp   = null;
    $row_temp         = null;
    $array_jurnal     = array();
    $temp_array       = array();
    while($data_jurnal   = mysqli_fetch_object($result)){ 
        
        //inisialisasi
        $saldo_debit  = null;  
        $saldo_lainya = null;
        $saldo_bank   = null;

        //get per row in jurnal debit kredit
        if($data_jurnal->row !== $row_temp){
            if(!empty($temp_array)){
                array_push($array_jurnal, $temp_array);
            }

            $temp_array     = array();
            $no_jurnal_temp = $data_jurnal->no_jurnal;
            $row_temp       = $data_jurnal->row;
            $temp_array['no_jurnal'] = $no_jurnal_temp;
            $temp_array['row'] = $row_temp;
            $temp_array['saldo_debit']    = null;
            $temp_array['saldo_bank']     = null;
            $temp_array['saldo_lainya']   = null; 
            $temp_array['no_akun_lainya'] = null;
            $temp_array['no_akun_debit']  = null;
            $temp_array['no_akun_bank']   = null;
        }

        //cek apakah masuk ke debit/kredit
        if (strpos($data_jurnal->keterangan, 'Debit') !== false) {
            $temp_array['saldo_debit'] = $data_jurnal->nominal;
            $temp_array['no_akun_debit'] = $data_jurnal->no_akun;
        }else{
            if ($data_jurnal->keterangan == 'Kredit Bank') {
                $temp_array['saldo_bank'] = $data_jurnal->nominal;
                $temp_array['no_akun_bank'] = $data_jurnal->no_akun;
            }else{
                $temp_array['saldo_lainya'] = $data_jurnal->nominal;
                $temp_array['no_akun_lainya'] = $data_jurnal->no_akun;
            }
        }  
    }
    array_push($array_jurnal, $temp_array);
    //im_debugging($array_jurnal);

    foreach ($array_jurnal as $row => $val) {
        //im_debugging($val);
      ?>
    
<tr>
    <td  class="header-debit"  >
        <button type="button" class="btn btn-sm btn-danger" id="min-<?= $id; ?>" onclick="delete_row('min-<?= $id; ?>')"> 
            <i class="fa fa-minus "></i>
        </button>
    </td>

<!-- Debit -------------------------------------------->
<td class="header-debit" > 
    <select class="form-control select2" name="debit[<?= $id; ?>]">
        <option value="">--</option>
        <?php foreach ($data_debit as $key => $rowx) { ?>
            <option value="<?= $rowx['no_akun']; ?>" <?= selected_droplist($rowx['no_akun'],$val['no_akun_debit']); ?>><?= $rowx['no_akun'].' - '.$rowx['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="header-debit">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_debit[<?= $id; ?>]" value="<?= $val['saldo_debit']; ?>">
    </div>
</td>

<!-- End Debit -------------------------------------------->


<!-- Bank -------------------------------------------->
<td class="header-bank" > 
    <select class="form-control select2" name="kredit_bank[<?= $id; ?>]" id="kredit_bank_<?= $id; ?>">
        <option value="">--</option>
        <?php foreach ($data_bank as $key => $rowx) { ?>
            <option value="<?= $rowx['no_akun']; ?>" <?= selected_droplist($rowx['no_akun'],$val['no_akun_bank']); ?>><?= $rowx['no_akun'].' - '.$rowx['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="header-bank">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo"  name="saldo_kredit_bank[<?= $id; ?>]" id="saldo_kredit_bank_<?= $id; ?>" value="<?= $val['saldo_bank']; ?>">
    </div>
</td>

<!-- End Bank -------------------------------------------->

<!-- Kredit lainya -------------------------------------------->
<td class="header-kredit" > 
    <select class="form-control select2"   name="kredit_lainya[<?= $id; ?>]" id="kredit_lainya_<?= $id; ?>"   name="saldo_kredit_lainya[]" id="saldo_kredit_lainya_<?= $id; ?>">
        <option value="">--</option>
        <?php foreach ($data_kredit as $key => $rowx) { ?>
            <option value="<?= $rowx['no_akun']; ?>" <?= selected_droplist($rowx['no_akun'],$val['no_akun_lainya']); ?>><?= $rowx['no_akun'].' - '.$rowx['nama_akun']; ?></option>
        <?php } ?>
    </select>
</td>
<td class="header-kredit">
    &nbsp <br>
    <div class="input-group mb-3">
      <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_lainya[<?= $id; ?>]" value="<?= $val['saldo_lainya']; ?>">
    </div>
</td>

<!-- End Kredit Lainya -------------------------------------------->
</tr>
<?php $id++; }   ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="form-group" style="text-align: center;">
                          <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-save fa-fw"></i> Simpan</button>
                      </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</form>

<script type="text/javascript">
        
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 
            var values = $(this).serialize();
            simple_ajax(values+'&no_jurnal=<?= $fetch->no_jurnal; ?>','config/proses_edit_jkk','jurnal_kas_keluar','Berhasil Ubah Jurnal');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

        //table form
        var i = <?= $id; ?>;
        function addrow(){
            get_with_ajax("id="+i, "config/get_form_jkk", "tbl","no");
            i++;
        }


        function delete_row(id){
            $("#"+id).closest('tr').remove()
        }

    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>