<?php 
include 'config/config.php';
include 'template/header.php';
cek_tahun_buku();
?>

<!-- Main Start Here -->
	
     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class="">
                        <h2>In Develop</h2>
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
	<div class="row">
		<div class="col-md-3">
			<div class="card text-white bg-primary mb-3" style="">
			  <div class="card-body row">
			  	<div class="col-md-4">
			  		<i class="fa fa-spin fa-spinner fa-3x"></i> 
			  	</div>
			  	<div class="col-md-8 text-center">
			  		Request
			  		<h3 class="card-title">
			  			Request
			  		</h3>
			  	</div>
			    
			  </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-success mb-3" style="">
			  <div class="card-body row">
			  	<div class="col-md-4">
			  		<i class="fa fa-spin fa-refresh fa-3x"></i> 
			  	</div>
			  	<div class="col-md-8 text-center">
			  		Request
			  		<h3 class="card-title">
			  			Request
			  		</h3>
			  	</div>
			    
			  </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-warning mb-3" style="">
			  <div class="card-body row">
			  	<div class="col-md-4">
			  		<i class="fa fa-spin fa-spinner fa-3x"></i> 
			  	</div>
			  	<div class="col-md-8 text-center">
			  		Request
			  		<h3 class="card-title">
			  			 Request
			  		</h3>
			  	</div>
			    
			  </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card text-white bg-danger mb-3" style="">
			  <div class="card-body row">
			  	<div class="col-md-4">
			  		<i class="fa fa-spin fa-refresh fa-3x"></i> 
			  	</div>
			  	<div class="col-md-8 text-center">
			  		Request
			  		<h3 class="card-title">
			  			 Request
			  		</h3>
			  	</div>
			    
			  </div>
			</div>
		</div>
	</div>
    
<!-- Main End Here -->

<?php include 'template/footer.php'; ?>