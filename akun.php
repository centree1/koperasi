<?php
include 'config/config.php';
$title = "Daftar & Penjelasan Akun";
include 'template/header.php';
?>

<!-- ============ Body content start ============= -->
<?php
    get_notif('psnakn');
?>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <h4 style="display: inline-block;"> <i class="fa fa-list fa-fw"></i> Data Akun </h4>
                    </div> 
                    <div class="row">
                        <div class="col-md-12 row">
                            <div class="col-md-5" style="padding-right: 0 !important;">
                                <select name="filter" class="form-control form-control-lg select2" onchange="get_kategori(this.value);">
                                <option value=""> Semua Kategori </option>
                                <?php 
                                $query = mysqli_query($koneksi,'select * from tb_kategori_akun ORDER BY nama_kategori ASC');
                                while($row = mysqli_fetch_object($query)){?>                              
                                <option value="<?= $row->id_kategori; ?>"><?= $row->nama_kategori; ?></option>
                                <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-7 col-xs-12" style="margin-left: 0 !important;">
                                <div class="float-right">
                                    <?php if($_SESSION['role']=='ADMIN'){ ?>
                                    <a href="tambah_coa" class="btn btn-success btn-icon-split mb-3">
                                        <i class="fa fa-plus"></i> Akun </a>
                                    <a href="tipe_akun" class="btn btn-primary btn-icon-split mb-3">
                                        <i class="fa fa-list"></i> Tipe  
                                    </a>
                                    <a href="kategori_akun" class="btn btn-warning btn-icon-split mb-3">
                                        <i class="fa fa-list"></i> Kategori  
                                    </a>
                                    </a>
                                <?php } ?>
                                </div>
                            </div>
                            
                                
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   
                    <div class="container-form table-responsive" id="datax">
                      
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    function deleteUser(id){
        pesan_confirm("Apakah anda yakin?", "Data yang telah dihapus tidak dapat dikembalikan", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("no_akun="+id,'config/delete_coa','','User berhasil dihapus','User gagal dihapus');
            }
        });
    }

    get_kategori();
    function get_kategori(id=""){
        get_with_ajax('id='+id, 'get_kategori', "datax");
    }
</script>

<?php include 'template/footer.php'; ?>