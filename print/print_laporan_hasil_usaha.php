<?php 
include '../config/config.php';
 include 'print_bootstrap.php'; 
$title = "Laporan Perhitungan Hasil Usaha";

cek_tahun_buku();

//tahun buku sekarang
$tahun_buku_sekarang    = $_SESSION['tahun_buku'];
$awal_periode_sekarang  = $_SESSION['awal_periode'];
$tahun_sekarang         = date('Y', strtotime($awal_periode_sekarang));

//tahun buku kemarin
$query                  = "SELECT id_tahun_buku,awal_periode,akhir_periode from tb_tahun_buku where awal_periode < '".$awal_periode_sekarang."' order by awal_periode DESC LIMIT 1";
$exe                    = mysqli_query($koneksi,$query);
$old_book               = mysqli_fetch_object($exe);
if(empty($old_book)){
    $tahun_buku_sebelumnya  = 0; 
    $awal_periode_sebelumnya  = 0;
    $tahun_sebelumnya       = "Tahun ".date("Y", strtotime( date( "Y-m-d", strtotime( $awal_periode_sekarang ) ) . "-1 year" ) );
}else{
    $tahun_buku_sebelumnya  = $old_book->id_tahun_buku; 
    $awal_periode_sebelumnya  = $old_book->awal_periode;
    $tahun_sebelumnya       = "Tahun ".date('Y', strtotime($awal_periode_sebelumnya));
}

?>


     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    

              <style>
               table {border-collapse:collapse; table-layout:fixed;width: 400px;}
               table td {word-wrap:break-word;width: 14%;}
               </style>
               <!--  -->
               <style type="text/css">
                 .dim-memo{
                        background-color: #fff5c9 !important;
                        font-style: italic !important;
                        text-align: center !important;
                    }
                  .table-header{
                      text-align: center !important;
                      font-weight: bold;
                      color: black;
                      height: 40px !important;
                      padding-bottom: 40px !important;
                  }
               </style>
<br>
                <body>
                  <div class="row" style="padding-bottom: 20px;">
                    <div class="col-md-4" style="padding-left: 100px; text-align: center;">
                      <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
                      <h4> KOPKARKIM BIDA</h4>
                    </div>
                    <div class="col-md-8" style="padding-top: 40px;">
                      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA BP BATAM</h3>
                  <h4 style="text-align: center;">LAPORAN PERHITUNGAN HASIL USAHA</h4>
                  <h5 style="text-align: center;">UNTUK TAHUN YANG BERAKHIR <?= tgl_indo($_SESSION['akhir_periode']); ?> </h5>
                    </div>
                  </div>
                <div style="margin-top: 10px; margin-bottom: 10px; border-style: solid; border-width: 1px; "></div>


                <div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
                    <table class="table table-hover table-bordered table-sm">
                        <tbody>
                            <tr>
                                <td width="60%"></td>
                                <td width="40%" style="font-weight: bold; text-align: center;">Tahun <?= $tahun_sekarang; ?></td>

                            </tr>
                            <tr>
                                <td colspan="2" style="font-weight: bold; background-color: #f5ffb5">PENDAPATAN</td>    
                            </tr>
                            <?php
                                $id_tahun_buku = $_SESSION['tahun_buku'];
                                $total1 = 0;
                                $query = "SELECT a.nama_akun, sum(b.saldo) as saldo from bb_akun b, tb_akun a, tb_kategori_akun k where a.no_akun = b.no_akun AND a.id_kategori = k.id_kategori AND k.id_kategori in('20') AND b.id_tahun_buku = '".$id_tahun_buku."' AND a.no_akun <> '402'  group by a.no_akun";
                                $exe   = mysqli_query($koneksi,$query);
                                while($row = mysqli_fetch_object($exe)){ 
                                    $total1 = $row->saldo + $total1;
                                    ?>
                                    <tr>
                                        <td><?= $row->nama_akun; ?></td>
                                        <td style="text-align: center;"><?= rupiah($row->saldo,"Rp. "); ?></td>
                                    </tr>
                            <?php } ?>
                       
                            
                           
                            </tbody>
                            <tr><td colspan="2" class="separator"></td></tr>
                            <tr colspan="2" style="font-weight: bold; background-color: #ccffb5;">
                                <td>TOTAL PENDAPATAN</td>    
                                <td style="text-align: center; font-weight: bold;"><?php echo rupiah($total1, "Rp. ");?></td>
                   
                            </tr>
                            <tbody id="aset_tetap" style="margin-top: 100px;">
                            <tr><td colspan="2" class="separator"></td></tr>
                            <tr>
                                <td colspan="2" style="font-weight: bold; background-color: #f5ffb5">BEBAN - BEBAN</td>    
                              
                               
                            </tr>
                            <tr>
                                <td colspan="2" style="font-weight: bold; background-color: #f5ffb5">BEBAN OPERASIONAL</td>    
                                
                   
                            </tr>
                            <?php
                                $id_tahun_buku = $_SESSION['tahun_buku'];
                                $total2 = 0;
                                $query = "SELECT a.nama_akun, sum(b.saldo) as saldo from bb_akun b, tb_akun a, tb_kategori_akun k where a.no_akun = b.no_akun AND a.id_kategori = k.id_kategori AND k.id_kategori in('21') AND b.id_tahun_buku = '".$id_tahun_buku."'  group by a.no_akun";
                                $exe   = mysqli_query($koneksi,$query);
                                while($row = mysqli_fetch_object($exe)){ 
                                       $total2 = $row->saldo + $total2;

                                    ?>
                                    <tr>
                                        <td><?= $row->nama_akun; ?></td>
                                        <td style="text-align: center;"><?= rupiah($row->saldo,"Rp. "); ?></td>
                                    </tr>
                            <?php } ?>
                            
                            <tr><td colspan="2" class="separator"></td></tr>
                            <tr colspan="2" style="font-weight: bold; background-color: #ccffb5;">
                                <td style="font-weight: bold;">TOTAL BEBAN OPERASIONAL</td>    
                               <td style="text-align: center; font-weight: bold;"><?php echo rupiah($total2, "Rp. ");?></td>
                   
                            </tr>
                            </tbody>
                            <tr><td colspan="2" class="separator"></td></tr>
                           

                            <tr>
                            <td colspan="2" style="font-weight: bold; background-color: #f5ffb5">BEBAN UMUM DAN ADMINISTRASI</td> 
                   
                            </tr>
                            <?php
                                $id_tahun_buku = $_SESSION['tahun_buku'];
                                $total3 = 0;
                                $query = "SELECT a.nama_akun, sum(b.saldo) as saldo from bb_akun b, tb_akun a, tb_kategori_akun k where a.no_akun = b.no_akun AND a.id_kategori = k.id_kategori AND k.id_kategori in('22') AND b.id_tahun_buku = '".$id_tahun_buku."' AND a.no_akun <> '623'  group by a.no_akun";
                                $exe   = mysqli_query($koneksi,$query);
                                while($row = mysqli_fetch_object($exe)){ 
                                        $total3 = $row->saldo + $total3;
                                    ?>
                                    <tr>
                                        <td><?= $row->nama_akun; ?></td>
                                        <td style="text-align: center;"><?= rupiah($row->saldo,"Rp. "); ?></td>
                                    </tr>
                            <?php } ?>                    
                            <tr><td colspan="2" class="separator"></td></tr>
                            <tr colspan="2" style="font-weight: bold; background-color: #ccffb5;">
                                <td style="font-weight: bold;">TOTAL BEBAN UMUM DAN ADMINISTRASI</td>    
                                <td style="text-align: center; font-weight: bold;"><?php echo rupiah($total3, "Rp. ");?></td>
                   
                            </tr>
                            
                            <tr><td colspan="2" class="separator"></td></tr>
                            <tr colspan="2" style="font-weight: bold; background-color: #ccffb5;">
                                <?php 
                                $hasil = $total2 + $total3; 
                                ?>
                                <td style="font-weight: bold;">TOTAL BEBAN-BEBAN</td>    
                                <td style="text-align: center; font-weight: bold;"><?php echo rupiah($hasil, "Rp. ");?></td>
                   
                            </tr>

                            <tr><td colspan="2" class="separator"></td></tr>
                            <tr>
                            <td colspan="2" style="font-weight: bold; background-color: #f5ffb5">PENDAPATAN DAN BEBAN LAINNYA</td>
                            </tr>
                            <?php
                                $id_tahun_buku = $_SESSION['tahun_buku'];
                                $total4 = 0;
                                $query = "SELECT a.no_akun, a.nama_akun, sum(b.saldo) as saldo from bb_akun b, tb_akun a, tb_kategori_akun k where a.no_akun = b.no_akun AND a.id_kategori = k.id_kategori AND k.id_kategori in('23') AND b.id_tahun_buku = '".$id_tahun_buku."'  group by a.no_akun ORDER BY a.saldo_normal DESC";
                                $exe   = mysqli_query($koneksi,$query);
                                $saldo_xyz = 0;
                                while($row = mysqli_fetch_object($exe)){ 
                                    $arr = array('802','803');
                                    if(in_array($row->no_akun, $arr)){
                                        continue;
                                    }elseif(in_array($row->no_akun, array('801','800'))){
                                        $saldo_xyz = $saldo_xyz+$row->saldo;
                                    }else{
                                        $saldo_xyz = $saldo_xyz-$row->saldo;
                                    }
                                    ?>
                                    <tr>
                                        <td><?= $row->nama_akun; ?></td>
                                        <td style="text-align: center;"><?= rupiah($row->saldo,"Rp. "); ?></td>
                                    </tr>
                            <?php } ?>
                            
                           <tr><td colspan="2" class="separator"></td></tr>
                            <tr colspan="2" style="font-weight: bold; background-color: #ccffb5;">
                                <td style="font-weight: bold;">TOTAL PENDAPATAN DAN BEBAN LAINNYA</td>    
                                <td style="text-align: center; font-weight: bold;"><?php echo rupiah($saldo_xyz, "Rp. ");?></td>
                   
                            </tr>
                            
                            <tr><td colspan="2" class="separator"></td></tr>
                            <tr colspan="2" style="font-weight: bold; background-color: #ccffb5;">
                                <?php 
                                    $hasil2 = $total1 - ($hasil + $saldo_xyz); 
                                ?>
                                <td style="font-weight: bold;">SISA HASIL USAHA KOTOR</td>    
                                <td style="text-align: center; font-weight: bold;"><?php echo rupiah($hasil2, "Rp. ");?></td>
                   
                            </tr>
                            
                            <tr colspan="2" style="font-weight: bold; background-color: #ccffb5;">
                                <?php 
                                $query = "SELECT a.nama_akun, sum(b.saldo) as saldo from bb_akun b, tb_akun a, tb_kategori_akun k where a.no_akun = b.no_akun AND a.id_kategori = k.id_kategori AND k.id_kategori in('22') AND a.no_akun = '623' AND b.id_tahun_buku = '".$id_tahun_buku."'  group by a.no_akun";
                                $exe   = mysqli_query($koneksi,$query);
                                $row = mysqli_fetch_object($exe);
                                $hasil3 = 0;
                                if(!empty($row->saldo)){
                                    $hasil3 = $row->saldo;
                                } 
                                ?>
                                <td style="font-weight: bold;">BEBAN PPH BADAN</td>    
                                <td style="text-align: center; font-weight: bold;"><?php echo rupiah($hasil3, "Rp. ");?></td>
                   
                            </tr>
                                                
                            <tr colspan="2" style="font-weight: bold; background-color: #ccffb5;">
                                <?php 
                                    $hasil4 = $hasil2 - $hasil3; 
                                ?>
                                <td style="font-weight: bold;">SISA HASIL USAHA (SHU) LABA/RUGI TAHUN BERJALAN</td>    
                                <td style="text-align: center; font-weight: bold;"><?php echo rupiah($hasil4, "Rp. ");?></td>
                   
                            </tr>                         
                    </table>
                </div>

  

    <script type="text/javascript"> 
        window.print();
        setTimeout(window.close, 100);

    </script>
                    
 
<!-- ============ Body content End ============= -->

