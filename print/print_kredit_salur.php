<?php ob_start();
include '../config/config.php';
cek_tahun_buku();


    @$tglAwal = $_GET['tglAwal'];
    @$tglAkhir = $_GET['tglAkhir'];
    $tahun_buku = $_SESSION['tahun_buku'];
    $awal_periode = $_SESSION['awal_periode'];
    $akhir_periode = $_SESSION['akhir_periode'];

 ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 400px;}
   table td {word-wrap:break-word;width: 14%;}
   </style>
   <?php include 'print_bootstrap.php'; ?>
   <style type="text/css">
     .dim-memo{
            background-color: #fff5c9 !important;
            font-style: italic !important;
            text-align: center !important;
        }
      .table-header{
          text-align: center !important;
          font-weight: bold;
          color: black;
          height: 40px !important;
          padding-bottom: 40px !important;
      }
   </style>
</head>
<body  style="padding: 20px;"><br>
  <div class="row" style="padding-bottom: 20px;">
    <div class="col-md-4" style="padding-left: 100px; text-align: center;">
      <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
      <h4> KOPKARKIM BIDA</h4>
    </div>
    <div class="col-md-8" style="padding-top: 40px;">
      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">KREDIT SALUR</h4>
  <h5 style="text-align: center;">Periode <?= $awal_periode; ?>  sampai <?= $akhir_periode; ?> </h5>
    </div>
  </div>
  
<table class="table table-sm table-bordered" width="50%" style="text-align: center;">
                  <thead>
                      <tr>
                        <th>No Akun</th>
                        <th>Nama Akun</th>
                        <th>Jenis Kredit</th>
                        <th>Saldo</th>

                    </thead>
                     <?php
                                $query = 'select s.*,t.nama_akun from tb_kredit_salur s , tb_akun t where t.no_akun = s.no_akun';
                                $execute = mysqli_query($koneksi,$query);
                                while($row=mysqli_fetch_object($execute)){
                      ?>
                    <tbody>

                    <tr> 

                        <td><?= $row->no_akun; ?></td>
                        <td><?= $row->nama_akun; ?></td>
                        <td><?= $row->jenis_kredit; ?></td>
                        <td><?= (rupiah ($row ->saldo));?></td>
                       
                    <?php } ?>
                    </tr>
</table>

</body>
<script type="text/javascript">

  window.print();
 setTimeout(window.close, 100);
</script>

</html>
