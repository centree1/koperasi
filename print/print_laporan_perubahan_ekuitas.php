<?php 
include '../config/config.php';
$title = "Laporan Perubahan Ekuitas";
include 'print_bootstrap.php'; 

cek_tahun_buku();

//tahun buku sekarang
$tahun_buku_sekarang    = $_SESSION['tahun_buku'];
$awal_periode_sekarang  = $_SESSION['awal_periode'];
$tahun_sekarang         = date('Y', strtotime($awal_periode_sekarang));

//tahun buku kemarin
$query                  = "SELECT id_tahun_buku,awal_periode,akhir_periode from tb_tahun_buku where awal_periode < '".$awal_periode_sekarang."' order by awal_periode DESC LIMIT 1";
$exe                    = mysqli_query($koneksi,$query);
$old_book               = mysqli_fetch_object($exe);
if(empty($old_book)){
    $tahun_buku_sebelumnya  = 0; 
    $awal_periode_sebelumnya  = 0;
    $tahun_sebelumnya       =   date("Y", strtotime( date( "Y-m-d", strtotime( $awal_periode_sekarang ) ) . "-1 year" ) );
}else{
    $tahun_buku_sebelumnya  = $old_book->id_tahun_buku; 
    $awal_periode_sebelumnya  = $old_book->awal_periode;
    $tahun_sebelumnya       = date('Y', strtotime($awal_periode_sebelumnya));
}


?>
<style type="text/css">
    .separator{
         background-color: white; padding-top: 15px; border-right: 0px !important; border-left: 0px !important;
    }
    .kategori1{
        font-weight: bold;
    }
    .kategori2{
        font-weight:bold !important; 
        padding-left: 30px !important;
    }
    .kategori3{
        padding-left: 50px !important;
    }
</style>
<!-- Main Start Here -->
 <br>
     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-4" style="padding-left: 100px; text-align: center;">
                          <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
                          <h4> KOPKARKIM BIDA</h4>
                        </div>
                        <div class="col-md-8" style="padding-top: 40px;">
                          <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA BP BATAM</h3>
                      <h4 style="text-align: center;">LAPORAN PERUBAHAN EKUITAS</h4>
                      <h4 style="text-align: center;"> UNTUK TAHUN YANG BERAKHIR <?= tgl_indo($_SESSION['akhir_periode']); ?></h4>
                      
                      <!-- <h5 style="text-align: center;">Periode <?= $awal_periode; ?>  sampai <?= $akhir_periode; ?> </h5>
                       -->  </div>
                      </div>

                    <div style="margin-top: 10px; margin-bottom: 10px; border-style: solid; border-width: 1px; "></div>
                     
                    <div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
                <table class="table table-hover table-bordered table-sm"  >
                    <tr>  
                        <td width="25%" style="font-weight: bold;">KETERANGAN</td>
                        <td width="15%" style="font-weight: bold;">SIMPANAN POKOK</td>
                        <td width="15%" style="font-weight: bold;">SIMPANAN WAJIB</td>
                        <td width="15%" style="font-weight: bold;">CADANGAN MODAL</td>
                        <td width="15%" style="font-weight: bold;">TOTAL SHU</td>
                        <td width="15%" style="font-weight: bold;">JUMLAH</td>
                    </tr>
                    
                    <tr style="font-weight: bold; background-color: #ccffb5;">  
                        <?php
                              $query1 = mysqli_query($koneksi, "SELECT SUM(saldo) as saldo FROM bb_akun WHERE id_tahun_buku = $tahun_buku_sebelumnya and no_akun = '300'" );
                              $ambil1 = mysqli_fetch_object($query1);
   
                        ?>
                        <?php
                              $query1 = mysqli_query($koneksi, "SELECT SUM(saldo) as saldo FROM bb_akun WHERE id_tahun_buku = $tahun_buku_sebelumnya and no_akun = '301'");
                              $ambil2 = mysqli_fetch_object($query1);
   
                        ?>
                        <?php
                              $query1 = mysqli_query($koneksi, "SELECT SUM(saldo) as saldo FROM bb_akun WHERE id_tahun_buku = $tahun_buku_sebelumnya and no_akun = '302'");
                              $ambil3 = mysqli_fetch_object($query1);
   
                        ?>
                        <?php
                              $query1 = mysqli_query($koneksi, "SELECT SUM(saldo) as saldo FROM bb_akun WHERE id_tahun_buku = $tahun_buku_sebelumnya and no_akun = '303'");

                              $ambil4 = mysqli_fetch_object($query1);

   
                        ?>
                        <?php
                              $query1 = mysqli_query($koneksi, "SELECT SUM(saldo) as saldo FROM bb_akun WHERE id_tahun_buku = $tahun_buku_sebelumnya and no_akun in ('300','301','302','303') ");
                              $ambil5 = mysqli_fetch_object($query1);

        
                        ?>



                        <td width="25%" style="font-weight: bold;">SALDO PER 31 DESEMBER <?= $tahun_sebelumnya; ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($ambil1->saldo,"Rp. "); ?> </td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($ambil2->saldo,"Rp. "); ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($ambil3->saldo,"Rp. "); ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($ambil4->saldo,"Rp. "); ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($ambil5->saldo,"Rp. "); ?></td>
                        
                    </tr>
                    

                    <tr>
                    <td width='25%'>PENAMBAHAN DI TAHUN <?= $tahun_sekarang; ?> </td>
                    <?php
                     $query_ = "SELECT a.no_akun,a.nama_akun,
                                (SELECT SUM(saldo) as saldodebit from bb_akun WHERE id_tahun_buku = ".$tahun_buku_sekarang." and no_akun = a.no_akun and keterangan LIKE '%Debit%') as saldo_debit,
                                (SELECT SUM(saldo) as saldokredit from bb_akun WHERE id_tahun_buku = ".$tahun_buku_sekarang." and no_akun = a.no_akun and keterangan LIKE '%Kredit%') as saldo_kredit
                                from tb_akun a WHERE a.no_akun IN(300,301,302,303)
                                GROUP BY a.no_akun ";

                    $exe = mysqli_query($koneksi, $query_); 
                    $saldo_penambahan = 0;
                    //$saldo300kredit1 = 0;
                    while($row = mysqli_fetch_object($exe)){?> 
                        <td width='15%'>
                            <?php 
                                $saldoxyz = 0;
                                if($row->no_akun == "303"){
                                    $saldoxyz = $row->saldo_debit;
                                    $saldo303debit1 = $row->saldo_debit;
                                }else{
                                    
                                    if($row->no_akun == "300"){
                                        $saldo300kredit1 = $row->saldo_kredit;

                                    }
                                    elseif($row->no_akun == "301"){
                                        $saldo301kredit1 = $row->saldo_kredit;

                                    }
                                    elseif($row->no_akun == "302"){
                                        $saldo302kredit1 = $row->saldo_kredit;

                                    }
                                    $saldoxyz = $row->saldo_kredit;
                                }
                                echo rupiah($saldoxyz,"Rp. ");
                                $saldo_penambahan += $saldoxyz;
                            ?>        
                        </td>
                    <?php } ?>
                    <td> <?= rupiah($saldo_penambahan,"Rp. "); ?></td>
                    </tr>

                    <tr>
                    <td width='25%'>PENGURANGAN DI TAHUN <?= $tahun_sekarang; ?> </td>
                    <?php
                  $query_ = "SELECT a.no_akun,a.nama_akun,
                                (SELECT SUM(saldo) as saldodebit from bb_akun WHERE id_tahun_buku = ".$tahun_buku_sekarang." and no_akun = a.no_akun and keterangan LIKE '%Debit%') as saldo_debit,
                                (SELECT SUM(saldo) as saldokredit from bb_akun WHERE id_tahun_buku = ".$tahun_buku_sekarang." and no_akun = a.no_akun and keterangan LIKE '%Kredit%') as saldo_kredit
                                from tb_akun a WHERE a.no_akun IN(300,301,302,303)
                                GROUP BY a.no_akun ";
                    $exe = mysqli_query($koneksi, $query_); 
                    $saldo_pengurangan = 0;
                    while($row = mysqli_fetch_object($exe)){?> 
                        <td width='15%'>
                            <?php 
                                $saldoxyz = 0;
                                //$saldo300debit2 = 0;
                                if($row->no_akun == "303"){
                                    $saldoxyz = $row->saldo_kredit;
                                    $saldo303kredit2 = $row->saldo_kredit;
                                }else{
                                    
                                    if($row->no_akun == "300"){
                                      $saldo300debit2 = $row->saldo_debit;

                                    }
                                    elseif($row->no_akun == "301"){
                                      $saldo301debit2 = $row->saldo_debit;

                                    }
                                    elseif($row->no_akun == "302"){
                                      $saldo302debit2 = $row->saldo_debit;

                                    }
                                    $saldoxyz = $row->saldo_debit;
                                    //$saldo300debit2 = $row->saldo_debit;
                                }
                                echo rupiah($saldoxyz,"Rp. ");
                                $saldo_pengurangan += $saldoxyz;
                            ?>        
                        </td>
                    <?php } ?>
                    <td> <?= rupiah($saldo_pengurangan,"Rp. "); ?></td>
                    </tr>
                     
                    
                    <?php 
                    $total300 = 0;
                    $totalsebelum = $ambil1->saldo;
                    $total300 = $totalsebelum + ($saldo300kredit1 - $saldo300debit2) ;

                    $total301 = 0;
                    $totalsebelum = $ambil2->saldo;
                    $total301 = $totalsebelum + ($saldo301kredit1 - $saldo301debit2) ;

                    $total302 = 0;
                    $totalsebelum = $ambil3->saldo;
                    $total302 = $totalsebelum + ($saldo302kredit1 - $saldo302debit2) ;

                    $total303 = 0;
                    $totalsebelum = $ambil3->saldo;
                    $total303 = $totalsebelum + ($saldo303debit1 - $saldo303kredit2) ;

                    $totaljumlah = 0;
                    $totalsebelum = $ambil5->saldo;
                    $totaljumlah = $totalsebelum + ($saldo_penambahan - $saldo_pengurangan) ;
                    

                    ?>
                    <tr style="font-weight: bold; background-color: #ccffb5;">  
                        <td width="25%" style="font-weight: bold;">SALDO PER 31 DESEMBER <?= $tahun_sekarang; ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($total300,"Rp. "); ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($total301,"Rp. "); ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($total302,"Rp. "); ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($total303,"Rp. "); ?></td>
                        <td width="15%" style="font-weight: bold;"><?= rupiah($totaljumlah,"Rp. "); ?></td>
                    </tr>
                    </table>
                    <div style="margin-top: 10px; margin-bottom: 10px; border-style: solid; border-width: 1px; "></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- Main End Here -->
<script type="text/javascript">
    function toggle(id){
        $("#"+id).fadeToggle();
     }
     window.print();
    setTimeout(window.close, 100);
</script>

<!-- ============ Body content End ============= -->
