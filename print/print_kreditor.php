<?php ob_start();
include '../config/config.php';
cek_tahun_buku();


    @$tglAwal = $_GET['tglAwal'];
    @$tglAkhir = $_GET['tglAkhir'];
    @$tipe = $_GET['tipe'];
    $tahun_buku = $_SESSION['tahun_buku'];
    $awal_periode = $_SESSION['awal_periode'];
    $akhir_periode = $_SESSION['akhir_periode'];
    $tahun_buku = $_SESSION['tahun_buku'];
    $GLOBALS['conn'] = $koneksi;


    if(empty($tipe) && empty($tglAwal) && empty($tglAkhir)){
        $query = "select * from v_kreditor  ORDER BY tanggal_peminjaman ASC";
    }else{
        $query = "select * from v_kreditor where ".$tipe." BETWEEN '".$tglAwal."' AND '".$tglAkhir."' ORDER BY tanggal_peminjaman ASC";
    }   


    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }

    function get_total_dilunasi($no_akun,$tahun_buku){
        $query =  "SELECT  sum( dk.nominal ) as jumlah FROM tb_jurnal_debit_kredit dk, tb_jurnal dj WHERE dk.no_jurnal = dj.no_jurnal AND dj.tipe_jurnal = 'JKK' AND  dk.no_akun = '".$no_akun."'";
        $data = mysqli_fetch_object(mysqli_query($GLOBALS['conn'],$query));
        if(!empty($data)){
            return $data->jumlah;
        }else{
            return '0';
        }
    }

    function total_cicilan_dilunasi($no_akun,$tahun_buku){
        $query =  "SELECT  count( dk.nominal ) as jumlah FROM tb_jurnal_debit_kredit dk, tb_jurnal dj WHERE dk.no_jurnal = dj.no_jurnal AND dj.tipe_jurnal = 'JKK' AND  dk.no_akun = '".$no_akun."'";
        $data = mysqli_fetch_object(mysqli_query($GLOBALS['conn'],$query));
        if(!empty($data)){
            return $data->jumlah;
        }else{
            return '0';
        }
    }



 ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 400px;}
   table td {word-wrap:break-word;width: 14%;}
   </style>
   <?php include 'print_bootstrap.php'; ?>
   <style type="text/css">
     .dim-memo{
            background-color: #fff5c9 !important;
            font-style: italic !important;
            text-align: center !important;
        }
      .table-header{
          text-align: center !important;
          font-weight: bold;
          color: black;
          height: 40px !important;
          padding-bottom: 40px !important;
      }
   </style>
</head>
<body  style="padding: 20px;"><br>
  <div class="row" style="padding-bottom: 20px;">
    <div class="col-md-4" style="padding-left: 100px; text-align: center;">
      <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
      <h4> KOPKARKIM BIDA</h4>
    </div>
    <div class="col-md-8" style="padding-top: 40px;">
      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">KREDITOR</h4>
  <h5 style="text-align: center;">Periode <?= $awal_periode; ?>  sampai <?= $akhir_periode; ?> </h5>
    </div>
  </div>
  
<table class="table table-sm table-bordered" width="50%" style="text-align: center;">
  <thead>
            <tr>
                <th>No</th>
                <th>No ID Bank</th>
                <th>Nama Bank</th>
                <th>Tanggal Peminjaman</th>
                <th>Tanggal Jatuh Tempo</th>
                <th>Term</th>
                <th>Banyak Cicilan</th>
                <th>Nominal Pinjaman</th>
                <th>Total yang sudah dilunasi</th>
                <th>Saldo Akhir</th>
                <th>Sisa Masa Cicilan</th>
            </tr>
 </thead>

<?php $i = 1; while($row = mysqli_fetch_object($execute)){ ?>
                <tr onclick="select_me('<?= $row->id_kreditor; ?>');" id="row<?= $row->id_kreditor; ?>">
                <td><?= $i; ?></td>
                <td><?= $row->no_akun." ".$row->nama_akun; ?></td>
                <td><?= $row->nama_bank; ?></td>
                <td><?= tgl_indo($row->tanggal_peminjaman); ?></td>
                <td><?= tgl_indo($row->tanggal_jatuh_tempo); ?></td>
                <td> <?= $row->term; ?></td>
                <td><?= $row->banyak_cicilan; ?></td>
                <td><?= $row->nominal_pinjaman; ?></td>
                <td><?= rupiah(get_total_dilunasi($row->no_akun,$tahun_buku)); ?></td>
                <td><?= rupiah($row->saldo_akhir); ?></td>
                <td><?= total_cicilan_dilunasi($row->no_akun,$tahun_buku); ?></td>
            </tr>
            <?php $i++;} ?>
</table>
</body>
<script type="text/javascript">
  window.print();
 setTimeout(window.close, 100);
</script>

</html>
