<?php ob_start();
include '../config/config.php';
cek_tahun_buku();

    @$tglAwal = $_GET['tglAwal'];
    @$tglAkhir = $_GET['tglAkhir'];
    $tahun_buku = $_SESSION['tahun_buku'];
    $awal_periode = $_SESSION['awal_periode'];
    $akhir_periode = $_SESSION['akhir_periode'];
    
    if(empty($tglAwal) && empty($tglAkhir)){
        $query = "select * from v_jurnal_debitor WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JPK ' ORDER BY no_jurnal,keterangan ASC";
    }else{
        $query = "select * from v_jurnal_debitor WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JPK' AND tanggal BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  ORDER BY no_jurnal,keterangan ASC";
    }   

    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }


 ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 400px;}
   table td {word-wrap:break-word;width: 14%;}
   </style>
   <?php include 'print_bootstrap.php'; ?>
   <style type="text/css">
     .dim-memo{
            background-color: #fff5c9 !important;
            font-style: italic !important;
            text-align: center !important;
        }
      .table-header{
          text-align: center !important;
          font-weight: bold;
          color: black;
          height: 40px !important;
          padding-bottom: 40px !important;
      }
   </style>
</head>
<body style="padding: 20px;"><br>
  <div class="row" style="padding-bottom: 20px;">
    <div class="col-md-4" style="padding-left: 100px; text-align: center;">
      <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
      <h4> KOPKARKIM BIDA</h4>
    </div>
    <div class="col-md-8" style="padding-top: 40px;">
      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">JURNAL PENYALURAN KREDIT</h4>
  <h5 style="text-align: center;">Periode <?= tgl_indo($awal_periode); ?>  sampai <?= tgl_indo($akhir_periode); ?> </h5>
    </div>
  </div>
  
<table class="table table-sm table-bordered" width="50%" style="text-align: center;">
  <thead>
<tr>
                <th>No. Jurnal</th>
                <th>Tanggal</th>
                <th>No. Bukti</th>
                <th>Jenis Kredit</th>
                <th>Nomor Akun</th>
                <th>Nama Akun</th>
                <th>No. Id Debitor</th>
                <th>Debit</th>
                <th>Kredit</th>
            </tr>
        </thead>
        <tbody>
            
            <?php  
            $jenis_kredit    = null;
            $no_id_debitor   = null;
            $temp_memo       = null;
            $temp_tahun_buku = null;
            $temp_ju         = null;
            $total_debit     = 0 ;
            $total_kredit    = 0 ;
            while($row = mysqli_fetch_object($execute)){ 
                    $no_jurnal     = $row->no_jurnal;
                    if($temp_ju !== $no_jurnal){
                        $temp_ju    = $no_jurnal;
                        $tanggal    = tgl_indo($row->tanggal);
                        $no_bukti   = $row->no_bukti;
                        $no_jurnal  = $row->no_jurnal;
                        $jenis_kredit    = $row->jenis_kredit;
                        $no_id_debitor   = $row->no_id_debitor;
                        $no_jurnal  = $row->no_jurnal;

                        /*jika no jurnal tidak sama dengan temp berarti awal row baru
                         1. keluarkan temp memo => memo dari jurnal sebelumnya
                         2. set temp memo => memo dari jurnal sekarang*/
                        echo $temp_memo;

                        //set isi kolom, HARUS SESUAI DENGAN HEADER walau ada colspan
                        //4 kolom pertama wajib, agar saat dicari ter group
                        $arr = array($row->no_jurnal,$row->memo,$no_jurnal,$tanggal,$no_bukti,$no_id_debitor,$jenis_kredit,"",""); 
                        $i = 0;
                        $temp_memo = '<tr class="dim-memo">';
                        while($i<count($arr)){
                            if($i==0){
                                $x = "<td colspan='".count($arr)."'>".$arr[$i]."</td>";
                            }else{
                                $x = "<td style='display: none;'>".$arr[$i]."</td>";
                            }
                            $temp_memo  = $temp_memo.$x;
                            $i++;
                        }
                        $temp_memo = $temp_memo."</tr>";
                        
                    }else{
                        $no_jurnal    = "<p style='display:none;>".$row->no_jurnal."</p>'";
                        $tanggal    = "<p style='display:none;>".$row->tanggal."</p>'";
                        $no_bukti   = "<p style='display:none;>".$row->no_bukti."</p>'";
                        $no_jurnal  = "<p style='display:none;>".$row->no_jurnal."</p>'";
                        $no_id_debitor  = "<p style='display:none;>".$row->no_id_debitor."</p>'";
                        $jenis_kredit  = "<p style='display:none;>".$row->jenis_kredit."</p>'";
                    }
                    
                    $no_akun    = $row->no_akun;
                    $nama       = $row->nama_akun;
                    $ket        = $row->keterangan;
                    $debit      = null;
                    $kredit     = null; 
                    if($ket=='Debit'){
                        $debit = rupiah($row->nominal,"Rp. ");
                        $total_debit = $total_debit+$row->nominal;
                    }else{
                        $kredit = rupiah($row->nominal,"Rp. ");
                        $total_kredit = $total_kredit+$row->nominal;
                    }

                ?>
                    
                    <tr onclick="select_me('<?= $temp_ju; ?>');" class="row<?= $temp_ju; ?>">
                        <td><?= $no_jurnal; ?></td>
                        <td><?= $tanggal; ?></td>
                        <td><?= $no_bukti; ?></td>
                        <td><?= $jenis_kredit; ?></td>
                        <td align="right"><?= $no_akun; ?></td>
                        <td><?= $nama; ?></td>
                        <td><?= $no_id_debitor; ?></td>
                        <td style="text-align: right;"><?= $debit; ?></td>
                        <td style="text-align: right;"><?= $kredit; ?></td>
                    </tr>
            <?php } echo $temp_memo; ?>
        </tbody>
                    <tr>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td colspan="7" class="dim-saldo">Saldo</td>
                        <td class="dim-saldo-debit" style="text-align: right;"><?= rupiah($total_debit,"Rp. "); ?></td>
                        <td class="dim-saldo-kredit" style="text-align: right;"><?= rupiah($total_kredit,"Rp. "); ?></td>
                    </tr>
</table>
</body>
<script type="text/javascript">
  window.print();
 setTimeout(window.close, 100);
</script>

</html>
