<?php
include 'config/config.php';
$title = "Edit User";
include 'template/header.php';
get_role_page('edit');

if(EMPTY($_GET['id'])){
    set_notif('psnakn','Maaf, User tidak ditemukan','user','danger','close');
}

$id = $_GET['id'];
$query = "select * from tb_user where no_id='".$id."'";
$data = mysqli_fetch_object(mysqli_query($koneksi,$query));
if(empty($data)){
    set_notif('psnusr','Maaf, User tidak ditemukan','user','danger','close');
}
?>

<!-- ============ Body content start ============= -->

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <h4 style="display: inline-block;"> <i class="fa fa-pencil fa-fw"></i> Edit User </h4>
                        <div class="float-right">
                            <button onclick="window.location = 'user'" class="btn btn-secondary btn">
                                    <i class="fa fa-arrow-left"></i> Kembali 
                            </button>
                        </div>
                        <br>
                    </div> 
                    <form id="frmz">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama" required="" value="<?= $data->nama; ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" value="<?= $data->username; ?>" class="form-control" name="username" required="" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select class="select2" required="" name="role"> 
                                    <option value=""> ---- Pilih Roler ---- </option>
                                    <option value="ADMIN" <?= selected_droplist('ADMIN',$data->role); ?>> Admin </option>
                                    <option value="STAFF" <?= selected_droplist('STAFF',$data->role); ?>> Staff </option>
                                </select>
                            </div>
                             <div class="form-group">
                                <label><b style="color: #f70000"> *</b>Password</label><br>
                                <input type="checkbox" name="pwd_chg" id="pwd_chg" >
                                Ubah Password
                                <div id="pwd" hidden>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" id="pwd1" class="form-control" name="password" placeholder="Password" required="" onchange="cek_pwd();" disabled="disabled">
                                        </div>

                                        <div class="form-group">
                                        <label>Retype Password</label>
                                        <input type="password" id="pwd2" class="form-control" name="password2" placeholder="Retype Password" required="" onchange="cek_pwd();" disabled="disabled">
                                        </div>

                                        <div id="notif"></div>
                                </div>
                               </div>
                            </div>

                            
                        <center>
                        <button type="submit" class="btn btn-success btn-lg" id="btn"> <i class="fa fa-save"></i> Simpan</button>
                        </center>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- ============ Body content End ============= -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#pwd_chg').change(function () {
            if ($('#pwd_chg').is(':checked'))  {
                $('#pwd').removeAttr("hidden", "hidden");
                $('#pwd1').attr("required", "required");
                $('#pwd2').attr("required", "required");
                $('#pwd1').removeAttr("disabled", "disabled");
                $('#pwd2').removeAttr("disabled", "disabled");
            }else{
                $('#pwd').attr("hidden", "hidden");
                $('#pwd1').removeAttr("required", "required");
                $('#pwd2').removeAttr("required", "required");
                $('#pwd1').attr("disabled", "disabled");
                $('#pwd2').attr("disabled", "disabled");
                $("#btn").prop("disabled", false);
            }
        });
    });
</script>

<script type="text/javascript"> 
    //Tambah Data User
    $('#frmz').submit(function(event) { 
        event.preventDefault();
        var idx = '&old_username=<?= $data->username; ?>&id=<?= $_GET["id"]; ?>';
        var values = $(this).serialize();
	    simple_ajax(values+idx,'config/edit_user','user','Berhasil ubah data User');
        return false; //stop
    });
</script>
<script type="text/javascript">      
//jika pwd n re type tidak sama 
      function cek_pwd(){
         if ($('#pwd_chg').is(':checked')){
            $("#notif").empty();
            var pwd1 = $("#pwd1").val();
            var pwd2 = $("#pwd2").val();

            if(pwd1 !== pwd2){
              var notif = '<div class="alert alert-danger"> <i class="fa fa-close"> </i> Password tidak sama </i>';
              $("#notif").append(notif);
              $("#btn").prop("disabled", true);
            }else{
              $("#notif").empty();
              $("#btn").prop("disabled", false);
            }
         }
      }
</script>

<?php include 'template/footer.php'; ?>