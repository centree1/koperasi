<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		include 'config/config.php';
		$base_url= base_url()."login/";
	?>
	<title>Login Koperasi</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?= $base_url; ?>images/icons/logo-ico.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>css/main.css">
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/jquery/jquery-3.2.1.min.js"></script>
	
	<!-- swal2 -->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/sweetalert2/dist/sweetalert2.css'); ?>">
    <script src="<?= base_url('assets/vendor/sweetalert2/dist/sweetalert2.min.js'); ?>"></script>

	<script src="<?= $base_url; ?>../config/config.js"></script>

	
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">

			<div class="wrap-login100">

				<form action="#" id="frmz" class="login100-form validate-form" style="padding-top: 100px !important; " >
					<span class="login100-form-title p-b-34">

					<img style="width: 200px; height: auto;" src="<?= base_url(); ?>assets/img/sorry.gif"> <br><br>

						Sorry, Under Maintenance <br> Please wait until 7 AM -dimas 
					</span>
					
					

				</form>

				<div class="login100-more" style="background-image: url('<?= $base_url; ?>images/bg-01.jpg');"></div>
			</div>
		</div>
	</div>
	
	<!-- Js custom -->
<script type="text/javascript">
	$(document).ready(function(){
	   $('#frmz').submit(function(event) { 
	   		event.preventDefault(); //Prevent the default submit
            var values = $(this).serialize();
            simple_ajax(values,'config/login','dashboard','','Maaf, Username/Password Salah');
            
        });
	});
</script>

<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?= $base_url; ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?= $base_url; ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>js/main.js"></script>



</body>
</html>