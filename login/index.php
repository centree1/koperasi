<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		include 'config/config.php';
		$base_url= base_url()."login/";
	?>
	<title>Login Koperasi</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?= $base_url; ?>images/icons/logo-ico.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= $base_url; ?>css/main.css">
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/jquery/jquery-3.2.1.min.js"></script>
	
	<!-- swal2 -->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/sweetalert2/dist/sweetalert2.css'); ?>">
    <script src="<?= base_url('assets/vendor/sweetalert2/dist/sweetalert2.min.js'); ?>"></script>

	<script src="<?= $base_url; ?>../config/config.js"></script>

	<style type="text/css">
		body {
			margin: 0 !important;
			width: 100%;
			height: 100vh;
			font-family: "Exo", sans-serif;
			color: #fff;
			background: linear-gradient(-45deg,  #1bb355, #1bb379, #1b37b3) !important;
			background-size: 400% 400% !important;
			animation: gradientBG 15s ease infinite !important;
		}

@keyframes gradientBG {
	0% {
		background-position: 0% 50%;
	}
	50% {
		background-position: 100% 50%;
	}
	100% {
		background-position: 0% 50%;
	}
}
	</style>
	
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">

			<div class="wrap-login100">

				<form action="#" id="frmz" class="login100-form validate-form" style="padding-top: 50px !important; padding-left: -300px !important; padding-right: -300px !important; " >
					<span class="login100-form-title p-b-3">

					<img style="width: 200px; height: auto;" src="<?= $base_url; ?>images/logo.png"> <br><br>

						Login 
					</span>
					
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="first-name" class="input100" type="text" name="usernamex" placeholder="Username" autocomplete="off" >
						<span class="focus-input100"></span>
					</div>
					<br>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="password" name="passwordx" placeholder="Password" autocomplete="new-password">
						<span class="focus-input100"></span>
					</div>
					
					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn" >
							Log In
						</button>
					</div>

				</form>

				
			</div>
		</div>
	</div>
	
	<!-- Js custom -->
<script type="text/javascript">
	$(document).ready(function(){
	   $('#frmz').submit(function(event) { 
	   		event.preventDefault(); //Prevent the default submit
            var values = $(this).serialize();
            simple_ajax(values,'config/login','dashboard','','Maaf, Username/Password Salah');
            
        });
	});
</script>

<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?= $base_url; ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?= $base_url; ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?= $base_url; ?>js/main.js"></script>



</body>
</html>