<?php
include 'config/config.php';
$title = "User";
include 'template/header.php';
get_role_page('menu_user');

?>

<!-- ============ Body content start ============= -->
<?php
    get_notif('psnusr');
?>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <h4 style="display: inline-block;"> <i class="fa fa-user fa-fw"></i> User </h4>
                        <div class="float-right">
                            <a href="tambah_user" class="btn btn-success btn btn-icon-split mb-3">
                                <i class="fa fa-plus"></i> User 
                            </a>
                        </div>
                        <br>
                    </div> 
                    <div cla ss="container-form" id="datax">
                    <div class="table-responsive">
                      <table class="table table-striped table-hovered table-bordered dataTable" >
                       <thead><tr>
                            <th>ID</th>
                            <th>Nama User</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Config</th>
                       </tr></thead>

                            <?php
                                $query = 'select * from tb_user where role != "ANGGOTA" && role != "SYSADMIN"';
                                $execute = mysqli_query($koneksi,$query);
                                while($row=mysqli_fetch_object($execute)){
                            ?>
                            <tr>
                            <td><?= $row->no_id; ?></td>
                            <td><?= $row->nama; ?></td>
                            <td><?= $row->username; ?></td>
                            <td><?= $row->role; ?></td>
                            <td>
                                <a class='btn btn-primary btn-sm' href='edit_user?id=<?= $row->no_id; ?>'>Edit</a>  
                                <button class='btn btn-danger btn-sm' onclick="deleteUser('<?= $row->no_id; ?>')">Delete</button>
                            </td>
                            </tr>
                                <?php } ?>
                            
                        
                    </table>
                    </div>
<!-- 
                    <center>
                    <a href="tambah_user" class="btn btn-success  btn-icon-split mb-3">
                                <i class="fa fa-plus"></i> User 
                    </a>
                    </center> -->
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    function deleteUser(id){
        pesan_confirm("Apakah anda yakin?", "Data yang telah dihapus tidak dapat dikembalikan", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("id="+id,'config/delete_user','','User berhasil dihapus','User gagal dihapus');
            }
        });
    }
    
</script>

<?php include 'template/footer.php'; ?>