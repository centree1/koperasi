<?php
include 'config/config.php';
$title = "Tambah Tahun Buku";
include 'template/header.php';

?>

<!-- ============ Body content start ============= -->

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Tambah Tahun Buku
                        </label-tabel>
                        <div class="float-right">
                        </div>
                        <br>
                    </div>
                    <form id="frm_tambah">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Awal Periode</label>
                                <input type="text" class="form-control" id="dtp1" name="dtp1" required="" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Akhir Periode</label>
                            <input type="text" class="form-control" id="dtp2" name="dtp2" required="" autocomplete="off">
                        </div>
                        </div>
                        
                        

                        <center>
                        <a href="tahun_buku" class="btn btn-warning btn-lg" role="button" aria-pressed="true">Kembali</a>
                        <button type="submit" class="btn btn-success btn-lg">Simpan</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    //Tambah Data User
    $('#frm_tambah').submit(function(event) { 
        event.preventDefault();
        var values = $(this).serialize();
	    simple_ajax(values,'config/tambah_buku','tahun_buku','Berhasil menambah Tahun Buku baru','Data Buku gagal Ditambah');
        return false; //stop
    });
</script>

 <script type="text/javascript">
    $(function () {
        $('#dtp1').datetimepicker({
            format: 'DD-MM-YYYY',
            viewMode: 'years'
        });
        $('#dtp2').datetimepicker({
            useCurrent: false ,
            format: 'DD-MM-YYYY',
            viewMode: 'years'
        });
        $("#dtp1").on("dp.change", function (e) {
            $('#dtp2').data("DateTimePicker").minDate(e.date);
        });
        /*$("#dtp2").on("dp.change", function (e) {
            $('#dtp1').data("DateTimePicker").maxDate(e.date);
        });*/
    });
</script>
<?php include 'template/footer.php'; ?>