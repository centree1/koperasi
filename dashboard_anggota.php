<?php   
$id_user = $_SESSION['no_id'];
?>

<!-- Main Start Here -->
    
    <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class=" row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center">
                             <h5 style="display: inline-block;"> <i class="fa fa-credit-card-alt fa-fw"></i>  Dashboard </h5>
                        </div> 
                        <div class="col-md-6 col-xs-12 dim-center text-right">
                            <!-- <button type="button" onclick="toggle_filter()" class="btn btn-primary" ><i class="fa fa-filter fa-fw"></i> Filter </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        get_notif('psnjrnalPn');
    ?>

    <!-- Filter ------------------------------------------------------------>
    <div class="row" id="filter" style="display: none;">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <form method="POST" id="frmz"> 
                <div class="card-body">
                    <div class="row mb-4" >
                        <div class="col-md-6 col-6 ">
                             <h4 style="display: inline-block;"> <i class="fa fa-filter fa-fw"></i> Filter </h4>
                        </div> 
                        <div  class="col-md-6 col-6 text-right">
                            <button type="button" onclick="toggle_filter()" class="btn btn-secondary"><i class="fa fa-close fa-fw"></i> </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Filter Berdasarkan</label>
                                <select id="tipe" class="form-control select2" required="" name="tipe" >
                                    <option value=""> -- Pilih --</option>
                                    <option value="tanggal_penyaluran"> Tanggal Penyaluran Kredit </option>
                                    <option value="tanggal_jatuh_tempo"> Tanggal Jatuh Tempo </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Dari Tanggal</label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Dari Tanggal" required="" id="tglAwal" name="dari_tgl" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Sampai Tanggal</label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Sampai Tanggal" required="" id="tglAkhir" name="sampe" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label></label>
                                <button type="submit" class="btn btn-primary btn-block" > <i class="fa fa-filter fa-fw"></i> Filter </button>
                                <button type="button" onclick="load_table()" class="btn btn-warning btn-block" ><i class="fa fa-refresh fa-spin fa-fw"></i> Reset </button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Filter -------------------------------------------------------->


     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    
                    <div id="tbl" class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
                        
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript">
    function toggle_filter(){
        $("#filter").fadeToggle();
    }

    load_table();
    function load_table(){    
        get_with_ajax("", "config/get_table_debitor_anggota", "tbl");
    } 

    $('#frmz').submit(function(event) { 
        event.preventDefault(); //Prevent the default submit
        var values = $(this).serialize();
        console.log(values);
        get_with_ajax(values, "config/get_table_debitor_anggota", "tbl");
        return false; //stop
    });
</script>

<?php include 'template/footer.php'; ?>