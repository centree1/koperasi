<?php include 'config/config.php'; ?>

<table class="table table-striped table-hovered table-bordered dataTable" >
   <thead><tr>
        <th>No Akun</th>
        <th>Nama Akun</th>
        <th>Tipe Akun</th>
        <th>Kategori Akun</th>
        <th>Saldo Normal</th>
        <th>Aksi</th>
   </tr></thead>

        <?php
            //get all Tipe
            $query = 'select * from tb_kategori_akun';
            $query = mysqli_query($koneksi,$query);
            while($row=mysqli_fetch_object($query)){
                $kategori[$row->id_kategori]= $row->nama_kategori; 
            }


            //get all Tipe
            $query = 'select * from tb_tipe_akun';
            $query = mysqli_query($koneksi,$query);
            while($row=mysqli_fetch_object($query)){
                $tipe[$row->id_tipe]= $row->nama_tipe; 
            }

            if(empty($_POST['id'])){
                $query = 'select * from tb_akun';
            }else{
                $query = 'select * from tb_akun where id_kategori="'.$_POST['id'].'"';
            }
            $query = mysqli_query($koneksi,$query);
            while($row=mysqli_fetch_array($query)){
        ?>
        <tr>
        <td><?= $row['no_akun']?></td>
        <td><?= $row['nama_akun']?></td>
        <td><?= $tipe[$row['tipe_akun']]; ?></td>
        <td><?= $kategori[$row['id_kategori']]; ?></td>
        <td><?= $row['saldo_normal']; ?></td>
        <td>
            <?php if($_SESSION['role']=='ADMIN'){ ?>
                    <a class='btn btn-primary btn-sm' href='edit_coa?id=<?= $row['no_akun']; ?>'>Edit</a>  
                    <button class='btn btn-danger btn-sm' onclick="deleteUser(<?= $row['no_akun']; ?>)">Delete</button>
                </td>
            <?php }else{ echo " - "; } ?>
            <?php } ?>
        </tr>
    
</table>

<script type="text/javascript">
    $('.dataTable').DataTable();
</script>