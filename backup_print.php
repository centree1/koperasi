<?php ob_start();
include 'config/config.php';
cek_tahun_buku();
$tahun_buku = $_SESSION['tahun_buku'];
//tahun priode
$query1 = "select * from tb_tahun_buku WHERE id_tahun_buku=".$tahun_buku."";

$sql1 = mysqli_query($koneksi, $query1);

$priod = mysqli_fetch_object($sql1);
$tee = $priod->awal_periode; 


 ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 400px;}
   table td {word-wrap:break-word;width: 14%;}
   </style>
</head>
<body>
  <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">JURNAL PENYESUAIAN</h4>
  <h5 style="text-align: center;">Periode hh/mm/tt  sampai hh/mm/tt </h5>
<table border="1" width="50%" style="text-align: center;">
<tr>
  <th>Tanggal</th>
  <th>No. Jurnal</th>
  <th>No. Bukti</th>
  <th>No. Akun</th>
  <th>Nama Akun</th>
  <th>Debit</th>
  <th>Kredit</th>
 </tr>

<?php

$query = "select * from v_jurnal WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JP' ORDER BY tanggal,no_jurnal,keterangan,row ";
$execute = mysqli_query($koneksi, $query); // Eksekusi/Jalankan query dari variabel $query

            $temp_ju = null;
            $temp_memo = null;

                    $total_debit = 0;
                    $total_kredit = 0;
            while($row = mysqli_fetch_object($execute)){ 
              
                    $no_jurnal     = $row->no_jurnal;
                    if($temp_ju !== $no_jurnal){
                        $temp_ju    = $no_jurnal;
                        $tanggal    = $row->tanggal;
                        $no_bukti   = $row->no_bukti;
                        $nama_akun    = $row->nama_akun;
                      
                                                /*jika n
                                                /*jika no jurnal tidak sama dengan temp berarti awal row baru
                         1. keluarkan temp memo => memo dari jurnal sebelumnya
                         2. set temp memo => memo dari jurnal sekarang*/
                        echo $temp_memo;

                        //set isi kolom, HARUS SESUAI DENGAN HEADER walau ada colspan
                        //4 kolom pertama wajib, agar saat dicari ter group
                        $arr = array($row->memo,"","",""); 
                        $i = 0;
                        $temp_memo = '<tr class="dim-memo">';
                        while($i<count($arr)){
                            if($i==0){
                                $x = "<td colspan='".count($arr)."'>".$arr[$i]."</td>";
                            }else{
                                $x = "<td style='display: none;'>".$arr[$i]."</td>";
                            }
                            $temp_memo  = $temp_memo.$x;
                            $i++;
                        }
                        $temp_memo = $temp_memo."</tr>";
                       // im_debugging($temp_memo);
                        
                    
                      }
                      else{
                        $tanggal    = "<p>".$row->tanggal."</p>";
                        $no_bukti   = "<p>".$row->no_bukti."</p>";
                        $no_jurnal  = "<p>".$row->no_jurnal."</p>";
                    }


                    $no_akun    = $row->no_akun;
                    $nama       = $row->nama_akun;
                    $ket        = $row->keterangan;
                    $debit = null;
                    $kredit = null;

                    if($ket=='Debit'){
                        $debit = rupiah($row->nominal);
                        $total_debit = $total_debit+$row->nominal;
                    }else{
                        $kredit = rupiah($row->nominal);
                        $total_kredit = $total_kredit+$row->nominal;
                    }
?>
                    <tr class="row<?= $temp_ju; ?>">
                        <td><?= $tanggal; ?></td>
                        <td><?= $no_jurnal; ?></td>
                        <td><?= $no_bukti; ?></td>
                        <td align="right"><?= $no_akun; ?></td>
                        <td><?= $nama; ?></td>
                        <td align="right"><?= $debit; ?></td>
                        <td align="right"><?= $kredit; ?></td>
                    </tr>
                    <?php } echo $temp_memo; ?>
                    <tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td class="dim-saldo">Saldo</td>
                        <td class="dim-saldo-debit"><?= rupiah($total_debit,"Rp. "); ?></td>
                        <td class="dim-saldo-kredit"><?= rupiah($total_kredit,"Rp. "); ?></td>
                    </tr>
</table>
</body>

</html>

<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Jurnal Penyesuaian.pdf', 'D');
?>
