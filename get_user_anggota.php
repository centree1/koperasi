<?php
  include 'config/config.php';
  cek_tahun_buku();  

  $query = mysqli_query($koneksi,'select * from tb_user where role="ANGGOTA"'); 
  
?>

<table class="table table-hover table-striped datatable">
  <thead>
  <tr> 
    <th> Nama </th>
    <th> Username </th>
    <th> Aksi </th>
  </tr>
  </thead>
  <tbody>
  <?php while ($row = mysqli_fetch_object($query)) { ?>
   <tr>
    <td> <?= $row->nama; ?>  </td>
    <td> <?= $row->username; ?>  </td>
    <td> <button class="btn btn-success" onclick="choose('<?= $row->no_id; ?>','<?= $row->username; ?>')"> <i class="fa fa-hand-pointer-o"></i> </button></td>
  </tr>
  </tbody>
  <?php } ?>
  
</table>

<script type="text/javascript">
  $('.dataTable').DataTable({
      "ordering": false
  });

  function choose(id,nama){
      $("#nama_anggota").val(nama);
      $("#id_anggota").val(id);
      $('#exampleModal').modal('hide');
  }
</script>