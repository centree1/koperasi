<?php
include 'config/config.php';
$title = "Tambah Tipe Akun";
include 'template/header.php';
cek_tahun_buku();
get_role_page('edit');
if(empty($_GET['id'])){
    set_notif('psndebitor','Maaf, Debitor tidak ditemukan','debitor','danger','close');
}

$id = $_GET['id'];
$query = "select * from tb_debitor, tb_debitor_pekerjaan, tb_debitor_pribadi, tb_kredit_salur, tb_akun where id_debitor='".$id."' AND tb_debitor.no_id_debitor = tb_debitor_pekerjaan.no_id_debitor AND tb_debitor.no_id_debitor = tb_debitor_pribadi.no_id_debitor AND tb_debitor.id_kredit_salur=tb_kredit_salur.id_kredit_salur AND tb_kredit_salur.no_akun = tb_akun.no_akun";
$data = mysqli_fetch_array(mysqli_query($koneksi,$query));
if(empty($data)){
    set_notif('psndebitor','Maaf, Debitor tidak ditemukan','debitor','danger','close');
}

$query = "select * from tb_user where no_id='".$data['id_user']."'";
$data_user = mysqli_fetch_array(mysqli_query($koneksi,$query)); 
?>
<style type="text/css">
    .container-form{
        padding-top: 40px;
        color: black;
    }
    label{
        font-weight: bold;
    }
</style>
<!-- ============ Body content start ============= -->

<div class="">
    <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class=" row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center">
                             <h5 style="display: inline-block;"> <i class="fa fa-pencil fa-fw"></i>  Edit Debitor   </h5>
                        </div> 
                        <div class="col-md-6 col-xs-12 dim-center text-right">
                             <a href="debitor" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   <div id="carouselExampleIndicators" data-interval="false" class="carousel slide" data-ride="carousel">
                      <form id="frmz">
                      <div class="carousel-inner">
                        <!-- Referensi Data Debitor -->
                        <div class="carousel-item active dim-border">
                          <label-tabel-small> Referensi Data Debitor</label-tabel-small>
                          <br><br>
                            <div class="row">
                                <input hidden readonly id="id_debitor" name="id_debitor" type="text" class="form-control" value="<?=$data['id_debitor'];?>">
                                <input hidden readonly id="id_kredit_salur" name="id_kredit_salur" type="text" class="form-control" value="<?=$data['id_kredit_salur'];?>">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Kredit yang Disalurkan</label>
                                        <input  readonly id="kredit_salur" name="kredit_salur" type="text" class="form-control" value="<?=$data['nama_akun']." | ".$data['jenis_kredit'];?>">
                                    </div>                                  
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>No Identitas Debitor</label>
                                    <input  readonly id="no_identitas_debitor" name="no_identitas_debitor" type="text" class="form-control" value="<?=$data['no_id_debitor'];?>">
                                  </div>
                                </div>
                            </div>

                            <div class="mt-3 row">
                                <div class="col-md-6">
                                     <button class="btn btn-danger" onclick="history.back();" > <i class="fa fa-arrow-left"></i> Kembali </button>  
                                </div>
                                <div class="col-md-6 text-right">
                                    <a class="btn btn-info" href="#carouselExampleIndicators" role="button" data-slide="next"> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                                </div>
                            </div>
                        </div>

                        <!-- Data Pribadi -->
                        <div class="carousel-item  dim-border">
                            <label-tabel-small> Data Pribadi </label-tabel-small>
                            <br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama</label>
                                    <input  name="nama" type="text" class="form-control" placeholder="Nama" value="<?=$data['nama'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <select  name="jenis_kelamin" class="form-control select2">
                                            <option value="Pria">Pria</option>
                                            <option <?php if($data['jenis_kelamin']=="Wanita") echo "selected";?> value="Wanita">Wanita</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input  name="tempat_lahir" type="text" class="form-control" placeholder="Tempat Lahir" value="<?=$data['tempat_lahir'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input  name="tanggal_lahir" type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Lahir" value="<?=$data['tanggal_lahir'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Warga Negara</label>
                                    <input  name="warga_negara" type="text" class="form-control" placeholder="Warga Negara" value="<?=$data['warga_negara'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama Ibu Kandung (Sebelum Menikah)</label>
                                    <input  name="nama_ibu" type="text" class="form-control" placeholder="Nama Ibu Kandung" value="<?=$data['nama_ibu'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>NPWP</label>
                                    <input  name="npwp" type="text" class="form-control" placeholder="NPWP" value="<?=$data['npwp'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>No KTP/Passpor</label>
                                    <input  name="no_ktp_passpor" type="text" class="form-control" placeholder="No KTP/Passpor" value="<?=$data['no_ktp_passport'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Agama</label>
                                    <input  name="agama" type="text" class="form-control" placeholder="Agama" value="<?=$data['agama'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>E-mail</label>
                                    <input  name="email" type="text" class="form-control" placeholder="E-mail" value="<?=$data['email'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Rumah(Sesuai KTP/Passpor)</label>
                                    <input  name="alamat_ktp" type="text" class="form-control" placeholder="Alamat Rumah" value="<?=$data['alamat_ktp'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Sesuai KTP/Passpor)</label>
                                    <input  name="kota_ktp" type="text" class="form-control" placeholder="Kota" value="<?=$data['kota_ktp'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Sesuai KTP/Passpor)</label>
                                    <input  name="kode_pos_ktp" type="text" class="form-control" placeholder="Kode Pos" value="<?=$data['kode_pos_ktp'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Sesuai KTP/Passpor)</label>
                                    <input  name="telp" type="text" class="form-control" placeholder="Telp" value="<?=$data['telp_ktp'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Sesuai KTP/Passpor)</label>
                                    <input  name="hp" type="text" class="form-control" placeholder="HP" value="<?=$data['hp_ktp'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Rumah(Sekarang)</label>
                                    <input  name="alamat_sekarang" type="text" class="form-control" placeholder="Alamat Rumah" value="<?=$data['alamat_sekarang'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Ditempati Sejak</label>
                                    <input  name="ditempati_sejak_sekarang" type="text" class="form-control" placeholder="Ditempati Sejak" value="<?=$data['ditempati_sejak_sekarang'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Sekarang)</label>
                                    <input  name="kota_sekarang" type="text" class="form-control" placeholder="Kota" value="<?=$data['kota_sekarang'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Sekarang)</label>
                                    <input  name="kode_pos_sekarang" type="text" class="form-control" placeholder="Kode Pos" value="<?=$data['kode_pos_sekarang'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Sekarang)</label>
                                    <input  name="telp_sekarang" type="text" class="form-control" placeholder="Telp" value="<?=$data['telp_sekarang'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Sekarang)</label>
                                    <input  name="hp_sekarang" type="text" class="form-control" placeholder="HP" value="<?=$data['hp_sekarang'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status Kepemilikan</label>
                                        <select  name="status_kepemilikan" class="form-control select2">
                                            <option value="Pribadi">Pribadi</option>
                                            <option <?php if($data['status_kepemilikan']=="Keluarga") echo "selected";?> value="Keluarga">Keluarga</option>
                                            <option <?php if($data['status_kepemilikan']=="Dinas") echo "selected";?> value="Dinas">Dinas</option>
                                            <option <?php if($data['status_kepemilikan']=="Sewa") echo "selected";?> value="Sewa">Sewa</option>
                                            <option <?php if($data['status_kepemilikan']=="Kost") echo "selected";?> value="Kost">Kost</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat Surat</label>
                                    <input  name="alamat_surat" type="text" class="form-control" placeholder="Alamat Surat" value="<?=$data['alamat_surat'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota</label>
                                    <input  name="kota_surat" type="text" class="form-control" placeholder="Kota" value="<?=$data['kota_surat'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pendidikan Terakhir</label>
                                        <select  name="pendidikan_terakhir" class="form-control select2">
                                            <option  value="SMA">SMA</option>
                                            <option <?php if($data['pendidikan_terakhir']=="D3") echo "selected";?> value="D3">D3</option>
                                            <option <?php if($data['pendidikan_terakhir']=="S1") echo "selected";?> value="S1">S1</option>
                                            <option <?php if($data['pendidikan_terakhir']==">S1") echo "selected";?> value=">S1">>S1</option>
                                        </select>
                                      </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status Perkawinan</label>
                                        <select  id="status_perkawinan" name="status_perkawinan" class="form-control select2" id="statusx" onchange="cek_status_perkawinan(this.value);">
                                            <option <?php if($data['status_perkawinan']=="Belum Nikah") echo "selected";?> value="Belum Nikah">Belum Nikah</option>
                                            <option <?php if($data['status_perkawinan']=="Nikah") echo "selected";?> value="Nikah">Nikah</option>
                                            <option <?php if($data['status_perkawinan']=="Duda/Janda") echo "selected";?> value="Duda/Janda">Duda/Janda</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Jumlah Tanggungan</label>
                                    <input  name="jumlah_tanggungan" type="number" class="form-control" placeholder="Jumlah Tanggunan" value="<?=$data['jumlah_tanggungan'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama(Untuk Keperluan Mendadak)</label>
                                    <input  name="nama_keperluan" type="text" class="form-control" placeholder="Nama" value="<?=$data['nama_keperluan'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Alamat(Untuk Keperluan Mendadak)</label>
                                    <input  name="alamat_keperluan" type="text" class="form-control" placeholder="Alamat" value="<?=$data['alamat_keperluan'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kota(Untuk Keperluan Mendadak)</label>
                                    <input  name="kota_keperluan" type="text" class="form-control" placeholder="Kota" value="<?=$data['kota_keperluan'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Kode Pos(Untuk Keperluan Mendadak)</label>
                                    <input  name="kode_pos_keperluan" type="text" class="form-control" placeholder="Kode Pos" value="<?=$data['kode_pos_keperluan'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Telp(Untuk Keperluan Mendadak)</label>
                                    <input  name="telp_keperluan" type="text" class="form-control" placeholder="Telp" value="<?=$data['telp_keperluan'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>HP(Untuk Keperluan Mendadak)</label>
                                    <input  name="hp_keperluan" type="text" class="form-control" placeholder="HP" value="<?=$data['hp_keperluan'];?>">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Hubungan(Untuk Keperluan Mendadak)</label>
                                    <input  name="hubungan_keperluan" type="text" class="form-control" placeholder="Hubungan" value="<?=$data['hubungan_keperluan'];?>">
                                  </div>
                                </div>
                            </div>
                            <div class="mt-3 row">
                                <div class="col-md-6">
                                    <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev"> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                                </div>
                                <div class="col-md-6 text-right">
                                    <a class="btn btn-info" href="#carouselExampleIndicators" role="button" data-slide="next"> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                                </div>
                            </div>
                        </div>

                        <!-- Data Pekerjaan -->
                        <div class="carousel-item  dim-border">
                          <label-tabel-small> Data Pekerjaan </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jenis Pekerjaan</label>
                                <select id="jenis_pekerjaan"  name="jenis_pekerjaan" class="form-control select2" onchange="cek_pekerjaan(this.value);">
                                  <option <?php if($data['jenis_pekerjaan']=="Pegawai Negeri") echo "selected";?> value="Pegawai Negeri">Pegawai Negeri</option>
                                  <option <?php if($data['jenis_pekerjaan']=="CPNS/Honor") echo "selected";?> value="CPNS/Honor">CPNS/Honor</option>
                                  <option <?php if($data['jenis_pekerjaan']=="Pegawai Swasta") echo "selected";?> value="Pegawai Swasta">Pegawai Swasta</option>
                                  <option <?php if($data['jenis_pekerjaan']=="Profesional") echo "selected";?> value="Profesional">Profesional</option>
                                  <option <?php if($data['jenis_pekerjaan']=="Wiraswasta") echo "selected";?> value="Wiraswasta">Wiraswasta</option>
                                  <option <?php if($data['jenis_pekerjaan']!="Wiraswasta" && $data['jenis_pekerjaan']!="Pegawai Negeri" && $data['jenis_pekerjaan']!="CPNS/Honor" && $data['jenis_pekerjaan']!="Pegawai Swasta" && $data['jenis_pekerjaan']!="Profesional") echo "selected";?> value="Lainnya">Lainnya</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan Lainnya</label>
                                <input id="jenis_pekerjaan_lainnya" disabled="disabled"  name="jenis_pekerjaan_lainnya" type="text" class="form-control" placeholder="Jenis Pekerjaan" value="<?=$data['jenis_pekerjaan'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input  name="nama_perusahaan" type="text" class="form-control" placeholder="Nama Perusahaan" value="<?=$data['nama_perusahaan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Bidang Usaha</label>
                                <input  name="bidang_usaha" type="text" class="form-control" placeholder="Bidang Usaha" value="<?=$data['bidang_usaha'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Alamat</label>
                                <input  name="alamat_perusahaan" type="text" class="form-control" placeholder="Alamat" value="<?=$data['alamat'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kota</label>
                                <input  name="kota_perusahaan" type="text" class="form-control" placeholder="Kota" value="<?=$data['kota'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kode Pos</label>
                                <input  name="kode_pos_perusahaan" type="text" class="form-control" placeholder="Kode Pos" value="<?=$data['kode_pos'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Telp</label>
                                <input  name="telp_perusahaan" type="text" class="form-control" placeholder="Telp" value="<?=$data['telp'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>HP</label>
                                <input  name="hp_perusahaan" type="text" class="form-control" placeholder="HP" value="<?=$data['hp'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Mulai Bekerja Sejak(Tahun)</label>
                                <input  name="mulai_bekerja" type="text" class="form-control" placeholder="Mulai Bekerja Sejak" value="<?=$data['mulai_bekerja'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jabatan</label>
                                <input  name="jabatan_perusahaan" type="text" class="form-control" placeholder="Jabatan" value="<?=$data['jabatan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Atasan</label>
                                <input  name="nama_atasan" type="text" class="form-control" placeholder="Nama Atasan" value="<?=$data['nama_atasan'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev"> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                            </div>
                            <div class="col-md-6 text-right">
                              <button type="button" class="btn btn-info"  role="button" data-slide="next" id="nextx"> Selanjutnya <i class="fa fa-arrow-right"></i> </button> 
                            </div>
                          </div>
                        </div>

                        <!-- Data Suami Istri -->
                        <?php
                        $query_pernikahan = mysqli_query($koneksi, "select * from tb_debitor_pekerjaan_pasangan where tb_debitor_pekerjaan_pasangan.no_id_debitor = '".$data['no_id_debitor']."'");
                        $num = mysqli_num_rows($query_pernikahan);
                        if($num < 1 ){?>
                        <div class="carousel-item  dim-border" id="carousel_suami_istri">
                          <label-tabel-small> Data Suami Istri </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama</label>
                                <input name="nama_pasangan" type="text" class="form-control" placeholder="Nama">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input name="tempat_lahir_pasangan" type="text" class="form-control" placeholder="Tempat Lahir">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input name="tanggal_lahir_pasangan" type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Lahir">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan</label>
                                <select id="jenis_pekerjaan_pasangan" name="jenis_pekerjaan_pasangan" class="form-control select2" onchange="cek_pekerjaan_pasangan(this.value);">
                                  <option  value="Pegawai Negeri">Pegawai Negeri</option>
                                  <option value="CPNS/Honor">CPNS/Honor</option>
                                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                                  <option value="Profesional">Profesional</option>
                                  <option value="Wiraswasta">Wiraswasta</option>
                                  <option value="Lainnya">Lainnya</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input name="nama_perusahaan_pasangan" type="text" class="form-control" placeholder="Nama Perusahaan">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan Lainnya</label>
                                <input id="jenis_pekerjaan_pasangan_lainnya" disabled="disabled" name="jenis_pekerjaan_pasangan_lainnya" type="text" class="form-control" placeholder="Jenis Pekerjaan">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Bidang Usaha</label>
                                <input name="bidang_usaha_pasangan" type="text" class="form-control" placeholder="Bidang Usaha">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Mulai Bekerja Sejak(Tahun)</label>
                                <input name="mulai_bekerja_pasangan" type="text" class="form-control" placeholder="Mulai Bekerja Sejak">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Alamat</label>
                                <input name="alamat_perusahaan_pasangan" type="text" class="form-control" placeholder="Alamat">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kota</label>
                                <input name="kota_perusahaan_pasangan" type="text" class="form-control" placeholder="Kota">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kode Pos</label>
                                <input name="kode_pos_perusahaan_pasangan" type="text" class="form-control" placeholder="Kode Pos">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Telp</label>
                                <input name="telp_perusahaan_pasangan" type="text" class="form-control" placeholder="Telp">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>HP</label>
                                <input name="hp_perusahaan_pasangan" type="text" class="form-control" placeholder="HP">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jabatan</label>
                                <input name="jabatan_perusahaan_pasangan" type="text" class="form-control" placeholder="Jabatan">
                              </div>
                            </div>
                          </div>                            
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev"> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                            </div>
                            <div class="col-md-6 text-right">
                              <a class="btn btn-info" href="#carouselExampleIndicators" role="button" data-slide="next"> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                            </div>
                          </div>
                        </div>
                        <?php }else{ $data_pernikahan = mysqli_fetch_array($query_pernikahan); ?>
                        <div class="carousel-item  dim-border" id="carousel_suami_istri">
                          <label-tabel-small> Data Suami Istri </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama</label>
                                <input name="nama_pasangan" type="text" class="form-control" placeholder="Nama" value="<?=$data_pernikahan['nama_pasangan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input name="tempat_lahir_pasangan" type="text" class="form-control" placeholder="Tempat Lahir" value="<?=$data_pernikahan['tempat_lahir_pasangan'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input name="tanggal_lahir_pasangan" type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Lahir" value="<?=$data_pernikahan['tanggal_lahir_pasangan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan</label>
                                <select id="jenis_pekerjaan_pasangan" name="jenis_pekerjaan_pasangan" class="form-control select2" onchange="cek_pekerjaan_pasangan(this.value);">
                                  <option <?php if($data_pernikahan['pekerjaan_pasangan']=="Pegawai Negeri") echo "selected";?> value="Pegawai Negeri">Pegawai Negeri</option>
                                  <option <?php if($data_pernikahan['pekerjaan_pasangan']=="CPNS/Honor") echo "selected";?> value="CPNS/Honor">CPNS/Honor</option>
                                  <option <?php if($data_pernikahan['pekerjaan_pasangan']=="Pegawai Swasta") echo "selected";?> value="Pegawai Swasta">Pegawai Swasta</option>
                                  <option <?php if($data_pernikahan['pekerjaan_pasangan']=="Profesional") echo "selected";?> value="Profesional">Profesional</option>
                                  <option <?php if($data_pernikahan['pekerjaan_pasangan']=="Wiraswasta") echo "selected";?> value="Wiraswasta">Wiraswasta</option>
                                  <option <?php if($data_pernikahan['pekerjaan_pasangan']!="Wiraswasta" && $data_pernikahan['pekerjaan_pasangan']!="Pegawai Negeri" && $data_pernikahan['pekerjaan_pasangan']!="CPNS/Honor" && $data_pernikahan['pekerjaan_pasangan']!="Pegawai Swasta" && $data_pernikahan['pekerjaan_pasangan']!="Profesional") echo "selected";?> value="Lainnya">Lainnya</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input name="nama_perusahaan_pasangan" type="text" class="form-control" placeholder="Nama Perusahaan" value="<?=$data_pernikahan['nama_perusahaan_pasangan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Pekerjaan Lainnya</label>
                                <input id="jenis_pekerjaan_pasangan_lainnya" disabled="disabled" name="jenis_pekerjaan_pasangan_lainnya" type="text" class="form-control" placeholder="Jenis Pekerjaan" value="<?=$data_pernikahan['pekerjaan_pasangan'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Bidang Usaha</label>
                                <input name="bidang_usaha_pasangan" type="text" class="form-control" placeholder="Bidang Usaha" value="<?=$data_pernikahan['bidang_usaha_pasangan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Mulai Bekerja Sejak(Tahun)</label>
                                <input name="mulai_bekerja_pasangan" type="text" class="form-control" placeholder="Mulai Bekerja Sejak" value="<?=$data_pernikahan['mulai_bekerja_pasangan'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Alamat</label>
                                <input name="alamat_perusahaan_pasangan" type="text" class="form-control" placeholder="Alamat" value="<?=$data_pernikahan['alamat_pasangan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kota</label>
                                <input name="kota_perusahaan_pasangan" type="text" class="form-control" placeholder="Kota" value="<?=$data_pernikahan['kota_pasangan'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Kode Pos</label>
                                <input name="kode_pos_perusahaan_pasangan" type="text" class="form-control" placeholder="Kode Pos" value="<?=$data_pernikahan['kode_pos_pasangan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Telp</label>
                                <input name="telp_perusahaan_pasangan" type="text" class="form-control" placeholder="Telp" value="<?=$data_pernikahan['telp_pasangan'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>HP</label>
                                <input name="hp_perusahaan_pasangan" type="text" class="form-control" placeholder="HP" value="<?=$data_pernikahan['hp_pasangan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jabatan</label>
                                <input name="jabatan_perusahaan_pasangan" type="text" class="form-control" placeholder="Jabatan" value="<?=$data_pernikahan['jabatan_pasangan'];?>">
                              </div>
                            </div>
                          </div>                            
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <a class="btn btn-secondary" href="#carouselExampleIndicators" role="button" data-slide="prev"> <i class="fa fa-arrow-left"></i> Sebelum </a> 
                            </div>
                            <div class="col-md-6 text-right">
                              <a class="btn btn-info" href="#carouselExampleIndicators" role="button" data-slide="next"> Selanjutnya <i class="fa fa-arrow-right"></i> </a> 
                            </div>
                          </div>
                        </div>
                        <?php } ?>

                        <!-- Data Pemberian Kredit -->
                        <div class="carousel-item dim-border">
                          <label-tabel-small> Data Pemberian Kredit </label-tabel-small>
                          <br><br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jumlah Kredit yang Diajukan</label>
                                <input  name="jumlah_kredit" type="text" class="form-control CentreeRupiah" placeholder="Jumlah Kredit" value="<?=$data['jumlah_kredit'];?>"  onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Keterangan</label>
                                <select id="keterangan"  name="keterangan" class="form-control select2" onchange="cek_keterangan(this.value);">
                                  <option <?php if($data['keterangan']=="Baru") echo "selected";?> value="Baru">Baru</option>
                                  <option <?php if($data['keterangan']!="Baru") echo "selected";?> value="Take Over">Take Over</option>
                                </select>
                              </div>
                            </div>
                          </div>       
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Jangka Waktu</label>
                                <input  name="jangka_waktu" type="text" class="form-control" placeholder="Jangka Waktu" value="<?=$data['jangka_waktu'];?>" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Take Over dari</label>
                                <input id="keterangan_take_over" disabled="disabled"  name="keterangan_take_over" type="text" class="form-control" placeholder="Take Over" value="<?=$data['keterangan'];?>" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Maksimum Angsuran yang Diinginkan</label>
                                <input  name="maksimum_angsuran" type="text" class="form-control CentreeRupiah" placeholder="Maksimum Angsuran" value="<?=$data['maksimum_angsuran'];?>" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6"><div class="form-group">
                                <label>Tujuan Penggunaan Kredit</label>
                                <select  id="tujuan_penggunaan" name="tujuan_penggunaan" class="form-control select2" onchange="cek_tujuan_kredit(this.value);">
                                  <option <?php if($data['tujuan_penggunaan']=="Rehabilitasi/Renovasi Rumah") echo "selected";?> value="Rehabilitasi/Renovasi Rumah">Rehabilitasi/Renovasi Rumah</option>
                                  <option <?php if($data['tujuan_penggunaan']=="Keperluan Rumah Tangga") echo "selected";?> value="Keperluan Rumah Tangga">Keperluan Rumah Tangga</option>
                                  <option <?php if($data['tujuan_penggunaan']=="Biaya Pendidikan") echo "selected";?> value="Biaya Pendidikan">Biaya Pendidikan</option>
                                  <option <?php if($data['tujuan_penggunaan']=="Biaya Pengobatan") echo "selected";?> value="Biaya Pengobatan">Biaya Pengobatan</option>
                                  <option <?php if($data['tujuan_penggunaan']=="Biaya Pernikahan/Perkawinan") echo "selected";?> value="Biaya Pernikahan/Perkawinan">Biaya Pernikahan/Perkawinan</option>
                                  <option <?php if($data['tujuan_penggunaan']=="KAG Mix") echo "selected";?> value="KAG Mix">KAG Mix</option>
                                  <option <?php if($data['tujuan_penggunaan']!="Rehabilitasi/Renovasi Rumah" && $data['tujuan_penggunaan']!="Keperluan Rumah Tangga" && $data['tujuan_penggunaan']!="Biaya Pendidikan" && $data['tujuan_penggunaan']!="Biaya Pengobatan" && $data['tujuan_penggunaan']!="Biaya Pernikahan/Perkawinan" && $data['tujuan_penggunaan']!="KAG Mix") echo "selected";?> value="Keperluan Lainnya yang bersifat konsumtif">Keperluan Lainnya yang bersifat konsumtif</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Penyaluran Kredit</label>
                                <input required=""  name="tanggal_penyaluran" type="text" class="form-control datetimepicker CentreeTgl" placeholder="Tanggal Penyaluran Kredit" autocomplete="off" onchange="" onblur="" value="<?=$data['tanggal_penyaluran'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tujuan Penggunaan Lainnya</label>
                                <input id="tujuan_penggunaan_lainnya" disabled="disabled"  name="tujuan_penggunaan_lainnya" type="text" class="form-control" placeholder="Tujuan Penggunaan Lainnya" value="<?=$data['tujuan_penggunaan'];?>"  onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Jatuh Tempo</label>
                                <input required="" name="tanggal_jatuh_tempo" type="text" class="form-control datetimepicker CentreeTgl"  autocomplete="off" placeholder="Tanggal Jatuh Tempo" onchange="" onblur="" value="<?=$data['tanggal_jatuh_tempo'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Term</label>
                                <input  name="term" type="text" class="form-control" placeholder="Term" onchange="" onblur="" value="<?=$data['term'];?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Periode Cicilan</label>
                                <select name="periode_cicilan" class="form-control select2">
                                    <option <?= selected_droplist($data['periode_cicilan'],"bulanan"); ?> value="bulanan">Bulanan</option>
                                    <option <?= selected_droplist($data['periode_cicilan'],"tahunan"); ?> value="tahunan">Tahunan</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Banyak Cicilan</label>
                                <input  name="banyak_cicilan" type="number" class="form-control" placeholder="Banyak Cicilan" value="<?=$data['banyak_cicilan'];?>" onchange="" onblur="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Tanggal Jatuh Tempo Cicilan</label>
                                <input  name="jatuh_tempo_cicilan" type="number" class="form-control" placeholder="Jatuh Tempo Cicilan"  value="<?=$data['jatuh_tempo_cicilan'];?>" onchange="" onblur="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label >Bunga</label>
                                <input  name="bunga" type="number" class="form-control" placeholder="Bunga" onchange="" onblur="" value="<?=$data['bunga'];?>">
                                <small>Angka diatas sudah dalam bentuk persen</small>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Nominal Cicilan</label>
                                <input  name="nominal_cicilan" type="text" class="form-control CentreeRupiah" placeholder="Nominal Cicilan" onchange="" onblur=""  value="<?=$data['nominal_cicilan'];?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                                <?php

                                ?>
                              <div class="form-group">
                                <label>Username Anggota</label><br>
                                <input id="nama_anggota" readonly="" type="text" class="form-control" placeholder="Pilih Anggota" style="width: 60%; display: inline-block;" onchange="" onblur="" required="" value="<?= $data_user['username']; ?>">
                                <button type="button" class="btn btn-primary" style="display: inline-block;" onclick="open_modal()"> <i class="fa fa-search"></i> Cari </button>
                                <input id="id_anggota" readonly="" name="anggota" type="text" class="form-control" placeholder="Pilih Aggota" style="width: 60%; display: none;" onchange="" onblur="" value="<?= $data_user['no_id']; ?>">
                              </div>
                            </div>
                          </div>
                          <div class="mt-3 row">
                            <div class="col-md-6">
                              <button class="btn btn-secondary" type="button" id="previousx" onclick='
                $("html, body").animate({scrollTop: 0}, 1000);'> <i class="fa fa-arrow-left"></i> Sebelum </button> 
                            </div>
                            <div class="col-md-6 text-right">
                              <button class="btn btn-info" type="submit" id="save"> <i class="fa fa-save"></i> Simpan  </button> 
                            </div>
                          </div>
                        </div>                        
                    </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


<!-- Modal Tambah Akun-->
<div class="modal fade" id="exampleModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mdlhdr">Mohon Tunggu . . .</h5>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="mdlbdy"></div>    
    </div>
  </div>
</div>

<script type="text/javascript">
    function open_modal(id="",action="tmbh"){
        $("#mdlhdr").empty(); 
        $("#mdlhdr").append("Browse Karyawan"); 
        get_with_ajax('id='+id, 'get_user_anggota', "mdlbdy");
        $('#exampleModal').modal('show');
    }

        $('document').ready(function(){
            let pekerjaan = $("#jenis_pekerjaan").val();
            let pekerjaan_pasangan = $("#jenis_pekerjaan_pasangan").val();
            let keterangan = $("#keterangan").val();
            let tujuan_kredit = $("#tujuan_penggunaan").val();
            let status_perkawinan = $("#status_perkawinan").val();
            cek_pekerjaan(pekerjaan);
            cek_pekerjaan_pasangan(pekerjaan_pasangan);
            cek_keterangan(keterangan);
            cek_tujuan_kredit(tujuan_kredit);
            cek_status_perkawinan(status_perkawinan);
        });
        //cek status kawin
        var status = "false"; //if true = sudah nikah
        function cek_status_perkawinan(value){
            if(value=="Nikah"){
               status = "true";
               console.log("nikah ="+status);
            }else{
               status = "false";
               console.log("nikah ="+status);
            }
        }

        function cek_pekerjaan(value){
          if(value=="Lainnya"){
            document.getElementById("jenis_pekerjaan_lainnya").disabled = "";
          }else{
            document.getElementById("jenis_pekerjaan_lainnya").disabled = "disabled";
          }
        }

        function cek_pekerjaan_pasangan(value){
          if(value=="Lainnya"){
            document.getElementById("jenis_pekerjaan_pasangan_lainnya").disabled = "";
          }else{
            document.getElementById("jenis_pekerjaan_pasangan_lainnya").disabled = "disabled";
          }
        }

        function cek_keterangan(value){
          if(value=="Take Over"){
            document.getElementById("keterangan_take_over").disabled = "";
          }else{
            document.getElementById("keterangan_take_over").disabled = "disabled";
          }
        }
        function cek_tujuan_kredit(value){
          if(value=="Keperluan Lainnya yang bersifat konsumtif"){
            document.getElementById("tujuan_penggunaan_lainnya").disabled = "";
          }else{
            document.getElementById("tujuan_penggunaan_lainnya").disabled = "disabled";
          }
        }

        $( document ).ready(function() {
            $("#nextx").click(function(){
                if(status=="false"){
                    console.log('false');
                     $("#carouselExampleIndicators").carousel(4); //Skip slide nikah
                }else{
                    console.log('true');
                    $("#carouselExampleIndicators").carousel("next");
                }
          });

            $("#previousx").click(function(){
                if(status=="false"){
                    console.log('false');
                     $("#carouselExampleIndicators").carousel(2); //Skip slide nikah
                }else{
                    console.log('true');
                    $("#carouselExampleIndicators").carousel("prev");
                }
          });
        });
         
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 

            var values = $(this).serialize();
            simple_ajax(values,'config/edit_debitor','debitor','Data Debitor Sukses Diubah','Gagal ubah Akun');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });
    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>