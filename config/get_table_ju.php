
<?php
    include 'config.php';
    cek_tahun_buku();

    @$tglAwal = $_POST['dari_tgl'];
    @$tglAkhir = $_POST['sampe'];
    @$tipe = $_POST['tipe']; //tipe pencarian
    $tahun_buku = $_SESSION['tahun_buku'];

    if(empty($tglAwal) && empty($tglAkhir)){
        $query = "select * from v_jurnal WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JU' ORDER BY no_jurnal,row ASC";
    }else{
        $query = "select * from v_jurnal WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JU' AND tanggal BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  ORDER BY no_jurnal,row ASC";
    }   

    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }

?>


<div class="border border-top-0 border-left-0 border-right-0 row" style="padding-bottom: 10px; margin-bottom: 20px; ">
<div class="col-md-8 col-xs-12 dim-center">
    <label-tabel-small>
    Periode  
    <?php 
    if(empty($tipe)){
        echo tgl_indo2($_SESSION['awal_periode'])." - ".tgl_indo2($_SESSION['akhir_periode']);
    }else{
        echo tgl_indo2($tglAwal)." - ".tgl_indo2($tglAkhir);
    }
    ?>
    </label-tabel-small>
</div>                    
<div class="col-md-4 col-xs-12 dim-center text-right">
    <button onclick="window.location = 'tambah_ju'" class="btn btn-success btn-sm">
            <i class="fa fa-plus"></i> Tambah 
    </button>
    <button id="edit" class="btn btn-primary btn-sm dim-hidden centree-btn-edit" onclick="edit()"> <i class="fa fa-pencil"></i> Ubah </button>
    <button id="hapus" class="btn btn-danger btn-sm dim-hidden centree-btn-hapus" onclick="deleteBuku()"> <i class="fa fa-trash"></i> Hapus </button>
    |
    <button id="print" class="btn btn-warning btn-sm" onclick="print()"> <i class="fa fa-print"></i> Print </button> 
    |
    <button id="print" class="btn btn-success btn-sm" onclick="printx()"> <i class="fa fa-file-excel-o "></i> Excel </button>
</div>

</div>
<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <table class="table table-bordered table-hover table-striped dataTable"  >
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>No.JU</th> 
                <th>No. Bukti</th>
                <th>No. Akun</th>
                <th>Nama Akun</th>
                <th>Debit</th>
                <th>Kredit</th>
            </tr>
        </thead>
        <tbody>
            
            <?php  
            $temp_ju = null;
            $temp_memo = null;
            $total_debit  = 0 ;
            $total_kredit = 0 ;
            while($row = mysqli_fetch_object($execute)){ 
                    $no_jurnal     = $row->no_jurnal;
                    if($temp_ju !== $no_jurnal){
                        $temp_ju    = $no_jurnal;
                        $tanggal    = tgl_indo($row->tanggal);
                        $no_bukti   = $row->no_bukti;

                        /*jika no jurnal tidak sama dengan temp berarti awal row baru
                         1. keluarkan temp memo => memo dari jurnal sebelumnya
                         2. set temp memo => memo dari jurnal sekarang*/
                        echo $temp_memo;

                        //set isi kolom, HARUS SESUAI DENGAN HEADER walau ada colspan
                        //4 kolom pertama wajib, agar saat dicari ter group
                        $arr = array($row->memo,$no_jurnal,$tanggal,$no_bukti,"","",""); 
                        $i = 0;
                        $temp_memo = '<tr class="dim-memo">';
                        while($i<count($arr)){
                            if($i==0){
                                $x = "<td colspan='".count($arr)."'>".$arr[$i]."</td>";
                            }else{
                                $x = "<td style='display: none;'>".$arr[$i]."</td>";
                            }
                            $temp_memo  = $temp_memo.$x;
                            $i++;
                        }
                        $temp_memo = $temp_memo."</tr>";
                        
                    }else{
                        $tanggal    = "<p style='display:none;>".tgl_indo($row->tanggal)."</p>'";
                        $no_bukti   = "<p style='display:none;>".$row->no_bukti."</p>'";
                        $no_jurnal  = "<p style='display:none;>".$row->no_jurnal."</p>'";
                    }
                    
                    $no_akun    = $row->no_akun;
                    $nama       = $row->nama_akun;
                    $ket        = $row->keterangan;
                    $debit = 0;
                    $kredit = 0; 
                    if($ket=='Debit'){
                        $debit = $row->nominal;
                        $total_debit = $total_debit+$row->nominal;
                    }else{
                        $kredit = $row->nominal;
                        $total_kredit = $total_kredit+$row->nominal;
                    }

                ?>
                
                    <tr onclick="select_me('<?= $temp_ju; ?>');" class="row<?= $temp_ju; ?>">
                        <td><?= $tanggal; ?></td>
                        <td><?= $no_jurnal; ?></td>
                        <td><?= $no_bukti; ?></td>
                        <td align="right"><?= $no_akun; ?></td>
                        <td><?= $nama; ?></td>
                        <td style="text-align: right;"><?= rupiah($debit, "Rp. "); ?></td>
                        <td style="text-align: right;"><?= rupiah($kredit, "Rp. "); ?></td>
                    </tr>
            <?php } echo $temp_memo; ?>
                    <tr>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td colspan="5" class="dim-saldo">Saldo</td>
                        <td class="dim-saldo-debit" style="text-align: right;"><?= rupiah($total_debit,"Rp. "); ?></td>
                        <td class="dim-saldo-kredit" style="text-align: right;"><?= rupiah($total_kredit,"Rp. "); ?></td>
                    </tr>
        </tbody>
    </table>
</div>

   <script type="text/javascript">
                        
    function select_me(id){
        $("tr").removeClass('dim-selected');
        $(".row"+id).toggleClass('dim-selected');
        var selected = $(".row"+id).hasClass( "dim-selected" );
        if(selected){
            selected_row = id;
        }else{
            selected_row = null;
        }
        console.log(selected_row);

        if(selected_row !==null){
            $('#edit').removeClass('dim-hidden');
            $('#hapus').removeClass('dim-hidden');
        }else{
            $('#edit').addClass('dim-hidden');
            $('#hapus').addClass('dim-hidden');
        }
    } 

    function edit(){
        window.location = 'edit_ju?id='+selected_row;
        
    }
    function print(){
        var tglAwal =  '<?= $tglAwal; ?>';
        var tglAkhir = '<?= $tglAkhir; ?>';
        window.open('print/print_jurnal_umum?tglAwal='+tglAwal+'&tglAkhir='+tglAkhir, '_blank');
    }
    function printx(){
        var tglAwal =  '<?= $tglAwal; ?>';
        var tglAkhir = '<?= $tglAkhir; ?>';
        window.open('print_excel/print_jurnal_umumx?tglAwal='+tglAwal+'&tglAkhir='+tglAkhir, '_blank');
    }

    function deleteBuku(){
        pesan_confirm("Apakah anda yakin?", "Hapus data Jurnal dengan No Jurnal='"+selected_row+"'", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("no_id="+selected_row,'config/delete_ju','','Jurnal berhasil dihapus','Jurnal gagal dihapus');
            }   
        });
    }
    </script>

    <script type="text/javascript">
        $('.dataTable').DataTable({
            "paging": false,
            "ordering": false
        });
    </script>

    <?php get_role_button(); ?>   