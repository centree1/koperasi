<?php
    include 'config.php';
    cek_tahun_buku();

    @$tglAwal = $_POST['dari_tgl'];
    @$tglAkhir = $_POST['sampe'];
    @$tipe = $_POST['tipe']; //tipe pencarian
    @$channeling = $_POST['channeling'];
    $tahun_buku = $_SESSION['tahun_buku'];

    if($channeling=="true"){
        $tipe_jurnal = "AND tipe_jurnal='JPKA_CHANNELING' ";
        $url_tambah  = "tambah_jpka_channeling";
        $url_edit    = "edit_jpka";
    }else{
        $tipe_jurnal = "AND tipe_jurnal='JPKA' ";
        $url_tambah  = "tambah_jpka";
        $url_edit    = "edit_jpka";
    }

    if(empty($tglAwal) && empty($tglAkhir)){
        $query = "select * from v_jurnal_debitor WHERE id_tahun_buku='".$tahun_buku."' ".$tipe_jurnal." ORDER BY no_jurnal,keterangan ASC";
    }else{
        $query = "select * from v_jurnal_debitor WHERE id_tahun_buku='".$tahun_buku."' ".$tipe_jurnal." AND tanggal BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  ORDER BY no_jurnal,keterangan ASC";
    }   

    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }
 
?>

<div class="border border-top-0 border-left-0 border-right-0 row" style="padding-bottom: 10px; margin-bottom: 20px; ">
<div class="col-md-8 col-xs-12 dim-center">
    <label-tabel-small>
    Periode  
    <?php 
    if(empty($tipe)){
        echo tgl_indo2($_SESSION['awal_periode'])." - ".tgl_indo2($_SESSION['akhir_periode']);
    }else{
        echo tgl_indo2($tglAwal)." - ".tgl_indo2($tglAkhir);
    }
    ?>
    </label-tabel-small>
</div>                    
<div class="col-md-4 col-xs-12 dim-center text-right">
    <button onclick="window.location = '<?= $url_tambah; ?>'" class="btn btn-success btn-sm">
            <i class="fa fa-plus"></i> Tambah 
    </button>
    <button id="edit" class="btn btn-primary btn-sm dim-hidden centree-btn-edit" onclick="edit()"> <i class="fa fa-pencil"></i> Ubah </button>
    <button id="hapus" class="btn btn-danger btn-sm dim-hidden centree-btn-hapus" onclick="deleteBuku()"> <i class="fa fa-trash"></i> Hapus </button> 
    <?php get_role_button(); ?>
    |
    <button id="print" class="btn btn-warning btn-sm" onclick="print()"> <i class="fa fa-print"></i> Print </button>
    |
    <button id="print" class="btn btn-warning btn-sm" onclick="printx()"> <i class="fa fa-print"></i> Excel </button>
</div>
</div>
<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <table class="table table-bordered table-hover table-striped dataTable"  >
        <thead>
            <tr>
                <th style="width: 3%">No. Jurnal</th>
                <th>Tanggal</th>
                <th>No. Bukti</th>
                <th>Jenis Kredit</th>
                <th style="width: 2%">Nomor Akun</th>
                <th>Nama Akun</th>
                <th>No. Id Debitor</th>
                <th>Debit</th>
                <th>Kredit</th>
            </tr>
        </thead>
        <tbody>
            <?php  
            $jenis_kredit    = null;
            $no_id_debitor   = null;
            $temp_memo       = null;
            $temp_tahun_buku = null;
            $temp_ju         = null;
            $total_debit     = 0 ;
            $total_kredit    = 0 ;
            while($row = mysqli_fetch_object($execute)){ 
                    $no_jurnal     = $row->no_jurnal;
                    if($temp_ju !== $no_jurnal){
                        $temp_ju    = $no_jurnal;
                        $tanggal    = tgl_indo($row->tanggal);
                        $no_bukti   = $row->no_bukti;
                        $no_jurnal  = $row->no_jurnal;
                        $jenis_kredit    = $row->jenis_kredit;
                        $no_id_debitor   = $row->no_id_debitor;

                        /*jika no jurnal tidak sama dengan temp berarti awal row baru
                         1. keluarkan temp memo => memo dari jurnal sebelumnya
                         2. set temp memo => memo dari jurnal sekarang*/
                        echo $temp_memo;

                        //set isi kolom, HARUS SESUAI DENGAN HEADER walau ada colspan
                        //4 kolom pertama wajib, agar saat dicari ter group
                        $arr = array($row->memo,$tanggal,$no_bukti,$jenis_kredit, $row->no_akun,$row->nama_akun,$no_id_debitor,$row->memo,"","",""); 
                        $i = 0;
                        $temp_memo = '<tr class="dim-memo">';
                        while($i<count($arr)){
                            if($i==0){
                                $x = "<td colspan='".count($arr)."'>".$arr[$i]."</td>";
                            }else{
                                $x = "<td style='display: none;'>".$arr[$i]."</td>";
                            }
                            $temp_memo  = $temp_memo.$x;
                            $i++;
                        }
                        $temp_memo = $temp_memo."</tr>";
                        
                    }else{
                        $tanggal    = "<p style='display:none;>".tgl_indo($row->tanggal)."</p>'";
                        $no_bukti   = "<p style='display:none;>".$row->no_bukti."</p>'";
                        $no_jurnal  = "<p style='display:none;>".$row->no_jurnal."</p>'";
                        $no_id_debitor  = "<p style='display:none;>".$row->no_id_debitor."</p>'";
                        $jenis_kredit  = "<p style='display:none;>".$row->jenis_kredit."</p>'";
                    }
                    
                    $no_akun    = $row->no_akun;
                    $nama       = $row->nama_akun;
                    $ket        = $row->keterangan;
                    $debit      = null;
                    $kredit     = null; 
                    if (preg_match('/\bDebit\b/', $ket)) {
                        $debit = rupiah($row->nominal,"Rp. ");
                        $total_debit = $total_debit+$row->nominal;
                    }else{
                        $kredit = rupiah($row->nominal,"Rp. ");
                        $total_kredit = $total_kredit+$row->nominal;
                    }

                ?>
                
                    <tr onclick="select_me('<?= $temp_ju; ?>');" class="row<?= $temp_ju; ?>">
                        <td><?= $no_jurnal; ?></td>
                        <td><?= $tanggal; ?></td>
                        <td><?= $no_bukti; ?></td>
                        <td><?= $jenis_kredit; ?></td>
                        <td align="right"><?= $no_akun; ?></td>
                        <td><?= $nama; ?></td>
                        <td><?= $no_id_debitor; ?></td>
                        <td style="text-align: right;"><?= $debit; ?></td>
                        <td style="text-align: right;"><?= $kredit; ?></td>
                    </tr>
            <?php } echo $temp_memo; ?>
        </tbody>
        <tr>
            <td colspan="7" class="dim-saldo">Saldo</td>
            <td class="dim-saldo-debit" style="text-align: right;"><?= rupiah($total_debit,"Rp. "); ?></td>
            <td class="dim-saldo-kredit" style="text-align: right;"><?= rupiah($total_kredit,"Rp. "); ?></td>
        </tr>
    </table>
</div>

   <script type="text/javascript">
                        
    function select_me(id){
        $("tr").removeClass('dim-selected');
        $(".row"+id).toggleClass('dim-selected');
        var selected = $(".row"+id).hasClass( "dim-selected" );
        if(selected){
            selected_row = id;
        }else{
            selected_row = null;
        }
        console.log(selected_row);

        if(selected_row !==null){
            $('#edit').removeClass('dim-hidden');
            $('#hapus').removeClass('dim-hidden');
        }else{
            $('#edit').addClass('dim-hidden');
            $('#hapus').addClass('dim-hidden');
        }
    } 

    function edit(){
        window.location = '<?= $url_edit; ?>?id='+selected_row;
        
    }

     function print(){
        var tglAwal =  '<?= $tglAwal; ?>';
        var tglAkhir = '<?= $tglAkhir; ?>';
        var channeling = '<?= $channeling; ?>';
        window.open('print/print_jurnal_pelunasan_kredit_anggota?tglAwal='+tglAwal+'&tglAkhir='+tglAkhir+'&channeling='+channeling, '_blank');
    }
    function printx(){
        var tglAwal =  '<?= $tglAwal; ?>';
        var tglAkhir = '<?= $tglAkhir; ?>';
        var channeling = '<?= $channeling; ?>';
        window.open('print_excel/print_jurnal_pelunasan_kredit_anggotax?tglAwal='+tglAwal+'&tglAkhir='+tglAkhir+'&channeling='+channeling, '_blank');
    }

    function deleteBuku(){
        pesan_confirm("Apakah anda yakin?", "Hapus data Jurnal dengan No Jurnal='"+selected_row+"'", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("no_id="+selected_row,'config/delete_jpk','','Jurnal berhasil dihapus','Jurnal gagal dihapus');
            }   
        });
    }
    </script>
    <script type="text/javascript">
        $('.dataTable').DataTable({
            "paging": false,
            "ordering": false
        });
    </script>