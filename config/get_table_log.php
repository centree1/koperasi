<?php
    include 'config.php'; 
 
    @$no_unique = $_POST['no_akun'];
    //$tahun_buku     = $_SESSION['tahun_buku'];
    if(empty($no_unique)){
        echo "<center> <b> Tidak ada Data yang dipilih </b> </center>";
        exit;
    }

    #inisialisasi array untuk di table
    $buku_besar_akun = array();
    $i = 0;

    #get data akun
    $query = "select * from tb_log WHERE no_unique='".$no_unique."'";
    $execute = mysqli_query($koneksi,$query);
?>
<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <table class="table table-bordered table-hover table-striped dataTable"  >
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Jam</th>
                <th>Log</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                while($row = mysqli_fetch_object($execute)){
            ?>
            <tr>
            <td> <?= tgl_indo($row->tanggal); ?> </td>
            <td> <?= $row->jam; ?> </td>
            <td> <?= $row->keterangan; ?> </td> 
            </tr>
            <?php } ?>
        </tbody>  
    </table>
</div>
 <script type="text/javascript">
     $('.dataTable').DataTable({
            "ordering": false
        });
 </script>