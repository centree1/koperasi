<?php
    include 'config.php';
    cek_tahun_buku();

    $id_tahun_buku = $_SESSION['tahun_buku'];
    @$no_akun = $_POST['no_akun'];
    //$tahun_buku     = $_SESSION['tahun_buku'];
    if(empty($no_akun)){
        echo "<center> <b> Tidak ada Data yang dipilih </b> </center>";
        exit;
    }

    #inisialisasi array untuk di table
    $buku_besar_akun = array();
    $i = 0;

    #get data akun
    $query = "select nama_akun,saldo_normal from tb_akun WHERE no_akun='".$no_akun."'";
    $execute = mysqli_query($koneksi,$query);
    $fetch = mysqli_fetch_object($execute);
    $nama  = $fetch->nama_akun;
 
    $sn    = $fetch->saldo_normal;


    #Get buku besar jurnal biasa
    $query = "select * from bb_akun WHERE no_akun='".$no_akun."' AND id_tahun_buku='".$id_tahun_buku."' ORDER BY tanggal ASC ";
    //im_debugging($query);
    $execute = mysqli_query($koneksi,$query);
?>
<div class="row" style="margin-bottom: 30px;">
<div class="col-md-6 col-xs-12 dim-center">
    <h6> Akun : <b> <?= $no_akun; ?></b> | <b><?= $nama; ?></b>    </h6>
</div>                    
<div class="col-md-6 col-xs-12 dim-center text-right">

                        <button id="print" class="btn btn-primary btn-sm" onclick="print()"> <i class="fa fa-print"></i> Print </button>
                        |
                        <button id="print" class="btn btn-success btn-sm" onclick="printx()"> <i class="fa fa-print"></i> Excel </button> 
 

    <h6>  Saldo Normal : <b> <?= $sn; ?></b> </h6>
</div>
</div>

<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <table class="table table-bordered table-hover table-striped dataTable"  >
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Deskripsi</th>
                <th>Debit</th>
                <th>Kredit</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $x = 0;
                $saldo = 0;
                while($row = mysqli_fetch_object($execute)){
                $saldo = $saldo + $row->saldo;

                $debit = null;
                $kredit = null;
                if (strpos($row->keterangan, 'Debit') !== false) {
                    $debit  = rupiah($row->nominal_pada_jurnal,"Rp. ");
                }else{
                    $kredit = rupiah($row->nominal_pada_jurnal,"Rp. ");
                }
            ?>
            <tr>
            <td> <?= tgl_indo($row->tanggal); ?> </td>
            <td> <?= $row->deskripsi; ?> </td>
            <td align="right"> <?= $debit; ?> </td>
            <td align="right"> <?= $kredit; ?> </td>
            <td align="right"> <?= rupiah($saldo,"Rp. "); ?> </td>
            </tr>
            <?php } ?>
        </tbody>  
    </table>
</div>

    <script type="text/javascript">
        $('.dataTable').DataTable({
            "ordering": false
        });

        function print(){
            var akun =  '<?= $no_akun; ?>';
             
              window.open('print/print_bb_akun?no_akun='+akun,'_blank');
        }
        function printx(){
            var akun =  '<?= $no_akun; ?>';
              window.open('print_excel/print_bb_akunx?no_akun='+akun, '_blank');
        }
    </script>