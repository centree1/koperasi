<?php
include 'config.php';
mysqli_autocommit($koneksi, FALSE);

@$id_debitor 			= $_POST['no_identitas_debitor'];
@$no_jurnal 			= $_POST['no_jurnal'];
@$tanggal 				= $_POST['tanggal'];
@$no_bukti 				= $_POST['no_bukti'];
@$memo 					= $_POST['memo'];
@$debit 				= $_POST['debit'];
@$kredit_bank 			= $_POST['kredit_bank'];
@$kredit_lainya 		= $_POST['kredit_lainya'];
$id_tahun_buku  		= $_SESSION['tahun_buku'];
$tipe_jurnal			= 'JPK';
@$saldo_debit 			= $_POST['saldo_debit'];
@$saldo_kredit_bank 	= $_POST['saldo_kredit_bank'];
@$saldo_kredit_lainya 	= $_POST['saldo_kredit_lainya'];
@$urutan_debit		 	= $_POST['pencairan_ke'];

if(empty($debit)){
	echo "Maaf, data debit/kredit tidak ditemukan";
	exit;
}

if(empty($kredit_bank) && empty($kredit_lainya)){
	echo "Maaf, Data Kredit tidak ditemukan";
	exit;
}

if(empty($no_jurnal)){
	echo "Maaf, No Jurnal tidak ditemukan";
	exit;
}


#Set Array
$tot_debit  = 0;
$tot_kredit = 0;

$arr_debit = array();
foreach ($debit as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit[$num]
	);
	$arr_debit[$num] = $arr_tmp;
	$tot_debit += (int)$saldo_debit[$num];
}

$arr_kredit_bank = array();
foreach ($kredit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_bank[$num]
	);
	$arr_kredit_bank[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_bank[$num];
}

$arr_kredit_lainya = array();
foreach ($kredit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_lainya[$num]
	);
	$arr_kredit_lainya[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_lainya[$num];
}
//im_debugging($tot_kredit);
//im_debugging($tot_debit);

#checking balance
if($tot_debit !== $tot_kredit){
	echo "Maaf, Debit & Kredit tidak Balance";
	exit;
}

#check jumlah yang disalurkan apakah lebih dari yg diajukan
$query = "SELECT
	j.id_debitor,
	sum( jk.nominal ) AS total_telah_disalurkan 
FROM
	tb_jurnal_debitor j,
	tb_jurnal_debitor_debit_kredit jk 
WHERE
	j.no_jurnal = jk.no_jurnal 
	AND jk.keterangan = 'Debit' 
	AND jk.no_jurnal <> '".$no_jurnal."'
	AND j.id_debitor = '".$id_debitor."'
GROUP BY
	j.id_debitor";
$exe   = mysqli_query($koneksi, $query);
$data_debitor = mysqli_fetch_object($exe);
if(!empty($data_debitor)){
	$total_pengajuan = (int)$data_debitor->total_pengajuan - (int)$data_debitor->total_telah_disalurkan;
	if($tot_debit > $total_pengajuan){
	echo "Maaf, jumlah yang ingin disalurkan lebih besar dari yang telah diajukan";
	exit;
}
}



#1 Update ke table jurnal
$query  = 'UPDATE tb_jurnal_debitor SET no_bukti="'.$no_bukti.'",tanggal="'.$tanggal.'",memo="'.$memo.'",urutan_debit="'.$urutan_debit.'" where no_jurnal="'.$no_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);
if(!$exec)
{
	echo $query;
	exit;
}

#2 delete all in tb_jurnal_debit_kredit
$query  = 'DELETE FROM tb_jurnal_debitor_debit_kredit WHERE no_jurnal="'.$no_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);


$x=1;
foreach ($arr_debit as $num => $row) {
	$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$row->no_akun.'","'.$num.'","'.$row->saldo.'","Debit")';
	$exec   = mysqli_query($koneksi,$query);

	if(!empty($arr_kredit_bank[$num])){
		$no_akunx 	= $arr_kredit_bank[$num]->no_akun;
		$saldox		= $arr_kredit_bank[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Bank")';
		$exec   = mysqli_query($koneksi,$query);
	}

	if(!empty($arr_kredit_lainya[$num])){
		$no_akunx 	= $arr_kredit_lainya[$num]->no_akun;
		$saldox		= $arr_kredit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Lainya")';
		$exec   = mysqli_query($koneksi,$query);
	}
}

insert_log($no_jurnal,"Mengubah Data");
mysqli_commit($koneksi);
echo 1;
?>