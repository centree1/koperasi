<?php
	include 'config.php';
	$id = @$_POST['id'];
	if(empty($id)){
		exit;
	}
	//kredit pada jkk = Debit
	//debit pada jkk = kredit
    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_debit   = mysqli_fetch_all($result,MYSQLI_ASSOC);


    $query  = "SELECT no_akun, nama_akun from tb_akun where tipe_akun=20";
    $result = mysqli_query($koneksi,$query);
    $data_bank   = mysqli_fetch_all($result,MYSQLI_ASSOC);


    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_kredit   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    
?>
<tr>
    <td  class="bg-debit"  >
    	<button type="button" class="btn btn-sm btn-danger" id="min-<?= $id; ?>" onclick="delete_row('min-<?= $id; ?>')"> 
    		<i class="fa fa-minus "></i>
    	</button>
    </td>

<!-- Debit -------------------------------------------->
<td class="bg-debit" > 
	<select class="form-control select22" name="debit[<?= $id; ?>]">
		<option value="">--</option>
		<?php foreach ($data_debit as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="bg-debit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_debit[<?= $id; ?>]">
	</div>
</td>

<!-- End Debit -------------------------------------------->


<!-- Bank -------------------------------------------->
<td class="bg-kredit" > 
	<select class="form-control select22" name="kredit_bank[<?= $id; ?>]" id="kredit_bank_<?= $id; ?>">
		<option value="">--</option>
		<?php foreach ($data_bank as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="bg-kredit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo"  name="saldo_kredit_bank[<?= $id; ?>]" id="saldo_kredit_bank_<?= $id; ?>">
	</div>
</td>

<!-- End Bank -------------------------------------------->

<!-- Kredit lainya -------------------------------------------->
<td class="bg-kredit" > 
	<select class="form-control select22"   name="kredit_lainya[<?= $id; ?>]" id="kredit_lainya_<?= $id; ?>"   name="saldo_kredit_lainya[]" id="saldo_kredit_lainya_<?= $id; ?>">
		<option value="">--</option>
		<?php foreach ($data_kredit as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="bg-kredit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_lainya[<?= $id; ?>]">
	</div>
</td>

<!-- End Kredit Lainya -------------------------------------------->
</tr>

<script type="text/javascript">
$('.select22').select2({
    theme: "bootstrap",
    width: "100%",
    placeholder:"--",
  allowClear: true
});


$(document).ready(function() {
    $('#kredit_bank_<?= $id; ?>').change(function() { 
	});
}); 

</script>

<script type="text/javascript">
	$( ".CentreeRupiah" ).CentreeRupiah();
	$(".CentreeRupiah").keyup(function(){
          //get attribut
        var name    = $(this).attr('name');
        var name_tmp  = name.split("___");
        var name_real = name_tmp[1];
        var angka     = $(this).val();
        var prefix    = "Rp. ";

        //change rupiah
        returnx = CentreeFormatRupiah(angka);

        var value  = $(this).val(returnx[0]);
        $("input[name='"+name_real+"']").val(returnx[1]); 

    });
</script>