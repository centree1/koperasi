 <?php
 include 'config.php';
 cek_tahun_buku();
@$tglAwal = $_POST['dari_tgl'];
@$tglAkhir = $_POST['sampe'];
@$tipe = $_POST['tipe']; //tipe pencarian
$tahun_buku = $_SESSION['tahun_buku'];

if(empty($tglAwal) && empty($tglAkhir) && empty($tipe) ){
    //$query = "SELECT *, c.jumlah_telah_dibayar as total_sudah_dilunasi, c.jumlah_cicilan as total_cicilan_dilunasi  FROM tb_debitor, tb_debitor_pribadi, tb_kredit_salur, v_cicilan_debitor_2 c where c.no_id_debitor = tb_debitor.no_id_debitor AND tb_debitor.no_id_debitor=tb_debitor_pribadi.no_id_debitor AND tb_debitor.id_kredit_salur=tb_kredit_salur.id_kredit_salur AND tb_debitor.id_tahun_buku = '".$tahun_buku."'";
    $query = "SELECT *, c.jumlah_telah_dibayar as total_sudah_dilunasi, c.jumlah_cicilan as total_cicilan_dilunasi  FROM tb_debitor, tb_debitor_pribadi, tb_kredit_salur, v_cicilan_debitor_2 c where c.no_id_debitor = tb_debitor.no_id_debitor AND tb_debitor.no_id_debitor=tb_debitor_pribadi.no_id_debitor AND tb_debitor.id_kredit_salur=tb_kredit_salur.id_kredit_salur";
}else{
    //$query = "SELECT * , c.jumlah_telah_dibayar as total_sudah_dilunasi, c.jumlah_cicilan as total_cicilan_dilunasi FROM tb_debitor, tb_debitor_pribadi, tb_kredit_salur, v_cicilan_debitor_2 c where tb_debitor.no_id_debitor=tb_debitor_pribadi.no_id_debitor AND tb_debitor.id_kredit_salur=tb_kredit_salur.id_kredit_salur AND ".$tipe." BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND tb_debitor.id_tahun_buku = '".$tahun_buku."' ";
    $query = "SELECT * , c.jumlah_telah_dibayar as total_sudah_dilunasi, c.jumlah_cicilan as total_cicilan_dilunasi FROM tb_debitor, tb_debitor_pribadi, tb_kredit_salur, v_cicilan_debitor_2 c where tb_debitor.no_id_debitor=tb_debitor_pribadi.no_id_debitor AND tb_debitor.id_kredit_salur=tb_kredit_salur.id_kredit_salur AND ".$tipe." BETWEEN '".$tglAwal."' AND '".$tglAkhir."' group by tb_debitor.no_id_debitor";
}   


$query=mysqli_query($koneksi, $query); ?>

 <table class="table table-bordered table-hover table-striped dataTable"  >
    <thead>
        <tr>
            <th>No</th>
            <th>No ID Debitor</th>
            <th>Nama Debitor</th>
            <th>Jenis Kredit</th>
            <th>Tanggal Transaksi</th>
            <th>Tanggal Jatuh Tempo Pelunasan</th>
            <th>Term</th>
            <th>Banyak Cicilan</th>
            <th>Nominal Kredit</th>
            <th>Total yang sudah dilunasi</th>
            <th>Saldo Akhir</th>
            <th>Sisa Masa Cicilan</th>
        </tr>
    </thead>
    <tbody> 
    <?php
    $a=0;
    while($row=mysqli_fetch_array($query)){
    $a++;
    ?>
    <tr onclick="select_me(<?=$row['id_debitor'];?>);" id="row<?=$row['id_debitor'];?>">
        <td><?=$a;?></td>
        <td><?=$row['no_id_debitor'];?></td>
        <td><?=$row['nama'];?></td>
        <td><?=$row['jenis_kredit'];?></td>
        <td><?=tgl_indo($row['tanggal_penyaluran']);?></td>
        <td><?=tgl_indo($row['tanggal_jatuh_tempo']);?></td>
        <td><?=$row['term'];?></td>
        <td><?= $row['banyak_cicilan']; ?> Kali</td>
        <td><?=rupiah($row['jumlah_kredit'],'Rp. '); ?></td>
        <td><?=rupiah($row['total_sudah_dilunasi'],'Rp. '); ?></td>
        <td><?=rupiah($row['jumlah_kredit']-$row['total_sudah_dilunasi'],'Rp. '); ?></td>
        <td><?php
        if($row['jumlah_kredit']==$row['total_sudah_dilunasi']){
            echo "-";
        }else{
            echo $row['banyak_cicilan']-$row['total_cicilan_dilunasi']." Kali"; 
        } ?> </td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<script type="text/javascript">
        $('.dataTable').DataTable();
    </script>