<?php
include 'config.php';
mysqli_autocommit($koneksi, FALSE);
@$id_debitor 			= $_POST['no_identitas_debitor'];
@$no_jurnal 			= $_POST['no_jurnal'];
@$tanggal 				= $_POST['tanggal'];
@$no_bukti 				= $_POST['no_bukti'];
@$memo 					= $_POST['memo'];
@$debit 				= $_POST['debit'];
@$kredit_bank 			= $_POST['kredit_bank'];
@$kredit_lainya 		= $_POST['kredit_lainya'];
$id_tahun_buku  		= $_SESSION['tahun_buku'];
$tipe_jurnal			= 'JPK';
@$saldo_debit 			= $_POST['saldo_debit'];
@$saldo_kredit_bank 	= $_POST['saldo_kredit_bank'];
@$saldo_kredit_lainya 	= $_POST['saldo_kredit_lainya'];
@$urutan_debit		 	= $_POST['pencairan_ke'];

if(empty($debit)){
	echo "Maaf, data debit/kredit tidak ditemukan";
	exit;
}

if(empty($kredit_bank) && empty($kredit_lainya)){
	echo "Maaf, Data Kredit tidak ditemukan";
	exit;
}

if(empty($no_jurnal)){
	echo "Maaf, No Jurnal tidak ditemukan";
	exit;
}


#Set Array
$tot_debit  = 0;
$tot_kredit = 0;

$arr_debit = array();
foreach ($debit as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit[$num]
	);
	$arr_debit[$num] = $arr_tmp;
	$tot_debit += (int)$saldo_debit[$num];
}

$arr_kredit_bank = array();
foreach ($kredit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_bank[$num]
	);
	$arr_kredit_bank[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_bank[$num];
}

$arr_kredit_lainya = array();
foreach ($kredit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_lainya[$num]
	);
	$arr_kredit_lainya[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_lainya[$num];
}
//im_debugging($tot_kredit);
//im_debugging($tot_debit);
//im_debugging($arr_kredit_lainya);


#checking balance
if($tot_debit !== $tot_kredit){
	echo "Maaf, Debit & Kredit tidak Balance";
	exit;
}

#check jumlah yang disalurkan apakah lebih dari yg diajukan
$query = "select total_pengajuan,total_telah_disalurkan from v_pencairan_debitor where id_debitor='".$id_debitor."'";
$exe   = mysqli_query($koneksi, $query);
$data_debitor = mysqli_fetch_object($exe);
$total_pengajuan = (int)$data_debitor->total_pengajuan - (int)$data_debitor->total_telah_disalurkan;
if($tot_debit > $total_pengajuan){
	echo "Maaf, jumlah yang ingin disalurkan lebih besar dari yang telah diajukan";
	exit;
}



#1 insert ke table jurnal
$query  = 'INSERT INTO tb_jurnal_debitor VALUES ("'.$no_jurnal.'","'.$no_bukti.'","'.$id_debitor.'","'.$tanggal.'","'.$memo.'","'.$tipe_jurnal.'","'.$id_tahun_buku.'","'.$urutan_debit.'")';
$exec   = mysqli_query($koneksi,$query);
if(!$exec)
{
	echo $query;
	exit;
}

$x=1;
foreach ($arr_debit as $num => $row) {
	$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$row->no_akun.'","'.$num.'","'.$row->saldo.'","Debit")';
	$exec   = mysqli_query($koneksi,$query);

	if(!empty($arr_kredit_bank[$num])){
		$no_akunx 	= $arr_kredit_bank[$num]->no_akun;
		$saldox		= $arr_kredit_bank[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Bank")';
		$exec   = mysqli_query($koneksi,$query);
	}

	if(!empty($arr_kredit_lainya[$num])){
		$no_akunx 	= $arr_kredit_lainya[$num]->no_akun;
		$saldox		= $arr_kredit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Lainya")';
		$exec   = mysqli_query($koneksi,$query);
	}
}
mysqli_commit($koneksi);
echo 1;
?>