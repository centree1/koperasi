<?php
	include 'config.php';
	$id = @$_POST['id'];
	if(empty($id)){
		exit;
	}
	//kredit pada jkm = kredit
	//debit pada jkm = debit
    $query  = "SELECT no_akun, nama_akun from tb_akun where saldo_normal = 'Debit'";
    $result = mysqli_query($koneksi,$query);
    $data_debit   = mysqli_fetch_all($result,MYSQLI_ASSOC);


    $query  = "SELECT no_akun, nama_akun from tb_akun where saldo_normal = 'Debit' AND tipe_akun=20";
    $result = mysqli_query($koneksi,$query);
    $data_bank   = mysqli_fetch_all($result,MYSQLI_ASSOC);


    $query  = "SELECT no_akun, nama_akun from tb_akun where saldo_normal = 'Kredit'";
    $result = mysqli_query($koneksi,$query);
    $data_kredit   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    
?>
<tr>
    <td  class="header-debit"  >
    	<button type="button" class="btn btn-sm btn-danger" id="min-<?= $id; ?>" onclick="delete_row('min-<?= $id; ?>')"> 
    		<i class="fa fa-minus "></i>
    	</button>
    </td>


<!-- Bank -------------------------------------------->
<td class="header-debit" > 
	<select class="form-control select22" name="debit_bank[<?= $id; ?>]" id="debit_bank_<?= $id; ?>">
		<option value="">--</option>
		<?php foreach ($data_bank as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="header-debit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control" placeholder="Saldo"  name="saldo_debit_bank[<?= $id; ?>]" id="saldo_debit_bank_<?= $id; ?>">
	</div>
</td>

<!-- End Bank -------------------------------------------->

<!-- Debit -------------------------------------------->
<td class="header-bank" > 
	<select class="form-control select22" name="debit[<?= $id; ?>]">
		<option value="">--</option>
		<?php foreach ($data_debit as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="header-bank">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control" placeholder="Saldo" name="saldo_debit[<?= $id; ?>]">
	</div>
</td>

<!-- End Debit -------------------------------------------->



<!-- Kredit lainya -------------------------------------------->
<td class="header-kredit" > 
	<select class="form-control select22"   name="kredit_lainya[<?= $id; ?>]" id="kredit_lainya_<?= $id; ?>"   name="saldo_kredit_lainya[]" id="saldo_kredit_lainya_<?= $id; ?>">
		<option value="">--</option>
		<?php foreach ($data_kredit as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="header-kredit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control" placeholder="Saldo" name="saldo_kredit_lainya[<?= $id; ?>]">
	</div>
</td>

<!-- End Kredit Lainya -------------------------------------------->
</tr>

<script type="text/javascript">
$('.select22').select2({
    theme: "bootstrap",
    width: "100%",
    placeholder:"--",
  allowClear: true
});


$(document).ready(function() {
    $('#kredit_bank_<?= $id; ?>').change(function() { 
	});
}); 

</script>