<?php
include 'config.php';
$id_debitor = $_POST['id_debitor'];
$id_kredit_salur = $_POST['id_kredit_salur'];
$no_id_debitor = $_POST['no_identitas_debitor'];
// Data Pribadi
$nama = $_POST['nama'];
$jenis_kelamin = $_POST['jenis_kelamin'];
$tempat_lahir = $_POST['tempat_lahir'];
$tanggal_lahir = $_POST['tanggal_lahir'];
$warga_negara = $_POST['warga_negara'];
$nama_ibu = $_POST['nama_ibu'];
$npwp = $_POST['npwp'];
$no_ktp_passport = $_POST['no_ktp_passpor'];
$agama = $_POST['agama'];
$email = $_POST['email'];
$alamat_ktp = $_POST['alamat_ktp'];
$kota_ktp = $_POST['kota_ktp'];
$kode_pos_ktp = $_POST['kode_pos_ktp'];
$telp_ktp = $_POST['telp'];
$hp_ktp = $_POST['hp'];
$alamat_sekarang = $_POST['alamat_sekarang'];
$ditempati_sejak_sekarang = $_POST['ditempati_sejak_sekarang'];
$kota_sekarang = $_POST['kota_sekarang'];
$kode_pos_sekarang = $_POST['kode_pos_sekarang'];
$telp_sekarang = $_POST['telp_sekarang'];
$hp_sekarang = $_POST['hp_sekarang'];
$status_kepemilikan = $_POST['status_kepemilikan'];
$alamat_surat = $_POST['alamat_surat'];
$kota_surat = $_POST['kota_surat'];
$pendidikan_terakhir = $_POST['pendidikan_terakhir'];
$status_perkawinan = $_POST['status_perkawinan'];
$jumlah_tanggungan = $_POST['jumlah_tanggungan'];
$nama_keperluan = $_POST['nama_keperluan'];
$alamat_keperluan = $_POST['alamat_keperluan'];
$kota_keperluan = $_POST['kota_keperluan'];
$kode_pos_keperluan = $_POST['kode_pos_keperluan'];
$telp_keperluan = $_POST['telp_keperluan'];
$hp_keperluan = $_POST['hp_keperluan'];
$hubungan_keperluan = $_POST['hubungan_keperluan'];
// Data Pekerjaan
$jenis_pekerjaan = $_POST['jenis_pekerjaan'];
if($jenis_pekerjaan=="Lainnya")
	$jenis_pekerjaan = $_POST['jenis_pekerjaan_lainnya'];
$nama_perusahaan = $_POST['nama_perusahaan'];
$bidang_usaha = $_POST['bidang_usaha'];
$alamat_perusahaan = $_POST['alamat_perusahaan'];
$kota_perusahaan = $_POST['kota_perusahaan'];
$kode_pos_perusahaan = $_POST['kode_pos_perusahaan'];
$telp_perusahaan = $_POST['telp_perusahaan'];
$hp_perusahaan = $_POST['hp_perusahaan'];
$mulai_bekerja = $_POST['mulai_bekerja'];
$jabatan_perusahaan = $_POST['jabatan_perusahaan'];
$nama_atasan = $_POST['nama_atasan'];
// Data Kredit
$jumlah_kredit = $_POST['jumlah_kredit'];
$keterangan = $_POST['keterangan'];
if($keterangan=="Take Over")
	$keterangan = "Take Over dari ".$_POST['keterangan_take_over'];
$jangka_waktu = $_POST['jangka_waktu'];
$maksimum_angsuran = $_POST['maksimum_angsuran'];
$tujuan_penggunaan = $_POST['tujuan_penggunaan'];
if($tujuan_penggunaan=="Keperluan Lainnya yang bersifat konsumtif")
	$tujuan_penggunaan = $_POST['tujuan_penggunaan_lainnya'];
$tanggal_penyaluran = $_POST['tanggal_penyaluran'];
$tanggal_jatuh_tempo = $_POST['tanggal_jatuh_tempo'];
$term = $_POST['term'];
$periode_cicilan = $_POST['periode_cicilan'];
$banyak_cicilan = $_POST['banyak_cicilan'];
$jatuh_tempo_cicilan = $_POST['jatuh_tempo_cicilan'];
$bunga = $_POST['bunga'];
$nominal_cicilan = $_POST['nominal_cicilan'];
$id_user = $_POST['anggota']; //id anggota

$query = mysqli_query($koneksi,'UPDATE tb_debitor_pribadi SET nama="'.$nama.'",jenis_kelamin="'.$jenis_kelamin.'",tempat_lahir="'.$tempat_lahir.'",tanggal_lahir="'.$tanggal_lahir.'",warga_negara="'.$warga_negara.'",nama_ibu="'.$nama_ibu.'",npwp="'.$npwp.'",no_ktp_passport="'.$no_ktp_passport.'",agama="'.$agama.'",email="'.$email.'",alamat_ktp="'.$alamat_ktp.'",kota_ktp="'.$kota_ktp.'",kode_pos_ktp="'.$kode_pos_ktp.'",telp_ktp="'.$telp_ktp.'",hp_ktp="'.$hp_ktp.'",alamat_sekarang="'.$alamat_sekarang.'",ditempati_sejak_sekarang="'.$ditempati_sejak_sekarang.'",kota_sekarang="'.$kota_sekarang.'",kode_pos_sekarang="'.$kode_pos_sekarang.'",telp_sekarang="'.$telp_sekarang.'",hp_sekarang="'.$hp_sekarang.'",status_kepemilikan="'.$status_kepemilikan.'",alamat_surat="'.$alamat_surat.'",kota_surat="'.$kota_surat.'",pendidikan_terakhir="'.$pendidikan_terakhir.'",status_perkawinan="'.$status_perkawinan.'",jumlah_tanggungan="'.$jumlah_tanggungan.'",nama_keperluan="'.$nama_keperluan.'",alamat_keperluan="'.$alamat_keperluan.'",kota_keperluan="'.$kota_keperluan.'",kode_pos_keperluan="'.$kode_pos_keperluan.'",telp_keperluan="'.$telp_keperluan.'",hp_keperluan="'.$hp_keperluan.'",hubungan_keperluan="'.$hubungan_keperluan.'" WHERE no_id_debitor="'.$no_id_debitor.'"');
if($query){	
	$query2 = mysqli_query($koneksi,'UPDATE tb_debitor_pekerjaan SET jenis_pekerjaan="'.$jenis_pekerjaan.'",nama_perusahaan="'.$nama_perusahaan.'",bidang_usaha="'.$bidang_usaha.'",alamat="'.$alamat_perusahaan.'",kota="'.$kota_perusahaan.'",kode_pos="'.$kode_pos_perusahaan.'",telp="'.$telp_perusahaan.'",hp="'.$hp_perusahaan.'",mulai_bekerja="'.$mulai_bekerja.'",jabatan="'.$jabatan_perusahaan.'",nama_atasan="'.$nama_atasan.'" WHERE no_id_debitor="'.$no_id_debitor.'"');
	if($query2){
		$query3 = mysqli_query($koneksi,'UPDATE tb_debitor SET jumlah_kredit="'.$jumlah_kredit.'",keterangan="'.$keterangan.'",jangka_waktu="'.$jangka_waktu.'",maksimum_angsuran="'.$maksimum_angsuran.'",tujuan_penggunaan="'.$tujuan_penggunaan.'",tanggal_penyaluran="'.$tanggal_penyaluran.'",tanggal_jatuh_tempo="'.$tanggal_jatuh_tempo.'",term="'.$term.'",periode_cicilan="'.$periode_cicilan.'",banyak_cicilan="'.$banyak_cicilan.'",jatuh_tempo_cicilan="'.$jatuh_tempo_cicilan.'",bunga="'.$bunga.'",nominal_cicilan="'.$nominal_cicilan.'",id_user="'.$id_user.'" WHERE id_debitor="'.$id_debitor.'"');
		if($query3){
			// Data Suami Istri
			if($status_perkawinan=="Nikah"){
				$nama_pasangan = $_POST['nama_pasangan'];
				$tempat_lahir_pasangan = $_POST['tempat_lahir_pasangan'];
				$tanggal_lahir_pasangan = $_POST['tanggal_lahir_pasangan'];
				$jenis_pekerjaan_pasangan = $_POST['jenis_pekerjaan_pasangan'];
				if($jenis_pekerjaan_pasangan=="Lainnya")
					$jenis_pekerjaan_pasangan = $_POST['jenis_pekerjaan_pasangan_lainnya'];
				$nama_perusahaan_pasangan = $_POST['nama_perusahaan_pasangan'];
				$bidang_usaha_pasangan = $_POST['bidang_usaha_pasangan'];
				$mulai_bekerja_pasangan = $_POST['mulai_bekerja_pasangan'];
				$alamat_perusahaan_pasangan = $_POST['alamat_perusahaan_pasangan'];
				$kota_perusahaan_pasangan = $_POST['kota_perusahaan_pasangan'];
				$kode_pos_perusahaan_pasangan = $_POST['kode_pos_perusahaan_pasangan'];
				$telp_perusahaan_pasangan = $_POST['telp_perusahaan_pasangan'];
				$hp_perusahaan_pasangan = $_POST['hp_perusahaan_pasangan'];
                $jabatan_perusahaan_pasangan = $_POST['jabatan_perusahaan_pasangan'];
                $query_cek = mysqli_query($koneksi, "select * from tb_debitor_pekerjaan_pasangan where tb_debitor_pekerjaan_pasangan.no_id_debitor = '".$no_id_debitor."'");
                $num = mysqli_num_rows($query_cek);
                if($num < 1 ){
                    $query4 = mysqli_query($koneksi,'INSERT INTO tb_debitor_pekerjaan_pasangan(no_id_debitor,nama_pasangan,tempat_lahir_pasangan,tanggal_lahir_pasangan,pekerjaan_pasangan,nama_perusahaan_pasangan,bidang_usaha_pasangan,mulai_bekerja_pasangan,alamat_pasangan,jabatan_pasangan,kota_pasangan,kode_pos_pasangan,telp_pasangan,hp_pasangan) VALUES ("'.$no_id_debitor.'","'.$nama_pasangan.'","'.$tempat_lahir_pasangan.'","'.$tanggal_lahir_pasangan.'","'.$jenis_pekerjaan_pasangan.'","'.$nama_perusahaan_pasangan.'","'.$bidang_usaha_pasangan.'","'.$mulai_bekerja_pasangan.'","'.$alamat_perusahaan_pasangan.'","'.$jabatan_perusahaan_pasangan.'","'.$kota_perusahaan_pasangan.'","'.$kode_pos_perusahaan_pasangan.'","'.$telp_perusahaan_pasangan.'","'.$hp_perusahaan_pasangan.'")');
                    if($query4){
                    	insert_log("Debitor : ".$no_id_debitor,"Mengubah Data");
                        echo 1;
                    }else{
                        echo "Gagal Mengubah Data Kredit";
                    }
                }else{
                    $query4 = mysqli_query($koneksi,'UPDATE tb_debitor_pekerjaan_pasangan SET nama_pasangan="'.$nama_pasangan.'",tempat_lahir_pasangan="'.$tempat_lahir_pasangan.'",tanggal_lahir_pasangan="'.$tanggal_lahir_pasangan.'",pekerjaan_pasangan="'.$jenis_pekerjaan_pasangan.'",nama_perusahaan_pasangan="'.$nama_perusahaan_pasangan.'",bidang_usaha_pasangan="'.$bidang_usaha_pasangan.'",mulai_bekerja_pasangan="'.$mulai_bekerja_pasangan.'",alamat_pasangan="'.$alamat_perusahaan_pasangan.'",jabatan_pasangan="'.$jabatan_perusahaan_pasangan.'",kota_pasangan="'.$kota_perusahaan_pasangan.'",kode_pos_pasangan="'.$kode_pos_perusahaan_pasangan.'",telp_pasangan="'.$telp_perusahaan_pasangan.'",hp_pasangan="'.$hp_perusahaan_pasangan.'" WHERE no_id_debitor="'.$no_id_debitor.'"');
                    if($query4){
                    	insert_log("Debitor : ".$no_id_debitor,"Mengubah Data");
                        echo 1;
                    }else{
                        echo "Gagal Mengubah Data Debitor";
                    }
                }
			}else{
                insert_log("Debitor : ".$no_id_debitor,"Mengubah Data");
				echo 1;
			}
		}else{
			echo "Gagal Mengubah Data Debitor";
		}
	}else{
		echo "Gagal Mengubah Data Debitor";
	}
}
else
{
	echo "Gagal Mengubah Data Debitor";
}
?>