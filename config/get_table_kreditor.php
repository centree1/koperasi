<?php
    include 'config.php';
    cek_tahun_buku();
    
    $tglAwal = $_POST['dari_tgl'];
    $tglAkhir = $_POST['sampe'];
    $tipe = $_POST['tipe'];
    $tahun_buku = $_SESSION['tahun_buku'];
    $GLOBALS['conn'] = $koneksi;

    if(empty($tipe) && empty($tglAwal) && empty($tglAkhir)){
        $query = "select * from v_kreditor   ORDER BY tanggal_peminjaman ASC";
    }else{
        $query = "select * from v_kreditor where  ".$tipe." BETWEEN '".$tglAwal."' AND '".$tglAkhir."' ORDER BY tanggal_peminjaman ASC";
    }   

    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }

    function get_total_dilunasi($no_akun,$tahun_buku){
        $query =  "SELECT  sum( dk.nominal ) as jumlah FROM tb_jurnal_debit_kredit dk, tb_jurnal dj WHERE dk.no_jurnal = dj.no_jurnal AND dj.tipe_jurnal = 'JKK' AND  dk.no_akun = '".$no_akun."'";
        $data = mysqli_fetch_object(mysqli_query($GLOBALS['conn'],$query));
        if(!empty($data)){
            return $data->jumlah;
        }else{
            return '0';
        }
    }

    function total_cicilan_dilunasi($no_akun,$tahun_buku){
        $query =  "SELECT  count( dk.nominal ) as jumlah FROM tb_jurnal_debit_kredit dk, tb_jurnal dj WHERE dk.no_jurnal = dj.no_jurnal AND dj.tipe_jurnal = 'JKK' AND  dk.no_akun = '".$no_akun."'";
        $data = mysqli_fetch_object(mysqli_query($GLOBALS['conn'],$query));
        if(!empty($data)){
            return $data->jumlah;
        }else{
            return '0';
        }
    }

?>

<div class="border border-top-0 border-left-0 border-right-0 row" style="padding-bottom: 10px; margin-bottom: 20px; ">
<div class="col-md-8 col-xs-12 dim-center">
    <!-- <label-tabel-small>
    Periode <?= tgl_indo2($_SESSION['awal_periode'])." - ".tgl_indo2($_SESSION['akhir_periode']); ?>  
    <?php if($tipe == "tanggal_peminjaman"){  echo "| Tanggal Peminjaman : ".$tglAwal." - ".$tglAkhir;  }elseif($tipe == "tanggal_jatuh_tempo : "){ echo "| Tanggal Jatuh Tempo : ".$tglAwal." - ".$tglAkhir;  }  
    ?>
    </label-tabel-small> -->
</div>                    
<div class="col-md-4 col-xs-12 dim-center text-right">
    <button onclick="window.location = 'tambah_kreditor'" class="btn btn-success btn-sm">
            <i class="fa fa-plus"></i> Tambah 
    </button>
    <button id="edit" class="btn btn-primary btn-sm dim-hidden centree-btn-edit" onclick="edit()"> <i class="fa fa-pencil"></i> Ubah </button>
    <button id="hapus" class="btn btn-danger btn-sm dim-hidden centree-btn-hapus" onclick="deleteBuku()"> <i class="fa fa-trash"></i> Hapus </button>
    <?php get_role_button(); ?>
    |
    <button id="print" class="btn btn-warning btn-sm" onclick="print()"> <i class="fa fa-print"></i> Print </button>
</div>
</div>
<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <table class="table table-bordered table-hover table-striped dataTable"  >
        <thead>
            <tr>
                <th>No</th>
                <th>No ID Bank</th>
                <th>Nama Bank</th>
                <th>Tanggal Peminjaman</th>
                <th>Tanggal Jatuh Tempo</th>
                <th>Term</th>
                <th>Banyak Cicilan</th>
                <th>Nominal Pinjaman</th>
                <th>Total yang sudah dilunasi</th>
                <th>Saldo Akhir</th>
                <th>Sisa Masa Cicilan</th>
            </tr>
        </thead>
        <tbody>
            
            <?php $i = 1; while($row = mysqli_fetch_object($execute)){ ?>
                <tr onclick="select_me('<?= $row->id_kreditor; ?>');" id="row<?= $row->id_kreditor; ?>">
                <td><?= $i; ?></td>
                <td><?= $row->no_akun." ".$row->nama_akun; ?></td>
                <td><?= $row->nama_bank; ?></td>
                <td><?= tgl_indo($row->tanggal_peminjaman); ?></td>
                <td><?= tgl_indo($row->tanggal_jatuh_tempo); ?></td>
                <td> <?= $row->term; ?></td>
                <td><?= $row->banyak_cicilan; ?> Kali</td>
                <td><?= rupiah($row->nominal_pinjaman,'Rp. '); ?></td>
                <td><?php 
                    $sudah_dilunasi = get_total_dilunasi($row->no_akun,$tahun_buku);
                 echo rupiah($sudah_dilunasi,'Rp. '); ?>
                  </td>
                <td><?= rupiah($row->nominal_pinjaman-$sudah_dilunasi,'Rp. '); ?></td>
                <td><?php
                    if($row->nominal_pinjaman==$sudah_dilunasi){
                        echo "-";
                     }else{
                        echo $row->banyak_cicilan-total_cicilan_dilunasi($row->no_akun,$tahun_buku)." Kali";
                     } ?>
                 </td>
            </tr>
            <?php $i++;} ?>
        </tbody>
    </table>
</div>

   <script type="text/javascript">
                        
    function select_me(id){
        $("tr").removeClass('dim-selected');
        $("#row"+id).toggleClass('dim-selected');
        var selected = $("#row"+id).hasClass( "dim-selected" );
        if(selected){
            selected_row = id;
        }else{
            selected_row = null;
        }
        console.log(selected_row);

        if(selected_row !==null){
            $('#edit').removeClass('dim-hidden');
            $('#hapus').removeClass('dim-hidden');
        }else{
            $('#edit').addClass('dim-hidden');
            $('#hapus').addClass('dim-hidden');
        }
    } 

    function edit(){
        window.location = 'edit_kreditor?id='+selected_row;
    }
    function print(){
        var tglAwal =  '<?= $tglAwal; ?>';
        var tglAkhir = '<?= $tglAkhir; ?>';
        var tipe = '<?= $tipe; ?>';
        window.open('print/print_kreditor?tglAwal='+tglAwal+'&tglAkhir='+tglAkhir+'&tipe='+tipe, '_blank');
    }

    function deleteBuku(){
        pesan_confirm("Apakah anda yakin?", "Hapus data Kreditor dengan id='"+selected_row+"'", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("no_id="+selected_row,'config/delete_kreditor','','Kreditor berhasil dihapus','Kreditor gagal dihapus');
            }   
        });
    }
    </script>

    <script type="text/javascript">
        $('.dataTable').DataTable();
    </script>