<?php
    include 'config.php';
    cek_tahun_buku();

    @$tglAwal = $_POST['dari_tgl'];
    @$tglAkhir = $_POST['sampe'];
    @$tipe = $_POST['tipe']; //tipe pencarian
    $tahun_buku = $_SESSION['tahun_buku'];

    if(empty($tglAwal) && empty($tglAkhir)){
        $query = "select * from v_jurnal WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JKK' GROUP BY no_jurnal ORDER BY no_jurnal,row ASC";
    }else{
        $query = "select * from v_jurnal WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JKK' AND tanggal BETWEEN '".$tglAwal."' AND '".$tglAkhir."' GROUP BY no_jurnal ORDER BY no_jurnal,row ASC";
    }   


    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }

?>

<div class="border border-top-0 border-left-0 border-right-0 row" style="padding-bottom: 10px; margin-bottom: 20px; ">
<div class="col-md-8 col-xs-12 dim-center">
    <label-tabel-small>
    Periode  
    <?php 
    if(empty($tipe)){
        echo tgl_indo2($_SESSION['awal_periode'])." - ".tgl_indo2($_SESSION['akhir_periode']);
    }else{
        echo tgl_indo2($tglAwal)." - ".tgl_indo2($tglAkhir);
    }
    ?>
    </label-tabel-small>
</div>                    
<div class="col-md-4 col-xs-12 dim-center text-right">
    <button onclick="window.location = 'tambah_jkk'" class="btn btn-success btn-sm">
            <i class="fa fa-plus"></i> Tambah 
    </button>
    <button id="edit" class="btn btn-primary btn-sm dim-hidden" onclick="edit()"> <i class="fa fa-pencil"></i> Ubah </button>
    <button id="hapus" class="btn btn-danger btn-sm dim-hidden" onclick="deleteBuku()"> <i class="fa fa-trash"></i> Hapus </button>
    
    |
    <button id="print" class="btn btn-warning btn-sm"> <i class="fa fa-print"></i> Print </button>
</div>
</div>
<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <table class="table table-bordered table-hover table-striped dataTable"  >
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>No.JKK</th>
                <th>No. Bukti</th>
                <th>No. Akun</th>
                <th>Nama Akun</th>
                <th>Debit</th>
                <th>Kredit</th>
            </tr>
        </thead>
        <tbody>
            
            <?php  while($row = mysqli_fetch_object($execute)){ ?>
                
                    <tr onclick="select_me('<?= $row->no_jurnal; ?>');" class="row<?= $row->no_jurnal; ?>">
                        <td><?= $row->tanggal; ?></td>
                        <td><?= $row->no_jurnal; ?></td>
                        <td><?= $row->no_bukti; ?></td>
                        <td align="right"><?= $row->no_akun; ?></td>
                        <td><?= $row->nama_akun; ?></td>
                        <?php
                            $no_jurnal_tmp = $row->no_jurnal;
                            $row_tmp       = $row->row;
                            $no_akun_tmp   = $row->no_akun;
                            $ket = $row->keterangan;
                            $debit = 0;
                            $kredit = 0;
                            if($ket=='Debit'){
                                $debit = $row->nominal;
                            }else{
                                $kredit = $row->nominal;
                            }
                        ?>
                        <td><?= rupiah($debit); ?></td>
                        <td><?= rupiah($kredit); ?></td>
                    </tr>

                    <?php
                        $query2 = "select * from v_jurnal WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JKK' AND no_jurnal='".$no_jurnal_tmp."' ORDER BY row ASC LIMIT 1,20";
                        $execute2 = mysqli_query($koneksi,$query2);
                        while($row2 = mysqli_fetch_object($execute2)){ ?>
                                <tr onclick="select_me('<?= $row2->no_jurnal; ?>');" class="row<?= $row2->no_jurnal; ?>">
                                    <td><p style="display: none;"><?= $row2->tanggal; ?></p></td>
                                    <td><p style="display: none;"><?= $row2->no_jurnal; ?></p></td>
                                    <td><p style="display: none;"><?= $row2->no_bukti; ?></p></td>
                                    <td align="right"><?= $row2->no_akun; ?></td>
                                    <td><?= $row2->nama_akun; ?></td>
                                    <?php
                                        $ket = $row2->keterangan;
                                        $debit = 0;
                                        $kredit = 0;
                                        if($ket=='Debit'){
                                            $debit = $row2->nominal;
                                        }else{
                                            $kredit = $row2->nominal;
                                        }
                                    ?>
                                    <td><?= rupiah($debit); ?></td>
                                    <td><?= rupiah($kredit); ?></td>
                                </tr>
                    <?php } ?>
                    <tr style="background-color: #fff1cc;">
                        <td colspan="7" style="text-align: center; font-style: italic;"> 
                             <?= $row->memo; ?>
                        </td>
                         <td style="display: none;"><?= $row->no_jurnal; ?></td>
                         <td style="display: none;"><?= $row->no_bukti; ?></td>
                         <td style="display: none;"><?= $row->tanggal; ?></td>
                         <td style="display: none;"></td>
                         <td style="display: none;"></td>
                         <td style="display: none;"></td>
                    </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

   <script type="text/javascript">
                        
    function select_me(id){
        $("tr").removeClass('dim-selected');
        $(".row"+id).toggleClass('dim-selected');
        var selected = $(".row"+id).hasClass( "dim-selected" );
        if(selected){
            selected_row = id;
        }else{
            selected_row = null;
        }
        console.log(selected_row);

        if(selected_row !==null){
            $('#edit').removeClass('dim-hidden');
            $('#hapus').removeClass('dim-hidden');
        }else{
            $('#edit').addClass('dim-hidden');
            $('#hapus').addClass('dim-hidden');
        }
    } 

    function edit(){
        //window.location = 'edit_jkk?id='+selected_row;
        location.reload();
    }

    function deleteBuku(){
        pesan_confirm("Apakah anda yakin?", "Hapus data Jurnal dengan No Jurnal='"+selected_row+"'", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("no_id="+selected_row,'config/delete_jkk','','Jurnal berhasil dihapus','Jurnal gagal dihapus');
            }   
        });
    }
    </script>

    <script type="text/javascript">
        $('.dataTable').DataTable({
            "paging": false,
            "ordering": false
        });
    </script>