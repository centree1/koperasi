<?php
    include 'config.php';
    cek_tahun_buku();

    @$no_id_debitor = $_POST['no_id_debitor'];
    //$tahun_buku     = $_SESSION['tahun_buku'];
    if(empty($no_id_debitor)){
        echo "<center> <b> Tidak ada Data yang dipilih </b> </center>";
        exit;
    }

    #Get Data Debitor
    $query = "select no_id_debitor, nama, jumlah_kredit, tanggal from v_debitor_channeling WHERE no_id_debitor='".$no_id_debitor."'";
    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }
    $data = mysqli_fetch_object($execute);
    $nama = $data->nama;
    $saldo = $data->jumlah_kredit;

    #Get data buku besar
    //id_tahun_buku='".$tahun_buku."' AND
    $query = "select * from bb_pembantu_piutang_channeling WHERE no_id_debitor='".$no_id_debitor."' ORDER BY tanggal";
    
    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }

?>
<div class="row" style="margin-bottom: 30px;">
<div class="col-md-6 col-xs-12 dim-center">
    <h6> No Identitas Debitor : <b> <?= $no_id_debitor; ?></b> </h6>
</div>                    

<div class="col-md-6 col-xs-12 dim-center text-right">
                        <button id="print" class="btn btn-primary btn-sm" onclick="print()"> <i class="fa fa-print"></i> Print </button>
                        |
                        <button id="print" class="btn btn-success btn-sm" onclick="printx()"> <i class="fa fa-print"></i> Excel </button> 
 
    <h6> Nama Akun: <b><?= $nama; ?></b> </h6>
</div>
</div>

<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <table class="table table-bordered table-hover table-striped dataTable"  >
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Deskripsi</th>
                <th>Debit</th>
                <th>Kredit</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td> <?= tgl_indo($data->tanggal); ?> </td>
            <td> DAFTAR DEBITOR </td>
            <td align="right"> <?= rupiah($saldo,"Rp. "); ?> </td>
            <td align="right">  </td>
            <td align="right"> <?= rupiah($saldo,"Rp. "); ?> </td>
            </tr>
            <?php
                while($row = mysqli_fetch_object($execute)){
                $saldo = $saldo + $row->debit - $row->kredit;

                $debit = null;
                $kredit = null;
                if($row->debit > 0){
                    $debit = rupiah($row->debit,"Rp. ");
                }

                if($row->kredit > 0 ){
                    $kredit = rupiah($row->kredit,"Rp. ");
                }

            ?>
            <tr>
            <td> <?= tgl_indo($row->tanggal); ?> </td>
            <td> <?= $row->no_jurnal; ?> </td>
            <td align="right"> <?= $debit; ?> </td>
            <td align="right"> <?= $kredit; ?> </td>
            <td align="right"> <?= rupiah($saldo,"Rp. "); ?> </td>
            </tr>
            <?php } ?>
        </tbody>  
    </table>
</div>

    <script type="text/javascript">
        $('.dataTable').DataTable({
            "ordering": false
        });
        function print(){
            var no_id_debitor =  '<?= $no_id_debitor; ?>';
             
              window.open('print/print_bb_pembantu_channeling?no_id_debitor='+no_id_debitor,'_blank');
        }
        function printx(){
            var no_id_debitor =  '<?= $no_id_debitor; ?>';
              window.open('print_excel/print_bb_pembantu_channelingx?no_id_debitor='+no_id_debitor, '_blank');
        }
    </script>