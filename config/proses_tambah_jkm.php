<?php
include 'config.php';
@$no_jurnal 			= $_POST['no_jurnal'];
@$tanggal 				= $_POST['tanggal'];
@$no_bukti 				= $_POST['no_bukti'];
@$memo 					= $_POST['memo'];  
$id_tahun_buku  		= $_SESSION['tahun_buku'];
$tipe_jurnal			= 'JKM'; 

//debit bank
@$debit_bank 			= $_POST['debit_bank'];
@$saldo_debit_bank		= $_POST['saldo_debit_bank'];

//debit lainya
@$debit_lainya 			= $_POST['debit_lainya'];
@$saldo_debit_lainya 	= $_POST['saldo_debit_lainya'];

//kredit lainya
@$kredit_lainya 		= $_POST['kredit_lainya'];
@$saldo_kredit_lainya 	= $_POST['saldo_kredit_lainya'];

if(empty($debit) && empty($debit_bank)){
	echo "Maaf, data debit/kredit tidak ditemukan";
	exit;
}

if(empty($kredit_lainya)){
	echo "Maaf, Data Kredit tidak ditemukan";
	exit;
}

if(empty($no_jurnal)){
	echo "Maaf, No Jurnal tidak ditemukan";
	exit;
}


$total_saldo_debit_bank    = array_sum($saldo_debit_bank);
$total_saldo_debit_lainya  = array_sum($saldo_debit_lainya);
$total_saldo_kredit_lainya = array_sum($saldo_kredit_lainya);

$saldo_debit = $total_saldo_debit_lainya + $total_saldo_debit_bank;
$saldo_kredit = $total_saldo_kredit_lainya;
if($saldo_debit !== $saldo_kredit){
	echo "Maaf, Total saldo debit & kredit tidak balance";
	exit;
}

#0 Transaction
mysqli_autocommit($koneksi, FALSE);


#prepare array jurnal_debit_kredit
$arr_debit = array();
foreach ($debit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit_lainya[$num]
	);
	$arr_debit[$num] = $arr_tmp;
}

$arr_debit_bank = array();
foreach ($debit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit_bank[$num]
	);
	$arr_debit_bank[$num] = $arr_tmp;
}

$arr_kredit_lainya = array();
foreach ($kredit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_lainya[$num]
	);
	$arr_kredit_lainya[$num] = $arr_tmp;
}


#1 insert ke table jurnal
$query  = 'INSERT INTO tb_jurnal VALUES ("'.$no_jurnal.'","'.$no_bukti.'","'.$tanggal.'","'.$memo.'","'.$tipe_jurnal.'","'.$id_tahun_buku.'")';
$exec   = mysqli_query($koneksi,$query);
if(!$exec)
{
	echo "Maaf, Nomor Jurnal Duplikat";
	exit;
}


$x=1;
if(!empty($arr_debit)){
	foreach ($arr_debit as $num => $row) {
		$query  = 'INSERT INTO tb_jurnal_debit_kredit VALUES ("'.$no_jurnal.'","'.$row->no_akun.'","'.$num.'","'.$row->saldo.'","Debit Lainya")';
		$exec   = mysqli_query($koneksi,$query);
	}
}

if(!empty($arr_debit_bank)){
	foreach ($arr_debit_bank as $num => $row) {
		$query  = 'INSERT INTO tb_jurnal_debit_kredit VALUES ("'.$no_jurnal.'","'.$row->no_akun.'","'.$num.'","'.$row->saldo.'","Debit Bank")';
		$exec   = mysqli_query($koneksi,$query);
	}
} 

if(!empty($arr_kredit_lainya)){
	foreach ($arr_kredit_lainya as $num => $row) {
		$query  = 'INSERT INTO tb_jurnal_debit_kredit VALUES ("'.$no_jurnal.'","'.$row->no_akun.'","'.$num.'","'.$row->saldo.'","Kredit Lainya")';
		$exec   = mysqli_query($koneksi,$query);
	}
}

insert_log($no_jurnal,"Menambah Jurnal Kas Masuk");
mysqli_commit($koneksi);
echo 1;
?>