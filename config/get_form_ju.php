<?php
	include 'config.php';
	$id = @$_POST['id'];
	if(empty($id)){
		exit;
	}
	//kredit pada jkk = Debit
	//debit pada jkk = kredit
    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_debit   = mysqli_fetch_all($result,MYSQLI_ASSOC);
?>
<tr>
    <td  class=""  >
    	<button type="button" class="btn btn-sm btn-danger" id="min-<?= $id; ?>" onclick="delete_row('min-<?= $id; ?>')"> 
    		<i class="fa fa-minus "></i>
    	</button>
    </td>

	<td class="" > 
		<select class="form-control select22" name="no_akun[<?= $id; ?>]">
			<option value="">--</option>
			<?php foreach ($data_debit as $key => $row) { ?>
				<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
			<?php } ?>
		</select>
	</td>
	<td class="">
		&nbsp <br>
		<div class="input-group mb-3">
		  <input type="text" class="form-control CentreeRupiah" onchange="change('<?= $id; ?>','debit');" id="debit<?= $id; ?>" placeholder="Debit" name="saldo_debit[<?= $id; ?>]">
		</div>
	</td>
	<td class="">
		&nbsp <br>
		<div class="input-group mb-3">
		  <input type="text" class="form-control CentreeRupiah" onchange="change('<?= $id; ?>','kredit');" id="kredit<?= $id; ?>" placeholder="Kredit"  name="saldo_kredit[<?= $id; ?>]" id="saldo_kredit<?= $id; ?>">
		</div>
	</td>

</tr>

<script type="text/javascript">
$('.select22').select2({
    theme: "bootstrap",
    width: "100%",
    placeholder:"--",
  allowClear: true
});

function change(id,tipe){
	if(tipe=='kredit'){
		$("#debit"+id).val(0);
	}else if(tipe=='debit'){
		$("#kredit"+id).val(0);
	}
}


$(document).ready(function() {
    $('#kredit_bank_<?= $id; ?>').change(function() { 
	});
}); 

</script>


<script type="text/javascript">
	$( ".CentreeRupiah" ).CentreeRupiah();
	$(".CentreeRupiah").keyup(function(){
      //get attribut
	    var name    = $(this).attr('name');
	    var name_tmp  = name.split("___");
	    var name_real = name_tmp[1];
	    var angka     = $(this).val();
	    var prefix    = "Rp. ";

	    returnx = CentreeFormatRupiah(angka);
	    var value  = $(this).val(returnx[0]);
	    $("input[name='"+name_real+"']").val(returnx[1]); 

	  });
</script>