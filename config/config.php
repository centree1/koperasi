<?php 

//start session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
 
//Cek Session ------------------------------------------------------
if(empty($_SESSION['no_id']) && $_SERVER['REQUEST_URI'] !== '/' && $_SERVER['REQUEST_URI'] !== '/config/login' && $_SERVER['REQUEST_URI'] !== '/config/loginx'){  
	echo "<script type='text/javascript'> window.location = '".base_url()."'; </script>";
	exit;
}

//Versi Lama//
//Cek Session ------------------------------------------------------
/*if(empty($_SESSION['no_id']) && $_SERVER['REQUEST_URI'] !== '/' && $_SERVER['REQUEST_URI'] !== '/config/login' && $_SERVER['REQUEST_URI'] !== '/config/loginx'){  
	echo "<script type='text/javascript'> window.location = '".base_url()."'; </script>";
	exit;
}
*/

//koneksi ---------------------------------------------------- 
	$koneksi = mysqli_connect("sinargroups.id","sinargro_rider","chittychittybangbang","sinargro_koprasi_fix");

	// Check connection
	if (mysqli_connect_errno()){
		echo "Koneksi Gagal : " . mysqli_connect_error();
		exit;
	}

//end koneksi ------------------------------------------------

//base url ---------------------------------------------------
function base_url($uri=null){
	$base_url = 'https://koperasi-new.sinargroups.id/';
	if($uri == null){
		return $base_url;
	}else{
		return $base_url.$uri;
	}
}

//end base url ------------------------------------------------

//Cek Tahun buku sudah dipilih atau belum
function cek_tahun_buku(){
	if(empty($_SESSION['tahun_buku'])){
		set_notif('msgthnbku',"Maaf, anda belum memilih tahun buku","","danger","close");
		$url = base_url('dashboard');
		echo "<script> window.location = '".$url."'; </script>";
		exit;
	}
}

//------------------------------------------------------------

//404-------------------------------------------------------
function selected_droplist($a1,$a2){
	if($a1==$a2){
		return 'Selected';
	}else{
		return 0;
	}
}
//--------------------------------------------------------

//log r-------------------------------------------------
function dim($text){
	print_r($text);
	exit;
}

//-----------------------------------------------------

//notifikasi-------------------------------------------------
function set_notif($session_id,$pesan,$go_to=NULL,$alert=NULL,$fa=NULL){
	/*
		session_id = bebas, id untuk pesan
		pesan = bebas
		go_to = pergi kehalaman lain ketika notif sudah di set 
		alert = jenis alert -> danger,warning,success,info
		fa = fa icon, ex: fa fa-xxxx ====> cukup xxxx nya saja yang dimasukkan
	*/

	if(empty($alert)){
		$alert = 'danger';
	}

	if(empty($fa)){
		$fa = 'close';
	}

	$div = '<div class="alert alert-'.$alert.' alert-dismissible fade show" role="alert" style="font-weight: bold; margin-top: 10px; margin-bottom: 10px;"> <i class="fa fa-'.$fa.'"></i> '.$pesan.'<button type="button" class="close" data-dismiss="alert" ><span aria-hidden="true">&times;</span></button> </div>';
	$_SESSION[$session_id] = $div;

	if(!empty($go_to)){
		echo "<script> window.location = '".$go_to."'; </script>";
		exit;
	}
}

function get_notif($session_id){
	if(!empty($_SESSION[$session_id])){
		echo $_SESSION[$session_id];
		unset ($_SESSION[$session_id]);
	}
	
}

//-----------------------------------------------------

//Tanggal ----------------------------------------
function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'JANUARI',
		'FEBRUARI',
		'MARET',
		'APRIL',
		'MEI',
		'JUNI',
		'JULI',
		'AGUSTUS',
		'SEPTEMBER',
		'OKTOBER',
		'NOVEMBER',
		'DESEMBER'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}


function tgl_indo2($tanggal){
	$bulan = array (
		1 =>   'Jan',
		'Feb',
		'Mar',
		'Apr',
		'Mei',
		'Jun',
		'Jul',
		'Agust',
		'Sept',
		'Okt',
		'Nov',
		'Des'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function change_format_date($tanggal){
	$pecahkan = explode('-', $tanggal);
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
//----------------------------------------------

//Rupiah ---------------------------------------
function rupiah($angka,$mata_uang=null){
	if($angka==0){
		return "-";
	}
	$hasil_rupiah = $mata_uang. number_format($angka,0,',',',');
	return $hasil_rupiah;
 
}

//------------------------------------------
// id encrypt
function encrypt( $s ) {
    $cryptKey  = 'd8578edf8458ce06fbc5bb76a58c5ca4';
    $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $s, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
    return( $qEncoded );
}
 
function decrypt($s) {
    $cryptKey  = 'd8578edf8458ce06fbc5bb76a58c5ca4';
    $qDecoded  = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $s ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
    return( $qDecoded );
}

//Debugging--------------------------------------
function im_debugging($param){
	ob_get_clean();
    echo "<pre>";
    print_r($param);
    echo "</pre>";
    exit;
}
//-----------------------------------------------

//Role------------------------------------------
function get_role_button(){
    global $koneksi;
    $tipe_akun = $_SESSION['role'];
    $query = "select * from tb_role where tipe_akun='".$tipe_akun."' AND role=0";
    $execute = mysqli_query($koneksi,$query);
    echo '<script>$(document).ready(function(){ ';
    while($row = mysqli_fetch_object($execute)){
        $action = $row->action;
        if($action=='edit'){
            $class = 'centree-btn-edit';
        }elseif($action=='hapus'){
            $class = 'centree-btn-hapus';
        }else{
            continue;
        }
        echo ' $(".'.$class.'").remove(); ';
    }
    echo '}); </script>';
}

function get_role_page($action){
    global $koneksi;
    $tipe_akun = $_SESSION['role'];
    if($tipe_akun=="ADMIN" || $tipe_akun=="SYSADMIN"){
    	return 0;
    }

    $query = "select * from tb_role where tipe_akun='".$tipe_akun."' AND action='".$action."' AND role=1";
    $execute = mysqli_query($koneksi,$query);
    $data = mysqli_fetch_object($execute);
    if(empty($data)){
    	set_notif('msgthnbku','Maaf, Akses ditolak','dashboard','danger','close');
    }
}
//----------------------------------------------

//log-------------------------------------------
function insert_log($no_unique,$keterangan){
    global $koneksi;
    global $_SESSION;
    $tanggal = date("Y-m-d");
    $jam = date("h:i:s");
    $user = $_SESSION['nama'];
    $keterangan2 = $user." ".$keterangan;
    $query = "insert into tb_log(no_unique,tanggal,jam,keterangan) values('$no_unique','$tanggal','$jam','$keterangan2')";
    $execute = mysqli_query($koneksi,$query);
}
//-------------------------- 
?>