<?php
	include 'config.php';
	$id = @$_POST['id'];
	$rowx = @$_POST['row'];
	if(empty($id) || empty($rowx)){
		exit;
	}

	$id_identitas_debitor = explode(",",$id); 
	$akun = $id_identitas_debitor[0];
	//im_debugging($akun);

	//get akun tipe bank
    $query  = "SELECT no_akun, nama_akun from tb_akun where tipe_akun=20";
    $result = mysqli_query($koneksi,$query);
    $data_bank   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    //get akun Kewajiban Lancar
    $query  = "SELECT no_akun, nama_akun from tb_akun where tipe_akun=32";
    $result = mysqli_query($koneksi,$query);
    $data_hutang_chaneling   = mysqli_fetch_all($result,MYSQLI_ASSOC);
    //im_debugging($data_hutang_chaneling);


    //get akun tipe bank lainya
    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_lainya   = mysqli_fetch_all($result,MYSQLI_ASSOC);
    //im_debugging($data_lainya);

?>
<tr>
    <td  class="header-debit"  >
    	<button type="button" class="btn btn-sm btn-danger" id="min-<?= $rowx; ?>" onclick="delete_row('min-<?= $rowx; ?>')"> 
    		<i class="fa fa-minus "></i>
    	</button>
    </td>

<!-- Debit -------------------------------------------->
<td class="header-debit" > 
	<select class="form-control select22" name="debit_bank[<?= $rowx; ?>]">
		<option value="">--</option>
		<?php foreach ($data_bank as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="header-debit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_debit_bank[<?= $rowx; ?>]" value="">
	</div>
</td>
<!-- End Debit -->

<!-- Debit Lainya -------------------------------------------->
<td class="header-debit" > 
	<select class="form-control select22" name="debit_lainya[<?= $rowx; ?>]">
		<option value="">--</option>
		<?php foreach ($data_lainya as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="header-debit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_debit_lainya[<?= $rowx; ?>]" value="">
	</div>
</td>
<!-- End Debit -->

<!-- Kredit chaneling -------------------------------------------->
<td class="header-bank" > 
	<select class="form-control select22" name="kredit_channeling[<?= $rowx; ?>]">
		<option value="">--</option>
		<?php foreach ($data_hutang_chaneling as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="header-bank">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_channeling[<?= $rowx; ?>]" >
	</div>
</td>
<!-- End Kredit chaneling  -->

<!-- Kredit Lainya -------------------------------------------->
<td class="header-bank" > 
	<select class="form-control select22" name="kredit_lainya[<?= $rowx; ?>]">
		<option value="">--</option>
		<?php foreach ($data_lainya as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="header-bank">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_lainya[<?= $rowx; ?>]" >
	</div>
</td>
<!-- End Kredit Lainya  -->

</tr>

<script type="text/javascript">
$('.select22').select2({
    theme: "bootstrap",
    width: "100%",
    placeholder:"--",
  allowClear: true,
  dropdownAutoWidth : true
});



</script>

<script type="text/javascript">
	$( ".CentreeRupiah" ).CentreeRupiah();
	$(".CentreeRupiah").keyup(function(){
      //get attribut
	    var name    = $(this).attr('name');
	    var name_tmp  = name.split("___");
	    var name_real = name_tmp[1];
	    var angka     = $(this).val();
	    var prefix    = "Rp. ";

	    returnx = CentreeFormatRupiah(angka);
	    var value  = $(this).val(returnx[0]);
	    $("input[name='"+name_real+"']").val(returnx[1]); 

	  });
</script>