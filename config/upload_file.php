<?php
include 'config.php';
    $ekstensi_diperbolehkan    = array('pdf');
    $nama1      = $_FILES['pdf']['name'];
    $x          = explode('.', $nama1);
    $ekstensi   = pathinfo($nama1, PATHINFO_EXTENSION);
    $ukuran     = $_FILES['pdf']['size'];
    $file_tmp   = $_FILES['pdf']['tmp_name'];
    $tipe       = $_POST['tipe'];
    $nama2      = $tipe.'.'.$ekstensi;

    if($nama1 == ""){
        echo 'Tidak ada file yang diunggah';
        exit;
    }

    if($tipe == "profil_perusahaan" || $tipe == "kebijakan_akuntansi" || $tipe == "panduan_aplikasi"){ }else{
        echo "Undefined File";
        exit;
    }

    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
        if ($ukuran < 2097152 && $ukuran > 0) {
                move_uploaded_file($file_tmp, '../file_koperasi/'.$nama2);            
        } else {
            echo 'Maksimal ukuran file 2MB';
            exit;
        }
    } else {
        echo 'Tipe File tidak diperbolehkan';
        exit;
    }

    echo 1;

?>