<?php
	include 'config.php';
	@$id = $_POST['id'];
	@$no_identitas_debitor = $_POST['no_identitas_debitor'];
    @$channeling = $_POST['channeling'];

	if(empty($id) || empty($no_identitas_debitor)){
		$arr['nama'] = "";
		$arr['jumlah_cicilan'] = "";

		echo json_encode($arr);
		exit;
	}

    //cek apakah sudah lunas atau belum
    $query = "select total_pengajuan,total_telah_disalurkan,jumlah_telah_dibayar from v_cicilan_debitor_2 where id_debitor='".$id."'";
    $exe   = mysqli_query($koneksi, $query);
    $data_debitor = mysqli_fetch_object($exe);
    #jika selain chaneling maka menggunakan kredit salur, jadi total yang telah disalurkan dikurangi total yang telah dibayar
    #jika channeling maka total yang diajukan dikurangi total yang telah dibayar
    if($channeling=='true'){
        $totalx = (int)$data_debitor->total_pengajuan - (int)$data_debitor->jumlah_telah_dibayar;
    }else{
        $totalx = (int)$data_debitor->total_telah_disalurkan - (int)$data_debitor->jumlah_telah_dibayar;
    }
    if($totalx <= 0){
        $arr['jumlah_cicilan'] = "Lunas";
        echo json_encode($arr);
        exit;
    }

	//get nama
	$query = "select nama from tb_debitor_pribadi where no_id_debitor = '".$no_identitas_debitor."'";
    $exe   = mysqli_query($koneksi, $query);
    $data_debitor = mysqli_fetch_object($exe);
    $arr['nama'] = $data_debitor->nama;

    //get jumlah cicilan
    $query = "select jumlah_cicilan from v_cicilan_debitor_2 where id_debitor='".$id."'";
    $exe   = mysqli_query($koneksi, $query);
    $data_debitor = mysqli_fetch_object($exe);
    if(is_null($data_debitor->jumlah_cicilan)){
    	$arr['jumlah_cicilan'] = 0;
    }else{
    	$arr['jumlah_cicilan'] = $data_debitor->jumlah_cicilan;
    } 

    echo json_encode($arr);
?>