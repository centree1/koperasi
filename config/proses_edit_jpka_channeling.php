<?php
include 'config.php';
mysqli_autocommit($koneksi, FALSE);

//im_debugging($_POST);
@$id_debitor 			= $_POST['no_identitas_debitor'];
@$no_jurnal 			= $_POST['no_jurnal'];
@$tanggal 				= $_POST['tanggal'];
@$no_bukti 				= $_POST['no_bukti'];
@$memo 					= $_POST['memo'];

@$debit_bank 			= $_POST['debit_bank'];
@$saldo_debit_bank		= $_POST['saldo_debit_bank'];

@$debit_lainya 			= $_POST['debit_lainya'];
@$saldo_debit_lainya	= $_POST['saldo_debit_lainya'];

@$kredit_channeling   		= $_POST['kredit_channeling'];
@$saldo_kredit_channeling 	= $_POST['saldo_kredit_channeling'];

@$kredit_lainya 		= $_POST['kredit_lainya'];
@$saldo_kredit_lainya 	= $_POST['saldo_kredit_lainya'];

$id_tahun_buku  		= $_SESSION['tahun_buku'];
$tipe_jurnal			= 'JPKA_CHANNELING';
#@$urutan_debit		 	= $_POST['cicilan_ke'];

$tipe_pelunasan			= $_POST['tipe_pelunasan'];
if($tipe_pelunasan=='cicilan'){
	@$urutan_debit		 	= $_POST['pencairan_ke'];
}else{
	$urutan_debit		 	= 'dipercepat';
}


if(empty($debit_bank) && empty($debit_lainya)){
	echo "Maaf, data debit tidak ditemukan";
	exit;
}

if(empty($kredit_channeling) && empty($kredit_lainya)){
	echo "Maaf, Data Kredit tidak ditemukan";
	exit;
}

if(empty($no_jurnal)){
	echo "Maaf, No Jurnal tidak ditemukan";
	exit;
}


#Set Array
$tot_debit  = 0;
$tot_kredit = 0;
$jumlah_row = $_POST['jumlah_row'];
$x=0;

$arr_debit_bank = array();
foreach ($debit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit_bank[$num]
	);
	$arr_debit_bank[$num] = $arr_tmp;
	$tot_debit += (int)$saldo_debit_bank[$num];
}

$arr_debit_lainya = array();
foreach ($debit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit_lainya[$num]
	);
	$arr_debit_lainya[$num] = $arr_tmp;
	$tot_debit += (int)$saldo_debit_lainya[$num];
}

$arr_kredit_channeling = array();
foreach ($kredit_channeling as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_channeling[$num]
	);
	$arr_kredit_channeling[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_channeling[$num];
}

$arr_kredit_lainya = array();
foreach ($kredit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_lainya[$num]
	);
	$arr_kredit_lainya[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_lainya[$num];
}

//im_debugging($tot_kredit);
//im_debugging($tot_debit." = ".$tot_kredit);

#checking balance
if($tot_debit !== $tot_kredit){
	echo "Maaf, Debit & Kredit tidak Balance";
	exit;
}

#get data sebelumnya
$query = "select sum(nominal) as total_sebelumnya  from tb_jurnal_debitor_debit_kredit where no_jurnal='".$no_jurnal."' and keterangan like '%Debit%'";
$exe   = mysqli_query($koneksi, $query);
$data_sebelumnya = mysqli_fetch_object($exe);
if(empty($data_sebelumnya)){
	$total_sebelumnya = 0;
}else{
	$total_sebelumnya = $data_sebelumnya->total_sebelumnya;
}

#check jumlah cicilan, jika lebih dari sisa pembayaran
$query = "select total_pengajuan,total_telah_disalurkan,jumlah_telah_dibayar from v_cicilan_debitor_2 where no_id_debitor='".$id_debitor."'";
$exe   = mysqli_query($koneksi, $query);
$data_debitor = mysqli_fetch_object($exe);  
$total_yang_belum_dibayar = (int)$data_debitor->total_pengajuan - (int)$data_debitor->jumlah_telah_dibayar;

$totalx = $total_sebelumnya+($total_yang_belum_dibayar - $tot_debit);

/*if($totalx < 0){
	echo "Maaf, Jumlah yang dibayarkan melebihi sisa pinjaman yang belum dibayar";
	exit;
}*/ 
if($tipe_pelunasan=='cicilan'){
	if($totalx < 0){
		echo "Maaf, Jumlah yang dibayarkan melebihi sisa yang belum dilunasi";
		exit;
	}
}else{
	if($totalx != 0){
		echo "Maaf, Jumlah Pelunasan Dipercepat harus sesuai dengan sisa yang belum dilunasi";
		exit;
	}
}


#1 Update ke table jurnal
$query  = 'UPDATE tb_jurnal_debitor SET no_bukti="'.$no_bukti.'",tanggal="'.$tanggal.'",memo="'.$memo.'",urutan_debit="'.$urutan_debit.'" where no_jurnal="'.$no_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);
if(!$exec)
{
	echo "Gagal ubah data jurnal";
	exit;
}

#2 delete all in tb_jurnal_debit_kredit
$query  = 'DELETE FROM tb_jurnal_debitor_debit_kredit WHERE no_jurnal="'.$no_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);

$num = 0;
while ($num <= $jumlah_row) {

	if(!empty($arr_debit_bank[$num])){
		$no_akunx 	= $arr_debit_bank[$num]->no_akun;
		$saldox		= $arr_debit_bank[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Debit Bank")';
		$exec   = mysqli_query($koneksi,$query);
		//echo $query."<br>";
	}

	if(!empty($arr_debit_lainya[$num])){
		$no_akunx 	= $arr_debit_lainya[$num]->no_akun;
		$saldox		= $arr_debit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Debit Lainya")';
		$exec   = mysqli_query($koneksi,$query);
		//echo $query."<br>";
	}

	if(!empty($arr_kredit_channeling[$num])){
		$no_akunx 	= $arr_kredit_channeling[$num]->no_akun;
		$saldox		= $arr_kredit_channeling[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Channeling")';
		$exec   = mysqli_query($koneksi,$query);
		//echo $query."<br>";
	}

	if(!empty($arr_kredit_lainya[$num])){
		$no_akunx 	= $arr_kredit_lainya[$num]->no_akun;
		$saldox		= $arr_kredit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debitor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Lainya")';
		$exec   = mysqli_query($koneksi,$query);
		//echo $query."<br>";
	}

	$num++;
}

insert_log($no_jurnal,"Mengubah Data");
mysqli_commit($koneksi);
echo 1;
?>