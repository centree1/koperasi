<?php
include 'config.php';
mysqli_autocommit($koneksi, FALSE);

@$id_kreditor 			= $_POST['id_kreditor'];
@$no_jurnal 			= $_POST['no_jurnal'];
@$tanggal 				= $_POST['tanggal'];
@$no_bukti 				= $_POST['no_bukti'];
@$memo 					= $_POST['memo'];
@$id_bank 				= $_POST['no_akun'];
@$nama_bank 			= $_POST['nama_akun'];

@$debit_bank 			= $_POST['debit_bank'];
@$saldo_debit_bank 		= $_POST['saldo_debit_bank'];
@$debit_lainya			= $_POST['debit_lainya'];
@$saldo_debit_lainya 	= $_POST['saldo_debit_lainya'];

@$kredit_bank 			= $_POST['kredit_bank'];
@$saldo_kredit_bank 	= $_POST['saldo_kredit_bank'];
@$kredit_lainya 		= $_POST['kredit_lainya'];
@$saldo_kredit_lainya 	= $_POST['saldo_kredit_lainya'];

$id_tahun_buku  		= $_SESSION['tahun_buku'];
$tipe_jurnal			= 'JPKB';


//@$urutan_debit		 	= $_POST['pencairan_ke'];

if(empty($debit_bank)){
	echo "Maaf, data debit/kredit tidak ditemukan";
	exit;
}

if(empty($kredit_bank) && empty($kredit_lainya)){
	echo "Maaf, Data Kredit tidak ditemukan";
	exit;
}

if(empty($no_jurnal)){
	echo "Maaf, No Jurnal tidak ditemukan";
	exit;
}


#Set Array
$tot_debit  = 0;
$tot_kredit = 0;

$arr_debit_bank = array();
foreach ($debit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit_bank[$num]
	);
	$arr_debit_bank[$num] = $arr_tmp;
	$tot_debit += (int)$saldo_debit_bank[$num];
}
$arr_debit_lainya = array();
foreach ($debit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit_lainya[$num]
	);
	$arr_debit_lainya[$num] = $arr_tmp;
	$tot_debit += (int)$saldo_debit_lainya[$num];
}


$arr_kredit_bank = array();
foreach ($kredit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_bank[$num]
	);
	$arr_kredit_bank[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_bank[$num];
}

$arr_kredit_lainya = array();
foreach ($kredit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_lainya[$num]
	);
	$arr_kredit_lainya[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_lainya[$num];
}
//im_debugging($tot_kredit);
//im_debugging($tot_debit."=".$tot_kredit);
//im_debugging($arr_kredit_lainya);


#checking balance
if($tot_debit !== $tot_kredit){
	echo "Maaf, Debit & Kredit tidak Balance";
	exit;
}

#check jumlah yang disalurkan apakah lebih dari yg diajukan
$query = "select nominal_pinjaman from v_kreditor where id_kreditor='".$id_kreditor."'";
$exe   = mysqli_query($koneksi, $query);
$data_kreditor = mysqli_fetch_object($exe);
//im_debugging($data_debitor);
//$total_pengajuan = (int)$data_debitor->total_pengajuan - (int)$data_debitor->nominal_pinjaman;
//im_debugging($tot_debit."  --  ".$data_kreditor->nominal_pinjaman);
if($tot_debit !=  $data_kreditor->nominal_pinjaman ){
	echo "Maaf, jumlah perolehan tidak sesuai dengan data peminjaman";
	exit;
}



#1 insert ke table jurnal
$query  = 'INSERT INTO tb_jurnal_kreditor VALUES ("'.$no_jurnal.'","'.$id_tahun_buku.'","'.$no_bukti.'","'.$tanggal.'","'.$memo.'","'.$tipe_jurnal.'","'.$id_kreditor.'")';
$exec   = mysqli_query($koneksi,$query);
if(!$exec)
{
	echo "Silahkan Refresh dulu";
	exit;
}

$x=1;
foreach ($arr_kredit_bank as $num => $row) {
	$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$row->no_akun.'","'.$num.'","'.$row->saldo.'","Kredit Bank")';
	$exec   = mysqli_query($koneksi,$query);
	//im_debugging($query);

	if(!empty($arr_debit_lainya[$num])){
		$no_akunx 	= $arr_debit_lainya[$num]->no_akun;
		$saldox		= $arr_debit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Debit lainnya")';
		$exec   = mysqli_query($koneksi,$query);
	}

	if(!empty($arr_debit_bank[$num])){
		$no_akunx 	= $arr_debit_bank[$num]->no_akun;
		$saldox		= $arr_debit_bank[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Debit Bank")';
		$exec   = mysqli_query($koneksi,$query);
	}

	if(!empty($arr_kredit_lainya[$num])){
		$no_akunx 	= $arr_kredit_lainya[$num]->no_akun;
		$saldox		= $arr_kredit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit lainnya")';
		$exec   = mysqli_query($koneksi,$query);
	}
}
mysqli_commit($koneksi);
echo 1;
?>