<?php
include 'config.php';
mysqli_autocommit($koneksi, FALSE);

cek_tahun_buku();

@$no_jurnal 			= $_POST['no_jurnal'];
@$tanggal 				= $_POST['tanggal'];
@$no_bukti 				= $_POST['no_bukti'];
@$memo 					= $_POST['memo'];
@$debit 				= $_POST['saldo_debit'];
@$kredit 	 			= $_POST['saldo_kredit'];
@$no_akun 				= $_POST['no_akun'];
$id_tahun_buku  		= $_SESSION['tahun_buku'];
$tipe_jurnal			= 'JP';


if(empty($no_akun)){
	echo "Maaf, data debit/kredit tidak ditemukan";
	exit;
}


#1 insert ke table jurnal
$query  = 'UPDATE tb_jurnal SET no_bukti="'.$no_bukti.'" ,tanggal="'.$tanggal.'" ,memo="'.$memo.'" WHERE no_jurnal="'.$no_jurnal.'" AND tipe_jurnal="'.$tipe_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);
if(!$exec)
{
	echo "Maaf, Terjadi Kesalahan";
	exit;
}

#2 delete all in tb_jurnal_debit_kredit
$query  = 'DELETE FROM tb_jurnal_debit_kredit WHERE no_jurnal="'.$no_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);

#insert ke table tb_jurnal_debit_kredit
$arr = array();
foreach ($no_akun as $num => $row) {
	 
	if($kredit[$num] == '0' || $kredit[$num] == ''){
		$query  = 'INSERT INTO tb_jurnal_debit_kredit VALUES ("'.$no_jurnal.'","'.$row.'","'.$num.'","'.$debit[$num].'","Debit")';
	}else{
		$query  = 'INSERT INTO tb_jurnal_debit_kredit VALUES ("'.$no_jurnal.'","'.$row.'","'.$num.'","'.$kredit[$num].'","Kredit")';
	}
	$exec   = mysqli_query($koneksi,$query);
}


insert_log($no_jurnal,"Mengubah Data");
mysqli_commit($koneksi);
echo 1;
?>