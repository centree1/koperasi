<?php
include 'config.php';
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=jurnal_umum.xls");
mysqli_autocommit($koneksi, FALSE);

@$no_jurnal 			= $_POST['no_jurnal'];
@$tanggal 				= $_POST['tanggal'];
@$no_bukti 				= $_POST['no_bukti'];
@$memo 					= $_POST['memo'];
@$no_identitas_bank		= $_POST['no_identitas_bank'];
@$nama_bank				= $_POST['nama_bank'];



@$debit_bank			= $_POST['debit_bank'];
@$saldo_debit_bank 		= $_POST['saldo_debit_bank'];
@$debit_lainya			= $_POST['debit_lainya'];
@$saldo_debit_lainya 	= $_POST['saldo_debit_lainya'];

@$kredit_bank			= $_POST['kredit_bank'];
@$saldo_kredit_bank 	= $_POST['saldo_kredit_bank'];
@$kredit_lainya			= $_POST['kredit_lainya'];
@$saldo_kredit_lainya 	= $_POST['saldo_kredit_lainya'];


$id_tahun_buku  		= $_SESSION['tahun_buku'];
$tipe_jurnal			= 'JPKB';

if(empty($debit_bank) && empty($debit_lainya)){
	echo "Maaf, data Debit tidak ditemukan";
	exit;
}

if(empty($kredit_bank) && empty($kredit_lainya)){
	echo "Maaf, Data Kredit tidak ditemukan";
	exit;
}

if(empty($no_jurnal)){
	echo "Maaf, No Jurnal tidak ditemukan";
	exit;
}


#Set Array
$tot_debit  = 0;
$tot_kredit = 0;

#1 insert ke table jurnal
$query  = 'UPDATE tb_jurnal_kreditor SET no_bukti="'.$no_bukti.'" ,tanggal="'.$tanggal.'" ,memo="'.$memo.'" WHERE no_jurnal="'.$no_jurnal.'" AND tipe_jurnal="'.$tipe_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);
if(!$exec)
{
	echo "Maaf, Terjadi Kesalahan";
	exit;
}

#2 delete all in tb_jurnal_debit_kredit
$query  = 'DELETE FROM tb_jurnal_kreditor_debit_kredit WHERE no_jurnal="'.$no_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);


#insert ke table tb_jurnal_debit_kredit
$arr_debit_bank = array();
foreach ($debit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit_bank[$num]
	);
	$arr_debit_bank[$num] = $arr_tmp;
	$tot_debit += (int)$saldo_debit_bank[$num];
}

$arr_debit_lainya = array();

foreach ($debit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit_lainya[$num]
	);
	$arr_debit_lainya[$num] = $arr_tmp;
	$tot_debit += (int)$saldo_debit_lainya[$num];
}

$arr_kredit_bank = array();
foreach ($kredit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_bank[$num]
	);
	$arr_kredit_bank[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_bank[$num];
}

$arr_kredit_lainya = array();
foreach ($kredit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_lainya[$num]
	);
	$arr_kredit_lainya[$num] = $arr_tmp;
	$tot_kredit += (int)$saldo_kredit_lainya[$num];
}
//im_debugging($arr_kredit_lainya);

#checking balance
if($tot_debit !== $tot_kredit){
	echo "Maaf, Debit & Kredit tidak Balance";
	exit;
}
//im_debugging($tot_kredit ."=". $tot_debit);





/*foreach ($arr_debit_bank as $num => $row) {
	$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$row->no_akun.'","'.$num.'","'.$row->saldo.'","Debit Bank")';
	$exec   = mysqli_query($koneksi,$query);*/
$x=1;
foreach ($arr_kredit_bank as $num => $row) {
	$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$row->no_akun.'","'.$num.'","'.$row->saldo.'","Kredit Bank")';
	$exec   = mysqli_query($koneksi,$query);


	//im_debugging($query);

	if(!empty($arr_debit_lainya[$num])){
		$no_akunx 	= $arr_debit_lainya[$num]->no_akun;
		$saldox		= $arr_debit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Debit lainnya")';
		$exec   = mysqli_query($koneksi,$query);
	}

	if(!empty($arr_debit_bank[$num])){
		$no_akunx 	= $arr_debit_bank[$num]->no_akun;
		$saldox		= $arr_debit_bank[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Debit Bank")';
		$exec   = mysqli_query($koneksi,$query);
	}
//backup
	/*	if(!empty($arr_kredit_bank[$num])){
		$no_akunx 	= $arr_kredit_bank[$num]->no_akun;
		$saldox		= $arr_kredit_bank[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Bank")';
		$exec   = mysqli_query($koneksi,$query);
	}*/

	if(!empty($arr_kredit_lainya[$num])){
		$no_akunx 	= $arr_kredit_lainya[$num]->no_akun;
		$saldox		= $arr_kredit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_kreditor_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit lainnya")';
		$exec   = mysqli_query($koneksi,$query);
	}
}

insert_log($no_jurnal,"Mengubah Data");
mysqli_commit($koneksi);
echo 1;
?>