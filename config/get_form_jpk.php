<?php
	include 'config.php';
	$id = @$_POST['id'];
	$rowx = @$_POST['row'];
	if(empty($id) || empty($rowx)){
		exit;
	}

	$id_identitas_debitor = explode(",",$id); 
	$akun = $id_identitas_debitor[0];
	//im_debugging($akun);

	//get selected debit
    $query  = "SELECT no_akun, nama_akun from tb_akun where no_akun='".$akun."'";
    $result = mysqli_query($koneksi,$query);
    $data_selected   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    //get saldo
	$query  = "SELECT total_pengajuan,total_telah_disalurkan from v_pencairan_debitor where no_id_debitor='".$id."'";
    $exe 	= mysqli_query($koneksi,$query);
    $data   = mysqli_fetch_object($exe);
    $saldo  = (int)$data->total_pengajuan-(int)$data->total_telah_disalurkan;

	//get akun tipe bank
    $query  = "SELECT no_akun, nama_akun from tb_akun where tipe_akun=20";
    $result = mysqli_query($koneksi,$query);
    $data_bank   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    //get akun tipe bank
    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_lainya   = mysqli_fetch_all($result,MYSQLI_ASSOC);
    //im_debugging($data_lainya);

    //get akun tipe bank dan sesuai dengan kredit salur
    /*$query  = "SELECT no_akun, nama_akun from tb_akun a, tb_kredit_salur k where k.no_akun = a.no_akun and saldo_normal = 'Debit' AND ";
    $result = mysqli_query($koneksi,$query);
    $data_kredit   = mysqli_fetch_all($result,MYSQLI_ASSOC);*/

?>
<tr>
    <td  class="bg-debit"  >
    	<button type="button" class="btn btn-sm btn-danger" id="min-<?= $rowx; ?>" onclick="delete_row('min-<?= $rowx; ?>')"> 
    		<i class="fa fa-minus "></i>
    	</button>
    </td>

<!-- Debit -------------------------------------------->
<td class="bg-debit" > 
	<select class="form-control select22" name="debit[<?= $rowx; ?>]">
		<option value="">--</option>
		<?php foreach ($data_selected as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>" selected><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="bg-debit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_debit[<?= $rowx; ?>]" value="<?= $saldo; ?>">
	</div>
</td>
<!-- End Debit -->

<!-- Kredit Bank -------------------------------------------->
<td class="bg-kredit" > 
	<select class="form-control select22" name="kredit_bank[<?= $rowx; ?>]">
		<option value="">--</option>
		<?php foreach ($data_bank as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="bg-kredit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_bank[<?= $rowx; ?>]" value="">
	</div>
</td>
<!-- End Kredit Bank  -->

<!-- Kredit Lainya -------------------------------------------->
<td class="bg-kredit" > 
	<select class="form-control select22" name="kredit_lainya[<?= $rowx; ?>]">
		<option value="">--</option>
		<?php foreach ($data_lainya as $key => $row) { ?>
			<option value="<?= $row['no_akun']; ?>"><?= $row['no_akun'].' - '.$row['nama_akun']; ?></option>
		<?php } ?>
	</select>
</td>
<td class="bg-kredit">
	&nbsp <br>
	<div class="input-group mb-3">
	  <input type="text" class="form-control CentreeRupiah" placeholder="Saldo" name="saldo_kredit_lainya[<?= $rowx; ?>]" >
	</div>
</td>
<!-- End Kredit Lainya  -->

</tr>

<script type="text/javascript">
$('.select22').select2({
    theme: "bootstrap",
    width: "100%",
    placeholder:"--",
  allowClear: true
});



</script>

<script type="text/javascript">
	$( ".CentreeRupiah" ).CentreeRupiah();
	$(".CentreeRupiah").keyup(function(){
          //get attribut
        var name    = $(this).attr('name');
        var name_tmp  = name.split("___");
        var name_real = name_tmp[1];
        var angka     = $(this).val();
        var prefix    = "Rp. ";

        //change rupiah
        returnx = CentreeFormatRupiah(angka);

        var value  = $(this).val(returnx[0]);
        $("input[name='"+name_real+"']").val(returnx[1]); 

    });
</script>