<?php
include 'config.php';
@$no_jurnal 			= $_POST['no_jurnal'];
@$tanggal 				= $_POST['tanggal'];
@$no_bukti 				= $_POST['no_bukti'];
@$memo 					= $_POST['memo'];
@$debit 				= $_POST['debit'];
@$kredit_bank 			= $_POST['kredit_bank'];
@$kredit_lainya 		= $_POST['kredit_lainya'];
$id_tahun_buku  		= $_SESSION['tahun_buku'];
$tipe_jurnal			= 'JKK';
@$saldo_debit 			= $_POST['saldo_debit'];
@$saldo_kredit_bank 	= $_POST['saldo_kredit_bank'];
@$saldo_kredit_lainya 	= $_POST['saldo_kredit_lainya'];

if(empty($debit)){
	echo "Maaf, data Debit tidak ditemukan";
	exit;
}

if(empty($kredit_bank) && empty($kredit_lainya)){
	echo "Maaf, Data Kredit tidak ditemukan";
	exit;
}

if(empty($no_jurnal)){
	echo "Maaf, No Jurnal tidak ditemukan";
	exit;
}


$total_saldo_debit         = array_sum($saldo_debit);
$total_saldo_kredit_bank   = array_sum($saldo_kredit_bank);
$total_saldo_kredit_lainya = array_sum($saldo_kredit_lainya);

$saldo_kredit = $total_saldo_kredit_lainya + $total_saldo_kredit_bank;
if($total_saldo_debit !== $saldo_kredit){
	echo "Maaf, Total saldo debit & kredit tidak balance";
	exit;
}

#0 Transaction
mysqli_autocommit($koneksi, FALSE);

#1 insert ke table jurnal
$query  = 'UPDATE tb_jurnal SET no_bukti="'.$no_bukti.'" ,tanggal="'.$tanggal.'" ,memo="'.$memo.'" WHERE no_jurnal="'.$no_jurnal.'" AND tipe_jurnal="'.$tipe_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);
if(!$exec)
{
	echo "Maaf, Terjadi Kesalahan";
	exit;
}

#2 delete all in tb_jurnal_debit_kredit
$query  = 'DELETE FROM tb_jurnal_debit_kredit WHERE no_jurnal="'.$no_jurnal.'"';
$exec   = mysqli_query($koneksi,$query);

#insert ke table tb_jurnal_debit_kredit
$arr_debit = array();
foreach ($debit as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_debit[$num]
	);
	$arr_debit[$num] = $arr_tmp;
}

$arr_kredit_bank = array();
foreach ($kredit_bank as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_bank[$num]
	);
	$arr_kredit_bank[$num] = $arr_tmp;
}

$arr_kredit_lainya = array();
foreach ($kredit_lainya as $num => $row) {
	$arr_tmp = (object)array(
		'no_akun'=>$row,
		'saldo'=>$saldo_kredit_lainya[$num]
	);
	$arr_kredit_lainya[$num] = $arr_tmp;
}
//im_debugging($arr_kredit_lainya);

if(!empty($arr_debit[$num])){
	foreach ($arr_debit as $num => $row) {
		$no_akunx 	= $arr_debit[$num]->no_akun;
		$saldox		= $arr_debit[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Debit")';
		$exec   = mysqli_query($koneksi,$query);
	}
}

if(!empty($arr_kredit_bank[$num])){
	foreach ($arr_kredit_bank as $num => $row) {
		$no_akunx 	= $arr_kredit_bank[$num]->no_akun;
		$saldox		= $arr_kredit_bank[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Bank")';
		$exec   = mysqli_query($koneksi,$query);
	}
}

if(!empty($arr_kredit_lainya[$num])){
	foreach ($arr_kredit_lainya as $num => $row) {
		$no_akunx 	= $arr_kredit_lainya[$num]->no_akun;
		$saldox		= $arr_kredit_lainya[$num]->saldo;
		$query  = 'INSERT INTO tb_jurnal_debit_kredit VALUES ("'.$no_jurnal.'","'.$no_akunx.'","'.$num.'","'.$saldox.'","Kredit Lainya")';
		$exec   = mysqli_query($koneksi,$query);
	}
} 


insert_log($no_jurnal,"Mengubah Data");
mysqli_commit($koneksi);
echo 1;
?>