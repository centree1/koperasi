<?php 
include 'config/config.php';
$title = "Buku Pembantu Utang Bank";
include 'template/header.php';
cek_tahun_buku();
?>

<!-- Main Start Here -->
    <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class=" row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center">
                             <h5 style="display: inline-block;"> <i class="fa fa-book fa-fw"></i>  Buku Besar Akun  </h5>
                        </div> 
                        <div class="col-md-6 col-xs-12 dim-center text-right">
                            <button type="button" onclick="toggle_filter()" class="btn btn-primary" ><i class="fa fa-filter fa-fw"></i> Filter </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        get_notif('psnjrnal');
    ?>

    <!-- Filter ------------------------------------------------------------>
    <div class="row" id="filter" style="display: block;">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <form method="POST" id="frmz"> 
                <div class="card-body">
                    <div class="row mb-4" >
                        <div class="col-md-6 col-6 ">
                             <h4 style="display: inline-block;"> <i class="fa fa-filter fa-fw"></i> Filter </h4>
                        </div> 
                        <div  class="col-md-6 col-6 text-right">
                            <button type="button" onclick="toggle_filter()" class="btn btn-secondary"><i class="fa fa-close fa-fw"></i> </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-12">
                            <div class="form-group">
                                <label>Akun</label>
                                <select name="no_akun" class="form-control select2" required="" name="no_akun" >
                                    <option value=""> -- Pilih --</option>
                                    <?php
                                        $query = mysqli_query($koneksi,"select * from tb_akun where id_kategori not in(24)");
                                        while($row = mysqli_fetch_object($query)){ ?>
                                            <option value="<?= $row->no_akun; ?>"><?= $row->no_akun."  |  ".$row->nama_akun; ?></option>
                                    <?php }  ?>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label></label>
                                <button type="submit" class="btn btn-primary btn-block" > <i class="fa fa-filter fa-fw"></i> Filter </button>
                                <button type="button" onclick="load_table()" class="btn btn-warning btn-block" ><i class="fa fa-refresh fa-spin fa-fw"></i> Reset </button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Filter -------------------------------------------------------->

     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    
                </div>
                    
                </div>
            </div>
        </div>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript">
     $('#frmz').submit(function(event) { 
        event.preventDefault(); //Prevent the default submit
        var values = $(this).serialize();
        console.log(values);
        get_with_ajax(values, "config/get_table_bb_akun", "tbl");
        return false; //stop
    });

    var selected_row = null;

    function toggle_filter(){
        $("#filter").fadeToggle();
    }
    function filterMe(){
        var tgl1 = $("#tglAwal").val();
        var tgl2 = $("#tglAkhir").val();
        var tipe = $("#tipe").val();
        load_table(tglAwal,tglAkhir);
    }
    
    load_table();
    function load_table(){
        get_with_ajax("", "config/get_table_bb_akun", "tbl"); 
    }

</script>

<?php include 'template/footer.php'; ?>