<?php
include 'config/config.php';
$title = "Edit User";
include 'template/header.php';

if(EMPTY($_GET['id'])){
    set_notif('psnkredit','Maaf, Data tidak ditemukan','kredit_salur','danger','close');
}
get_role_page('edit');




$id = $_GET['id'];

$query = 'select s.*,t.nama_akun from tb_kredit_salur s , tb_akun t where t.no_akun = s.no_akun and id_kredit_salur='.$id.'';
//$query = "select * from tb_kredit_salur where id_kredit_salur='".$id."'";
$data = mysqli_fetch_object(mysqli_query($koneksi,$query));
if(empty($data)){
    set_notif('psnkredit','Maaf, Data tidak ditemukan','kredit_salur','danger','close');
}
?>

<!-- ============ Body content start ============= -->

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <h4 style="display: inline-block;"> <i class="fa fa-pencil fa-fw"></i> Edit Kredit Salur </h4>
                        <div class="float-right">
                            <button onclick="window.location = 'kredit_salur'" class="btn btn-secondary btn">
                                    <i class="fa fa-arrow-left"></i> Kembali 
                            </button>
                        </div>
                        <br>
                    </div> 
                    
                    <form id="frmz">
                            <div class="form-group">
                               <b style="color: red;">*</b><label> Akun</label>
                                <select name="No_akun" id="no_akun" class="form-control select2">
                                    
                                    <?php 
                                        $sql = mysqli_query($koneksi,"Select * from tb_akun where saldo_normal='Kredit'");
                                        while($row = mysqli_fetch_object($sql)){ ?>
                                            <option value="<?= $row->no_akun ?>" <?= selected_droplist($row->no_akun , $data->no_akun) ?> > <?= $row->no_akun." ".$row->nama_akun ?></option>
                                    <?php } ?> 
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Jenis Kredit Salur</label>
                                <select class="select2" required="" name="Kredit_salur"> 
                                    <option value="Executing" <?= selected_droplist($data->jenis_kredit,"Executing") ?>> Executing </option>
                                    <option value="Dana Sendiri" <?= selected_droplist($data->jenis_kredit,"Dana Sendiri") ?>> Dana Sendiri  </option>
                                    <option value="Chaneling" <?= selected_droplist($data->jenis_kredit,"Chaneling") ?>> Chaneling  </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Saldo</label>
                                <input type="text" value="<?= $data->saldo; ?>" class="form-control CentreeRupiah" name="Saldo" required="" autocomplete="off">
                            </div>
                            
                            
                            
                            </div>

                            
                        <center>
                        <button type="submit" class="btn btn-success btn-lg" id="btn"> <i class="fa fa-save"></i> Simpan</button>
                        </center>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 
    //Tambah Data User
    $('#frmz').submit(function(event) { 
        event.preventDefault();
        var values = $(this).serialize();
        simple_ajax(values+'&id=<?= $id; ?>','config/edit_kredit_salur','kredit_salur','Berhasil ubah data Kredit Salur');
        return false; //stop
    });
</script>



<!-- <script type="text/javascript"> 
    //Tambah Data User
    $('#frmz').submit(function(event) { 
        event.preventDefault();
        var values = $(this).serialize();
	    simple_ajax(values,'config/edit_kredit_salur','kredit_salur','Berhasil ubah data Kredit Salur');
        return false; //stop
    });
</script> -->


<?php include 'template/footer.php'; ?>