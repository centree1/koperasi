<?php
include 'config/config.php';
$title = 'Edit Jurnal Umum';
include 'template/header.php';
cek_tahun_buku();
get_role_page('edit');

if(empty($_GET['id'])){
    set_notif('psnjrnalPn','Maaf, Jurnal tidak ditemukan','jurnal_penyesuaian','danger','close');
}


@$id         = $_GET['id'];

$query      = "select * from tb_jurnal where tipe_jurnal='JPN' AND no_jurnal='".$id."' ORDER BY no_jurnal DESC LIMIT 1";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);
if(empty($fetch)){
    set_notif('psnjrnalP','Maaf, Jurnal tidak ditemukan','jurnal_penyesuaian','danger','close');
}

?>
<!-- ============ Body content start ============= -->
<style type="text/css">
    .header-debit{
        background-color: #deffe7;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-kredit{
        background-color: #e3fdff;
        color: black;
        font-weight: bold;
        text-align: center;
    }
    .header-bank{
        background-color: #fff6b8;
        color: black;
        font-weight: bold;
        text-align: center;
    }
</style>

<form id="frmz">
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Ubah Jurnal Penyesuaian
                        </label-tabel>
                        <div class="float-right">
                            <a href="jurnal_penyesuaian" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label >No. Jurnal Umum</label>
                            <input required type="text" class="form-control" placeholder="No. Jurnal Umum" value="<?= $fetch->no_jurnal; ?>" readonly>
                          </div>
                          <div class="form-group">
                            <label >Tanggal</label>
                            <input class="form-control datetimepicker CentreeTgl" required name="tanggal" type="text" class="form-control" placeholder="Tanggal"value="<?= $fetch->tanggal; ?>" >
                          </div>
                          
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >No. Bukti Kas Keluar</label>
                                <input required name="no_bukti" type="text" class="form-control" value="<?= $fetch->no_bukti; ?>" placeholder="No Bukti">
                              </div>
                              <div class="form-group">
                                <label >Memo</label>
                                <input name="memo" type="text" class="form-control" value="<?= $fetch->memo; ?>" placeholder="Memo">
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body table-responsive ">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th class=""  style="width: 2%;">
                                    <button type="button" class="btn btn-sm btn-success" onclick="addrow()"> <i  class="fa fa-plus "></i></button>
                                </th>
                                <th  class="" style="width: 40%;">Akun</th>
                                <th  class=""  style="width: 30%;">Debit</th>
                                <th  class=""  style="width: 30%;">Kredit</th>
                            </tr>
                        </thead>
                        <tbody id="tbl">
 <?php
    //get data akun
    $query  = "SELECT no_akun, nama_akun from tb_akun";
    $result = mysqli_query($koneksi,$query);
    $data_debit   = mysqli_fetch_all($result,MYSQLI_ASSOC);

    //get data jurnal debit_kredit
    $query  = "SELECT * from tb_jurnal_debit_kredit where no_jurnal = '".$id."' order by row ASC";
    $result = mysqli_query($koneksi,$query);
    $id = 1;


    while($data_jurnal   = mysqli_fetch_object($result)){ 
        $no_akun       = $data_jurnal->no_akun;
        $saldo_debit   = 0;  
        $saldo_kredit  = 0;  
        if($data_jurnal->keterangan == 'Debit'){
            $saldo_debit   = $data_jurnal->nominal;  
        }else{
            $saldo_kredit  = $data_jurnal->nominal; 
        }
    ?>
    <tr>
        <td  class=""  >
            <button type="button" class="btn btn-sm btn-danger" id="min-<?= $id; ?>" onclick="delete_row('min-<?= $id; ?>')"> 
                <i class="fa fa-minus "></i>
            </button>
        </td>

        <td class="" > 
            <select class="form-control select22" name="no_akun[<?= $id; ?>]">
                <option value="">--</option>
                <?php foreach ($data_debit as $key => $row) { ?>
                    <option value="<?= $row['no_akun']; ?>" 
                        <?= selected_droplist($row['no_akun'],$no_akun); ?>>
                        <?= $row['no_akun'].' - '.$row['nama_akun']; ?>
                            
                    </option>
                <?php } ?>
            </select>
        </td>
        <td class="">
            &nbsp <br>
            <div class="input-group mb-3">
              <input type="text" value="<?= $saldo_debit; ?>" class="form-control CentreeRupiah" onchange="change('<?= $id; ?>','debit');" id="debit<?= $id; ?>"  placeholder="Debit" name="saldo_debit[<?= $id; ?>]">
            </div>
        </td>
        <td class="">
            &nbsp <br>
            <div class="input-group mb-3">
              <input type="text" value="<?= $saldo_kredit; ?>" class="form-control CentreeRupiah" onchange="change('<?= $id; ?>','kredit');" id="kredit<?= $id; ?>" placeholder="Kredit"  name="saldo_kredit[<?= $id; ?>]" id="saldo_kredit<?= $id; ?>">
            </div>
        </td>

    </tr>
    <?php $id++; } ?> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>


<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="form-group" style="text-align: center;">
                          <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-save fa-fw"></i> Simpan</button>
                      </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</form>

<script type="text/javascript">
        
        //Edit Data Karyawan
          $('#frmz').submit(function(event) { 
            var values = $(this).serialize();
            simple_ajax(values+"&no_jurnal=<?= $fetch->no_jurnal; ?>",'config/proses_edit_jpn','jurnal_penutup','Berhasil Edit ');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

        //table form
        var i = '<?= $id; ?>';
        function addrow(){
            get_with_ajax("id="+i, "config/get_form_jpn", "tbl","no");
            i++;
        }


        function delete_row(id){
            $("#"+id).closest('tr').remove()
        }

        function change(id,tipe){
            if(tipe=='kredit'){
                $("#debit"+id).val(0);
            }else if(tipe=='debit'){
                $("#kredit"+id).val(0);
            }
        }
    </script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>