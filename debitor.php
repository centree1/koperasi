<?php 
include 'config/config.php'; 
$title = "Daftar Nama Debitor";
include 'template/header.php';
cek_tahun_buku();
$id_tahun_buku = $_SESSION['tahun_buku'];
@$tglAwal = $_POST['dari_tgl'];
@$tglAkhir = $_POST['sampe'];
$query_cek=mysqli_query($koneksi, "SELECT * FROM tb_tahun_buku where id_tahun_buku='".$id_tahun_buku."'");
$row_cek=mysqli_fetch_array($query_cek);
$awal_periode = $row_cek['awal_periode'];
$akhir_periode = $row_cek['akhir_periode'];
?>

<!-- Main Start Here -->
    
    <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class=" row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center">
                             <h5 style="display: inline-block;"> <i class="fa fa-credit-card-alt fa-fw"></i>  Daftar Debitor </h5>
                        </div> 
                        <div class="col-md-6 col-xs-12 dim-center text-right">
                            <button type="button" onclick="toggle_filter()" class="btn btn-primary" ><i class="fa fa-filter fa-fw"></i> Filter </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        get_notif('psnjrnalPn');
    ?>

    <!-- Filter ------------------------------------------------------------>
    <div class="row" id="filter" style="display: none;">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <form method="POST" id="frmz"> 
                <div class="card-body">
                    <div class="row mb-4" >
                        <div class="col-md-6 col-6 ">
                             <h4 style="display: inline-block;"> <i class="fa fa-filter fa-fw"></i> Filter </h4>
                        </div> 
                        <div  class="col-md-6 col-6 text-right">
                            <button type="button" onclick="toggle_filter()" class="btn btn-secondary"><i class="fa fa-close fa-fw"></i> </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Filter Berdasarkan</label>
                                <select id="tipe" class="form-control select2" required="" name="tipe" >
                                    <option value=""> -- Pilih --</option>
                                    <option value="tanggal_penyaluran"> Tanggal Penyaluran Kredit </option>
                                    <option value="tanggal_jatuh_tempo"> Tanggal Jatuh Tempo </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Dari Tanggal</label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Dari Tanggal" required="" id="tglAwal" name="dari_tgl" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label>Sampai Tanggal</label>
                                <input type="text" class="form-control datetimepicker CentreeTgl" placeholder="Sampai Tanggal" required="" id="tglAkhir" name="sampe" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group">
                                <label></label>
                                <button type="submit" class="btn btn-primary btn-block" > <i class="fa fa-filter fa-fw"></i> Filter </button>
                                <button type="button" onclick="load_table()" class="btn btn-warning btn-block" ><i class="fa fa-refresh fa-spin fa-fw"></i> Reset </button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Filter -------------------------------------------------------->


     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0 row" style="padding-bottom: 10px; margin-bottom: 20px; ">
                    <div class="col-md-6 col-xs-12 dim-center">
                        <!-- <label-tabel-small> Data Debitor periode : <?=date_format(new DateTime($awal_periode),"d/M/Y")." - ".date_format(new DateTime($akhir_periode),"d/M/Y")?></label-tabel-small> -->
                    </div>                    
                    <div class="col-md-6 col-xs-12 dim-center text-right">
                        <button onclick="window.location = 'tambah_debitor'" class="btn btn-success btn-sm">
                                <i class="fa fa-plus"></i> Tambah 
                        </button>
                        <button id="edit" class="btn btn-primary btn-sm dim-hidden centree-btn-edit" onclick="edit()"> <i class="fa fa-pencil"></i> Ubah </button>
                        <button id="hapus" class="btn btn-danger btn-sm dim-hidden centree-btn-hapus" onclick="deleteBuku()"> <i class="fa fa-trash"></i> Hapus </button>
                        <?php get_role_button(); ?>
                        |
                        <button id="print" class="btn btn-warning btn-sm" onclick="print()"> <i class="fa fa-print"></i> Print </button>
                    </div>
                    </div>
                    <div id="tbl" class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
                       
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript">
    var selected_row = null;
    function select_me(id){

        $("tr").removeClass('dim-selected');
        $("#row"+id).toggleClass('dim-selected');
        var selected = $("#row"+id).hasClass( "dim-selected" );
        if(selected){
            selected_row = id;
        }else{
            selected_row = null;
        }
        console.log(selected_row);

        $('#edit').removeClass('dim-hidden');
        $('#hapus').removeClass('dim-hidden');
    } 

    function edit(){
        window.location = 'edit_debitor?id='+selected_row;
    }
    function print(){
        var tglAwal =  '<?= $tglAwal; ?>';
        var tglAkhir = '<?= $tglAkhir; ?>';
        window.open('print/print_debitor?tglAwal='+tglAwal+'&tglAkhir='+tglAkhir, '_blank');
    }

    function deleteBuku(){
        pesan_confirm("Apakah anda yakin?", "Hapus data Debitor dengan id='"+selected_row+"'", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("no_id="+selected_row,'config/delete_debitor','','Data Debitor berhasil dihapus','Data Debitor gagal dihapus');
            }   
        });
    }
    function toggle_filter(){
        $("#filter").fadeToggle();
    }

    load_table();
    function load_table(tipe="",tglAwal="",tglAkhir=""){    
        get_with_ajax("", "config/get_table_debitor", "tbl");
    }

    $('#frmz').submit(function(event) { 
        event.preventDefault(); //Prevent the default submit
        var values = $(this).serialize();
        console.log(values);
        get_with_ajax(values, "config/get_table_debitor", "tbl");
        return false; //stop
    });
</script>

<?php include 'template/footer.php'; ?>