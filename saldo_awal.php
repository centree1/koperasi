<?php
include 'config/config.php';
$title = "Saldo Awal";
include 'template/header.php';

cek_tahun_buku();
$id_tahun_buku      = $_SESSION['tahun_buku'];
?>

<!-- ============ Body content start ============= -->
<?php
    get_notif('psnakn');
?>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <h4 style="display: inline-block;"> <i class="fa fa-book fa-fw"></i> Saldo Awal </h4>
                    </div> 
                    <div class="row">
                        <div class="col-md-12 row">
                            <div class="col-md-5" style="padding-right: 0 !important;">
                                <select name="filter" class="form-control form-control-lg select2" onchange="get_kategori(this.value);">
                                <option value=""> Semua Kategori </option>
                                <?php 
                                $query = mysqli_query($koneksi,'select * from tb_kategori_akun ORDER BY nama_kategori ASC');
                                while($row = mysqli_fetch_object($query)){?>                              
                                <option value="<?= $row->id_kategori; ?>"><?= $row->nama_kategori; ?></option>
                                <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-7 col-xs-12" style="margin-left: 0 !important;">
                                <div class="float-right">
                                    <!-- <button href="edit_saldo_awal" class="btn btn-primary btn-icon-split mb-3">
                                        <i class="fa fa-pencil"></i> Edit Saldo Awal  
                                    </button> -->
                                </div>
                            </div>
                            
                                
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                   
                    <div class="container-form table-responsive" id="datax">
                      
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<!-- Modal Tambah Akun-->
<div class="modal fade" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mdlhdr">Mohon Tunggu . . .</h5>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="mdlbdy"></div>    
    </div>
  </div>
</div>


<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    function open_modal(id=""){
        $("#mdlhdr").empty();
        $("#mdlhdr").append("Ubah Saldo Awal Akun "+id);
        get_with_ajax('no_akun='+id, 'get_form_saldo_awal', "mdlbdy");
        $('#exampleModal').modal('show');
    }
</script>

<script type="text/javascript"> 
    /*function deleteUser(id){
        pesan_confirm("Apakah anda yakin?", "Data yang telah dihapus tidak dapat dikembalikan", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("no_akun="+id,'config/delete_coa','','User berhasil dihapus','User gagal dihapus');
            }
        });
    }*/

    get_kategori();
    function get_kategori(id=""){
        get_with_ajax('id='+id, 'get_saldo', "datax");
    }
</script>

<?php include 'template/footer.php'; ?>