<?php
  include 'config/config.php';
  $nama = NULL;
  $id = NULL;
  $no_kategori = NULL;
  $action = "tambah";
  if(!empty($_POST['id'])){
      $query = mysqli_query($koneksi,'select * from tb_kategori_akun where id_kategori="'.$_POST['id'].'"');
      $row = mysqli_fetch_object($query);
      $id = $row->id_kategori;
      $nama = $row->nama_kategori;
      $no_kategori = $row->no_kategori;
      $action = "edit";
?>
    <div class="form-group">
        <label >ID Tipe</label>
        <input name="id" type="text" class="form-control" value="<?= $id; ?>" placeholder="ID Kategori Akun" readonly>
    </div>
<?php } ?>
    
<form id="frmzx">
  <div class="form-group">
      <label >No Kategori Akun</label>
      <input name="no_kategori" type="text" class="form-control" placeholder="No Kategori Akun" value="<?= $no_kategori; ?>">
    </div>
    <div class="form-group">
      <label >Nama Kategori</label>
      <input name="kategori_akun" type="text" class="form-control" placeholder="Nama Kategori Akun" value="<?= $nama; ?>">
    </div>

  <div class="form-group" style="text-align: center;">
        <button type="submit" class="btn btn-success">Simpan</button>
  </div>
</form>

<script type="text/javascript">
//Tambah Karyawan
  $('#frmzx').submit(function(event) { 
    event.preventDefault(); //disable refresh when submit
    var action = '<?= $action; ?>';
    if(action=='edit'){
      var url = 'config/edit_kategori_akun';
    }else{
      var url = 'config/tambah_kategori_akun';
    }
    var values = $(this).serialize()+"&id=<?= $id; ?>";
    simple_ajax(values,url,'','','Gagal '+action+' Kategori Akun');
    return false; 
});
</script>