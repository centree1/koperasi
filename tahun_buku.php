<?php 
include 'config/config.php';
$title = "Tahun Buku";
include 'template/header.php';
get_role_page('menu_kategori_akun');
?>

<!-- Main Start Here -->
  
    <?php
        get_notif('psnbku');
    ?>
     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            <i class="fa fa-book"> </i> Tahun Buku
                        </label-tabel>
                        <div class="float-right">
                            <a href="tambah_tahun_buku" class="btn btn-success btn-icon-split mb-3">
                                <i class="fa fa-plus"></i> Tambah 
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
                        <table class="table table-bordered table-hover table-striped dataTable"  >
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Awal Periode</th>
                                    <th>Akhir Periode</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = "SELECT * FROM tb_tahun_buku order by awal_periode";
                                $sql = mysqli_query($koneksi, $query);
                                $a = 1;
                                while ($row = mysqli_fetch_array($sql)) {
                                    echo "<tr>";
                                    echo "<td>" . $a . "</td>";
                                    echo "<td>" . tgl_indo($row['awal_periode']). "</td>";
                                    echo "<td>" .tgl_indo($row['akhir_periode']) . "</td>";
                                    echo "<td>
                                        <button class='btn btn-success btn-sm' onclick='buka_buku(" . $row['id_tahun_buku'] . ")'>Pilih Buku</button> ";
                                    if($_SESSION['role']=='ADMIN'){
                                        echo  "<a class='btn btn-primary btn-sm' href='edit_buku?id=" . $row['id_tahun_buku'] . "'>Edit</a>  
                                        <button class='btn btn-danger btn-sm' onclick='deleteBuku(" . $row['id_tahun_buku'] . ")'>Hapus</button>
                                    </td>";
                                    }
                                    echo "</tr>";
                                    $a++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
    
<!-- Main End Here -->

<!-- ============ Body content End ============= -->
<script type="text/javascript"> 
    function deleteBuku(id){
        pesan_confirm("Apakah anda yakin?", "Data yang telah dihapus tidak dapat dikembalikan", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("no_id="+id,'config/delete_buku','','Tahun Buku berhasil dihapus','Tahun Buku gagal dihapus');
            }
        });
    }

    function buka_buku(id){
        simple_ajax("id="+id,'config/buka_buku','','','Gagal, Buku tidak ditemukan');
    }
</script>

<?php include 'template/footer.php'; ?>