<?php 
include '../config/config.php';
$title = "Laporan Arus Kas";
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan_arus_kas.xls");
cek_tahun_buku();

include '../config/get_lhu.php';

//tahun buku sekarang
$tahun_buku_sekarang    = $_SESSION['tahun_buku'];
$awal_periode_sekarang  = $_SESSION['awal_periode'];
$tahun_sekarang         = 'Tahun '.date('Y', strtotime($awal_periode_sekarang));

//tahun buku kemarin
$query                  = "SELECT id_tahun_buku,awal_periode,akhir_periode from tb_tahun_buku where awal_periode < '".$awal_periode_sekarang."' order by awal_periode DESC LIMIT 1";
$exe                    = mysqli_query($koneksi,$query);
$old_book               = mysqli_fetch_object($exe);
if(empty($old_book)){
    $tahun_buku_sebelumnya  = 0; 
    $awal_periode_sebelumnya  = 0;
    $tahun_sebelumnya       = "Tahun ".date("Y", strtotime( date( "Y-m-d", strtotime( $awal_periode_sekarang ) ) . "-1 year" ) );
}else{
    $tahun_buku_sebelumnya  = $old_book->id_tahun_buku; 
    $awal_periode_sebelumnya  = $old_book->awal_periode;
    $tahun_sebelumnya       = 'Tahun '.date('Y', strtotime($awal_periode_sebelumnya));
}


?>
<style type="text/css">
    .separator{
         background-color: white; padding-top: 15px; border-right: 0px !important; border-left: 0px !important;
    }
    .kategori1{
        font-weight: bold;
    }
    .kategori2{
        font-weight:bold !important; 
        padding-left: 30px !important;
    }
    .kategori3{
        padding-left: 50px !important;
    }
</style>
<!-- Main Start Here -->
 
     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    <table>
    <tr>
        <td width="70%">
            <img src="<?= base_url('assets/img/logoforexcel.png'); ?>"  style="text-align: center;"> </img>
        </td>
        <td align="center" colspan="2" width="15%" style="font-weight: bold;"> 
            KOPERASI KARYAWAN PEMUKIMAN BIDA <br />
            KOPKARKIM BIDA <br />
            LAPORAN ARUS KAS <br />
            UNTUK TAHUN YANG BERAKHIR <?= tgl_indo($_SESSION['akhir_periode']); ?> 
        </td> 
    </tr>
</table>            
<br>
<br>
<table border="1" class="table table-sm table-bordered" width="100%" style="text-align: center;">

    <tr style="height: 0.5px !important; font-size: 0.5pt; background-color: #f5ffb5;">
        <td width="60%"></td>
        <td width="20%" >&nbsp </td>
        <td width="20%" >&nbsp</td>
    </tr>


    <!-- Start  ---------------------------------------------------->                          
        <tr style="background-color: #cdf7f5;">
            <td colspan="3" class="kategori1">ARUS KAS DARI AKTIVITAS OPERASI</td>
        </tr>
        <tr>
            <td class="kategori1" style="font-weight: normal;">Sisa Hasil Usaha</td>
            <td style="text-align: right;"></td>
            <td style="text-align: right;"> <?= rupiah($shu,'Rp. '); ?></td>
        </tr>
        <tr>
            <td class="kategori1" style="font-weight: normal;"> Penyesuaian sisa hasil usaha ke hasil
</td>
            <td style="text-align: right;"><?php $x1=0; echo rupiah($x1,"Rp. "); ?></td>
            <td style="text-align: right;"></td>
        </tr>
        <tr>
            <td class="kategori1" style="font-weight: normal;">Kas yang disediakan dari akivitas operasi
</td>
            <td style="text-align: right;"><?php $x2=0; echo rupiah($x2,"Rp. "); ?></td>
            <td style="text-align: right;"></td>
        </tr>
    <?php
        $saldo_mutasi = 0;
        $saldo_beginning_balance = 0;
        $saldo_aset_lancar_sebelumnya = 0;
        $saldo_aset_lancar_sekarang   = 0;
        $query = "SELECT a.no_akun, a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '10','16' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ 
            $no_akun = $row->no_akun;
            $tidak_mutasi = array('100','101','102','103','105','106','107','108');
            if(in_array($no_akun, $tidak_mutasi)){
                continue;
            }
            ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya-$row->saldo_sekarang,"Rp. "); ?></td>
            <td style="text-align: right;"></td>
            
        </tr>
            <?php 
                $saldo_aset_lancar_sekarang    += $row->saldo_sekarang;
                $saldo_aset_lancar_sebelumnya  += $row->saldo_sebelumnya;
                $saldo_mutasi += $row->saldo_sebelumnya-$row->saldo_sekarang;
                } 
                $saldo_operasi = $shu+$x1+$x2+$saldo_mutasi;
            ?>
    <tr  style="background-color: #ccffb5;">
        <td colspan="2" class="kategori1">  Kas Bersih dari Aktivitas Operasi </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($saldo_operasi,"Rp. "); ?></td>
    </tr>
    <!-- END  ---------------------------------------------------->
    <tr> <td colspan="3" class="separator"></td></tr>
    <!-- Start  ---------------------------------------------------->                          
        <tr style="background-color: #cdf7f5;">
            <td colspan="3" class="kategori1">ARUS KAS DARI AKTIVITAS INVESTASI</td>
        </tr>
        
    <?php
        $saldo_investasi = 0;
        $saldo_beginning_balance = 0;
        $saldo_aset_lancar_sebelumnya = 0;
        $saldo_aset_lancar_sekarang   = 0;
        $query = "SELECT a.no_akun, a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '11' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ 
            
            ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya-$row->saldo_sekarang,"Rp. "); ?></td>
            <td style="text-align: right;"></td>
            
        </tr>
            <?php 
                $saldo_investasi += $row->saldo_sebelumnya-$row->saldo_sekarang;
                } 
            ?>
    <tr  style="background-color: #ccffb5;">
        <td colspan="2" class="kategori1">  Kas Bersih dari Aktivitas Investasi </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($saldo_investasi,"Rp. "); ?></td>
    </tr>
    <!-- END  ---------------------------------------------------->
    <tr> <td colspan="3" class="separator"></td></tr>
     <!-- Start  ---------------------------------------------------->                          
        <tr style="background-color: #cdf7f5;">
            <td colspan="3" class="kategori1">ARUS KAS DARI AKTIVITAS PENDANAAN</td>
        </tr>
        
    <?php
        $saldo_pendanaan = 0;
        $query = "SELECT a.no_akun, a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '18' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ 
            
            ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya-$row->saldo_sekarang,"Rp. "); ?></td>
            <td style="text-align: right;"></td>
            
        </tr>
            <?php 
                $saldo_pendanaan += $row->saldo_sebelumnya-$row->saldo_sekarang;
                } 
            ?>
    <tr  style="background-color: #ccffb5;">
        <td colspan="2" class="kategori1">  Kas Bersih dari Aktivitas Pendanaan </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($saldo_pendanaan,"Rp. "); ?></td>
    </tr>
    <!-- END  ---------------------------------------------------->
    <tr> <td colspan="3" class="separator"></td></tr>
    <!-- END EKUITAS  ---------------------------------------------------->
    <?php $total = $saldo_operasi+$saldo_investasi+$saldo_pendanaan; ?>

    <tr  style="background-color: #ccffb5;">
        <td colspan="2" class="kategori1">  Kenaikan atau Penurunan Kas </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($total,"Rp. "); ?></td>
    </tr>

    <?php
    //total kas awal tahun diambil dari total kas tipe bank tahun sebelumnya
    $query = "SELECT sum(xyz.saldo_sebelumnya) as tot_saldo_sebelumnya, sum(xyz.saldo_sekarang) as tot_saldo_sekarang from (SELECT a.no_akun, a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '0' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$id_tahun_buku."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '10' ) AND a.no_akun in ('100','101','102','103','105','106','107','108')) xyz";
    
    $exe   = mysqli_query($koneksi,$query);
    $data  = mysqli_fetch_object($exe);
    ?>
    <tr  style="background-color: #ccffb5;">
        <td colspan="2" class="kategori1">  Saldo Total Kas Awal Tahun
  </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($data->tot_saldo_sebelumnya,"Rp. "); ?></td>
    </tr>
    <tr  style="background-color: #ccffb5;">
        <td colspan="2" class="kategori1">  Saldo Total Kas Akhir Tahun
 </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($total+$data->tot_saldo_sebelumnya,"Rp. "); ?></td>
    </tr>   


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- Main End Here -->
<script type="text/javascript">
    function toggle(id){
        $("#"+id).fadeToggle();
    }
    
</script>

<!-- ============ Body content End ============= -->
