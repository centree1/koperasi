<?php
    include '../config/config.php';
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=buku_besar_akun.xls");

    cek_tahun_buku();
    
    $id_tahun_buku = $_SESSION['tahun_buku'];
    $awal_periode = $_SESSION['awal_periode'];
    $akhir_periode = $_SESSION['akhir_periode'];

    @$no_akun = $_GET['no_akun'];
    
    //$tahun_buku     = $_SESSION['tahun_buku'];
    if(empty($no_akun)){
        echo "<center> <b> Tidak ada Data yang dipilih </b> </center>";
        exit;
    }

    #inisialisasi array untuk di table
    $buku_besar_akun = array();
    $i = 0;

    #get data akun
    $query = "select nama_akun,saldo_normal from tb_akun WHERE no_akun='".$no_akun."'";
    $execute = mysqli_query($koneksi,$query);
    $fetch = mysqli_fetch_object($execute);
    $nama  = $fetch->nama_akun;
    $sn    = $fetch->saldo_normal;


    #Get buku besar jurnal biasa
    $query = "select * from bb_akun WHERE no_akun='".$no_akun."' AND id_tahun_buku='".$id_tahun_buku."' ORDER BY tanggal ASC ";
    //im_debugging($query);
    $execute = mysqli_query($koneksi,$query);
?>
<div class="row" style="padding-bottom: 20px;">
    <div class="col-md-4" style="padding-left: 100px; text-align: center;">
      <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
      <h4> KOPKARKIM BIDA</h4>
    </div>
    <div class="col-md-8" style="padding-top: 40px;">
      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">BUKU BESAR AKUN</h4>
  <h5 style="text-align: center;">Periode <?= tgl_indo($awal_periode); ?>  sampai <?= tgl_indo($akhir_periode); ?> </h5>
    </div>
  </div>

<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <div class="row" style="margin-bottom: 30px;">
<table>
<tr>
<td> <h6> Akun : <b> <?= $no_akun; ?></b> | <b><?= $nama; ?></b></h6></td>
<td></td>
<td></td>
<td></td>
<td><h6>  Saldo Normal : <b> <?= $sn; ?></b> </h6>
</td>
</tr>
</table>

    <table border="2" class="table table-sm table-bordered" width="100%" style="text-align: center;">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Deskripsi</th>
                <th>Debit</th>
                <th>Kredit</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $x = 0;
                $saldo = 0;
                while($row = mysqli_fetch_object($execute)){
                $saldo = $saldo + $row->saldo;

                $debit = null;
                $kredit = null;
                if (strpos($row->keterangan, 'Debit') !== false) {
                    $debit  = rupiah($row->nominal_pada_jurnal,"Rp. ");
                }else{
                    $kredit = rupiah($row->nominal_pada_jurnal,"Rp. ");
                }
            ?>
            <tr>
            <td> <?= tgl_indo($row->tanggal); ?> </td>
            <td> <?= $row->deskripsi; ?> </td>
            <td align="right"> <?= $debit; ?> </td>
            <td align="right"> <?= $kredit; ?> </td>
            <td align="right"> <?= rupiah($saldo,"Rp. "); ?> </td>
            </tr>
            <?php } ?>
        </tbody>  
    </table>
</div>

    <script type="text/javascript">
        $('.dataTable').DataTable({
            "ordering": false
        });
        
        window.print();
        setTimeout(window.close, 0);
    </script>