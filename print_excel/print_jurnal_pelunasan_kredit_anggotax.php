<?php ob_start();
    include '../config/config.php';
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=jurnal_pelunasan_kredit.xls");
    cek_tahun_buku();

    @$tglAwal = $_GET['tglAwal'];
    @$tglAkhir = $_GET['tglAkhir'];
    @$tipe = $_GET['tipe']; //tipe pencarian
    @$channeling = $_GET['channeling'];
    $tahun_buku = $_SESSION['tahun_buku'];
    $awal_periode = $_SESSION['awal_periode'];
    $akhir_periode = $_SESSION['akhir_periode'];

    if($channeling=="true"){
        $tipe_jurnal = "AND tipe_jurnal='JPKA_CHANNELING' ";
        $url_tambah  = "tambah_jpka_channeling";
        $url_edit    = "edit_jpka";
        $namatabel   = "CHANNELING";
    }else{
        $tipe_jurnal = "AND tipe_jurnal='JPKA' ";
        $url_tambah  = "tambah_jpka";
        $url_edit    = "edit_jpka";
        $namatabel   = "ANGGOTA";
    }

    if(empty($tglAwal) && empty($tglAkhir)){
        $query = "select * from v_jurnal_debitor WHERE id_tahun_buku='".$tahun_buku."' ".$tipe_jurnal." ORDER BY no_jurnal,keterangan ASC";
    }else{
        $query = "select * from v_jurnal_debitor WHERE id_tahun_buku='".$tahun_buku."' ".$tipe_jurnal." AND tanggal BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  ORDER BY no_jurnal,keterangan ASC";
    }   

    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }

 ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 400px;}
   table td {word-wrap:break-word;width: 50%;}
   </style>
   <?php include 'print_bootstrap.php'; ?>
   <style type="text/css">
     .dim-memo{
            background-color: #fff5c9;
            font-style: italic;
            text-align: center;
        }
      .table-header{
          text-align: center;
          font-weight: bold;
          color: black;
          height: 60%;
          padding-bottom: 60%;
      }
   </style>
</head>
<body>
  <div class="row" style="padding-bottom: 20px;">
    <!-- <div class="col-md-4" style="padding-left: 100px; text-align: center;">
      <img src="<?= base_url('assets/img/logoforexcel.png'); ?>">
      <h4> KOPKARKIM BIDA</h4>
    </div>
    <div class="col-md-8" style="padding-top: 40px;">
      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">JURNAL UMUM</h4>
  <h5 style="text-align: center;">Periode <?= $awal_periode; ?>  sampai <?= $akhir_periode; ?> </h5>
    </div> -->

    <!-- HEADER -->
    <table>
      <tr>
        <td></td> 
        <td  style="text-align: center;"> <img src="<?= base_url('assets/img/logoforexcel.png'); ?>"> </td>
        <td colspan="5"rowspan="5" valign="center" style="text-align: center; font-weight: bold;"> 
          <br>&nbsp
         KOPERASI KARYAWAN PEMUKIMAN BIDA <br>
         JURNAL PELUNASAN KREDIT <?= $namatabel ?><br>
         Periode <?= tgl_indo($awal_periode); ?>  sampai <?= tgl_indo($akhir_periode); ?>
         <br>&nbsp <br>&nbsp
       </td> 
      </tr> 
    </table>
  </div>
  
<table border="2" class="table table-sm table-bordered" width="100%" style="text-align: center;">
        <thead>
            <tr>
                <th>No. Jurnal</th>
                <th>Tanggal</th>
                <th>No. Bukti</th>
                <th>Jenis Kredit</th>
                <th>Nomor Akun</th>
                <th>Nama Akun</th>
                <th>No. Id Debitor</th>
                <th>Debit</th>
                <th>Kredit</th>
            </tr>
        </thead>
        <tbody>
            <?php  
            $jenis_kredit    = null;
            $no_id_debitor   = null;
            $temp_memo       = null;
            $temp_tahun_buku = null;
            $temp_ju         = null;
            $total_debit     = 0 ;
            $total_kredit    = 0 ;
            while($row = mysqli_fetch_object($execute)){ 
                    $no_jurnal     = $row->no_jurnal;
                    if($temp_ju !== $no_jurnal){
                        $temp_ju    = $no_jurnal;
                        $tanggal    = tgl_indo($row->tanggal);
                        $no_bukti   = $row->no_bukti;
                        $no_jurnal  = $row->no_jurnal;
                        $jenis_kredit    = $row->jenis_kredit;
                        $no_id_debitor   = $row->no_id_debitor;

                        /*jika no jurnal tidak sama dengan temp berarti awal row baru
                         1. keluarkan temp memo => memo dari jurnal sebelumnya
                         2. set temp memo => memo dari jurnal sekarang*/
                        echo $temp_memo;

                        //set isi kolom, HARUS SESUAI DENGAN HEADER walau ada colspan
                        //4 kolom pertama wajib, agar saat dicari ter group
                        $arr = array($row->memo); 
                        $i = 0;
                        $temp_memo = '<tr class="dim-memo">';
                        while($i<count($arr)){
                           
                                $x = "<td colspan='9'>".$arr[$i]."</td>";
                           
                            $temp_memo  = $temp_memo.$x;
                            $i++;
                        }
                        $temp_memo = $temp_memo."</tr>";
                        
                    }
                    
                    $no_akun    = $row->no_akun;
                    $nama       = $row->nama_akun;
                    $ket        = $row->keterangan;
                    $debit      = null;
                    $kredit     = null; 
                    if (preg_match('/\bDebit\b/', $ket)) {
                        $debit = rupiah($row->nominal);
                        $total_debit = $total_debit+$row->nominal;
                    }else{
                        $kredit = rupiah($row->nominal);
                        $total_kredit = $total_kredit+$row->nominal;
                    }

                ?>
                
                    <tr class="row<?= $temp_ju; ?>">
                        <td><?= $no_jurnal; ?></td>
                        <td><?= $tanggal; ?></td>
                        <td><?= $no_bukti; ?></td>
                        <td><?= $jenis_kredit; ?></td>
                        <td align="right"><?= $no_akun; ?></td>
                        <td><?= $nama; ?></td>
                        <td><?= $no_id_debitor; ?></td>
                        <td style="text-align: right;"><?= $debit; ?></td>
                        <td style="text-align: right;"><?= $kredit; ?></td>
                    </tr>
            <?php } echo $temp_memo; ?>
        </tbody>
        <tr>
            
            <td colspan="7" class="dim-saldo" align="center" style="text-align: center;">Saldo</td>
            <td class="dim-saldo-debit" style="text-align: right;"><?= rupiah($total_debit); ?></td>
            <td class="dim-saldo-kredit" style="text-align: right;"><?= rupiah($total_kredit); ?></td>
        </tr>
</table>
</body>
<script type="text/javascript">
  window.print();
 setTimeout(window.close, 0);
</script>

</html>
