<?php
    include '../config/config.php';
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=buku_besar_pembantu_piutang_anggota.xls");

    cek_tahun_buku();
    $awal_periode = $_SESSION['awal_periode'];
    $akhir_periode = $_SESSION['akhir_periode'];

    @$no_id_debitor = $_GET['no_id_debitor'];
    //$tahun_buku     = $_SESSION['tahun_buku'];
    if(empty($no_id_debitor)){
        echo "<center> <b> Tidak ada Data yang dipilih </b> </center>";
        exit;
    }

    #Get Data Debitor
    $query = "select no_id_debitor, nama from bb_pembantu_piutang_anggota WHERE no_id_debitor='".$no_id_debitor."'";
    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }
    $data = mysqli_fetch_object($execute);
    $nama = $data->nama;

    #Get data buku besar
    //id_tahun_buku='".$tahun_buku."' AND
    $query = "select * from bb_pembantu_piutang_anggota WHERE no_id_debitor='".$no_id_debitor."' ORDER BY tanggal";
    
    $execute = mysqli_query($koneksi,$query);
    if(!$execute){
        echo "<center> Kesalahan pada Sistem</center>";
        exit;
    }

?>
<div class="row" style="padding-bottom: 20px;">
    <div class="col-md-4" style="padding-left: 100px; text-align: center;">
      <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
      <h4> KOPKARKIM BIDA</h4>
    </div>
    <div class="col-md-8" style="padding-top: 40px;">
      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">BUKU PEMBANTU PIUTANG ANGGOTA </h4>
  <h5 style="text-align: center;">Periode <?= tgl_indo($awal_periode); ?>  sampai <?= tgl_indo($akhir_periode); ?> </h5>
    </div>
  </div>

<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <div class="row" style="margin-bottom: 30px;">


<div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
    <table>
<tr>
<td> <h6> No Identitas Debitor : <b> <?= $no_id_debitor; ?></b> </h6></td>
<td></td>
<td></td>
<td></td>
<td><h6>  Nama Akun: <b><?= $nama; ?></b> </h6>
</td>
</tr>
</table>
    <table border="2" class="table table-sm table-bordered" width="100%" style="text-align: center;">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Deskripsi</th>
                <th>Debit</th>
                <th>Kredit</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $saldo = 0;
                while($row = mysqli_fetch_object($execute)){
                $saldo = $saldo + $row->debit - $row->kredit;

                $debit = null;
                $kredit = null;
                if($row->debit > 0){
                    $debit = rupiah($row->debit,"Rp. ");
                }

                if($row->kredit > 0 ){
                    $kredit = rupiah($row->kredit,"Rp. ");
                }

            ?>
            <tr>
            <td> <?= tgl_indo($row->tanggal); ?> </td>
            <td> <?= $row->no_jurnal; ?> </td>
            <td align="right"> <?= $debit; ?> </td>
            <td align="right"> <?= $kredit; ?> </td>
            <td align="right"> <?= rupiah($saldo,"Rp. "); ?> </td>
            </tr>
            <?php } ?>
        </tbody>  
    </table>
</div>

    <script type="text/javascript">
        $('.dataTable').DataTable({
            "ordering": false
        });
    </script>