 <?php ob_start();
include '../config/config.php';
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=jurnal_kas_masuk.xls");
cek_tahun_buku();

    @$tglAwal = $_GET['tglAwal'];
    @$tglAkhir = $_GET['tglAkhir'];
    $tahun_buku = $_SESSION['tahun_buku'];
    $awal_periode = $_SESSION['awal_periode'];
    $akhir_periode = $_SESSION['akhir_periode'];


 ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 400px;}
   table td {word-wrap:break-word;width: 50%;}
   </style>
   <?php include 'print_bootstrap.php'; ?>
   <style type="text/css">
     .dim-memo{
            background-color: #fff5c9;
            font-style: italic;
            text-align: center;
        }
      .table-header{
          text-align: center;
          font-weight: bold;
          color: black;
          height: 60%;
          padding-bottom: 60%;
      }
   </style>
</head>
<body>
 <div class="row" style="padding-bottom: 20px;">
    <!-- <div class="col-md-4" style="padding-left: 100px; text-align: center;">
      <img src="<?= base_url('assets/img/logoforexcel.png'); ?>">
      <h4> KOPKARKIM BIDA</h4>
    </div>
    <div class="col-md-8" style="padding-top: 40px;">
      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">JURNAL UMUM</h4>
  <h5 style="text-align: center;">Periode <?= $awal_periode; ?>  sampai <?= $akhir_periode; ?> </h5>
    </div> -->

    <!-- HEADER -->
    <table>
      <tr>
        <td></td> 
        <td  style="text-align: center;"> <img src="<?= base_url('assets/img/logoforexcel.png'); ?>"> </td>
        <td colspan="5" rowspan="4" valign="center" style="text-align: center; font-weight: bold;"> 
          <br>&nbsp
         KOPERASI KARYAWAN PEMUKIMAN BIDA <br>
         JURNAL KAS MASUK <br>
         Periode <?= tgl_indo($awal_periode); ?>  sampai <?= tgl_indo($akhir_periode); ?>
         <br>&nbsp <br>&nbsp
       </td> 
      </tr> 
    </table>
  </div>
  
<table border="2" class="table table-sm table-bordered" width="100%" style="text-align: center;">
  <thead>
<tr>
  <th>Tanggal</th>
  <th>No. Jurnal</th>
  <th>No. Bukti</th>
  <th>No. Akun</th>
  <th>Nama Akun</th>
  <th>Debit</th>
  <th>Kredit</th>
 </tr>
 </thead>

<?php

if(empty($tglAwal) && empty($tglAkhir)){
    $query = "select * from v_jurnal WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JKM' ORDER BY tanggal,no_jurnal,keterangan,row ";
}else{
    $query = "select * from v_jurnal WHERE id_tahun_buku='".$tahun_buku."' AND tipe_jurnal='JKM' AND tanggal BETWEEN '".$tglAwal."' AND '".$tglAkhir."' ORDER BY tanggal,no_jurnal,keterangan,row ";
}   
$sql = mysqli_query($koneksi, $query); 
      
?>
 <?php  
            $temp_ju = null;
            $temp_memo = null;

                    $total_debit = 0;
                    $total_kredit = 0;
            while($row = mysqli_fetch_object($sql)){ 
                    $no_jurnal     = $row->no_jurnal;
                    if($temp_ju !== $no_jurnal){
                        $temp_ju    = $no_jurnal;
                        $tanggal    = tgl_indo($row->tanggal);
                        $no_bukti   = $row->no_bukti;

                        /*jika no jurnal tidak sama dengan temp berarti awal row baru
                         1. keluarkan temp memo => memo dari jurnal sebelumnya
                         2. set temp memo => memo dari jurnal sekarang*/
                        echo $temp_memo;

                        //set isi kolom, HARUS SESUAI DENGAN HEADER walau ada colspan
                        //4 kolom pertama wajib, agar saat dicari ter group
                        $arr = array($row->memo); 
                        $i = 0;
                        $temp_memo = '<tr class="dim-memo">';
                        while($i<count($arr)){
                           
                                $x = "<td colspan='6'>".$arr[$i]."</td>";
                            
                            $temp_memo  = $temp_memo.$x;
                            $i++;
                        }
                        $temp_memo = $temp_memo."</tr>";
                        
                    }
                    
                    $no_akun    = $row->no_akun;
                    $nama       = $row->nama_akun;
                    $ket        = $row->keterangan;
                    $debit = null;
                    $kredit = null;

                    if (strpos($row->keterangan, 'Debit') !== false) {
                        $debit  = rupiah($row->nominal,"Rp. ");
                        $total_debit = $total_debit+$row->nominal;
                    }else{
                        $kredit = rupiah($row->nominal,"Rp. ");
                        $total_kredit = $total_kredit+$row->nominal;
                    } 
                ?>
                
                    <tr onclick="select_me('<?= $temp_ju; ?>');" class="row<?= $temp_ju; ?>">
                        <td><?= $tanggal; ?></td>
                        <td><?= $no_jurnal; ?></td>
                        <td><?= $no_bukti; ?></td>
                        <td align="right"><?= $no_akun; ?></td>
                        <td><?= $nama; ?></td>
                        <td align="right"><?= $debit; ?></td>
                        <td align="right"><?= $kredit; ?></td>
                    </tr>
            <?php } echo $temp_memo; ?>
                    <tr>
                        <td colspan="5" class="dim-saldo" style="text-align: center;">Saldo</td>
                        <td class="dim-saldo-debit" style="text-align: right;"><?= rupiah($total_debit,"Rp. "); ?></td>
                        <td class="dim-saldo-kredit" style="text-align: right;"><?= rupiah($total_kredit,"Rp. "); ?></td>
                    </tr>
</table>
</body>
<script type="text/javascript">
  window.print();
 setTimeout(window.close, 0);
</script>

</html>
