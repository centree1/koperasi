<?php 
include '../config/config.php';
$title = "Mutasi Kas Masuk dan Keluar";
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=mutasi.xls");

cek_tahun_buku();

//tahun buku sekarang
$tahun_buku_sekarang    = $_SESSION['tahun_buku'];
$awal_periode_sekarang  = $_SESSION['awal_periode'];
$tahun_sekarang         = 'Tahun '.date('Y', strtotime($awal_periode_sekarang));

//tahun buku kemarin
$query                  = "SELECT id_tahun_buku,awal_periode,akhir_periode from tb_tahun_buku where awal_periode < '".$awal_periode_sekarang."' order by awal_periode DESC LIMIT 1";
$exe                    = mysqli_query($koneksi,$query);
$old_book               = mysqli_fetch_object($exe);
if(empty($old_book)){
    $tahun_buku_sebelumnya  = "-"; 
    $awal_periode_sebelumnya  = "-";
    $tahun_sebelumnya       = "Tahun ".date("Y", strtotime( date( "Y-m-d", strtotime( $awal_periode_sekarang ) ) . "-1 year" ) );
}else{
    $tahun_buku_sebelumnya  = $old_book->id_tahun_buku; 
    $awal_periode_sebelumnya  = $old_book->awal_periode;
    $tahun_sebelumnya       = 'Tahun '.date('Y', strtotime($awal_periode_sebelumnya));
}


?>
<style type="text/css">
    .separator{
         background-color: white; padding-top: 15px; border-right: 0px !important; border-left: 0px !important;
    }
    .kategori1{
        font-weight: bold;
    }
    .kategori2{
        font-weight:bold !important; 
        padding-left: 30px !important;
    }
    .kategori3{
        padding-left: 50px !important;
    }
</style>
<!-- Main Start Here -->
 
     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    <table>
    <tr>
        <td width="70%">
            <img src="<?= base_url('assets/img/logoforexcel.png'); ?>"  style="text-align: center;"> </img>
        </td>
        <td align="center" colspan="2" width="15%" style="font-weight: bold; "> 
            KOPERASI KARYAWAN PEMUKIMAN BIDA <br />
            MUTASI KAS MASUK DAN KELUAR <br />
            NERACA <br />
            UNTUK TAHUN YANG BERAKHIR <?= tgl_indo($_SESSION['akhir_periode']); ?> 
        </td> 
    </tr>
</table>            
<br>
<br>
<table border="1" class="table table-sm table-bordered" width="100%" style="text-align: center;">

    <tr>
        <td width="55%"></td>
        <td width="15%" style="font-weight: bold"><?= $tahun_sebelumnya; ?> </td>
        <td width="15%" style="font-weight: bold;"><?= $tahun_sekarang; ?></td>
        <td width="15%" style="font-weight: bold;">Mutasi Kas</td>
    </tr>

<tr style="background-color: #cdf7f5;">
    <td colspan="4" class="kategori1">ASET</td>
</tr>
    <!-- ASET LANCAR  ---------------------------------------------------->                            
        
        <tbody id="aset_lancar" style="">
        <tr style="background-color: #f5ffb5;" >
            <td colspan="4" class="kategori2">ASET LANCAR</td>
        </tr>
    <?php
        $saldo_mutasi = 0;
        $saldo_beginning_balance = 0;
        $saldo_aset_lancar_sebelumnya = 0;
        $saldo_aset_lancar_sekarang   = 0;
        $query = "SELECT a.no_akun, a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '10' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ 
            $no_akun = $row->no_akun;
            $tidak_mutasi = array('100','101','102','103','105','106','107','108');
            if(in_array($no_akun, $tidak_mutasi)){
                $saldo_beginning_balance += $row->saldo_sekarang;
                $jum = 0;
            }else{
                $jum = $row->saldo_sebelumnya-$row->saldo_sekarang;
            }
            ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($jum,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_aset_lancar_sekarang    += $row->saldo_sekarang;
                $saldo_aset_lancar_sebelumnya  += $row->saldo_sebelumnya;
                $saldo_mutasi += $row->saldo_sebelumnya-$row->saldo_sekarang;
                } 
            ?>
            </tbody>
      
    <!-- END ASET LANCAR  ---------------------------------------------------->

    <!-- ASET LANCAR  ---------------------------------------------------->                            
        
        <tbody id="aset_tetap" style="margin-top: 100px;">
        <tr> <td colspan="3" class="separator"></td></tr>
        <tr style="background-color: #f5ffb5;" >
            <td colspan="4" class="kategori2">ASET TETAP</td>
        </tr>
    <?php
        $saldo_aset_tetap_sebelumnya = 0;
        $saldo_aset_tetap_sekarang   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '11' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya-$row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_aset_tetap_sekarang    += $row->saldo_sekarang;
                $saldo_aset_tetap_sebelumnya  += $row->saldo_sebelumnya;
                $saldo_mutasi += $row->saldo_sebelumnya-$row->saldo_sekarang;
                } 
            ?>
            </tbody>
        

    <!-- END ASET   ---------------------------------------------------->
    
    <tr> <td colspan="3" class="separator"></td></tr>
    <tr style="background-color: #cdf7f5;">
        <td colspan="4" class="kategori1">KEWAJIBAN DAN EKUITAS</td>
    </tr>

    <tr style="background-color: #cdf7f5;">
        <td colspan="4" class="kategori1">KEWAJIBAN</td>
    </tr>
    <!-- KEWAJIBAN LANCAR  ---------------------------------------------------->                            
        
        <tbody id="kewajiban_pendek" style="">
        <tr style="background-color: #f5ffb5;">
            <td colspan="4" class="kategori2">KEWAJIBAN JANGKA PENDEK</td>
        </tr>
    <?php
        $saldo_kewajiban_jangka_pendek_sebelumnya = 0;
        $saldo_kewajiban_jangka_pendek_sekarang   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '16' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya-$row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_kewajiban_jangka_pendek_sekarang    += $row->saldo_sekarang;
                $saldo_kewajiban_jangka_pendek_sebelumnya  += $row->saldo_sebelumnya;
                $saldo_mutasi += $row->saldo_sebelumnya-$row->saldo_sekarang;
                } 
            ?>
            </tbody>
    <!-- END KEWAJIBAN LANCAR  ---------------------------------------------------->

    <!-- KEWAJIBAN JANGKA PANJANG  ---------------------------------------------------->                            
        
        <tbody id="kewajiban_panjang" style="margin-top: 100px;">
            <tr> <td colspan="4" class="separator"></td></tr>
        <tr style="background-color: #f5ffb5; ">
            <td colspan="4" class="kategori2">KEWAJIBAN JANGKA PANJANG</td>
        </tr>
    <?php
        $saldo_kewajiban_jangka_panjang_sekarang = 0;
        $saldo_kewajiban_jangka_panjang_sebelumnya   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '18' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya-$row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_kewajiban_jangka_panjang_sekarang    += $row->saldo_sekarang;
                $saldo_kewajiban_jangka_panjang_sebelumnya  += $row->saldo_sebelumnya;
                $saldo_mutasi += $row->saldo_sebelumnya-$row->saldo_sekarang;
                } 
            ?>
            </tbody>
       
    <!-- END KEWAJIBAN JANGKA PANJANG  ---------------------------------------------------->
    

    <tr> <td colspan="4" class="separator"></td></tr>
    <tr style="background-color: #cdf7f5; cursor: pointer;"  onclick="toggle('ekuitas');">
        <td colspan="4" class="kategori1">EKUITAS</td>
    </tr>
    <!-- EKUITAS  ---------------------------------------------------->                     
        
        <tbody id="ekuitas" style="">
    <?php
        $saldo_ekuitas_sebelumnya = 0;
        $saldo_ekuitas_sekarang   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '19' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya-$row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_ekuitas_sekarang    += $row->saldo_sekarang;
                $saldo_ekuitas_sebelumnya  += $row->saldo_sebelumnya;
                $saldo_mutasi += $row->saldo_sebelumnya-$row->saldo_sekarang;
                } 
            ?>
            </tbody>
    <!-- END  ---------------------------------------------------->


    <!-- END EKUITAS  ---------------------------------------------------->

    <tr> <td colspan="4" class="separator"></td></tr>
    <tr  style="background-color: #ccffb5;">
        <td colspan="3" class="kategori1">  INCREASE OR DECREASE OF CASH </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($saldo_mutasi,"Rp. "); ?></td>
    </tr>
    <tr  style="background-color: #ccffb5;">
        <td colspan="3" class="kategori1">  CASH BEGINNING BALANCE  </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($saldo_beginning_balance,"Rp. "); ?></td>
    </tr>
    <tr  style="background-color: #ccffb5;">
        <td colspan="3" class="kategori1">  CASH ENDING BALANCE  </td>
        <td  style="font-weight: bold; text-align: right;"><?= rupiah($saldo_mutasi+$saldo_beginning_balance,"Rp. "); ?></td>
    </tr>   


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- Main End Here -->
<script type="text/javascript">
    function toggle(id){
        $("#"+id).fadeToggle();
    }

</script>

<!-- ============ Body content End ============= -->

