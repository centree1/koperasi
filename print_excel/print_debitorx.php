<?php ob_start();
include '../config/config.php';
cek_tahun_buku();

    @$tglAwal = $_GET['tglAwal'];
    @$tglAkhir = $_GET['tglAkhir'];
    $tahun_buku = $_SESSION['tahun_buku'];
    $awal_periode = $_SESSION['awal_periode'];
    $akhir_periode = $_SESSION['akhir_periode'];


 ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table {border-collapse:collapse; table-layout:fixed;width: 400px;}
   table td {word-wrap:break-word;width: 14%;}
   </style>
   <?php include 'print_bootstrap.php'; ?>
   <style type="text/css">
     .dim-memo{
            background-color: #fff5c9 !important;
            font-style: italic !important;
            text-align: center !important;
        }
      .table-header{
          text-align: center !important;
          font-weight: bold;
          color: black;
          height: 40px !important;
          padding-bottom: 40px !important;
      }
   </style>
</head>
<body>
  <div class="row" style="padding-bottom: 20px;">
    <div class="col-md-4" style="padding-left: 100px; text-align: center;">
      <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
      <h4> KOPKARKIM BIDA</h4>
    </div>
    <div class="col-md-8" style="padding-top: 40px;">
      <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
  <h4 style="text-align: center;">DEBITOR</h4>
  <h5 style="text-align: center;">Periode <?= $awal_periode; ?>  sampai <?= $akhir_periode; ?> </h5>
    </div>
  </div>
  
<table class="table table-sm table-bordered" width="50%" style="text-align: center;">
  <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No ID Debitor</th>
                                    <th>Nama Debitor</th>
                                    <th>Jenis Kredit</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Tanggal Jatuh Tempo Pelunasan</th>
                                    <th>Term</th>
                                    <th>Banyak Cicilan</th>
                                    <th>Nominal Kredit</th>
                                    <th>Total yang sudah dilunasi</th>
                                    <th>Saldo Akhir</th>
                                    <th>Sisa Masa Cicilan</th>
                                </tr>
 </thead>

 <?php
 include 'config.php';
 cek_tahun_buku();

@$tglAwal = $_GET['dari_tgl'];
@$tglAkhir = $_GET['sampe'];
@$tipe = $_GET['tipe']; //tipe pencarian
$tahun_buku = $_SESSION['tahun_buku'];

if(empty($tglAwal) && empty($tglAkhir) && empty($tipe) ){
    $query = "SELECT * FROM tb_debitor, tb_debitor_pribadi, tb_kredit_salur where tb_debitor.no_id_debitor=tb_debitor_pribadi.no_id_debitor AND tb_debitor.id_kredit_salur=tb_kredit_salur.id_kredit_salur";
}else{
    $query = "SELECT * FROM tb_debitor, tb_debitor_pribadi, tb_kredit_salur where tb_debitor.no_id_debitor=tb_debitor_pribadi.no_id_debitor AND tb_debitor.id_kredit_salur=tb_kredit_salur.id_kredit_salur AND ".$tipe." BETWEEN '".$tglAwal."' AND '".$tglAkhir."' ";
}   


$query=mysqli_query($koneksi, $query);
$a=0;
while($row=mysqli_fetch_array($query)){
$a++;
?>
<tr onclick="select_me(<?=$row['id_debitor'];?>);" id="row<?=$row['id_debitor'];?>">
    <td><?=$a;?></td>
    <td><?=$row['no_id_debitor'];?></td>
    <td><?=$row['nama'];?></td>
    <td><?=$row['jenis_kredit'];?></td>
    <td><?=$row['tanggal_penyaluran'];?></td>
    <td><?=$row['tanggal_jatuh_tempo'];?></td>
    <td><?=$row['term'];?></td>
    <td><?=$row['banyak_cicilan'];?></td>
    <td><?=$row['nominal_cicilan'];?></td>
    <td>0</td>
    <td><?=$row['jumlah_kredit'];?></td>
    <td><?=$row['banyak_cicilan'];?></td>
    </tr>
<?php } ?>
</table>
</body>
<script type="text/javascript">
  window.print();
 setTimeout(window.close, 0);
</script>

</html>
