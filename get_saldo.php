<?php include 'config/config.php'; 
$id_tahun_buku      = $_SESSION['tahun_buku']; 

//get saldo awal pada tahun buku ini
$query = 'SELECT a.no_akun, a.nama_akun, s.saldo from tb_akun a , tb_saldo_awal s WHERE a.no_akun = s.no_akun AND s.id_tahun_buku = "'.$id_tahun_buku.'"';
$exe = mysqli_query($koneksi,$query);
$data=mysqli_fetch_all($exe);
$arr = array();
foreach ($data as $row) {
    $arr[$row[0]] = $row[2];
} 

?>

<table class="table table-striped table-hovered table-bordered dataTable" >
   <thead><tr>
        <th>No Akun</th>
        <th>Nama Akun</th>
        <th>Saldo Awal</th>
        <th>Aksi</th>
   </tr></thead>

        <?php
            
             if(empty($_POST['id'])){
                $query = 'select * from tb_akun';
            }else{
                $query = 'select * from tb_akun where id_kategori="'.$_POST['id'].'"';
            }
            $query = mysqli_query($koneksi,$query);
            while($row=mysqli_fetch_object($query)){
        ?>
        <tr>
        <td><?=$row->no_akun; ?></td>
        <td><?=$row->nama_akun; ?></td>
        <td><?php
            if(array_key_exists($row->no_akun, $arr)){
                echo rupiah($arr[$row->no_akun],'Rp. '); 
            }else{
                echo rupiah(0); 
            }
         ?>
         </td>
        <td>
                    <button onclick="open_modal('<?= $row->no_akun; ?>')"  class='btn btn-primary btn-sm'>Edit</a>  
                </td>
            <?php } ?>
        </tr>
    
</table>

<script type="text/javascript">
    $('.dataTable').DataTable();
</script>