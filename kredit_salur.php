<?php 
include 'config/config.php';
$title = "Daftar Nama Debitor";
include 'template/header.php';
    @$tglAwal = $_POST['dari_tgl'];
    @$tglAkhir = $_POST['sampe'];
    @$tipe = $_POST['tipe']; //tipe pencarian
    $tahun_buku = $_SESSION['tahun_buku'];
?>

<!-- Main Start Here -->
    <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body">
                    <div class=" row" style=" ">
                        <div class="col-md-6 col-xs-12 dim-center">
                             <h5 style="display: inline-block;"> <i class="fa fa-credit-card-alt fa-fw"></i>  Daftar Kredit Salur  </h5>
                        </div> 
                        <div class="col-md-6 col-xs-12 dim-center text-right">
                       <a button  href="tambah_kredit_salur" type="button" class="btn btn-primary">Tambah</button></a> 
                        <button type="button" class="btn btn-warning" onclick="print()">Cetak</button>
                        </div>
                        
                       
                        
                       <!-- <div class="col-md-6 col-xs-12 dim-center text-right">
                            <button type="button" onclick="toggle_filter()" class="btn btn-primary" ><i class="fa fa-filter fa-fw"></i> Filter </button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        get_notif('psnkredit');
    ?>

  
     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body table-responsive">
                    <table class="table table-striped table-hover dataTable">
                    <thead>
                    <tr>
                        <th>No Akun</th>
                        <th>Nama Akun</th>
                        <th>Jenis Kredit</th>
                        <th>Saldo</th>
                        <th>Aksi</th>  
                    </thead>
                     <?php
                                $query = 'select s.*,t.nama_akun from tb_kredit_salur s , tb_akun t where t.no_akun = s.no_akun';
                                $execute = mysqli_query($koneksi,$query);
                                while($row=mysqli_fetch_object($execute)){
                            ?>
                    <tbody>

                    <tr> 

                        <td><?= $row->no_akun; ?></td>
                        <td><?= $row->nama_akun; ?></td>
                        <td><?= $row->jenis_kredit; ?></td>
                        <td><?= (rupiah ($row ->saldo,'Rp. '));?></td>
                        
                        <td>
                            <a class='btn btn-primary btn-sm' href='edit_kredit_salur?id=<?= $row->id_kredit_salur; ?>'>Edit</a>  
                    <button class='btn btn-danger btn-sm' onclick="deleteSalur('<?= $row->id_kredit_salur; ?>')">Delete</button></td>
                    </td>
                                <?php } ?>
                            </tr>
                        
                    </tbody>
</table>

                </div>
                    
                </div>
            </div>
        </div>
    
<!-- Main End Here -->
<script type="text/javascript">
    function deleteSalur(id){
        pesan_confirm("Apakah anda yakin?", "Data yang telah dihapus tidak dapat dikembalikan", "Ya, Hapus").then((result) => {
            //eksekusi
            if(result===true){
                simple_ajax("id_kredit_salur="+id,'config/delete_kredit_salur','','Kredit Salur berhasil dihapus','Berhasil Hapus Kredit Salur');
            }
        });
    }
    function print(){
        var tglAwal =  '<?= $tglAwal; ?>';
        var tglAkhir = '<?= $tglAkhir; ?>';
        window.open('print/print_kredit_salur?tglAwal='+tglAwal+'&tglAkhir='+tglAkhir, '_blank');
    }
    
    </script>
    

<?php include 'template/footer.php'; ?>