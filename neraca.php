<?php 
include 'config/config.php';
$title = "Neraca";
include 'template/header.php';
cek_tahun_buku();

//tahun buku sekarang
$tahun_buku_sekarang    = $_SESSION['tahun_buku'];
$awal_periode_sekarang  = $_SESSION['awal_periode'];
$tahun_sekarang         = date('Y', strtotime($awal_periode_sekarang));

//tahun buku kemarin
$query                  = "SELECT id_tahun_buku,awal_periode,akhir_periode from tb_tahun_buku where awal_periode < '".$awal_periode_sekarang."' order by awal_periode DESC LIMIT 1";
$exe                    = mysqli_query($koneksi,$query);
$old_book               = mysqli_fetch_object($exe);
if(empty($old_book)){
    $tahun_buku_sebelumnya  = 0; 
    $awal_periode_sebelumnya  = 0;
    $tahun_sebelumnya       = "Tahun ".date("Y", strtotime( date( "Y-m-d", strtotime( $awal_periode_sekarang ) ) . "-1 year" ) );
}else{
    $tahun_buku_sebelumnya  = $old_book->id_tahun_buku; 
    $awal_periode_sebelumnya  = $old_book->awal_periode;
    $tahun_sebelumnya       = "Tahun ".date('Y', strtotime($awal_periode_sebelumnya));
}


?>
<style type="text/css">
    .separator{
         background-color: white; padding-top: 15px; border-right: 0px !important; border-left: 0px !important;
    }
    .kategori1{
        font-weight: bold;
    }
    .kategori2{
        font-weight:bold !important; 
        padding-left: 30px !important;
    }
    .kategori3{
        padding-left: 50px !important;
    }
</style>
<!-- Main Start Here -->
 
     <div class="row">
        <div class="container-fluid">
            <div class="card shadow-sm border-0 mb-4">
                <div class="card-body" id="tbl">
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-4" style="padding-left: 100px; text-align: center;">
                          <img style="width: auto; height: 200px;" src="<?= base_url('assets/img/logo.png'); ?>">
                          <h4> KOPKARKIM BIDA</h4>
                        </div>
                        <div class="col-md-8" style="padding-top: 40px;">
                          <h3 style="text-align: center;">KOPERASI KARYAWAN PEMUKIMAN BIDA</h3>
                      <h4 style="text-align: center;">NERACA</h4>
                      <h4 style="text-align: center;"> UNTUK TAHUN YANG BERAKHIR <?= tgl_indo($_SESSION['akhir_periode']); ?></h4>
                      
                      <center> 
                        <button id="print" class="btn btn-primary btn-sm" onclick="print()"> <i class="fa fa-print"></i> Print </button>
                        |
                        <button id="print" class="btn btn-success btn-sm" onclick="printx()"> <i class="fa fa-print"></i> Excel </button> 
                       </center>

                      <!-- <h5 style="text-align: center;">Periode <?= $awal_periode; ?>  sampai <?= $akhir_periode; ?> </h5>
                       -->  </div>
                      </div>

                    <div style="margin-top: 10px; margin-bottom: 10px; border-style: solid; border-width: 1px; "></div>
                     
                    <div class="table-responsive " style="padding-left: 2%; padding-right: 2%;">
<table class="table table-hover table-bordered  table-striped"  >
    <tr>
        <td width="70%"></td>
        <td width="15%" style="font-weight: bold;"> <?= $tahun_sebelumnya; ?> </td>
        <td width="15%" style="font-weight: bold;">Tahun <?= $tahun_sekarang; ?></td>
    </tr>

<tr style="background-color: #cdf7f5;">
    <td colspan="3" class="kategori1">ASET</td>
</tr>
    <!-- ASET LANCAR  ---------------------------------------------------->                            
        
        <tbody id="aset_lancar" style="">
        <tr style="background-color: #f5ffb5; cursor: pointer;" onclick="toggle('aset_lancar');">
            <td colspan="3" class="kategori2">ASET LANCAR</td>
        </tr>
    <?php
        $saldo_aset_lancar_sebelumnya = 0;
        $saldo_aset_lancar_sekarang   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '10' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_aset_lancar_sekarang    += $row->saldo_sekarang;
                $saldo_aset_lancar_sebelumnya  += $row->saldo_sebelumnya;
                } 
            ?>
            </tbody>
        <tr style="background-color: #ccffb5; cursor:pointer" onclick="toggle('aset_lancar');">
            <td width="70%" class="kategori2"> TOTAL ASET LANCAR</td>
            <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_aset_lancar_sebelumnya,"Rp. "); ?> </td>
            <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_aset_lancar_sekarang,"Rp. "); ?></td>
        </tr>
    <!-- END ASET LANCAR  ---------------------------------------------------->

    <!-- ASET LANCAR  ---------------------------------------------------->                            
        
        <tbody id="aset_tetap" style="margin-top: 100px;">
        <tr> <td colspan="3" class="separator"></td></tr>
        <tr style="background-color: #f5ffb5; cursor: pointer;" onclick="toggle('aset_tetap');">
            <td colspan="3" class="kategori2">ASET TETAP</td>
        </tr>
    <?php
        $saldo_aset_tetap_sebelumnya = 0;
        $saldo_aset_tetap_sekarang   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '11' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_aset_tetap_sekarang    += $row->saldo_sekarang;
                $saldo_aset_tetap_sebelumnya  += $row->saldo_sebelumnya;
                } 
            ?>
            </tbody>
        <tr style="background-color: #ccffb5; cursor:pointer" onclick="toggle('aset_tetap');">
            <td width="70%" class="kategori2"> TOTAL ASET TETAP</td>
            <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_aset_tetap_sebelumnya,"Rp. "); ?> </td>
            <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_aset_tetap_sekarang,"Rp. "); ?></td>
        </tr>
    <!-- END ASET LANCAR  ---------------------------------------------------->
    <!-- Total ASET -->
     <tr  style="background-color: #ccffb5;">
        <td width="70%" class="kategori1"> TOTAL ASET </td>
        <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_aset_tetap_sebelumnya+$saldo_aset_lancar_sebelumnya,"Rp. "); ?> </td>
        <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_aset_tetap_sekarang+$saldo_aset_lancar_sekarang,"Rp. "); ?></td>
    </tr>

    <!-- END ASET   ---------------------------------------------------->
    
    <tr> <td colspan="3" class="separator"></td></tr>
    <tr style="background-color: #cdf7f5;">
        <td colspan="3" class="kategori1">KEWAJIBAN DAN EKUITAS</td>
    </tr>

    <tr> <td colspan="3" class="separator"></td></tr>
    <tr style="background-color: #cdf7f5;">
        <td colspan="3" class="kategori1">KEWAJIBAN</td>
    </tr>
    <!-- KEWAJIBAN LANCAR  ---------------------------------------------------->                            
        
        <tbody id="kewajiban_pendek" style="">
        <tr style="background-color: #f5ffb5; cursor: pointer;" onclick="toggle('kewajiban_pendek');">
            <td colspan="3" class="kategori2">KEWAJIBAN JANGKA PENDEK</td>
        </tr>
    <?php
        $saldo_kewajiban_jangka_pendek_sebelumnya = 0;
        $saldo_kewajiban_jangka_pendek_sekarang   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '16' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_kewajiban_jangka_pendek_sekarang    += $row->saldo_sekarang;
                $saldo_kewajiban_jangka_pendek_sebelumnya  += $row->saldo_sebelumnya;
                } 
            ?>
            </tbody>
        <tr style="background-color: #ccffb5; cursor:pointer" onclick="toggle('kewajiban_pendek');">
            <td width="70%" class="kategori2"> TOTAL KEWAJIBAN JANGKA PENDEK</td>
            <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_kewajiban_jangka_pendek_sebelumnya,"Rp. "); ?> </td>
            <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_kewajiban_jangka_pendek_sekarang,"Rp. "); ?></td>
        </tr>
    <!-- END KEWAJIBAN LANCAR  ---------------------------------------------------->

    <!-- KEWAJIBAN JANGKA PANJANG  ---------------------------------------------------->                            
        
        <tbody id="kewajiban_panjang" style="margin-top: 100px;">
            <tr> <td colspan="3" class="separator"></td></tr>
        <tr style="background-color: #f5ffb5; cursor: pointer;" onclick="toggle('kewajiban_panjang');">
            <td colspan="3" class="kategori2">KEWAJIBAN JANGKA PANJANG</td>
        </tr>
    <?php
        $saldo_kewajiban_jangka_panjang_sekarang = 0;
        $saldo_kewajiban_jangka_panjang_sebelumnya   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '18' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_kewajiban_jangka_panjang_sekarang    += $row->saldo_sekarang;
                $saldo_kewajiban_jangka_panjang_sebelumnya  += $row->saldo_sebelumnya;
                } 
            ?>
            </tbody>
        <tr style="background-color: #ccffb5; cursor:pointer" onclick="toggle('kewajiban_panjang');">
            <td width="70%" class="kategori2"> TOTAL KEWAJIBAN JANGKA PANJANG</td>
            <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_kewajiban_jangka_panjang_sebelumnya,"Rp. "); ?> </td>
            <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_kewajiban_jangka_panjang_sekarang,"Rp. "); ?></td>
        </tr>
    <!-- END KEWAJIBAN JANGKA PANJANG  ---------------------------------------------------->
    <!-- Total KEWAJIBAN -->
     <tr  style="background-color: #ccffb5;">
        <td width="70%" class="kategori1">  TOTAL KEWAJIBAN  </td>
        <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_kewajiban_jangka_panjang_sebelumnya+$saldo_kewajiban_jangka_pendek_sebelumnya,"Rp. "); ?> </td>
        <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_kewajiban_jangka_panjang_sekarang+$saldo_kewajiban_jangka_pendek_sekarang,"Rp. "); ?></td>
    </tr>

    <!-- END KEWAJIBAN  ---------------------------------------------------->


    <tr> <td colspan="3" class="separator"></td></tr>
    <tr style="background-color: #cdf7f5; cursor: pointer;"  onclick="toggle('ekuitas');">
        <td colspan="3" class="kategori1">EKUITAS</td>
    </tr>
    <!-- EKUITAS  ---------------------------------------------------->                     
        
        <tbody id="ekuitas" style="">
    <?php
        $saldo_ekuitas_sebelumnya = 0;
        $saldo_ekuitas_sekarang   = 0;
        $query = "SELECT a.nama_akun, (select sum( c.saldo ) as saldo from bb_akun c where c.no_akun = a.no_akun AND c.id_tahun_buku = '".$tahun_buku_sebelumnya."' ) AS saldo_sebelumnya, (select sum(d.saldo ) as saldo from bb_akun d where d.no_akun = a.no_akun AND d.id_tahun_buku = '".$tahun_buku_sekarang."' ) AS saldo_sekarang FROM
    tb_akun a, tb_kategori_akun k WHERE 
    a.id_kategori = k.id_kategori AND k.id_kategori IN ( '19' ) ";
        $exe   = mysqli_query($koneksi,$query);
        while($row = mysqli_fetch_object($exe)){ ?>
        <tr>
            <td class="kategori3"><?= $row->nama_akun; ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sebelumnya,"Rp. "); ?></td>
            <td style="text-align: right;"><?= rupiah($row->saldo_sekarang,"Rp. "); ?></td>
        </tr>
            <?php 
                $saldo_ekuitas_sekarang    += $row->saldo_sekarang;
                $saldo_ekuitas_sebelumnya  += $row->saldo_sebelumnya;
                } 
            ?>
            </tbody>
    <!-- END  ---------------------------------------------------->

    <!-- Total EKUITAS -->
     <tr  style="background-color: #ccffb5; cursor: pointer;" onclick="toggle('ekuitas');">
        <td width="70%" class="kategori1">  TOTAL EKUITAS  </td>
        <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_ekuitas_sebelumnya,"Rp. "); ?> </td>
        <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_ekuitas_sekarang,"Rp. "); ?></td>
    </tr>

    <!-- END EKUITAS  ---------------------------------------------------->

    <tr> <td colspan="3" class="separator"></td></tr>
    <tr  style="background-color: #ccffb5; ">
        <td width="70%" class="kategori1">  TOTAL KEWAJIBAN DAN EKUITAS  </td>
        <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_ekuitas_sebelumnya+$saldo_kewajiban_jangka_panjang_sebelumnya+$saldo_kewajiban_jangka_pendek_sebelumnya,"Rp. "); ?> </td>
        <td width="15%" style="font-weight: bold; text-align: right;"> <?= rupiah($saldo_ekuitas_sekarang+$saldo_kewajiban_jangka_panjang_sekarang+$saldo_kewajiban_jangka_pendek_sekarang,"Rp. "); ?></td>
    </tr>


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

<!-- Main End Here -->
<script type="text/javascript">
    function toggle(id){
        $("#"+id).fadeToggle();
    }
    function print(){
        window.open('print/print_neraca', '_blank');
    }
    function printx(){
        window.open('print_excel/print_neracax', '_blank');
    }


</script>

<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>