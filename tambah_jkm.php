<?php
include 'config/config.php';
$title = 'Tambah Jurnal Kas Masuk';
include 'template/header.php';
cek_tahun_buku();



//get id jurnal
$query      =  "select get_id_jurnal('JKM') as id";
$execute    = mysqli_query($koneksi,$query);
$fetch      = mysqli_fetch_object($execute);
$id         = 'JKM'.$fetch->id;
?>
<!-- ============ Body content start ============= -->


<form id="frmz">
<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                    <div class="border border-top-0 border-left-0 border-right-0" style="padding-bottom: 10px; margin-bottom: 20px; ">
                        <label-tabel> 
                            Tambah Jurnal Kas Masuk
                        </label-tabel>
                        <div class="float-right">
                            <a href="jurnal_kas_masuk" class="btn btn-secondary btn-icon-split mb-3">
                                <i class="fa fa-arrow-left"></i> Kembali
                            </a>
                        </div>
                        <br>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label >No. Jurnal Kas Masuk</label>
                            <input required name="no_jurnal" type="text" class="form-control" placeholder="No. Jurnal Kas Keluar" value="<?= $id; ?>"  readonly>
                          </div>
                          <div class="form-group">
                            <label >Tanggal</label>
                            <input class="form-control datetimepicker CentreeTgl" required name="tanggal" type="text"  autocomplete="off" placeholder="Tanggal" >
                          </div>
                          
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >No. Bukti Kas Masuk</label>
                                <input required name="no_bukti" type="text" class="form-control" placeholder="No Bukti Kas Masuk">
                              </div>
                              <div class="form-group">
                                <label >Memo</label>
                                <input name="memo" type="text" class="form-control" placeholder="Memo">
                              </div>
                        </div>
                    </div>
                </div>

            <script type="text/javascript">
                function yakin(tipe,lokasi){
                    pesan_confirm("Ingin Pindah Halaman ?", "Data Akun yang belum tersimpan akan hilang", "Buka "+tipe).then((result) => {
                    //eksekusi
                    if(result===true){
                        window.location = lokasi;
                    }
                    });
                }
            </script>
            </div>
        </div>
        
    </div>
</div>


<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body table-responsive ">
                    <table class="table table-hover table-striped" border="1">
                        <thead>
                            <tr>
                                <td class="bg-debit"  style="width: 2%; "></td>
                                
                                <td colspan="4" class="bg-debit"  style="width: 60%;">DEBIT</td>
                                <td colspan="2" class="bg-kredit" style="width: 30%;">KREDIT </td>
                            </tr>
                            <tr>
                                <td class="bg-debit"  rowspan="2" style="width: 2%;">
                                    <button type="button" class="btn btn-sm btn-success" onclick="addrow()"> <i  class="fa fa-plus "></i></button>
                                </td>
                                <td class="bg-debit" colspan="2" style="width: 30%;">BANK</td>
                                <td class="bg-debit" colspan="2" style="width: 30%;">Lainya</td>
                                <td class="bg-kredit"  rowspan="2" style="width: 15%;">Akun</td>
                                <td class="bg-kredit" rowspan="2" style="width: 15%;">Saldo</td>
                            </tr>
                            <tr>
                                <td class="bg-debit"  style="width: 15%;"> Akun </td>
                                <td class="bg-debit" style="width: 15%;"> Saldo </td>
                                <td class="bg-debit" style="width: 15%;"> Akun </td>
                                <td class="bg-debit" style="width: 15%;"> Saldo </td>
                            </tr>
                        </thead>
                        <tbody id="tbl">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="animated fadeInUpShort my-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card r-0 b-0 shadow">
                <div class="card-body">
                     <div class="form-group" style="text-align: center;">
                          <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-save fa-fw"></i> Simpan</button>
                      </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</form>

<script type="text/javascript">
        
          $('#frmz').submit(function(event) { 
            var values = $(this).serialize();
            simple_ajax(values,'config/proses_tambah_jkm','jurnal_kas_masuk','Berhasil Tambah Jurnal');
            event.preventDefault(); //Prevent the default submit
            return false; //stop
        });

        //table form
        var i = 1;
        function addrow(){
            get_with_ajax("id="+i, "config/get_form_jkm", "tbl","no");
            i++;
        }


        function delete_row(id){
            $("#"+id).closest('tr').remove()
        }

    </script> 

   
<!-- ============ Body content End ============= -->

<?php include 'template/footer.php'; ?>